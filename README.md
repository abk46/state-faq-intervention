# state-faq-intervention

Forming QA-pair sets using US state-level COVID-19 Vaccine FAQs, then evaluating performance when using sets as 'pro-vax intervention' for anti-vax statements.

## Associated Files

**state_covid19_faq_urls** contains URLs collected for each of 50 states + DC with possible Vaccine FAQ. In total, 41 have some FAQ (15 are easily extractable and are included in downloaded results).

**WAYBACK_FILES** contains downloads for each state's Wayback Machine versions over time period between March 2020 and January 2022.

**QA_SETS** contains the results of extracting QA-pairs from Wayback Machine archive for each state. Includes standard and version with 'de-duplicated' questions by labeling orginal question. Stored in pickles, data within pandas dataframes.

The CSVs, **antivax-sentences** and **nc_questions**, contain manual labeling of type of vaccine misinformation contained/addressed in each item. Antivax_sentences relies on majority vote from 3 or 4 labelers within Vax Misinfo Group. NC_questions is the labeling of a single person.

**deduplicating_nc_answers_experiment** includes case-by-case results for various methods in determining when lexically-different answers from a common question are duplicates or different.

The JSONL files, **train** and **dev** are the training/validation data from the BoolQ set. This gets used for training a RoBERTa model for matching FAQ questions to anti-vax paragraphs.

## Code

### Forming QA set

**0_download_wayback** is the method of downloading Wayback Machine versions for all state FAQs. Applicable tool utilizes command line arguments, so this is stored within a shell file.

**1_wayback_to_qa** is the method of utilizing all Wayback Machine versions for a state to form state-level QA-sets. Stored in pickles, object is a dataframe consisting of ordinal QA-pair number, date, question, and answer.

**2_qa_deduplicate_questions** is the method of de-duplicating questions, using a RoBERTa model trained on Quora Questions set sourced from HuggingFace. Stored in pickles, object is a dataframe consisting of ordinal QA-pair number, date, question, answer, and ordinal QA-pair number of the original question that it is a duplicate of (it is the same as the ordinal number if the question in the QA-pair is not a duplicate).

### Matching QA set

Both methods utilize the CSVs of NC questions / Anti-vax paragraphs hand-labeled by first-level taxonomy node. With this data, we have two models to match the correct NC question to the correct anti-vax paragraph, conducted in a typical IR fashion: **bm25** utilizes BM25, while **boolq** utilizes a RoBERTa model trained with BoolQ dataset, largely adapting work conducted in Qianqian's Spring 2021 MS Project.