

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head><script src="//archive.org/includes/analytics.js?v=cf34f82" type="text/javascript"></script>
<script type="text/javascript">window.addEventListener('DOMContentLoaded',function(){var v=archive_analytics.values;v.service='wb';v.server_name='wwwb-app209.us.archive.org';v.server_ms=406;archive_analytics.send_pageview({});});</script>
<script type="text/javascript" src="/_static/js/bundle-playback.js?v=36gO9Ebf" charset="utf-8"></script>
<script type="text/javascript" src="/_static/js/wombat.js?v=UHAOicsW" charset="utf-8"></script>
<script type="text/javascript">
  __wm.init("https://web.archive.org/web");
  __wm.wombat("https://doh.sd.gov/COVID/Vaccine/faqs.aspx","20220109063045","https://web.archive.org/","web","/_static/",
	      "1641709845");
</script>
<link rel="stylesheet" type="text/css" href="/_static/css/banner-styles.css?v=fantwOh2" />
<link rel="stylesheet" type="text/css" href="/_static/css/iconochive.css?v=qtvMKcIJ" />
<!-- End Wayback Rewrite JS Include -->

<title>COVID-19 Vaccination FAQs</title>
<meta name="description" content="The South Dakota Department of Health COVID-19 vaccine information"/>
<meta name="keywords" content="South Dakota Health Department, COVID-19, COVID-19 vaccine, vaccine, vaccination, immunize, immunization, shot, COVID shot, Health, South Dakota, SoDakVax, Public Health, coronavirus"/>

<link rel="stylesheet" type="text/css" href="/web/20220109063045cs_/https://doh.sd.gov/stylesheets/user.css" media="screen">
<link rel="stylesheet" type="text/css" href="/web/20220109063045cs_/https://doh.sd.gov/stylesheets/screen.css" media="screen">
<link rel="stylesheet" type="text/css" href="/web/20220109063045cs_/https://doh.sd.gov/stylesheets/print.css" media="print">
	
	
<link href="/web/20220109063045cs_/https://doh.sd.gov/includes/accordian/jquery.ui.core.min.css" rel="stylesheet" type="text/css"/>
<link href="/web/20220109063045cs_/https://doh.sd.gov/includes/accordian/jquery.ui.theme.min.css" rel="stylesheet" type="text/css"/>
<link href="/web/20220109063045cs_/https://doh.sd.gov/includes/accordian/jquery.ui.accordion.min.css" rel="stylesheet" type="text/css"/>


    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    
    <meta name="robots" content="index, follow"/>
    <meta name="revisit-after" content="30 days"/>

	    
    <meta name="viewport" content="width=1000, initial-scale=1, minimum-scale=0.45"/>
    
   	<link rel="stylesheet" type="text/css" href="/web/20220109063045cs_/https://doh.sd.gov/includes/css/user.css" media="screen">
	<link rel="stylesheet" type="text/css" href="/web/20220109063045cs_/https://doh.sd.gov/includes/css/screen.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/web/20220109063045cs_/https://doh.sd.gov/includes/css/form.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/web/20220109063045cs_/https://doh.sd.gov/includes/css/search.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/web/20220109063045cs_/https://doh.sd.gov/includes/css/print.css" media="print">
    
	
	<link rel="shortcut icon" href="/web/20220109063045im_/https://doh.sd.gov/images/favicon.ico">
	<link rel="apple-touch-icon" href="/web/20220109063045im_/https://doh.sd.gov/images/apple-touch-icon.png">

	<meta name="viewport" content="width=device-width, initial-scale=1">
	

	
    <!--[if lt IE 8]>
    <link href="/includes/css/screen_ie7.css" rel="stylesheet" type="text/css" media="screen"/>
    <![endif]-->

	
    <script src="https://web.archive.org/web/20220109063045js_/https://cdn.sd.gov/lib/jquery-test/jquery.min.js"></script>
    
    
	<script type="text/javascript" src="/web/20220109063045js_/https://doh.sd.gov/includes/js/jquery/jquery.easing.1.3.js"></script>
	<script type="text/javascript">
	    $(function () {

	        if ($(window).width() > 830) {
	            $("#middle #main").show();
            }
            else { /*hiding treeview nav by default*/
                $(".treeview").hide();
            }

	        $(window).resize(function () {
	            if ($(window).width() > 813) {
	                $('#main li').bind('mouseenter', function () {
	                    $(this).find('.dropdown_box').stop(true, true).slideDown(500, 'easeOutBounce');
	                }).bind('mouseleave', function () {
	                    $(this).find('.dropdown_box').stop(true, true).hide();
	                });
	                $("#middle #main").show();
	            }
	            else {
	                $('#main li').bind('mouseenter', function () {
	                    $(this).find('.dropdown_box').stop(true, true).hide();
	                }).bind('mouseleave', function () {
	                    $(this).find('.dropdown_box').stop(true, true).hide();
	                });
	                $("#middle #main").hide();
	            }

	        });
	        //This will fire each time the window is resized:
	        if ($(window).width() > 813) {

	            $('#main li').bind('mouseenter', function () {
	                $(this).find('.dropdown_box').stop(true, true).slideDown(500, 'easeOutBounce');
	            }).bind('mouseleave', function () {
	                $(this).find('.dropdown_box').stop(true, true).hide();
	            });
	            $("#middle #main").show();
	        }
	        else {
	            $('#main li').bind('mouseenter', function () {
	                $(this).find('.dropdown_box').stop(true, true).hide();
	            }).bind('mouseleave', function () {
	                $(this).find('.dropdown_box').stop(true, true).hide();
	            });
	            $("#middle #main").hide();
	        }




	    });
    </script>           
	

	
	<script src="/web/20220109063045js_/https://doh.sd.gov/includes/js/jquery/jquery-treeview/jquery.treeview.js"></script>
    
	
	
	<script type="text/javascript" src="/web/20220109063045js_/https://doh.sd.gov/includes/js/jquery/jquery.li-scroller.1.0.js"></script>
	<script type="text/javascript" src="/web/20220109063045js_/https://doh.sd.gov/includes/js/jquery/jquery.cookie.js"></script>
 
	<script type="text/javascript">
		$(function(){
			$("#alerts ul").liScroll();		
		});

		$(document).ready(function(){
			var n = $("#alerts ul li").length;
			
			if(n<2){
				$('#alerts_section').hide();
			}
			
			if($.cookie('alert_status') == "off"){
				$('#alerts_section').hide();
			}
			
			$('#close_btn').click(function(){
				$.cookie('alert_status', 'off', {expires: 1}); //create cookie and set value, expires 1 day
				$('#alerts_section').hide();
			});
		});
	</script>
    <script>
    $(document).ready(function () {
        $(".stateDOTsdDOTus").click(function () {
            var domain = $(this).attr("class");
            domain = domain.replace(/DOT/, '.');
            domain = domain.replace(/DOT/, '.');
            var name = $(this).children("span").attr("class");

            var email = name + "@" + domain;
            location.href = "mailto:" + email;
        });
    });
</script>

<script>
       $(document).ready(function () {
              $(".stateDOTsdDOTusSUBJECT").click(function () {
                     var domain = $(this).attr("class");
                     domain = domain.replace(/DOT/, '.');
                     domain = domain.replace(/DOT/, '.');
                     domain = domain.replace(/SUBJECT/, '?subject=' + $(this).attr("name"));
                     var name = $(this).children("span").attr("class");

                     var email = name + "@" + domain;
                     location.href = "mailto:" + email;
              });
       });
</script>



<script src="/web/20220109063045js_/https://doh.sd.gov/includes/accordian/jquery.ui-1.10.4.accordion.min.js" type="text/javascript"></script>
<script type="text/javascript">
	    if (top.frames.length != 0)
    	top.location = self.document.location;
    </script>
	<style type="text/css">
a:link {
	color: #E88C00;
}
a:visited {
	color: #E88C00}
</style>
</head>
<body>
<style type="text/css">
.container.clearfix #upper font {
	font-family: Arial, Helvetica, sans-serif;
}
</style>
<div id="alerts_section">
	<a id="close_btn">CLOSE<div id="alert_x"></div></a>
    
	<div class="tkr_container">
		<div id="alert_box">
			<div id="alerts">
                                    
                                    		
                                    
            </div>
        </div>
    </div>
</div>
            
<header>
    <div class="container clearfix">
        <div id="logo"><a href="/web/20220109063045/https://doh.sd.gov/" title="SD DOH Home"><img src="/web/20220109063045im_/https://doh.sd.gov/images/layout/logo.png" alt="South Dakota Department of Health logo"/></a></div>
        <div id="upper" class="clearfix">
        <!--<a href="/social/" title="Social media links" class="button">Social Media links</a>-->
		WEBSITE OF THE STATE OF SOUTH DAKOTA DEPARTMENT OF HEALTH<br/>
		<strong><font face="Times New Roman, Times, serif">Joan Adam, Interim Secretary of Health
            </font></strong><a href="https://web.archive.org/web/20220109063045/https://www.youtube.com/user/GoodandHealthySD/videos" target="_blank">
            <div class="YTLogo"></div></a>
        <a href="https://web.archive.org/web/20220109063045/https://twitter.com/SDDOH" target="_blank">
        <div class="twLogo"></div></a>
        <a href="https://web.archive.org/web/20220109063045/https://www.facebook.com/SDHealthDepartment" target="_blank"><div class="fbLogo"></div></a>
        </div>
		
        <div id="lower" class="clearfix">  
            <div id="search">
                <form id="cse-search-box" action="https://web.archive.org/web/20220109063045/https://google.com/cse" target="search">
                  <input type="hidden" name="cx" value="012382400048732927914:v2d9d_dbrdw"/>
                  <input type="hidden" name="ie" value="UTF-8"/>
                  <input type="text" name="q" id="search_text" style="border:none;"/><input type="submit" name="sa" id="search_arrow" value=""/>                  
                </form>
                  <script type="text/javascript" src="/web/20220109063045js_/https://doh.sd.gov/includes/js/brand.js"></script>            
            </div>
            
            <a href="/web/20220109063045/https://doh.sd.gov/contact/" title="Contact us" class="button" id="head_contact">Contact us</a>
			
			<a href="https://web.archive.org/web/20220109063045/https://doh.sd.gov/COVID/Vaccine/ProviderMap/default.aspx" title="Get your free covid vaccine" class="button" id="subscribe">Free COVID Vaccine</a>
                
            <a href="https://web.archive.org/web/20220109063045/https://learn.vaulthealth.com/southdakota/" title="FREE AT-HOME COVID TEST" class="button" id="head_contact">Free COVID Test</a>

        </div>
        <div id="menu"><img src="/web/20220109063045im_/https://doh.sd.gov/images/menu.png" alt="menu"/><br/>MENU</div>
    </div>

</header>
        
<div class="force-clear"></div>	
<div id="middle" class="clearfix">
      	<div class="container clearfix">
		  <nav id="main" class="clearfix">
                	<div class="centerer">
                    
					
                            
                            
					<ul>
                    
                    	<li><a href="/web/20220109063045/https://doh.sd.gov/a-z-topics/" title="A-Z Topics" class="">A-Z Topics</a>
						  
                            
                           
                        </li>
                        
						<li id="coronavirusNavItem"><a href="/web/20220109063045/https://doh.sd.gov/news/Coronavirus.aspx" title="Coronavirus" class="">COVID-19</a>
							
						</li>

                        <li><a href="/web/20220109063045/https://doh.sd.gov/news/" title="News" class="">News</a>
						  <br/><div class="dropdown_box"><ul id="subtree" class="treeview"><li><a href="/web/20220109063045/https://doh.sd.gov/news/recent-releases.aspx" title="Recent Releases">Recent Releases</a></li></ul></div>
                        </li>
                        
                        
                        <li><a href="/web/20220109063045/https://doh.sd.gov/local-offices/" title="Local Offices" class="">Local Offices</a>
						  <br/><div class="dropdown_box"><ul id="subtree" class="treeview"><li><a href="/web/20220109063045/https://doh.sd.gov/local-offices/child-family-services/" title="Child &amp; Family Services, WIC">Child &amp; Family Services, WIC</a></li><li><a href="/web/20220109063045/https://doh.sd.gov/local-offices/family-planning/" title="Family Planning">Family Planning</a></li><li><a href="/web/20220109063045/https://doh.sd.gov/local-offices/hiv-std-testing/" title="HIV &amp; STD Testing">HIV &amp; STD Testing</a></li><li><a href="/web/20220109063045/https://doh.sd.gov/local-offices/vaccine-providers/" title="Vaccine Providers">Vaccine Providers</a></li></ul></div>
                        </li>
                          
                        
                        <li><a href="/web/20220109063045/https://doh.sd.gov/boards/" title="Licensing Boards" class="">Licensing Boards</a>
						  <br/><div class="dropdown_box"><ul id="subtree" class="treeview"><li><a href="/web/20220109063045/https://doh.sd.gov/boards/chiropractic/" title="South Dakota Board of Chiropractic Examiners">South Dakota Board of Chiropractic Examiners</a></li><li><a href="/web/20220109063045/https://doh.sd.gov/boards/dentistry/" title="South Dakota Board of Dentistry">South Dakota Board of Dentistry</a></li><li><a href="/web/20220109063045/https://doh.sd.gov/boards/funeral/" title="South Dakota Board of Funeral Service">South Dakota Board of Funeral Service</a></li><li><a href="/web/20220109063045/https://doh.sd.gov/boards/audiology/" title="South Dakota Board of Hearing Aid Dispensers and Audiologists">South Dakota Board of Hearing Aid Dispensers and Audiologists</a></li><li><a href="/web/20220109063045/https://doh.sd.gov/boards/massage/" title="South Dakota Board of Massage Therapy">South Dakota Board of Massage Therapy</a></li><li><a href="/web/20220109063045/https://doh.sd.gov/boards/medicine/" title="South Dakota Board of Medical and Osteopathic Examiners">South Dakota Board of Medical and Osteopathic Examiners</a></li><li><a href="/web/20220109063045/https://doh.sd.gov/boards/midwives/" title="South Dakota Board of Certified Professional Midwives">South Dakota Board of Certified Professional Midwives</a></li><li><a href="/web/20220109063045/https://doh.sd.gov/boards/nursing/" title="South Dakota Board of Nursing">South Dakota Board of Nursing</a></li><li><a href="/web/20220109063045/https://doh.sd.gov/boards/nursingfacility/" title="South Dakota Board of Nursing Facility Administrators ">South Dakota Board of Nursing Facility Administrators </a></li><li><a href="/web/20220109063045/https://doh.sd.gov/boards/optometry/" title="South Dakota Board of Examiners in Optometry">South Dakota Board of Examiners in Optometry</a></li><li><a href="/web/20220109063045/https://doh.sd.gov/boards/pharmacy/" title="South Dakota Board of Pharmacy">South Dakota Board of Pharmacy</a></li><li><a href="/web/20220109063045/https://doh.sd.gov/boards/podiatry/" title="South Dakota Board of Podiatry Examiners">South Dakota Board of Podiatry Examiners</a></li><li><a href="/web/20220109063045/https://doh.sd.gov/boards/speechpath/" title="South Dakota Board of Examiners for Speech-Language Pathology ">South Dakota Board of Examiners for Speech-Language Pathology </a></li></ul></div>
                        </li>
                        
                        <li><a href="/web/20220109063045/https://doh.sd.gov/calendar/" title="Event Calendar" class="">Event Calendar</a>
							  
                        </li>
                        
                        <li><a href="/web/20220109063045/https://doh.sd.gov/resources/" title="Resources and Publications" class="">Resources &amp; Publications</a>
						  <br/><div class="dropdown_box"><ul id="subtree" class="treeview"><li><a href="/web/20220109063045/https://doh.sd.gov/resources/publications/" title="Publications">Publications</a></li><li><a href="/web/20220109063045/https://doh.sd.gov/resources/statute-rules/" title="Statute &amp; Rules">Statute &amp; Rules</a></li><li><a href="/web/20220109063045/https://doh.sd.gov/resources/links/" title="Links">Links</a></li><li><a href="/web/20220109063045/https://doh.sd.gov/resources/bulletin.aspx" title="SD Public Health Bulletin">SD Public Health Bulletin</a></li></ul></div>
                        </li>
                        
                        <li><a href="/web/20220109063045/https://doh.sd.gov/faqs/" title="FAQs" class="">FAQs</a>
						  
                        </li>
                        
                       
                       
                  	</ul>
                   	</div>
				</nav>
            	<div class="force-clear"></div>


<nav id="breadcrumbs" class="clearfix">
    <div class="centerer">
    <a href="/web/20220109063045/https://doh.sd.gov/" title="Home">Home</a>
    
<img src="/web/20220109063045im_/https://doh.sd.gov/images/layout/breadcrumb.png" alt="separator"/><a href="/web/20220109063045/https://doh.sd.gov/COVID" title="COVID-19">COVID-19</a><img src="/web/20220109063045im_/https://doh.sd.gov/images/layout/breadcrumb.png" alt="separator"/><a href="/web/20220109063045/https://doh.sd.gov/COVID/Vaccine" title="COVID-19 Vaccine">COVID-19 Vaccine</a>
    </div>
</nav>

<div class="force-clear"></div>


                    <div id="covidBanner">
                        <a href="/web/20220109063045/https://doh.sd.gov/COVID/COVID/default.aspx">COVID-19 Updates and Information</a>
                    </div>
<div class="force-clear"></div>
<div id="content" class="clearfix COVID">
								<div id="sidebar">
    	<h2><a href="/web/20220109063045/https://doh.sd.gov/COVID/" title="COVID-19">COVID-19</a></h2><div id="treecontrol"><a title="Collapse the entire tree below" href="#"> Collapse All</a> | <a title="Expand the entire tree below" href="#"> Expand All</a></div><ul id="subtree" class="treeview"><li class="COVID sub"><a href="/web/20220109063045/https://doh.sd.gov/COVID/Dashboard.aspx" title="Dashboard">Dashboard</a><li class="COVID sub"><a href="/web/20220109063045/https://doh.sd.gov/COVID/YourHealth.aspx" title="Your Health">Your Health</a><li class="COVID sub"><a href="/web/20220109063045/https://doh.sd.gov/COVID/Calculator/" title="Quarantine &amp; Isolation Calculator">Quarantine &amp; Isolation Calculator</a><li class="COVID sub"><a href="/web/20220109063045/https://doh.sd.gov/COVID/Community.aspx" title="Community, Work, &amp; School">Community, Work, &amp; School</a><li class="COVID sub"><a href="/web/20220109063045/https://doh.sd.gov/COVID/Testing/" title="Testing">Testing</a><ul><li><a href="/web/20220109063045/https://doh.sd.gov/COVID/Testing/Locations.aspx" title="Locations">Locations</a></li><li><a href="/web/20220109063045/https://doh.sd.gov/COVID/Testing/FREETestKit.aspx" title="Free At-Home Testing">Free At-Home Testing</a></li><li><a href="/web/20220109063045/https://doh.sd.gov/COVID/Testing/Guidance.aspx" title="At-Home Testing Guidance">At-Home Testing Guidance</a></li></ul></li><li class="COVID sub"><a href="/web/20220109063045/https://doh.sd.gov/COVID/Vaccine/" title="COVID-19 Vaccine">COVID-19 Vaccine</a><ul><li><a href="/web/20220109063045/https://doh.sd.gov/COVID/Vaccine/Public.aspx" title="Vaccine Information">Vaccine Information</a></li><li><a href="/web/20220109063045/https://doh.sd.gov/COVID/Vaccine/faqs.aspx" title="Vaccine FAQs">Vaccine FAQs</a></li><li><a href="/web/20220109063045/https://doh.sd.gov/COVID/Vaccine/VaccineProviders.aspx" title="Vaccination Locations">Vaccination Locations</a></li><li><a href="/web/20220109063045/https://doh.sd.gov/COVID/Vaccine/VolunteerTraining.aspx" title="Volunteer Training">Volunteer Training</a></li></ul></li><li class="COVID sub"><a href="/web/20220109063045/https://doh.sd.gov/COVID/Providers/" title="Healthcare Providers">Healthcare Providers</a><ul><li><a href="/web/20220109063045/https://doh.sd.gov/COVID/Providers/VaccineProviders/" title="Vaccine Providers">Vaccine Providers</a><ul><li><a href="/web/20220109063045/https://doh.sd.gov/COVID/Providers/VaccineProviders/enrollment.aspx" title="Provider Enrollment">Provider Enrollment</a></li><li><a href="/web/20220109063045/https://doh.sd.gov/COVID/Providers/VaccineProviders/Reporting.aspx" title="Vaccination Reporting">Vaccination Reporting</a></li><li><a href="/web/20220109063045/https://doh.sd.gov/COVID/Providers/VaccineProviders/Vaccinators.aspx" title="Vaccinator Training">Vaccinator Training</a></li><li><a href="/web/20220109063045/https://doh.sd.gov/COVID/Providers/VaccineProviders/StorageHandling.aspx" title="Storage &amp; Handling Training">Storage &amp; Handling Training</a></li><li><a href="/web/20220109063045/https://doh.sd.gov/COVID/Providers/VaccineProviders/DataEntry.aspx" title="Data Entry Training">Data Entry Training</a></li><li><a href="/web/20220109063045/https://doh.sd.gov/COVID/Providers/VaccineProviders/Coordinators.aspx" title="Vaccine Coordinator Training">Vaccine Coordinator Training</a></li></ul></li><li><a href="/web/20220109063045/https://doh.sd.gov/COVID/Providers/Calls.aspx" title="Archived Webinars">Archived Webinars</a></li></ul></li><li class="COVID sub"><a href="/web/20220109063045/https://doh.sd.gov/COVID/Resources.aspx" title="Resources">Resources</a></ul>    
    </div>   
							<div id="content_block">
                              <h1 style="text-align: center"><a href="default.aspx"><img src="/web/20220109063045im_/https://doh.sd.gov/images/COVID_Header_DOH.jpg" alt="South Dakota Department of Health Logo and covid.sd.gov button." id="top"/></a></h1>
                              <h1 style="text-align: center; font-size: 24px;">COVID-19 Vaccine Frequently Asked Questions                              </h1>
                              <p style="text-align: center"><a href="ProviderMap/default.aspx#Tools"> FIND A VACCINE</a></p>
                              <div>
      <div></div></div>
      <div id="Accordion1">
        <h9><a href="#"><img src="/web/20220109063045im_/https://doh.sd.gov/COVID/Vaccine/Images/Children.jpg" alt="COVID-19 Vaccine Safety FAQs"/></a></h9>
        <div>
          <p><a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/no-cost.html" target="_blank"><img src="/web/20220109063045im_/https://doh.sd.gov/COVID/Vaccine/Images/FREE_Vaccine.jpg" alt="The COVID-19 
vaccine is FREE 
to all people living 
in the U.S., 
regardless of their immigration or health insurance status." align="right"/></a>The United States <a href="https://web.archive.org/web/20220109063045/https://www.fda.gov/news-events/press-announcements/fda-authorizes-pfizer-biontech-covid-19-vaccine-emergency-use-children-5-through-11-years-age" target="_blank">Food and Drug Administration (FDA)</a> and the <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/media/releases/2021/s1102-PediatricCOVID-19Vaccine.html" target="_blank">Centers for Disease Control and Prevention (CDC)</a> have recently approved and recommended the Pfizer BioNTech COVID-19 vaccine for <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/recommendations/children-teens.html" target="_blank">children aged 5 to 11</a>. </p>
          <h2>Who is eligible for the pediatric COVID-19 vaccine? </h2>
          <p>All children ages 5-11 are eligible to receive the pediatric  COVID-19 vaccine. Children ages 12-17 are eligible to receive the Pfizer COVID-19  vaccine authorized for adults.</p>
<h2>What is the difference between the pediatric Pfizer COVID-19 vaccine and the adult Pfizer COVID-19 vaccine?</h2>
          <p>The pediatric vaccine (for ages 5-11) is the same vaccine as that for persons ages 12 and older, but at a lower dose. Both vaccines come in a two-dose series given three weeks apart.</p>
          <h2>Is the vaccine safe for children? </h2>
          <p>Over 3,000 children have received the vaccine during clinical trials. The data collected during those trials proves that the vaccine is both <u>safe and effective</u>. The most common side effects include headaches, fever, and chills in the two days post-vaccination.</p>
          <h2>How well does the vaccine work?</h2>
          <p>Clinical trials conducted in children ages 5-11 have shown 90.7% efficacy in fighting COVID-19. The vaccine produces a strong immune response in children which helps prevent serious illness from the virus.</p>
          <h2>Why should children get vaccinated for COVID-19?</h2>
          <p>Getting a COVID-19 vaccine can help protect children ages 5 years and older from getting COVID-19 or its variants.</p>
          <ul>
            <li>
              <p>Vaccinating children can help <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/your-health/about-covid-19/caring-for-children/families.html" target="_blank">protect family members</a>, including siblings who are not eligible for vaccination and family members who may be at increased risk of getting very sick if they are infected.</p>
            </li>
            <li>
              <p>Vaccination can also help protect children from both short-term complications like hospitalization and <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/long-term-effects/" target="_blank">long-term health complications</a> due to COVID-19.</p>
            </li>
            <li>
              <p>Vaccinating children ages 5 years and older can help keep them in school and help them safely participate in sports, playdates, and other group activities.</p>
            </li>
          </ul>
          <p>Parents should talk with their child's pediatrician to get trusted, personalized medical advice and do what is right for their families.</p>
        <p style="text-align: center"><a href="/web/20220109063045/https://doh.sd.gov/documents/COVID19/Vaccine/ChildVaccine_1pager.pdf">ONE-PAGER</a> | <a href="/web/20220109063045/https://doh.sd.gov/documents/COVID19/Vaccine/ChildVaccineFAQs.pdf" target="_blank">MORE FAQs</a> | <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/recommendations/children-teens.html" target="_blank">CDC INFORMATION</a></div>
		  <h9><a href="#"><img src="/web/20220109063045im_/https://doh.sd.gov/COVID/Vaccine/Images/FluCovid.jpg" alt="COVID-19 Vaccine General FAQs"/></a></h9>
        <div>
          <blockquote>
            <h2>Is there a test that can detect both flu and COVID-19?  </h2>
            <p><strong>Yes.</strong> There are tests that will check for seasonal influenza A and B viruses and SARS-CoV-2, the virus that causes COVID-19. Testing for these viruses at the same time gives public health officials important information about how flu and COVID-19 are spreading and what prevention steps people should take. These tests also help public health laboratories save time and testing materials, and possibly return test results faster. More <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/lab/multiplex.html" target="_blank">information for laboratories </a>is available.</p>
          </blockquote>
          <blockquote>
            <h2>Will a flu vaccine protect me against COVID-19?  </h2>
            <p>Flu vaccines are not designed to protect against COVID-19. Flu vaccination reduces the risk of flu illness, hospitalization, and death in addition to other important <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/flu/prevent/vaccine-benefits.htm" target="_blank">benefits</a>.</p>
            <p>Likewise, <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/your-vaccination.html" target="_blank">getting a COVID-19 vaccine</a> is the best protection against COVID-19, but those vaccines are not designed to protect against flu. Visit the CDC's <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/faq.html" target="_blank">Frequently Asked Questions</a> page for information about COVID-19 vaccinations.            </p>
            <h2>Can COVID-19 and flu vaccines be administered at the same time?</h2>
            <p><strong>Yes.</strong> If  eligible, both influenza and COVID-19 vaccines can be administered at the same visit, without regard to timing as <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/vaccines/covid-19/clinical-considerations/covid-19-vaccines-us.html?CDC_AA_refVal=https%3A%2F%2Fwww.cdc.gov%2Fvaccines%2Fcovid-19%2Finfo-by-product%2Fclinical-considerations.html#Coadministration" target="_blank">recommended by CDC and its Advisory Committee on Immunizations Practices (ACIP).</a> If a you're due for both vaccines, providers are encouraged to offer both vaccines at the same visit. <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/vaccines/pubs/pinkbook/genrec.html" target="_blank">Coadministration</a> of all recommended vaccines is important because it increases the probability that people will be fully vaccinated.</p>
            <h2>Can I get a flu vaccine at the same time I get my COVID-19 booster shot?</h2>
            <p><strong>Yes,</strong> you can get a flu vaccine at the same time you get a COVID-19 vaccine, including a COVID-19 booster shot.</p>
          </blockquote>
        </div>
		  <h9><a href="#"><img src="/web/20220109063045im_/https://doh.sd.gov/COVID/Vaccine/Images/Booster.jpg" alt="COVID-19 Vaccine 3rd dose/booster shots FAQs"/></a></h9>
        <div>
          <h2><strong>What is the difference between a &ldquo;3rd dose&rdquo; and a &ldquo;booster shot&rdquo;? </strong></h2>
          <p>The dosages are the same, but the term used is different based on who is receiving it. Boosters are offered to people who received the full course of a vaccine and developed a good response initially, but antibodies waned over time. Third doses are given to people whose immune systems may not have been able to develop a good antibody response to the initial vaccines.</p>
          <h2>Who can receive a 3rd dose?</h2>
          <p>The CDC currently recommends individuals who received the Pfizer-BioNTech or Moderna COVID-19 vaccine and have a compromised immune system receive a 3rd dose. Immunocompromised status will be left to self-attestation. This group currently includes patients who&rsquo;ve received or are experiencing: </p>
          <ul>
            <li>Organ transplants </li>
            <li>Stem cell transplants within the past two years</li>
            <li>Active cancer treatment for tumors or blood cancer and are undergoing chemotherapy that affects the immune system </li>
            <li>Severe primary immunodeficiency</li>
            <li>Advanced or untreated HIV</li>
            <li>Active treatment with high-dose corticosteroids or other drugs that may suppress your immune response</li>
          </ul>
          <h2>I am immunocompromised and have already received my 2nd dose, when should I get my 3rd dose? </h2>
          <p>You should get your 3rd dose at least 28 days after your 2nd dose. You should attempt to receive the same vaccine for your 3rd dose as you received for your 1st and 2nd doses. Contact your healthcare provider to schedule a 3rd dose.</p>
          <p>CDC is recommending that moderately or severely immunocompromised 5-11-year-olds receive an additional primary dose of vaccine 28 days after their second shot. At this time, only the Pfizer-BioNTech COVID-19 vaccine is authorized and recommended for children aged 5-11. <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/recommendations/immuno.html" target="_blank">Click to learn more</a>. </p>
          <p><a href="/web/20220109063045/https://doh.sd.gov/documents/COVID19/Vaccine/COVID_Vax_Booster.pdf"><img src="/web/20220109063045im_/https://doh.sd.gov/COVID/Vaccine/Images/BoosterCOVIDVax_web.jpg" alt="Who is eligible for a COVID-19 Booster shot? Pfizer and Moderna recipients who are: 65 years and older, 18+ and live in long-term care settings, 18+ and have underlying medical conditions including asthma, diabetes, obesity, and 18+ and work and/or live in high-risk settings, including heathcare workers, teachers, grocery store workers.
Anyone 18+ who received the J&amp;J vaccine is eligible for a booster.
When should I get my booster? At least 6 months after Pfizer or Moderna and at least 2 months after J&amp;J. Which booster shot do I get? You may have a preference, but you can get any booster shot."/></a></p>
          <h2>Who is eligible for a booster shot? </h2>
          <p>Everyone ages 12 and older is eligible to get a booster shot. See above graphic for more information.</p>
          <blockquote>
          <blockquote>
            <h2>Can I receive a 3rd dose/booster vaccine at a different location than where I received my original doses? </h2>
              <p>Yes.</p>
              <h2>What are the risks to getting a booster?</h2>
              <p>So far, reactions reported after getting a booster were similar to that of the 2-shot or single-dose initial series. Fever, headache, fatigue, and pain at the injection site were the most commonly reported side effects, and overall, most side effects were mild to moderate. However, as with the 2-shot or single-dose initial series, <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/safety/safety-of-vaccines.html" target="_blank">serious side effects are rare</a>, but may occur. For Pfizer-BioNTech and Moderna, side effects were reported less frequently following a booster dose than the second dose of the primary series.</p>
              <h2>If we need a booster dose, does that mean that the vaccines arenâ€™t working?</h2>
              <p>No. <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/effectiveness/work.html" target="_blank">COVID-19 vaccines are working well</a> to prevent severe illness, hospitalization, and death, even against the widely circulating Delta variant. However, public health experts are starting to see reduced protection, especially among certain populations, against mild and moderate disease.</p>
              <h2>Does this change the definition of &quot;fully vaccinated&quot; for those eligible for booster shots?</h2>
              <p>People are still considered fully vaccinated two weeks after their second dose in a 2-shot series, such as the Pfizer-BioNTech or Moderna vaccines, or two weeks after a single-dose vaccine, such as the J&amp;J/Janssen vaccine. This definition applies to all people, including those who receive an additional dose as recommended for <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/recommendations/immuno.html" target="_blank">moderate to severely immunocompromised people</a> and those who receive a booster shot.</p>
            </blockquote>
          </blockquote>
        </div>
         <h9><a href="#"><img src="/web/20220109063045im_/https://doh.sd.gov/COVID/Vaccine/Images/Safety.jpg" alt="COVID-19 Vaccine Safety FAQs"/></a></h9>
        <div>
          <h2>Why should I get vaccinated for COVID-19? </h2>
          <p>COVID-19 can cause serious illness or even death. There's no way to know how COVID-19 will affect you. And if you get sick, you could spread the disease to friends, family, and others around you, putting their lives at risk. Getting a COVID-19 vaccine greatly reduces the risk that you'll develop COVID-19.</p>
          <h2>Are the COVID-19 vaccines safe? </h2>
          <p><strong>Yes.</strong> COVID-19 vaccines available in the U.S. are <strong><a href="https://web.archive.org/web/20220109063045/https://youtu.be/7bBmQaX2k4w" target="_blank">safe and effective</a></strong>. They were evaluated in tens of thousands of participants in clinical trials. The vaccines met the <a href="https://web.archive.org/web/20220109063045/https://www.fda.gov/emergency-preparedness-and-response/coronavirus-disease-2019-covid-19/covid-19-vaccines#eua-vaccines" target="_blank">FDA's rigorous scientific standards</a> for safety, effectiveness, and manufacturing quality needed to support approval or authorization of a vaccine.</p>
          <p>Millions of people in the United States have received COVID-19 vaccines since they were authorized for emergency use by FDA. These vaccines have undergone and will continue to undergo the most intensive safety monitoring in U.S. history. This monitoring includes using both <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/pdfs/vaccine-safety-monitoring.pdf" target="new">established and new safety monitoring systems </a>to make sure that COVID-19 vaccines are safe.</p>
          <p>Your risk for serious health problems is much lower from the vaccine than your risk if you're unvaccinated and get COVID-19. COVID-19 can leave you with heart and lung damage and other conditions that require long-term treatment. Vaccines are much safer paths to immunity than the disease itself.</p>
          <h2>How can we know the vaccines are safe and effective if they have only been authorized for emergency use?</h2>
          <p>The FDA has authorized the use of three vaccines<em> (Pfizer has been granted <strong><a href="https://web.archive.org/web/20220109063045/https://www.fda.gov/news-events/press-announcements/fda-approves-first-covid-19-vaccine" target="_blank">full FDA approval</a></strong> for those 16 years and older)</em> in response to the public health emergency caused by the COVID-19 pandemic. Emergency use authorizations get <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/vaccines/covid-19/hcp/answering-questions.html" target="_blank">vaccines distributed faster</a> than the formal FDA approval process without skipping any mandatory safety checks. Large clinical trials demonstrated that the vaccines were safe and effective, and real-world experience has confirmed those findings. <a href="https://web.archive.org/web/20220109063045/https://www.fda.gov/emergency-preparedness-and-response/coronavirus-disease-2019-covid-19/covid-19-vaccines#eua-vaccines" target="_blank">Learn more</a> about the FDA's emergency use authorization process.          </p>
          <h2>Will the shot hurt or make me sick? </h2>
          <p><strong>No.</strong> Some people might get sore muscles, feel tired, or have mild fever after getting the vaccine, but most people report only a sore arm where they got the shot. These reactions mean the vaccine is working to help teach your body how to fight COVID-19 if you are exposed. For most people, these side effects will go away on their own in a few days. If you have any concerns, call your doctor or nurse. </p>
          <h2>Can a COVID-19 vaccine make me sick with COVID-19? </h2>
          <p><strong>No.</strong> None of the authorized <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/different-vaccines.html" target="_blank">COVID-19 vaccines in the United States</a> contain the live virus that causes COVID-19. This means that a COVID-19 vaccine <strong>cannot</strong> make you sick with COVID-19.</p>
          <p>COVID-19 vaccines teach our immune systems how to recognize and fight the virus that causes COVID-19. Sometimes this process can cause symptoms, such as fever. These symptoms are normal and are signs that the body is building protection against the virus that causes COVID-19. Learn more about <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/about-vaccines/how-they-work.html" target="_blank">how COVID-19 vaccines work</a>.</p>
          <h2>If I am pregnant or planning to become pregnant, can I get a COVID-19 vaccine? </h2>
          <p><strong>Yes. </strong>COVID-19 vaccination is recommended for everyone 12 years of age or older, including people who are trying to get pregnant now or might become pregnant in the future, as well as their partners.</p>
          <p>Currently no evidence shows that any vaccines, including COVID-19 vaccines, cause fertility problems in women or men. Learn more about <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/planning-for-pregnancy.html" target="_blank">COVID-19 vaccines and people who would like to have a baby</a>.</p>
          <h2>Are the COVID-19 vaccines safe for people with certain underlying medical conditions? </h2>
          <p>People with underlying medical conditions can receive a COVID-19 vaccine as long as they have not had <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/safety/allergic-reaction.html" target="_blank">an </a><a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/safety/allergic-reaction.html">immediate or severe allergic reaction</a> to a COVID-19 vaccine or to any of the ingredients in the vaccine. Learn more about vaccination <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/recommendations/underlying-conditions.html" target="_blank">considerations for people with underlying medical conditions.</a> Vaccination is an important consideration for adults of any age with <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/need-extra-precautions/people-with-medical-conditions.html" target="_blank">certain underlying medical conditions</a> because they are at increased risk for severe illness from COVID-19.</p>
        </div><h9><a href="#"><img src="/web/20220109063045im_/https://doh.sd.gov/COVID/Vaccine/Images/Effectiveness.jpg" alt="COVID-19 Vaccine Effectiveness FAQs"/></a></h9>
        <div>
          <h2 id="information" style="text-align: left">How do COVID-19 vaccines work? </h2>
          <p style="text-align: left">Vaccines train your immune system to recognize and fight the virus that causes COVID-19. With vaccines, you can build immunity to a disease without getting the disease. </p>
          <h2 style="text-align: left">How effective are the COVID-19 vaccines? </h2>
          <p style="text-align: left">All FDA-authorized COVID-19 vaccines are highly effective at preventing severe illness, hospitalization, and death due to COVID-19, including from the Delta variant. Remember: You're not fully protected from COVID-19 unless you're fully vaccinated. </p>
          <ul>
            <li>Johnson &amp; Johnson's Janssen vaccine requires one dose.</li>
            <li>The Pfizer-BioNTech and Moderna vaccines require two doses. </li>
          </ul>
          <h2 style="text-align: left">Why should I get vaccinated if I can still get infected with COVID-19? </h2>
          <p style="text-align: left">It's important to understand that infection doesn't necessarily lead to illness. If you're fully vaccinated against COVID-19 and the virus manages to enter your body and begins to multiply&#8212;that is, infect you&#8212;your immune system will be prepared to quickly recognize the virus and keep it from doing real damage. That's why most people who get infected with COVID-19 despite being vaccinated&#8212;so-called breakthrough cases&#8212;have no symptoms (asymptomatic) or only mild-to-moderate illness.</p>
          <p style="text-align: left">Nearly everyone in the United States who is getting severely ill, needing hospitalization, and dying from COVID-19 is unvaccinated.          </p>
          <h2 style="text-align: left">How long do COVID-19 vaccines last? </h2>
          <p>We don&rsquo;t know how long protection lasts for those who are vaccinated. What we do know is that COVID-19 has caused very serious illness and death for a lot of people. If you get COVID-19, you also risk giving it to loved ones and immunocompromised individuals who may get very sick or even die.</p>
          <p>People with moderately to severely compromised immune systems should <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/recommendations/immuno.html" target="_blank">receive an additional dose</a> of mRNA COVID-19 vaccine after the initial 2 doses.</p>
          <p>Learn more about <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/keythingstoknow.html" target="_blank">population immunity</a>.</p>
          <h2 style="text-align: left">Do I need to get a COVID-19 vaccine if I've already had COVID-19? </h2>
          <p>Yes, you should be vaccinated regardless of whether you already had COVID-19 because:</p>
          <ul>
            <li>Research has not yet shown how long you are protected from getting COVID-19 again after you've recovered.</li>
            <li>Vaccination helps protect you even if you&rsquo;ve already had COVID-19.</li>
          </ul>
          <p>Evidence is emerging that people <strong>get better protection by being fully vaccinated</strong> compared with having had COVID-19. <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/mmwr/volumes/70/wr/mm7032e1.htm?s_cid=mm7032e1_e&amp;ACSTrackingID=USCDC_921-DM63289&amp;ACSTrackingLabel=MMWR%20Early%20Release%20-%20Vol.%2070%2C%20August%206%2C%202021&amp;deliveryName=USCDC_921-DM63289" target="_blank">One study</a> showed that unvaccinated people who already had COVID-19 are more than 2 times as likely than fully vaccinated people to get COVID-19 again.</p>
          <p>If you were treated for COVID-19 with monoclonal antibodies or convalescent plasma, you should wait 90 days before getting a COVID-19 vaccine. Talk to your doctor if you are unsure what treatments you received or if you have more questions about getting a COVID-19 vaccine.          </p>
          <h2 style="text-align: left">Will the COVID-19 vaccine prevent me from infecting others? </h2>
          <p style="text-align: left">COVID-19 vaccines reduce the likelihood that you'll develop and be able to spread COVID-19. In rare occasions, some vaccinated people can get COVID-19 from the highly contagious Delta variant and spread it to others. Importantly, only a very small amount of spread happening around the country comes from vaccinated individuals. </p>
          <h2 style="text-align: left">Do the vaccines work on the new COVID variants? </h2>
          <p>COVID-19 vaccines are effective against severe disease and death from variants currently circulating in the United States, including the <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/variants/delta-variant.html" target="_blank">Delta variant</a>.</p>
        </div>
        
        <h9><a href="#"><img src="/web/20220109063045im_/https://doh.sd.gov/COVID/Vaccine/Images/General.jpg" alt="COVID-19 Vaccine General FAQs"/></a></h9>
        <div>
          <blockquote>
            <h2>When can I get the COVID-19 vaccine?  </h2>
            <p>Vaccines are here now and everyone age 5 and older can get them. You have three ways to find vaccines near you:  </p>
          </blockquote>
          <ul>
            <li>Go to <a href="https://web.archive.org/web/20220109063045/https://www.vaccines.gov/" target="_blank">vaccines.gov</a></li>
            <li>Text your ZIP code to 438829  </li>
            <li>Call <a href="https://web.archive.org/web/20220109063045/tel:1-800-232-0233">1-800-232-0233</a></li>
          </ul>
          <blockquote>
            <h2>How much will the COVID-19 vaccine cost?  </h2>
            <p>The federal government is providing vaccines <strong>free of charge</strong> to all people living in the United States, regardless of their immigration or health insurance status.</p>
            <h2>Do I need to wear a mask and avoid close contact with others after I am fully vaccinated?  </h2>
            <p>After you are fully vaccinated for COVID-19, take these steps to protect yourself and others:</p>
            <ul>
              <li>In general, you do not need to wear a mask in outdoor settings.</li>
              <li>If you are in an area with <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/covid-data-tracker/#county-view" target="_blank">high numbers of COVID-19 cases</a>, consider wearing a mask in crowded outdoor settings and when you are in <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/php/contact-tracing/contact-tracing-plan/appendix.html#contact" target="_blank">close contact</a> with others who are not fully vaccinated.</li>
              <li>If you have a condition or taking medications that weaken your immune system, you may not be fully protected even if you are fully vaccinated. You should continue to take all <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/prevent-getting-sick/prevention.html" target="_blank">precautions recommended for unvaccinated people, including wearing a well-fitted mask</a>, until advised otherwise by their healthcare provider.</li>
              <li>If you are fully vaccinated, to maximize protection from the Delta variant and prevent possibly spreading it to others, wear a mask indoors in public if you are in an area <a href="https://web.archive.org/web/20220109063045/https://covid.cdc.gov/covid-data-tracker/#county-view" target="_blank">of substantial or high transmission</a>.</li>
            </ul>
            <h2>How can I get a replacement COVID-19 vaccination card?</h2>
            <p>The SD DOH does not issue cards. You will need to go back to the place you received your vaccine to request a card. Clinics/Hospitals/Pharmacies may be able to re-issue cards based on their own policies.</p>
            <h2>How do I get a copy of my immunization record?</h2>
            <p>You can request a copy from your clinic, access your patient portal (if available), or call the SD DOH at 605-773-3737.</p>
          </blockquote>
        </div>
		  <h9><a href="#"><img src="/web/20220109063045im_/https://doh.sd.gov/COVID/Vaccine/Images/Resources.jpg" alt="COVID-19 Vaccine General FAQs"/></a></h9>
        <div>
          <blockquote>
            <ul>
              <li>                
                <p><strong class="err">UPDATED OFTEN! </strong><a href="https://web.archive.org/web/20220109063045/https://publichealthcollaborative.org/faq/" target="_blank">Answers to Tough Questions about Public Health</a> (Public Health Communications Collaborative)                </p>
              </li>
              <li>                
                <p><strong><span class="err">NEW!</span></strong> <a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/stay-up-to-date.html" target="_blank">Stay Up to Date with Your Vaccines</a> (CDC)                </p>
              </li>
              <li>
                <p><a href="https://web.archive.org/web/20220109063045/https://emergency.cdc.gov/han/2021/han00453.asp" target="_blank">COVID-19 Vaccination for Pregnant Women to Prevent Serious Illness, Deaths, and Adverse Pregnancy Outcomes from COVID-19</a> <span class="err">(CDC Health Advisory)</span></p>
              </li>
            
              <li>
                <p><a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/recommendations/children-teens.html" target="_blank">COVID-19 Vaccines for Children and Teens</a></p>
              </li>
              <li>
                <p><a href="/web/20220109063045/https://doh.sd.gov/documents/COVID19/Vaccine/WCDT_VaccineQuestions_English.pdf" target="_blank">Top COVID-19 Vaccine Questions &amp; Answers from the Office of the Surgeon General</a></p>
              </li>
              <li>
                <p><strong>COVID-19 Vaccine Information Packets:</strong> <a href="/web/20220109063045/https://doh.sd.gov/documents/COVID19/Vaccine/WCDT_VaccineInformation_English.pdf">English</a> | <a href="/web/20220109063045/https://doh.sd.gov/documents/COVID19/Vaccine/WCDT_VaccineInformation_Spanish.pdf" target="_blank">Spanish</a></p>
              </li>
              <li>
                <p><a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/faq.html" target="_blank">Frequently Asked Questions about COVID-19 Vaccination</a> (CDC)</p>
              </li>
              <li>
                <p><a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/facts.html" target="_blank">Myths and Facts about COVID-19 Vaccines</a> (CDC)                </p>
              </li>
              <li>
                <p><a href="https://web.archive.org/web/20220109063045/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/booster-shot.html" target="_blank">COVID-19 Vaccine Booster Shot</a> (CDC)              </p>
              </li>
              <li>
                <p><a href="https://web.archive.org/web/20220109063045/https://www.fda.gov/emergency-preparedness-and-response/coronavirus-disease-2019-covid-19/covid-19-vaccines" target="_blank">COVID-19 Vaccines</a> (FDA)                </p>
              </li>
            </ul>
          </blockquote>
        </div>
      </div>
								<p style="text-align: right">Reviewed 01-07-2022</p>
      <div id="share-bar">

	Share via:&nbsp;&nbsp;
	<a href="https://web.archive.org/web/20220109063045/http://www.facebook.com/dialog/feed?app_id=200288113360061&amp;redirect_uri=http://doh.sd.gov/COVID/Vaccine/faqs.aspx" id="share-fb" target="_blank" title="Share via Facebook"></a>
    <a href="https://web.archive.org/web/20220109063045/http://twitter.com/home?status=I'm reading http://doh.sd.gov/COVID/Vaccine/faqs.aspx" id="share-tw" target="_blank" title="Share via Twitter"></a>
    <a href="https://web.archive.org/web/20220109063045/mailto:/?subject=I%20thought%20you%20might%20like%20this...&amp;body=Hi,%20I%20thought%20you%20might%20like%20this%20page%20on%20the%20SD%20Dept%20of%20Health%20website.%20http://doh.sd.gov/COVID/Vaccine/faqs.aspx" id="share-em" target="_blank" title="Share via Email"></a>
</div>
  </div>
	<script type="text/javascript">
$(function() {
	$( "#Accordion1" ).accordion({
		heightStyle:"content",
		active:9,
		collapsible:true
	}); 
})
    </script>

</div>
<nav id="quicklinks-footer">
    <hr class="top"/>
    
    <div id="quicklinks-lnk" class="centerer clearfix">
        <a href="/web/20220109063045/https://doh.sd.gov/records/">
            <img src="/web/20220109063045im_/https://doh.sd.gov/images/layout/sections/records-small.png" alt="Certificates and Records"/><br/>
            Certificates <br/>
            &amp; Records
        </a>
        
        <a href="/web/20220109063045/https://doh.sd.gov/statistics/">
            <img src="/web/20220109063045im_/https://doh.sd.gov/images/layout/sections/statistics-small.png" alt="Health Data and Statistics"/><br/>
            Health Data <br/>
            &amp; Statistics
        </a>
        
        <a href="/web/20220109063045/https://doh.sd.gov/diseases/">
            <img src="/web/20220109063045im_/https://doh.sd.gov/images/layout/sections/diseases-small.png" alt="Diseases and Conditions"/><br/>
            Diseases &amp; <br/>
            Conditions
        </a>
        
        <a href="/web/20220109063045/https://doh.sd.gov/family/">
            <img src="/web/20220109063045im_/https://doh.sd.gov/images/layout/sections/family-small.png" alt="Family and Child Development"/><br/>
            Family &amp; Child <br/>
            Development
        </a>
        
        <a href="/web/20220109063045/https://doh.sd.gov/prevention/">
            <img src="/web/20220109063045im_/https://doh.sd.gov/images/layout/sections/prevention-small.png" alt="Prevention and Healthy Living"/><br/>
            Prevention &amp; <br/>
            Healthy Living
        </a>
        
        <a href="/web/20220109063045/https://doh.sd.gov/food/">
            <img src="/web/20220109063045im_/https://doh.sd.gov/images/layout/sections/food-small.png" alt="Food and Lodging Safety"/><br/>
            Food &amp; <br/>
            Lodging Safety
        </a>
        
        <a href="/web/20220109063045/https://doh.sd.gov/providers/">
            <img src="/web/20220109063045im_/https://doh.sd.gov/images/layout/sections/providers-small.png" alt="Healthcare Providers"/><br/>
            Healthcare <br/>
            Providers
        </a>
    
        <a href="/web/20220109063045/https://doh.sd.gov/services/">
            <img src="/web/20220109063045im_/https://doh.sd.gov/images/layout/sections/services-small.png" alt="Services and Programs"/><br/>
            Services &amp;<br/>
            Programs
        </a>
        
        <a href="/web/20220109063045/https://doh.sd.gov/a-z-topics/" class="quicklast">
            <img src="/web/20220109063045im_/https://doh.sd.gov/images/layout/sections/a-z-topics-small.png" alt="A-Z Topics"/><br/>
            A-Z <br/>
            Topics
        </a>
    </div>
    
    <hr/>
    <hr/>
</nav>
                


</div>
<!--#container--></div><!--#middle-->      
	<footer>
        <div class="container">
            <div id="left"><a href="https://web.archive.org/web/20220109063045/https://sd.gov/" target="_blank" title="State of SD Home"><img src="/web/20220109063045im_/https://doh.sd.gov/images/layout/sdgov.png" alt="State of South Dakota Home" title="State of South Dakota Home"/></a></div>
            <div id="center">
            	<p><a href="/web/20220109063045/https://doh.sd.gov/" title="Dept of Healh Home">DOH Home</a>&nbsp;    <a href="https://web.archive.org/web/20220109063045/https://intranetdoh.sd.gov/" target="_blank">DOH Intranet&nbsp;</a>&nbsp;&nbsp;<a href="https://web.archive.org/web/20220109063045/http://bhr.sd.gov/workforus" target="_blank">Careers&nbsp;</a>&nbsp;&nbsp;<a href="https://web.archive.org/web/20220109063045/https://sd.gov/accpolicy.aspx" target="_blank" title="Accessibility Policy" target="_blank">Accessibility Policy</a>&nbsp;&nbsp;&nbsp;<a href="https://web.archive.org/web/20220109063045/https://sd.gov/privacy.aspx" target="_blank" title="Privacy Policy" target="_blank">Privacy Policy</a>&nbsp;&nbsp;&nbsp;<a href="https://web.archive.org/web/20220109063045/https://sd.gov/disclaim.aspx">Disclaimer</a></a>&nbsp;&nbsp;&nbsp;<a href="/web/20220109063045/https://doh.sd.gov/documents/HIPAANotice.pdf" title="DOH HIPAA" target="_blank">DOH HIPAA</a>	            </p>
            	<div id="disclaimer">
    		        &copy;2019 South Dakota Department of Health. All Rights Reserved.&nbsp;&nbsp;|&nbsp;&nbsp;<a href="https://web.archive.org/web/20220109063045/mailto:DOH.info@state.sd.us">DOH.info@state.sd.us</a><br/>
            		600 East Capitol Avenue&nbsp;&nbsp;|&nbsp;&nbsp;Pierre, SD 57501-2536&nbsp;&nbsp;|&nbsp;&nbsp;605-773-3361&nbsp;&nbsp;|&nbsp;&nbsp;1-800-738-2301 (In State)<br/>
		            <div id="imd"></div>
        	    </div>
            </div>
            <div id="right"><a href="/web/20220109063045/https://doh.sd.gov/contact/" title="Contact DOH" class="button">Contact DOH</a></div>
        </div>
	</footer>

	
	<script src="/web/20220109063045js_/https://doh.sd.gov/includes/js/jquery/slides.jquery.js"></script>
	<script type="text/javascript">
		$(function(){
			$('#slides').slides({				
				preload: true, 
				generateNextPrev: false, 
				pagination: true, 
				generatePagination: true, 
				paginationClass: 'pagination', 
				fadeSpeed: 1000, 
				effect: 'slide', 
				crossfade: true, 
				play: 5000, 
				pause: 2500, 
				hoverPause: true, 
				autoHeight: false, 
				bigTarget: false 
			});
		});
	</script>

	
	<script type="text/javascript">
		$(function(){
			$('#program_slides').slides({
				preload: true, 
				container: 'program_slides_container', 
				generateNextPrev: true, 
				pagination: false, 
				generatePagination: false, 
				effect: 'slide', 
				play: 5000, 
				pause: 2500, 
				hoverPause: true, 
				bigTarget: false 
				
				
			});
		});
	</script>



<script>// This 
    var dropdown = $(".dropdown-btn");
        dropdown.click( function() {
            $(".active").removeClass("active")
            $(this).toggleClass("active")
            $(".dropdown-container").hide()
            var dropdownContent = $(this).next();
            if (dropdownContent.is(":visible")){
                dropdownContent.hide()
            }
            else dropdownContent.show()
        });
</script>



    
	<script type="text/javascript">
	    $(document).ready(function () {

	        if ($(window).width() > 830) {
	            $("#sidebar").treeview({
	                animated: "fast",
	                collapsed: true,
	                control: "#treecontrol",
	                persist: "location"
	            });
	        }
	        else {
	            $("#sidebar").treeview({

	            });
	        }

	        $("#menu").click(function () {
	            $("#middle #main").toggle();

	        });

	        $("#sidebar").click(function () {

	            $("#sidebar #subtree").toggle();
	            $("#sidebar h2").toggleClass("select");

            });

	        $("#sidebar li, #subtree, .treeview").click(function (e) {
	           //e.stopPropagation()
	        });

            

	    });
	</script>
    
    
    
    <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//web.archive.org/web/20220109063045/https://www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-41724005-1', 'auto');
	  ga('send', 'pageview');	
	</script></body>
</html><!--
     FILE ARCHIVED ON 06:30:45 Jan 09, 2022 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 15:28:34 Apr 25, 2022.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
-->
<!--
playback timings (ms):
  captures_list: 200.838
  exclusion.robots: 0.202
  exclusion.robots.policy: 0.184
  RedisCDXSource: 3.932
  esindex: 0.015
  LoadShardBlock: 175.989 (3)
  PetaboxLoader3.datanode: 200.391 (4)
  CDXLines.iter: 16.032 (3)
  load_resource: 172.326
  PetaboxLoader3.resolve: 132.241
-->