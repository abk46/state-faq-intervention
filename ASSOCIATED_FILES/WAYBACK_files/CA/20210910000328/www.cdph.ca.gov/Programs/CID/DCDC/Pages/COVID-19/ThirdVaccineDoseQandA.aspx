
<!DOCTYPE html>
<html lang="en" dir="ltr">

    <head><script src="//archive.org/includes/analytics.js?v=cf34f82" type="text/javascript"></script>
<script type="text/javascript">window.addEventListener('DOMContentLoaded',function(){var v=archive_analytics.values;v.service='wb';v.server_name='wwwb-app203.us.archive.org';v.server_ms=345;archive_analytics.send_pageview({});});</script>
<script type="text/javascript" src="/_static/js/bundle-playback.js?v=36gO9Ebf" charset="utf-8"></script>
<script type="text/javascript" src="/_static/js/wombat.js?v=UHAOicsW" charset="utf-8"></script>
<script type="text/javascript">
  __wm.init("https://web.archive.org/web");
  __wm.wombat("https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/COVID-19/ThirdVaccineDoseQandA.aspx","20210910000328","https://web.archive.org/","web","/_static/",
	      "1631232208");
</script>
<link rel="stylesheet" type="text/css" href="/_static/css/banner-styles.css?v=fantwOh2" />
<link rel="stylesheet" type="text/css" href="/_static/css/iconochive.css?v=qtvMKcIJ" />
<!-- End Wayback Rewrite JS Include -->

            
		<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE10"/>
            
            <link rel="canonical" href="https://web.archive.org/web/20210910000328/https://www.cdph.ca.gov:443/Programs/CID/DCDC/Pages/COVID-19/ThirdVaccineDoseQandA.aspx"/>
            
        
        <meta http-equiv="Content-type" content="text/html; charset=utf-8"/><meta http-equiv="Expires" content="0"/><meta charset="utf-8"/><meta name="viewport" content="width=device-width, initial-scale=1.0"/><meta name="description"/><meta name="author"/><title>
	
            
            
            Questions and Answers: Additional COVID-19 Vaccine Doses for People Whose Immune Systems are Compromised
            
        
</title><link rel="shortcut icon" href="/web/20210910000328im_/https://www.cdph.ca.gov/Style%20Library/CDPH/Images/favicon.ico" type="image/vnd.microsoft.icon" id="favicon"/><link rel="stylesheet" type="text/css" href="/web/20210910000328cs_/https://www.cdph.ca.gov/_layouts/15/1033/styles/Themable/corev15.css?rev=WBDzE8Kp2NMrldHsGGXlEQ%3D%3D"/>
<link rel="stylesheet" type="text/css" href="/web/20210910000328cs_/https://www.cdph.ca.gov/Style%20Library/en-US/Themable/Core%20Styles/controls15.css"/>
<script type="text/javascript" src="/web/20210910000328js_/https://www.cdph.ca.gov/_layouts/15/init.js?rev=AS%2Bv0UYCkcLYkV95cqJXGA%3D%3D"></script>
<script type="text/javascript" src="/web/20210910000328js_/https://www.cdph.ca.gov/_layouts/15/cdph/js/setimagebackground.js"></script>
<script type="text/javascript" src="/web/20210910000328js_/https://www.cdph.ca.gov/ScriptResource.axd?d=G1tWcD9ITW8tl9c2zYWjQa1LI_bkDlGvKGXdF6NXgHm1H_7LX9_9YuBJsoGCDZPkVojZog9tuzfI3QVfh9HPmmvrcwYGnVAk2Ytsm60nL6DMwJaiX5sANX1HHDCDt0xbi0Cy_9_rN9nySDlT0RnppKr5tNvCjVyqfDNtY8CyRO65szE9-rF__CV6fSrL68m40&amp;t=2fe674eb"></script>
<script type="text/javascript" src="/web/20210910000328js_/https://www.cdph.ca.gov/_layouts/15/blank.js?rev=ZaOXZEobVwykPO9g8hq%2F8A%3D%3D"></script>
<script type="text/javascript" src="/web/20210910000328js_/https://www.cdph.ca.gov/ScriptResource.axd?d=WLt9lTlwxhoxzYK4tw-KCbwiiWZH7D9ZYcdIvtZgTmLJgtCWMxkGbNg-2oFn_iXGKYOToMfnFy2g4FWkX9aYkRRGS0-8pjn9kzjXSAiGTFymCwRyzCwGuJnj3Q8qGZ4SrC1mrG0XIRJ2vXg4_imslJT_AFWqi7cUJDcrvRlbk3n2wuY3-FT1YIz4agW78KEy0&amp;t=2fe674eb"></script>
<script type="text/javascript">RegisterSod("initstrings.js", "\u002f_layouts\u002f15\u002f1033\u002finitstrings.js?rev=S11vfGURQYVuACMEY0tLTg\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("strings.js", "\u002f_layouts\u002f15\u002f1033\u002fstrings.js?rev=xXYZY4hciX287lShPZuClw\u00253D\u00253D");RegisterSodDep("strings.js", "initstrings.js");</script>
<script type="text/javascript">RegisterSod("sp.init.js", "\u002f_layouts\u002f15\u002fsp.init.js?rev=jvJC3Kl5gbORaLtf7kxULQ\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.res.resx", "\u002f_layouts\u002f15\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=SP\u00252ERes\u0026rev=F0s6Jic25htNyvJ0DOTgdw\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.ui.dialog.js", "\u002f_layouts\u002f15\u002fsp.ui.dialog.js?rev=3Oh2QbaaiXSb7ldu2zd6QQ\u00253D\u00253D");RegisterSodDep("sp.ui.dialog.js", "sp.init.js");RegisterSodDep("sp.ui.dialog.js", "sp.res.resx");</script>
<script type="text/javascript">RegisterSod("core.js", "\u002f_layouts\u002f15\u002fcore.js?rev=BoOTONqXW5dYCwvqGhdhCw\u00253D\u00253D");RegisterSodDep("core.js", "strings.js");</script>
<script type="text/javascript">RegisterSod("menu.js", "\u002f_layouts\u002f15\u002fmenu.js?rev=cXv35JACAh0ZCqUwKU592w\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("mQuery.js", "\u002f_layouts\u002f15\u002fmquery.js?rev=VYAJYBo5H8I3gVSL3MzD6A\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("callout.js", "\u002f_layouts\u002f15\u002fcallout.js?rev=ryx2n4ePkYj1\u00252FALmcsXZfA\u00253D\u00253D");RegisterSodDep("callout.js", "strings.js");RegisterSodDep("callout.js", "mQuery.js");RegisterSodDep("callout.js", "core.js");</script>
<script type="text/javascript">RegisterSod("clienttemplates.js", "\u002f_layouts\u002f15\u002fclienttemplates.js?rev=OAp3\u00252BvnM\u00252FjzKrKWlIed9qg\u00253D\u00253D");RegisterSodDep("clienttemplates.js", "initstrings.js");</script>
<script type="text/javascript">RegisterSod("sharing.js", "\u002f_layouts\u002f15\u002fsharing.js?rev=XxxHIxIIc8BsW9ikVc6dgA\u00253D\u00253D");RegisterSodDep("sharing.js", "strings.js");RegisterSodDep("sharing.js", "mQuery.js");RegisterSodDep("sharing.js", "clienttemplates.js");RegisterSodDep("sharing.js", "core.js");</script>
<script type="text/javascript">RegisterSod("suitelinks.js", "\u002f_layouts\u002f15\u002fsuitelinks.js?rev=REwVU5jSsadDdOZlCx4wpA\u00253D\u00253D");RegisterSodDep("suitelinks.js", "strings.js");RegisterSodDep("suitelinks.js", "core.js");</script>
<script type="text/javascript">RegisterSod("clientrenderer.js", "\u002f_layouts\u002f15\u002fclientrenderer.js?rev=PWwV4FATEiOxN90BeB5Hzw\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("srch.resources.resx", "\u002f_layouts\u002f15\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=Srch\u00252EResources\u0026rev=fPbk\u00252B7qByTbyuNVTMptzRw\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("search.clientcontrols.js", "\u002f_layouts\u002f15\u002fsearch.clientcontrols.js?rev=8Q15HW2EU2aXBktJq5YW\u00252FA\u00253D\u00253D");RegisterSodDep("search.clientcontrols.js", "sp.init.js");RegisterSodDep("search.clientcontrols.js", "clientrenderer.js");RegisterSodDep("search.clientcontrols.js", "srch.resources.resx");</script>
<script type="text/javascript">RegisterSod("sp.runtime.js", "\u002f_layouts\u002f15\u002fsp.runtime.js?rev=5f2WkYJoaxlIRdwUeg4WEg\u00253D\u00253D");RegisterSodDep("sp.runtime.js", "sp.res.resx");</script>
<script type="text/javascript">RegisterSod("sp.search.js", "\u002f_layouts\u002f15\u002fsp.search.js?rev=dMkPlEXpdY6iJ\u00252FsY5RsB0g\u00253D\u00253D");RegisterSodDep("sp.search.js", "sp.init.js");RegisterSodDep("sp.search.js", "sp.runtime.js");</script>
<script type="text/javascript">RegisterSod("ajaxtoolkit.js", "\u002f_layouts\u002f15\u002fajaxtoolkit.js?rev=4hdiFSvb0aNeLsXllaqZDw\u00253D\u00253D");RegisterSodDep("ajaxtoolkit.js", "search.clientcontrols.js");</script>
<script type="text/javascript">RegisterSod("sp.js", "\u002f_layouts\u002f15\u002fsp.js?rev=UyyqxNkClmZdKSjw8bYMAQ\u00253D\u00253D");RegisterSodDep("sp.js", "sp.runtime.js");RegisterSodDep("sp.js", "sp.ui.dialog.js");RegisterSodDep("sp.js", "sp.res.resx");</script>
<script type="text/javascript">RegisterSod("userprofile", "\u002f_layouts\u002f15\u002fsp.userprofiles.js?rev=p5tCOm\u00252FlHUwcfll7W3pKNw\u00253D\u00253D");RegisterSodDep("userprofile", "sp.runtime.js");</script>
<script type="text/javascript">RegisterSod("followingcommon.js", "\u002f_layouts\u002f15\u002ffollowingcommon.js?rev=jWqEDmcjCSPmnQw2ZIfItQ\u00253D\u00253D");RegisterSodDep("followingcommon.js", "strings.js");RegisterSodDep("followingcommon.js", "sp.js");RegisterSodDep("followingcommon.js", "userprofile");RegisterSodDep("followingcommon.js", "core.js");RegisterSodDep("followingcommon.js", "mQuery.js");</script>
<script type="text/javascript">RegisterSod("profilebrowserscriptres.resx", "\u002f_layouts\u002f15\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=ProfileBrowserScriptRes\u0026rev=J5HzNnB\u00252FO1Id\u00252FGI18rpRcw\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.ui.mysitecommon.js", "\u002f_layouts\u002f15\u002fsp.ui.mysitecommon.js?rev=Ua8qmZSU9nyf53S7PEyJwQ\u00253D\u00253D");RegisterSodDep("sp.ui.mysitecommon.js", "sp.init.js");RegisterSodDep("sp.ui.mysitecommon.js", "sp.runtime.js");RegisterSodDep("sp.ui.mysitecommon.js", "userprofile");RegisterSodDep("sp.ui.mysitecommon.js", "profilebrowserscriptres.resx");</script>
<script type="text/javascript">RegisterSod("browserScript", "\u002f_layouts\u002f15\u002fnon_ie.js?rev=EbmYHBPzx543YPG1j0ioLA\u00253D\u00253D");RegisterSodDep("browserScript", "strings.js");</script>
<script type="text/javascript">RegisterSod("inplview", "\u002f_layouts\u002f15\u002finplview.js?rev=iMf5THfqukSYut7sl9HwUg\u00253D\u00253D");RegisterSodDep("inplview", "strings.js");RegisterSodDep("inplview", "core.js");RegisterSodDep("inplview", "sp.js");</script>
<script type="text/javascript">RegisterSod("sp.core.js", "\u002f_layouts\u002f15\u002fsp.core.js?rev=tZDGLPOvY1bRw\u00252BsgzXpxTg\u00253D\u00253D");RegisterSodDep("sp.core.js", "strings.js");RegisterSodDep("sp.core.js", "sp.init.js");RegisterSodDep("sp.core.js", "core.js");</script>
<script type="text/javascript">RegisterSod("dragdrop.js", "\u002f_layouts\u002f15\u002fdragdrop.js?rev=xyQu9SPqkjO4R2\u00252BS3IwO8Q\u00253D\u00253D");RegisterSodDep("dragdrop.js", "strings.js");</script>
<script type="text/javascript">RegisterSod("quicklaunch.js", "\u002f_layouts\u002f15\u002fquicklaunch.js?rev=\u00252BHeX6ARcp\u00252F9LpMq6FqQYyA\u00253D\u00253D");RegisterSodDep("quicklaunch.js", "strings.js");RegisterSodDep("quicklaunch.js", "dragdrop.js");</script>
<script type="text/javascript">RegisterSod("datepicker.js", "\u002f_layouts\u002f15\u002fdatepicker.js?rev=QSXAxtWIhtEi1Gv0uzsKeg\u00253D\u00253D");RegisterSodDep("datepicker.js", "strings.js");</script>
<link type="text/xml" rel="alternate" href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/_vti_bin/spsdisco.aspx"/><span id="DeltaOpenGraphPlaceHolder">
            
        </span>
        <!-- Global site tag (gtag.js) - Google Analytics 
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-109056085-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() { dataLayer.push(arguments); }
            gtag('js', new Date());
            
            gtag('config', 'UA-109056085-1');
        </script>-->
        <link href="/web/20210910000328cs_/https://www.cdph.ca.gov/Style Library/CDPH/css/bootstrap.min.css" rel="stylesheet"/><link href="/web/20210910000328cs_/https://www.cdph.ca.gov/Style Library/CDPH/css/sp-responsive.css" rel="stylesheet"/><link href="/web/20210910000328cs_/https://www.cdph.ca.gov/Style Library/CDPH/css/bootstrap3-custom.css" rel="stylesheet"/><link href="/web/20210910000328cs_/https://www.cdph.ca.gov/Style Library/CDPH/css/font-awesome.min.css" rel="stylesheet"/><link href="/web/20210910000328cs_/https://www.cdph.ca.gov/Style Library/CDPH/css/jquery.mCustomScrollbar.css" rel="stylesheet"/><link href="/web/20210910000328cs_/https://www.cdph.ca.gov/Style Library/CDPH/css/sm-core-css.css" rel="stylesheet"/><link href="/web/20210910000328cs_/https://www.cdph.ca.gov/Style Library/CDPH/css/sm-clean.css" rel="stylesheet"/><link href="/web/20210910000328cs_/https://www.cdph.ca.gov/Style Library/CDPH/css/vertical-tabs.css" rel="stylesheet"/><link href="/web/20210910000328cs_/https://www.cdph.ca.gov/Style Library/CDPH/css/portal.css" rel="stylesheet"/><link href="/web/20210910000328cs_/https://www.cdph.ca.gov/_catalogs/masterpage/KIEFER/css/custom.css" rel="stylesheet"/>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="/web/20210910000328js_/https://www.cdph.ca.gov/Style Library/CDPH/js/jquery.min.js"></script>
        <noscript></noscript>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="/web/20210910000328js_/https://www.cdph.ca.gov/Style Library/CDPH/js/bootstrap.min.js"></script>
        <noscript></noscript>
        <script src="/web/20210910000328js_/https://www.cdph.ca.gov/Style Library/CDPH/js/bootstrap3-custom.js"></script>
        <noscript></noscript>
        
        <script src="/web/20210910000328js_/https://www.cdph.ca.gov/Style Library/CDPH/js/jquery.mCustomScrollbar.concat.min.js"></script>
        <noscript></noscript>
        <!--For Custom scrollbar-->
        <script src="/web/20210910000328js_/https://www.cdph.ca.gov/Style Library/CDPH/js/jquery.easyResponsiveTabs.js"></script>
        <noscript></noscript>
        <script src="/web/20210910000328js_/https://www.cdph.ca.gov/Style Library/CDPH/js/jquery.mobile.custom.min.js"></script>
        <noscript></noscript>
        <script src="/web/20210910000328js_/https://www.cdph.ca.gov/Style Library/CDPH/js/CDPH-Portal.js"></script>
        <script src="/web/20210910000328js_/https://www.cdph.ca.gov/_layouts/15/cdph/js/ContextMenu.js"></script>
        <script src="/web/20210910000328js_/https://www.cdph.ca.gov/_catalogs/masterpage/KIEFER/js/cdph.custom.js"></script>
        <script src="/web/20210910000328js_/https://www.cdph.ca.gov/_catalogs/masterpage/KIEFER/js/cdph.general.api.js"></script>
        <script src="/web/20210910000328js_/https://www.cdph.ca.gov/_catalogs/masterpage/KIEFER/js/cdph.general.js"></script>
        <noscript></noscript>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
        <script>!function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https'; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.async = true; js.src = p + "://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); } }(document, "script", "twitter-wjs");</script>
        <noscript></noscript>
        <script>
            function ProcessImn() { }
            function ProcessImnMarkers() { }
            function FixWorkspace() {
                try {
                    // if you are using a header that is affixed to the top (i.e. SharePoint Ribbon) put the ID or class here to change the workspace height accordingly.
                    var header = '#suiteBar';
                    var width = $(window).width();
                    var height;
                    if ($(header).length) {
                        height = $(window).height() - $(header).height();
                    } else {
                        height = $(window).height();
                    }
                    $('#s4-workspace').width(width).height(height);
                }
                catch (err) { }
            }

            $(document).ready(function () {
                FixWorkspace();
            });

            $(window).resize(function () {
                FixWorkspace();
            });



            function searchResultMobile() {
                if ($('#txtSearchBoxMobile').val() != "") {
                    document.location.href = document.location.protocol + "//" + document.location.host + "/Pages/results.aspx?k=#k=" + $('#txtSearchBoxMobile').val();
                }
            }

            //Search on Key press Enter
            function handleKeyPressMobile(e) {
                var key = e.keyCode || e.which;
                if (key == 13) {
                    searchResultMobile();
                }
            }

            function searchResult() {
                if ($('.txtsearchbox').val() != "") {
                    document.location.href = document.location.protocol + "//" + document.location.host + "/Pages/results.aspx?k=#k=" + $('.txtsearchbox').val();
                }
            }

            //Search on Key press Enter
            function handleKeyPress(e) {
                var key = e.keyCode || e.which;
                if (key == 13) {
                    searchResult();
                }
            }

        </script>
        <noscript></noscript>
        <style type="text/css" media="print">
            @page {
                size: auto;
                /* auto is the initial value */
            }
        </style>
    <meta name="twitter:title" content="Questions and Answers: Additional COVID-19 Vaccine Doses for People Whose Immune Systems are Compromised"><meta name="twitter:site" content="California Department of Public Helath"><meta name="og:title" content="California Department of Public Health"><meta name="og:image" content="https://web.archive.org/web/20210910000328im_/https://www.cdph.ca.gov/Programs/CID/DCDC/Style%20Library/CDPH/images/FB-CDPH-Logo.png"><meta name="og:url" content="https://web.archive.org/web/20210910000328im_/https://www.cdph.ca.gov/Programs/CID/DCDC"><meta name="fb:app_id" content="848054818617317"></head>

    <body onhashchange="if (typeof(_spBodyOnHashChange) != 'undefined') _spBodyOnHashChange();">
        
                <!--<script type="text/javascript" src="/_layouts/_CDPH-Common/scripts/webtrends.load.cdph.js">
                </script>-->
            

        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        
        <!--<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57101faeb673d51d"></script>-->
        <noscript>This site requires javascript. Please enable javascript on your browser to view this site.</noscript>
        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id; js.async = true;
                js.src = "//web.archive.org/web/20210910000328/https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6&appId=848054818617317";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
        <noscript></noscript>

        

        <form method="post" action="./ThirdVaccineDoseQandA.aspx" onsubmit="javascript:return WebForm_OnSubmit();" id="aspnetForm">
<div class="aspNetHidden">
<input type="hidden" name="_wpcmWpid" id="_wpcmWpid" value=""/>
<input type="hidden" name="wpcmVal" id="wpcmVal" value=""/>
<input type="hidden" name="MSOWebPartPage_PostbackSource" id="MSOWebPartPage_PostbackSource" value=""/>
<input type="hidden" name="MSOTlPn_SelectedWpId" id="MSOTlPn_SelectedWpId" value=""/>
<input type="hidden" name="MSOTlPn_View" id="MSOTlPn_View" value="0"/>
<input type="hidden" name="MSOTlPn_ShowSettings" id="MSOTlPn_ShowSettings" value="False"/>
<input type="hidden" name="MSOGallery_SelectedLibrary" id="MSOGallery_SelectedLibrary" value=""/>
<input type="hidden" name="MSOGallery_FilterString" id="MSOGallery_FilterString" value=""/>
<input type="hidden" name="MSOTlPn_Button" id="MSOTlPn_Button" value="none"/>
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value=""/>
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value=""/>
<input type="hidden" name="__REQUESTDIGEST" id="__REQUESTDIGEST" value="noDigest"/>
<input type="hidden" name="MSOSPWebPartManager_DisplayModeName" id="MSOSPWebPartManager_DisplayModeName" value="Browse"/>
<input type="hidden" name="MSOSPWebPartManager_ExitingDesignMode" id="MSOSPWebPartManager_ExitingDesignMode" value="false"/>
<input type="hidden" name="MSOWebPartPage_Shared" id="MSOWebPartPage_Shared" value=""/>
<input type="hidden" name="MSOLayout_LayoutChanges" id="MSOLayout_LayoutChanges" value=""/>
<input type="hidden" name="MSOLayout_InDesignMode" id="MSOLayout_InDesignMode" value=""/>
<input type="hidden" name="_wpSelected" id="_wpSelected" value=""/>
<input type="hidden" name="_wzSelected" id="_wzSelected" value=""/>
<input type="hidden" name="MSOSPWebPartManager_OldDisplayModeName" id="MSOSPWebPartManager_OldDisplayModeName" value="Browse"/>
<input type="hidden" name="MSOSPWebPartManager_StartWebPartEditingName" id="MSOSPWebPartManager_StartWebPartEditingName" value="false"/>
<input type="hidden" name="MSOSPWebPartManager_EndWebPartEditing" id="MSOSPWebPartManager_EndWebPartEditing" value="false"/>
<input type="hidden" name="__LASTFOCUS" id="__LASTFOCUS" value=""/>
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUBMA9kFgJmD2QWAgIBD2QWBAIBD2QWBGYPZBYCAgMPZBYCZg9kFgJmDzwrAAYAZAIID2QWAmYPZBYCAgEPFgIeE1ByZXZpb3VzQ29udHJvbE1vZGULKYgBTWljcm9zb2Z0LlNoYXJlUG9pbnQuV2ViQ29udHJvbHMuU1BDb250cm9sTW9kZSwgTWljcm9zb2Z0LlNoYXJlUG9pbnQsIFZlcnNpb249MTUuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49NzFlOWJjZTExMWU5NDI5YwFkAgcPZBYMAgUPZBYCAgUPZBYCAgMPFgIeB1Zpc2libGVoFgJmD2QWBAICD2QWBgIBDxYCHwFoZAIDDxYCHwFoZAIFDxYCHwFoZAIDDw8WAh4JQWNjZXNzS2V5BQEvZGQCCw9kFgJmD2QWAmYPFgIeCWlubmVyaHRtbAX3cDx1bCBjbGFzcz0nbmF2IG5hdmJhci1uYXYgJz48bGkgY2xhc3M9J2Ryb3Bkb3duIG1lZ2EtZHJvcGRvd24gbWVudUl0ZW0xJz48YSBocmVmPSdqYXZhc2NyaXB0OnZvaWQoKTsnIGNsYXNzPSdkcm9wZG93bi10b2dnbGUnPkkgYW0gbG9va2luZyBmb3I8c3BhbiBjbGFzcz0nZmEgZmEtYW5nbGUtZG93bic+PC9zcGFuPjwvYT48dWwgY2xhc3M9J2Ryb3Bkb3duLW1lbnUgbWVnYS1kcm9wZG93bi1tZW51IHJvdyBtZW51Qm9yZGVyMSc+PGxpIGNsYXNzPSdjb2wtc20tNCc+PHVsPjxsaSBjbGFzcz0nZHJvcGRvd24taGVhZGVyJz48aDE+Q09WSUQtMTk8L2gxPjwvbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9DSUQvRENEQy9QYWdlcy9JbW11bml6YXRpb24vbmNvdjIwMTkuYXNweCcgPkxhdGVzdCBVcGRhdGVzIO+8hiBJbmZvcm1hdGlvbjwvYT48L2gyPjwvIGxpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvQ0lEL0RDREMvUGFnZXMvR3VpZGFuY2UuYXNweCcgPkd1aWRhbmNlIERvY3VtZW50czwvYT48L2gyPjwvIGxpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvQ0lEL0RDREMvUGFnZXMvQ09WSUQtMTkvQ292aWREYXRhQW5kVG9vbHMuYXNweCcgPkNPVklELTE5IERhdGEg77yGIFRvb2xzPC9hPjwvaDI+PC8gbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9DSUQvRENEQy9QYWdlcy9DT1ZJRC0xOS9Qcm90ZWN0QW5kUHJldmVudC5hc3B4JyA+UHJvdGVjdCDvvIYgUHJldmVudDwvYT48L2gyPjwvIGxpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvQ0lEL0RDREMvUGFnZXMvQ09WSUQtMTkvVGVzdGluZy5hc3B4JyA+VGVzdGluZzwvYT48L2gyPjwvIGxpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvQ0lEL0RDREMvUGFnZXMvQ09WSUQtMTktQ29udGFjdC1UcmFjaW5nLmFzcHgnID5Db250YWN0IFRyYWNpbmc8L2E+PC9oMj48LyBsaT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL0NJRC9EQ0RDL1BhZ2VzL0NPVklELTE5L0NvdmlkMTlWYWNjaW5lcy5hc3B4JyA+Q09WSUQtMTkgVmFjY2luZXM8L2E+PC9oMj48LyBsaT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL0NJRC9EQ0RDL1BhZ2VzL0NPVklELTE5L0NPVklEMTlDb3VudHlNb25pdG9yaW5nT3ZlcnZpZXcuYXNweCcgPkJsdWVwcmludCBmb3IgYSBTYWZlciBFY29ub215PC9hPjwvaDI+PC8gbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9DSUQvRENEQy9QYWdlcy9DT1ZJRC0xOS9IZWFsdGhDYXJlQW5kVGVzdGluZy5hc3B4JyA+SGVhbHRoIENhcmUg77yGIFRlc3Rpbmc8L2E+PC9oMj48LyBsaT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL0NJRC9EQ0RDL1BhZ2VzL0NPVklELTE5L0hvbWVBbmRDb21tdW5pdHkuYXNweCcgPkhvbWUg77yGIENvbW11bml0eTwvYT48L2gyPjwvIGxpPjwvdWw+PC9saT48bGkgY2xhc3M9J2NvbC1zbS00Jz48dWw+PGxpIGNsYXNzPSdkcm9wZG93bi1oZWFkZXInPjxoMT5EaXNlYXNlcyBhbmQgQ29uZGl0aW9uczwvaDE+PC9saT48bGk+PGgyPjxhIGhyZWYgPSAnL1BhZ2VzL2FsbERpc2Vhc2VzLmFzcHgnID5EaXNlYXNlcyBhbmQgQ29uZGl0aW9uczwvYT48L2gyPjwvIGxpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvQ0lEL0RPQS9QYWdlcy9PQW1haW4uYXNweCcgPkhJVi9BSURTPC9hPjwvaDI+PC8gbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9DSENRL0hBSS9QYWdlcy9IQUlfRGVmaW5pdGlvbnNBbmRJbmZvcm1hdGlvbi5hc3B4JyA+SGVhbHRoY2FyZS1Bc3NvY2lhdGVkIEluZmVjdGlvbnM8L2E+PC9oMj48LyBsaT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL0NDRFBIUC9EQ0RJQy9DRFNSQi9QYWdlcy9Qcm9ncmFtLUxhbmRpbmcyLmFzcHgnID5DYW5jZXI8L2E+PC9oMj48LyBsaT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL0NJRC9EQ0RDL1BhZ2VzL0RDREMuYXNweCcgPkNvbW11bmljYWJsZSBEaXNlYXNlczwvYT48L2gyPjwvIGxpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvQ0NEUEhQL0RDRElDL0NEQ0IvUGFnZXMvRGlhYmV0ZXNQcmV2ZW50aW9uLmFzcHgnID5EaWFiZXRlczwvYT48L2gyPjwvIGxpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvQ0lEL0RDREMvUGFnZXMvRENEQ0luZm9ybWF0aW9uZm9yTG9jYWxIZWFsdGhEZXBhcnRtZW50cy5hc3B4JyA+RGlzZWFzZSBSZXBvcnRpbmc8L2E+PC9oMj48LyBsaT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL0NIQ1EvSEFJL1BhZ2VzL01SU0FNZXRoaWNpbGxpbi1SZXNpc3RhbnRTdGFwaHlsb2NvY2N1c2F1cmV1cy5hc3B4JyA+TWV0aGljaWxsaW4tUmVzaXN0YW50IFN0YXBoeWxvY29jY3VzIEF1cmV1cyBCbG9vZHN0cmVhbSBJbmZlY3Rpb248L2E+PC9oMj48LyBsaT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL09QRy9QYWdlcy9vcGctbGFuZGluZy5hc3B4JyA+UHJvYmxlbSBHYW1ibGluZzwvYT48L2gyPjwvIGxpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvQ0lEL0RDREMvUGFnZXMvU1RELmFzcHgnID5TZXh1YWxseSBUcmFuc21pdHRlZCBEaXNlYXNlczwvYT48L2gyPjwvIGxpPjxsaT48YSBocmVmPScvcGFnZXMvYWxsRGlzZWFzZXMuYXNweCcgY2xhc3M9J2J0biBidG4tc2VlbW9yZSc+U2VlIE1vcmU8L2E+PC9saT48L3VsPjwvbGk+PGxpIGNsYXNzPSdjb2wtc20tNCc+PHVsPjxsaSBjbGFzcz0nZHJvcGRvd24taGVhZGVyJz48aDE+TGljZW5zaW5nLCBDZXJ0aWZpY2F0aW9uIGFuZCBPdGhlciBDcmVkZW50aWFsczwvaDE+PC9saT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL0NFSC9QYWdlcy9DTFBSLmFzcHgnID5DZXJ0aWZpY2F0ZXMsIExpY2Vuc2VzLCBQZXJtaXRzIGFuZCBSZWdpc3RyYXRpb25zPC9hPjwvaDI+PC8gbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9DSENRL0xDUC9QYWdlcy9IZWFsdGhDYXJlRmFjaWxpdGllcy5hc3B4JyA+SGVhbHRoIENhcmUgRmFjaWxpdHkgTGljZW5zZXM8L2E+PC9oMj48LyBsaT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL0NIQ1EvTENQL1BhZ2VzL0hlYWx0aENhcmVQcm9mZXNzaW9uYWxzLmFzcHgnID5IZWFsdGggQ2FyZSBQcm9mZXNzaW9uYWxzPC9hPjwvaDI+PC8gbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9PU1BITEQvUGFnZXMvSG9tZS5hc3B4JyA+TGFib3JhdG9yeTwvYT48L2gyPjwvIGxpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvQ0VIL0RSU0VNL1BhZ2VzL0VNQi9NZWRpY2FsV2FzdGUvTWVkaWNhbFdhc3RlLmFzcHgnID5NZWRpY2FsIFdhc3RlPC9hPjwvaDI+PC8gbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9DSUQvRENEQy9QYWdlcy9QZXREb2dhbmRDYXRJbXBvcnRhdGlvbmFuZEV4cG9ydGF0aW9uLmFzcHgnID5QZXQgSW1wb3J0YXRpb24gYW5kIEV4cG9ydGF0aW9uPC9hPjwvaDI+PC8gbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9DRUgvRFJTRU0vUGFnZXMvUkhCLmFzcHgnID5SYWRpYXRpb24gYW5kIE51Y2xlYXI8L2E+PC9oMj48LyBsaT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL0NFSC9EUlNFTS9QYWdlcy9FTUIvUkVIUy9SRUhTLmFzcHgnID5SZWdpc3RlcmVkIEVudmlyb25tZW50YWwgSGVhbHRoIFNwZWNpYWxpc3Q8L2E+PC9oMj48LyBsaT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL0NIU0kvUGFnZXMvVml0YWwtUmVjb3Jkcy5hc3B4JyA+Vml0YWwgUmVjb3JkczwvYT48L2gyPjwvIGxpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvQ0lEL0RDREMvUGFnZXMvV2lsZEFuaW1hbEltcG9ydGF0aW9uLmFzcHgnID5XaWxkIEFuaW1hbCBJbXBvcnRhdGlvbjwvYT48L2gyPjwvIGxpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvQ0VIL1BhZ2VzL0NMUFIuYXNweCcgPk1lZGljYWwgUHJvZmVzc2lvbmFsIExpY2Vuc2VzPC9hPjwvaDI+PC8gbGk+PC91bD48L2xpPjxsaSBjbGFzcz0nY29sLXNtLTQnPjx1bD48bGkgY2xhc3M9J2Ryb3Bkb3duLWhlYWRlcic+PGgxPkZhbWlseSBIZWFsdGg8L2gxPjwvbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9DRkgvRE1DQUgvQnJlYXN0ZmVlZGluZy9QYWdlcy9HZW5lcmFsLmFzcHgnID5CcmVhc3RmZWVkaW5nPC9hPjwvaDI+PC8gbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9DRkgvREdEUy9QYWdlcy9kZWZhdWx0LmFzcHgnID5HZW5ldGljIERpc2Vhc2UgU2NyZWVuaW5nPC9hPjwvaDI+PC8gbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9PSEUvUGFnZXMvT2ZmaWNlSGVhbHRoRXF1aXR5LmFzcHgnID5NZW50YWwgSGVhbHRoPC9hPjwvaDI+PC8gbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9DRkgvRE1DQUgvTlVQQS9QYWdlcy9kZWZhdWx0LmFzcHgnID5OdXRyaXRpb24gYW5kIFBoeXNpY2FsIEFjdGl2aXR5PC9hPjwvaDI+PC8gbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9DRkgvRE1DQUgvUGFnZXMvVG9waWNzL1ByZWduYW5jeS1hbmQtUmVwcm9kdWN0aXZlLUhlYWx0aC5hc3B4JyA+UHJlZ25hbmN5IGFuZCBSZXByb2R1Y3RpdmUgSGVhbHRoPC9hPjwvaDI+PC8gbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9DRkgvRFdJQ1NOL1BhZ2VzL1Byb2dyYW0tTGFuZGluZzEuYXNweCcgPldvbWVuLCBJbmZhbnRzLCBhbmQgQ2hpbGRyZW48L2E+PC9oMj48LyBsaT48L3VsPjwvbGk+PGxpIGNsYXNzPSdjb2wtc20tNCc+PHVsPjxsaSBjbGFzcz0nZHJvcGRvd24taGVhZGVyJz48aDE+UGVyc29uYWwgSGVhbHRoIGFuZCBQcmV2ZW50aW9uPC9oMT48L2xpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvRE8vbGV0c3RhbGtjYW5uYWJpcy9QYWdlcy9MZXRzVGFsa0Nhbm5hYmlzLmFzcHgnID5DYW5uYWJpcyAoTWFyaWp1YW5hKTwvYT48L2gyPjwvIGxpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvQ0lEL0RDREMvUGFnZXMvaW1tdW5pemUuYXNweCcgPkltbXVuaXphdGlvbnM8L2E+PC9oMj48LyBsaT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL0NDRFBIUC9EQ0RJQy9ORU9QQi9QYWdlcy9OdXRyaXRpb25fRWR1Y2F0aW9uX09iZXNpdHlfUHJldmVudGlvbl9CcmFuY2guYXNweCcgPk51dHJpdGlvbjwvYT48L2gyPjwvIGxpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvQ0NEUEhQL0RDRElDL0NUQ0IvUGFnZXMvQ2FsaWZvcm5pYVRvYmFjY29Db250cm9sQnJhbmNoLmFzcHgnID5RdWl0IFNtb2tpbmc8L2E+PC9oMj48LyBsaT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL0NJRC9EQ0RDL1BhZ2VzL1NURC5hc3B4JyA+U2V4dWFsbHkgVHJhbnNtaXR0ZWQgRGlzZWFzZSBUZXN0aW5nPC9hPjwvaDI+PC8gbGk+PC91bD48L2xpPjxsaSBjbGFzcz0nY29sLXNtLTQnPjx1bD48bGkgY2xhc3M9J2Ryb3Bkb3duLWhlYWRlcic+PGgxPkhlYWx0aCBhbmQgU2FmZXR5PC9oMT48L2xpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvT0hFL1BhZ2VzL0NDSEVQLmFzcHgnID5DbGltYXRlIENoYW5nZSBhbmQgSGVhbHRoPC9hPjwvaDI+PC8gbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9FUE8vUGFnZXMvUHJvZ3JhbS1MYW5kaW5nMS5hc3B4JyA+RW1lcmdlbmN5IFByZXBhcmVkbmVzczwvYT48L2gyPjwvIGxpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvQ0NEUEhQL0RFT0RDL1BhZ2VzL0Vudmlyb25tZW50YWwtSGVhbHRoLVRvcGljcy5hc3B4JyA+RW52aXJvbm1lbnRhbCBIZWFsdGg8L2E+PC9oMj48LyBsaT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL09IRS9QYWdlcy9ISUFQLmFzcHgnID5IZWFsdGggaW4gYWxsIFBvbGljaWVzPC9hPjwvaDI+PC8gbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9DQ0RQSFAvRENESUMvQ0RDQi9QYWdlcy9XSVNFV09NQU4uYXNweCcgPkhlYWx0aCBTY3JlZW5pbmcgTG9jYXRpb25zPC9hPjwvaDI+PC8gbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9DSUQvRENEQy9QYWdlcy9Nb3NxdWl0b0Rpc2Vhc2VTdXJ2ZWlsbGFuY2VhbmRDb250cm9sUmVzb3VyY2VzLmFzcHgnID5Nb3NxdWl0byBEaXNlYXNlIFN1cnZlaWxsYW5jZSBhbmQgQ29udHJvbDwvYT48L2gyPjwvIGxpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvQ0NEUEhQL0RFT0RDL09IQi9QYWdlcy9PSEIuYXNweCcgPldvcmtwbGFjZSBIZWFsdGg8L2E+PC9oMj48LyBsaT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL0NIU0kvUGFnZXMvVml0YWwtUmVjb3Jkcy5hc3B4JyA+Vml0YWwgUmVjb3JkczwvYT48L2gyPjwvIGxpPjwvdWw+PC9saT48bGkgY2xhc3M9J2NvbC1zbS00Jz48dWw+PGxpIGNsYXNzPSdkcm9wZG93bi1oZWFkZXInPjxoMT5IZWFsdGggRmFjaWxpdGllczwvaDE+PC9saT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL0NIQ1EvTENQL0NhbEhlYWx0aEZpbmQvUGFnZXMvQ29tcGxhaW50LmFzcHgnID5GaWxlIGEgQ29tcGxhaW50PC9hPjwvaDI+PC8gbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9DSENRL0xDUC9jYWxoZWFsdGhmaW5kL3BhZ2VzL2hvbWUuYXNweCcgPkNhbGlmb3JuaWEgSGVhbHRoIEZhY2lsaXRpZXMgSW5mb3JtYXRpb24gRGF0YWJhc2U8L2E+PC9oMj48LyBsaT48L3VsPjwvbGk+PGxpIGNsYXNzPSdjb2wtc20tNCc+PHVsPjxsaSBjbGFzcz0nZHJvcGRvd24taGVhZGVyJz48aDE+QWRtaW5pc3RyYXRpdmU8L2gxPjwvbGk+PGxpPjxoMj48YSBocmVmID0gJy9QYWdlcy9Cb2FyZHNBbmRBZHZpc29yeUNvbW1pdHRlZXMuYXNweCcgPkJvYXJkcyBhbmQgQWR2aXNvcnkgQ29tbWl0dGVlczwvYT48L2gyPjwvIGxpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvUFNCL1BhZ2VzL0Zvcm1zLmFzcHgnID5Gb3JtczwvYT48L2gyPjwvIGxpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvSFJCL1BhZ2VzL0h1bWFuUmVzb3VyY2VzRGl2aXNpb24uYXNweCcgPkpvYnMgYW5kIENhcmVlcnM8L2E+PC9oMj48LyBsaT48bGk+PGgyPjxhIGhyZWYgPSAnL1BhZ2VzL0xvY2FsSGVhbHRoU2VydmljZXNBbmRPZmZpY2VzLmFzcHgnID5Mb2NhbCBIZWFsdGggU2VydmljZXMvT2ZmaWNlczwvYT48L2gyPjwvIGxpPjxsaT48aDI+PGEgaHJlZiA9ICcvUGFnZXMvUHVibGljLVJlY29yZHMtQWN0LVJlcXVlc3QuYXNweCcgPlB1YmxpYyBSZWNvcmRzIEFjdCBSZXF1ZXN0PC9hPjwvaDI+PC8gbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9PTFMvUGFnZXMvT2ZmaWNlLW9mLVJlZ3VsYXRpb25zLmFzcHgnID5Qcm9wb3NlZCBSZWd1bGF0aW9uczwvYT48L2gyPjwvIGxpPjwvdWw+PC9saT48bGkgY2xhc3M9J2NvbC1zbS00Jz48dWw+PGxpIGNsYXNzPSdkcm9wZG93bi1oZWFkZXInPjxoMT5CcmllZmluZ3M8L2gxPjwvbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9PUEEvUGFnZXMvU3Rha2Vob2xkZXIuYXNweCcgPlN0YWtlaG9sZGVyIEJyaWVmPC9hPjwvaDI+PC8gbGk+PC91bD48L2xpPjwvdWw+PC9saT48L3VsPjx1bCBjbGFzcz0nbmF2IG5hdmJhci1uYXYnPjxsaSBjbGFzcz0nZHJvcGRvd24gbWVnYS1kcm9wZG93biBtZW51SXRlbTInPjxhIGhyZWY9J2phdmFzY3JpcHQ6dm9pZCgpOycgY2xhc3M9J2Ryb3Bkb3duLXRvZ2dsZSc+SSBhbSBhPHNwYW4gY2xhc3M9J2ZhIGZhLWFuZ2xlLWRvd24nPjwvc3Bhbj48L2E+PHVsIGNsYXNzPSdkcm9wZG93bi1tZW51IG1lZ2EtZHJvcGRvd24tbWVudSByb3cgbWVudUJvcmRlcjIgc2Vjb25kbWVudSc+PGxpIGNsYXNzPSdjb2wtc20tMTInPjx1bD48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL0NIQ1EvTENQL1BhZ2VzL0NEUEgtQ2xpbmljaWFucy1hbmQtUHJvdmlkZXJzLmFzcHgnID5DbGluaWNpYW4gLyBIZWFsdGhjYXJlIFByb3ZpZGVyPC9hPjwvaDI+PC9saT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL09MR0EvUGFnZXMvTGVnaXNsYXRpdmVBbmRHb3Zlcm5tZW50YWxBZmZhaXJzLmFzcHgnID5MZWdpc2xhdG9yIC8gTGVnaXNsYXRpdmUgU3RhZmY8L2E+PC9oMj48L2xpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvT1BBL1BhZ2VzL09mZmljZS1vZi1QdWJsaWMtQWZmYWlycy5hc3B4JyA+TWVkaWEgUmVwcmVzZW50YXRpdmU8L2E+PC9oMj48L2xpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvQ0hTSS9QYWdlcy9SZXNlYXJjaGVycy1hbmQtU3RhdGlzdGljaWFucy5hc3B4JyA+UmVzZWFyY2hlciAvIFN0YXRpc3RpY2lhbjwvYT48L2gyPjwvbGk+PGxpIGNsYXNzPSdkcm9wZG93bi1oZWFkZXInPjxoMT5QZXJzb24gSW50ZXJlc3RlZCBJbjwvaDE+PC9saT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL0NGSC9ETUNBSC9QYWdlcy9BdWRpZW5jZXMvSW5mYW50LWFuZC1DaGlsZHJlbi5hc3B4JyA+SW5mYW50IGFuZCBDaGlsZCBIZWFsdGg8L2E+PC9oMj48LyBsaT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL09IRS9QYWdlcy9NZW5zX0xhbmRpbmdfUGFnZS5hc3B4JyA+TWVu4oCZcyBIZWFsdGg8L2E+PC9oMj48LyBsaT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL0NDRFBIUC9ERU9EQy9QYWdlcy9TZW5pb3ItSGVhbHRoLmFzcHgnID5TZW5pb3IgSGVhbHRoPC9hPjwvaDI+PC8gbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9PSEUvUGFnZXMvV29tZW5zX0xhbmRpbmdfUGFnZS5hc3B4JyA+V29tZW7igJlzIEhlYWx0aDwvYT48L2gyPjwvIGxpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvQ0ZIL0RNQ0FIL1BhZ2VzL0F1ZGllbmNlcy9Zb3V0aC1hbmQtWW91bmctQWR1bHRzLmFzcHgnID5Zb3V0aCBhbmQgWW91bmcgQWR1bHQncyBIZWFsdGg8L2E+PC9oMj48LyBsaT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL0NJRC9ET0EvUGFnZXMvTEdCVF9IZWFsdGguYXNweCcgPkxHQlQgSGVhbHRoPC9hPjwvaDI+PC8gbGk+PC91bD48L2xpPjwvdWw+PC9saT48L3VsPjx1bCBjbGFzcz0nbmF2IG5hdmJhci1uYXYnPjxsaSBjbGFzcz0nZHJvcGRvd24gbWVnYS1kcm9wZG93biBtZW51SXRlbTMnPjxhIGhyZWY9J2phdmFzY3JpcHQ6dm9pZCgpOycgY2xhc3M9J2Ryb3Bkb3duLXRvZ2dsZSc+UHJvZ3JhbXM8c3BhbiBjbGFzcz0nZmEgZmEtYW5nbGUtZG93bic+PC9zcGFuPjwvYT48dWwgY2xhc3M9J2Ryb3Bkb3duLW1lbnUgbWVnYS1kcm9wZG93bi1tZW51IHJvdyBtZW51Qm9yZGVyMyc+PGxpIGNsYXNzPSdjb2wtc20tNCc+PHVsPjxsaSBjbGFzcz0nZHJvcGRvd24taGVhZGVyJz48aDE+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvQ0NEUEhQL1BhZ2VzL1Byb2dyYW0tTGFuZGluZzEuYXNweCcgPkNlbnRlciBmb3IgSGVhbHRoeSBDb21tdW5pdGllczwvYT48L2gxPjwvbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9DQ0RQSFAvRENESUMvQ1RDQi9QYWdlcy9DYWxpZm9ybmlhVG9iYWNjb0NvbnRyb2xCcmFuY2guYXNweCcgPkNhbGlmb3JuaWEgVG9iYWNjbyBDb250cm9sPC9hPjwvaDI+PC8gbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9DQ0RQSFAvREVPREMvQ0xQUEIvUGFnZXMvQ0xQUEJob21lLmFzcHgnID5DaGlsZGhvb2QgTGVhZCBQb2lzb25pbmcgUHJldmVudGlvbjwvYT48L2gyPjwvIGxpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvQ0NEUEhQL0RDRElDL0NEQ0IvUGFnZXMvQ2hyb25pY0Rpc2Vhc2VDb250cm9sQnJhbmNoLmFzcHgnID5DaHJvbmljIERpc2Vhc2UgQ29udHJvbDwvYT48L2gyPjwvIGxpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvQ0NEUEhQL0RDRElDL0NEU1JCL1BhZ2VzL1Byb2dyYW0tTGFuZGluZzIuYXNweCcgPkNocm9uaWMgRGlzZWFzZSBTdXJ2ZWlsbGFuY2UgYW5kIFJlc2VhcmNoPC9hPjwvaDI+PC8gbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9DQ0RQSFAvREVPREMvUGFnZXMvRW1lcmdlbmN5LVByZXBhcmVkbmVzcy1UZWFtLmFzcHgnID5FbWVyZ2VuY3kgUHJlcGFyZWRuZXNzPC9hPjwvaDI+PC8gbGk+PGxpPjxhIGhyZWY9Jy9wYWdlcy9DZW50ZXJEZXRhaWxzLmFzcHg/Y2VudGVyPUl4M1RyS0prTUFwekFmRE9idiUyRjBqOU9PeFk2NGIzVDZwUXQlMkI0ZThPM1Y0JTNEJyBjbGFzcz0nYnRuIGJ0bi1zZWVtb3JlJz5TZWUgTW9yZTwvYT48L2xpPjwvdWw+PC9saT48bGkgY2xhc3M9J2NvbC1zbS00Jz48dWw+PGxpIGNsYXNzPSdkcm9wZG93bi1oZWFkZXInPjxoMT48YSBocmVmID0gJy9Qcm9ncmFtcy9DRUgvUGFnZXMvQ0VILmFzcHgnID5DZW50ZXIgZm9yIEVudmlyb25tZW50YWwgSGVhbHRoPC9hPjwvaDE+PC9saT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL0NFSC9ERkRDUy9QYWdlcy9ERkRDUy5hc3B4JyA+RGl2aXNpb24gb2YgRm9vZCBhbmQgRHJ1ZyBTYWZldHk8L2E+PC9oMj48LyBsaT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL0NFSC9EUlNFTS9QYWdlcy9EUlNFTS5hc3B4JyA+RGl2aXNpb24gb2YgUmFkaWF0aW9uIFNhZmV0eSBhbmQgRW52aXJvbm1lbnRhbCBNYW5hZ2VtZW50PC9hPjwvaDI+PC8gbGk+PC91bD48L2xpPjxsaSBjbGFzcz0nY29sLXNtLTQnPjx1bD48bGkgY2xhc3M9J2Ryb3Bkb3duLWhlYWRlcic+PGgxPjxhIGhyZWYgPSAnL1Byb2dyYW1zL0NGSC9QYWdlcy9kZWZhdWx0LmFzcHgnID5DZW50ZXIgZm9yIEZhbWlseSBIZWFsdGg8L2E+PC9oMT48L2xpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvQ0ZIL0RHRFMvUGFnZXMvZGVmYXVsdC5hc3B4JyA+R2VuZXRpYyBEaXNlYXNlIFNjcmVlbmluZyBQcm9ncmFtPC9hPjwvaDI+PC8gbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9DRkgvRE1DQUgvUGFnZXMvZGVmYXVsdC5hc3B4JyA+TWF0ZXJuYWwsIENoaWxkLCBhbmQgQWRvbGVzY2VudCBIZWFsdGg8L2E+PC9oMj48LyBsaT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL0NGSC9EV0lDU04vUGFnZXMvUHJvZ3JhbS1MYW5kaW5nMS5hc3B4JyA+V29tZW4sIEluZmFudHMsIGFuZCBDaGlsZHJlbjwvYT48L2gyPjwvIGxpPjwvdWw+PC9saT48bGkgY2xhc3M9J2NvbC1zbS00Jz48dWw+PGxpIGNsYXNzPSdkcm9wZG93bi1oZWFkZXInPjxoMT48YSBocmVmID0gJy9Qcm9ncmFtcy9DSENRL1BhZ2VzL0NIQ1FIb21lLmFzcHgnID5DZW50ZXIgZm9yIEhlYWx0aCBDYXJlIFF1YWxpdHk8L2E+PC9oMT48L2xpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvQ0hDUS9IQUkvUGFnZXMvSEFJUHJvZ3JhbUhvbWUuYXNweCcgPkhlYWx0aGNhcmUtQXNzb2NpYXRlZCBJbmZlY3Rpb25zIFByb2dyYW08L2E+PC9oMj48LyBsaT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL0NIQ1EvTENQL1BhZ2VzL0xhbmRDUHJvZ3JhbUhvbWUuYXNweCcgPkxpY2Vuc2luZyBhbmQgQ2VydGlmaWNhdGlvbjwvYT48L2gyPjwvIGxpPjwvdWw+PC9saT48bGkgY2xhc3M9J2NvbC1zbS00Jz48dWw+PGxpIGNsYXNzPSdkcm9wZG93bi1oZWFkZXInPjxoMT48YSBocmVmID0gJy9Qcm9ncmFtcy9DSFNJL1BhZ2VzL1Byb2dyYW0tTGFuZGluZzEuYXNweCcgPkNlbnRlciBmb3IgSGVhbHRoIFN0YXRpc3RpY3MgYW5kIEluZm9ybWF0aWNzPC9hPjwvaDE+PC9saT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL0NIU0kvUGFnZXMvRW5kLW9mLUxpZmUtT3B0aW9uLUFjdC0uYXNweCcgPkVuZCBvZiBMaWZlIE9wdGlvbiBBY3Q8L2E+PC9oMj48LyBsaT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL0NIU0kvUGFnZXMvTU1JQ1AuYXNweCcgPk1lZGljYWwgTWFyaWp1YW5hIElkZW50aWZpY2F0aW9uIENhcmQgUHJvZ3JhbTwvYT48L2gyPjwvIGxpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvQ0hTSS9QYWdlcy9WaXRhbC1SZWNvcmRzLmFzcHgnID5WaXRhbCBSZWNvcmRzPC9hPjwvaDI+PC8gbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9DSFNJL1BhZ2VzL0RhdGEtYW5kLVN0YXRpc3RpY3MtLmFzcHgnID5WaXRhbCBSZWNvcmRzIERhdGEgYW5kIFN0YXRpc3RpY3M8L2E+PC9oMj48LyBsaT48L3VsPjwvbGk+PGxpIGNsYXNzPSdjb2wtc20tNCc+PHVsPjxsaSBjbGFzcz0nZHJvcGRvd24taGVhZGVyJz48aDE+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvQ0lEL1BhZ2VzL0NJRC5hc3B4JyA+Q2VudGVyIGZvciBJbmZlY3Rpb3VzIERpc2Vhc2VzPC9hPjwvaDE+PC9saT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL0NJRC9ET0EvUGFnZXMvT0FtYWluLmFzcHgnID5ISVYvQUlEUzwvYT48L2gyPjwvIGxpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvQ0lEL09CQkgvUGFnZXMvT0JCSEhvbWUuYXNweCcgPkJpbmF0aW9uYWwgQm9yZGVyIEhlYWx0aDwvYT48L2gyPjwvIGxpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvQ0lEL0RDREMvUGFnZXMvRENEQy5hc3B4JyA+Q29tbXVuaWNhYmxlIERpc2Vhc2UgQ29udHJvbDwvYT48L2gyPjwvIGxpPjxsaT48aDI+PGEgaHJlZiA9ICcvUHJvZ3JhbXMvQ0lEL0RDREMvUGFnZXMvQ0RFUi5hc3B4JyA+Q29tbXVuaWNhYmxlIERpc2Vhc2UgRW1lcmdlbmN5IFJlc3BvbnNlPC9hPjwvaDI+PC8gbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9DSUQvT1JIL1BhZ2VzL0hvbWUuYXNweCcgPlJlZnVnZWUgSGVhbHRoPC9hPjwvaDI+PC8gbGk+PGxpPjxhIGhyZWY9Jy9wYWdlcy9DZW50ZXJEZXRhaWxzLmFzcHg/Y2VudGVyPUl4M1RyS0prTUFwY1ptMmNYM2xidWJra2RBWGRTTWxvR0l6TUFOVERIekUlM0QnIGNsYXNzPSdidG4gYnRuLXNlZW1vcmUnPlNlZSBNb3JlPC9hPjwvbGk+PC91bD48L2xpPjxsaSBjbGFzcz0nY29sLXNtLTQnPjx1bD48bGkgY2xhc3M9J2Ryb3Bkb3duLWhlYWRlcic+PGgxPjxhIGhyZWYgPSAnL1Byb2dyYW1zL0RPL1BhZ2VzL0NEUEggRGlyZWN0b3IgV2VsY29tZSBQYWdlLmFzcHgnID5EaXJlY3RvciAvIFN0YXRlIFB1YmxpYyBIZWFsdGggT2ZmaWNlcjwvYT48L2gxPjwvbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9PTEdBL1BhZ2VzL0xlZ2lzbGF0aXZlQW5kR292ZXJubWVudGFsQWZmYWlycy5hc3B4JyA+TGVnaXNsYXRpdmUgYW5kIEdvdmVybm1lbnRhbCBBZmZhaXJzPC9hPjwvaDI+PC8gbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9ETy9sZXRzdGFsa2Nhbm5hYmlzL1BhZ2VzL0xldHNUYWxrQ2FubmFiaXMuYXNweCcgPkxldCdzIFRhbGsgQ2FubmFiaXM8L2E+PC9oMj48LyBsaT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL09IRS9QYWdlcy9PZmZpY2VIZWFsdGhFcXVpdHkuYXNweCcgPk9mZmljZSBvZiBIZWFsdGggRXF1aXR5PC9hPjwvaDI+PC8gbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9PUEEvUGFnZXMvT2ZmaWNlLW9mLVB1YmxpYy1BZmZhaXJzLmFzcHgnID5PZmZpY2Ugb2YgUHVibGljIEFmZmFpcnM8L2E+PC9oMj48LyBsaT48bGk+PGEgaHJlZj0nL3BhZ2VzL0NlbnRlckRldGFpbHMuYXNweD9jZW50ZXI9NWVoUGJQT3Q4UzZGdnBFM3JmUVNOcVBnVXRSWmZBTXc1UDRIdUNDSG1IdU95Tkl5amRnaXRRJTNEJTNEJyBjbGFzcz0nYnRuIGJ0bi1zZWVtb3JlJz5TZWUgTW9yZTwvYT48L2xpPjwvdWw+PC9saT48bGkgY2xhc3M9J2NvbC1zbS00Jz48dWw+PGxpIGNsYXNzPSdkcm9wZG93bi1oZWFkZXInPjxoMT48YSBocmVmID0gJ2phdmFzY3JpcHQ6dm9pZCgpOycgPk90aGVyIENEUEggT2ZmaWNlczwvYT48L2gxPjwvbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9FUE8vUGFnZXMvUHJvZ3JhbS1MYW5kaW5nMS5hc3B4JyA+RW1lcmdlbmN5IFByZXBhcmVkbmVzcyBPZmZpY2U8L2E+PC9oMj48LyBsaT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL09TUEhMRC9QYWdlcy9Ib21lLmFzcHgnID5PZmZpY2Ugb2YgU3RhdGUgUHVibGljIEhlYWx0aCBMYWJvcmF0b3J5IERpcmVjdG9yPC9hPjwvaDI+PC8gbGk+PGxpPjxoMj48YSBocmVmID0gJy9Qcm9ncmFtcy9PU1BITEQvTEZTL1BhZ2VzL0hvbWUuYXNweCcgPkxhYm9yYXRvcnkgRmllbGQgU2VydmljZXM8L2E+PC9oMj48LyBsaT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL09MUy9QYWdlcy9Qcml2YWN5LU9mZmljZS5hc3B4JyA+UHJpdmFjeSBPZmZpY2U8L2E+PC9oMj48LyBsaT48bGk+PGgyPjxhIGhyZWYgPSAnL1Byb2dyYW1zL09MUy9QYWdlcy9PZmZpY2Utb2YtUmVndWxhdGlvbnMuYXNweCcgPk9mZmljZSBvZiBSZWd1bGF0aW9uczwvYT48L2gyPjwvIGxpPjwvdWw+PC9saT48L3VsPjwvbGk+PC91bD48dWwgY2xhc3M9J25hdiBuYXZiYXItbmF2Jz48bGkgY2xhc3M9J2Ryb3Bkb3duIG1lZ2EtZHJvcGRvd24gbWVudUl0ZW00Jz48YSBocmVmPScvUGFnZXMvQXRvWkluZGV4LmFzcHgnIGNsYXNzPSdkcm9wZG93bi10b2dnbGUnPkEtWiBJbmRleDxzcGFuIGNsYXNzPSdmYSBmYS1hbmdsZS1kb3duJz48L3NwYW4+PC9hPjwvbGk+PC91bD5kAg8PZBYCAgEPZBYEAgEPPCsABQEADxYCHhVQYXJlbnRMZXZlbHNEaXNwbGF5ZWRmZGQCAw8WAh8ACysEAWQCGQ9kFgJmD2QWAmYPFgIfAwWRBDxkaXYgY2xhc3M9J2JyZWFkQ3J1bWJDdXN0b21Db250YWluZXIgbm9pbmRleCc+PHVsIGNsYXNzPSdjdXN0b21CcmVhZGNydW1iJz48bGk+PGEgaHJlZj0naHR0cHM6Ly93d3cuY2RwaC5jYS5nb3YnPkhvbWU8L2E+PC9saT48bGk+PGEgaHJlZj0naHR0cHM6Ly93d3cuY2RwaC5jYS5nb3YvUHJvZ3JhbXMnPlByb2dyYW1zPC9hPjwvbGk+PGxpPjxhIGhyZWY9J2h0dHBzOi8vd3d3LmNkcGguY2EuZ292L1Byb2dyYW1zL0NJRCc+Q2VudGVyIGZvciBJbmZlY3Rpb3VzIERpc2Vhc2VzPC9hPjwvbGk+PGxpPjxhIGhyZWY9J2h0dHBzOi8vd3d3LmNkcGguY2EuZ292L1Byb2dyYW1zL0NJRC9EQ0RDJz5EaXZpc2lvbiBvZiBDb21tdW5pY2FibGUgRGlzZWFzZSBDb250cm9sPC9hPjwvbGk+PGxpIGNsYXNzPSdhY3RpdmVQYWdlJz5RdWVzdGlvbnMgYW5kIEFuc3dlcnM6IEFkZGl0aW9uYWwgQ09WSUQtMTkgVmFjY2luZSBEb3NlcyBmb3IgUGVvcGxlIFdob3NlIEltbXVuZSBTeXN0ZW1zIGFyZSBDb21wcm9taXNlZDwvbGk+PC9kaXY+PC91bD5kAhsPZBYCAgEPZBYCAgMPDxYCHwFoZBYCAgMPZBYCAgMPZBYCAgEPPCsACQEADxYCHg1OZXZlckV4cGFuZGVkZ2RkAh8PZBYCAgIPZBYUAgEPZBYEZg8WAh8BaGQCAQ8WAh8BaGQCAw9kFgRmDxYCHwFoZAIBDxYCHwFoZAIFDxYCHwALKwQBZAIHDxYCHwALKwQBZAIJDxYCHwALKwQBZAILDxYCHwALKwQBFgJmDw9kZGQCDQ8WAh8ACysEAWQCEQ8WAh8ACysEAWQCFQ8WAh8ACysEARYCZg8PZGRkAhcPFgIfAAsrBAEWAmYPD2RkZBgCBS1jdGwwMCRQbGFjZUhvbGRlckxlZnROYXZCYXIkVjRRdWlja0xhdW5jaE1lbnUPD2QFBlJlY2VudGQFLGN0bDAwJFBsYWNlSG9sZGVyVG9wTmF2QmFyJFRvcE5hdmlnYXRpb25NZW51Dw9kBShEaXZpc2lvbiBvZiBDb21tdW5pY2FibGUgRGlzZWFzZSBDb250cm9sZJzd4yWIkwtsk7UWh220Xiv2XTiwk/0YFna4yVeHRiTq"/>
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['aspnetForm'];
if (!theForm) {
    theForm = document.aspnetForm;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/web/20210910000328js_/https://www.cdph.ca.gov/WebResource.axd?d=XC76c8jd_I94f1Z85AecrUsNmjrrSXJk0Q_01WcC59uI9S6DOlDmHANoEbE7_MQZXfX-xLLcx97454booXeHm1z9YN-FQZiHxKBKL20fJJQ1&amp;t=637453780939909757" type="text/javascript"></script>


<script type="text/javascript">
//<![CDATA[
var MSOWebPartPageFormName = 'aspnetForm';
var g_presenceEnabled = true;
var g_wsaEnabled = false;
var g_wsaQoSEnabled = false;
var g_wsaQoSDataPoints = [];
var g_wsaLCID = 1033;
var g_wsaListTemplateId = 850;
var g_wsaSiteTemplateId = 'CMSPUBLISHING#0';
var _fV4UI=true;var _spPageContextInfo = {webServerRelativeUrl: "\u002fPrograms\u002fCID\u002fDCDC", webAbsoluteUrl: "https:\u002f\u002fweb.archive.org\u002fweb\u002f20210910000328\u002fhttps:\u002f\u002fwww.cdph.ca.gov\u002fPrograms\u002fCID\u002fDCDC", siteAbsoluteUrl: "https:\u002f\u002fweb.archive.org\u002fweb\u002f20210910000328\u002fhttps:\u002f\u002fwww.cdph.ca.gov", serverRequestPath: "\u002fPrograms\u002fCID\u002fDCDC\u002fPages\u002fCOVID-19\u002fThirdVaccineDoseQandA.aspx", layoutsUrl: "_layouts\u002f15", webTitle: "Division of Communicable Disease Control", webTemplate: "39", tenantAppVersion: "0", isAppWeb: false, Has2019Era: true, webLogoUrl: "_layouts\u002f15\u002fimages\u002fsiteicon.png", webLanguage: 1033, currentLanguage: 1033, currentUICultureName: "en-US", currentCultureName: "en-US", clientServerTimeDelta: new Date("2021-09-10T00:03:28.1347587Z") - new Date(), siteClientTag: "2156$$15.0.5363.1000", crossDomainPhotosEnabled:false, webUIVersion:15, webPermMasks:{High:16,Low:2147680353},pageListId:"{6aad4e4a-f5c6-484e-8d56-fd7746cd9d60}",pageItemId:1133, pagePersonalizationScope:1, alertsEnabled:true, siteServerRelativeUrl: "\u002f", allowSilverlightPrompt:'True'};document.onreadystatechange=fnRemoveAllStatus; function fnRemoveAllStatus(){removeAllStatus(true)};
function _spNavigateHierarchy(nodeDiv, dataSourceId, dataPath, url, listInContext, type) {

    CoreInvoke('ProcessDefaultNavigateHierarchy', nodeDiv, dataSourceId, dataPath, url, listInContext, type, document.forms.aspnetForm, "", "\u002fPrograms\u002fCID\u002fDCDC\u002fPages\u002fCOVID-19\u002fThirdVaccineDoseQandA.aspx");

}

                            function DoCallBack(filterText)
                            {WebForm_DoCallback('ctl00$PlaceHolderMain$ctl03$ctl00',filterText,UpdateFilterCallback,0,CallBackError,true)
                            }
                            function CallBackError(result, clientsideString)
                            {                
                            }
                        //]]>
</script>

<script src="/web/20210910000328js_/https://www.cdph.ca.gov/_layouts/15/blank.js?rev=ZaOXZEobVwykPO9g8hq%2F8A%3D%3D" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
(function(){

        if (typeof(_spBodyOnLoadFunctions) === 'undefined' || _spBodyOnLoadFunctions === null) {
            return;
        }
        _spBodyOnLoadFunctions.push(function() 
        {
          ExecuteOrDelayUntilScriptLoaded(
            function()
            {
              var pairs = SP.ScriptHelpers.getDocumentQueryPairs();
              var followDoc, itemId, listId, docName;
              for (var key in pairs)
              {
                if(key.toLowerCase() == 'followdocument') 
                  followDoc = pairs[key];
                else if(key.toLowerCase() == 'itemid') 
                  itemId = pairs[key];
                else if(key.toLowerCase() == 'listid') 
                  listId = pairs[key];
                else if(key.toLowerCase() == 'docname') 
                  docName = decodeURI(pairs[key]);
              } 

              if(followDoc != null && followDoc == '1' && listId!=null && itemId != null && docName != null)
              {
                SP.SOD.executeFunc('followingcommon.js', 'FollowDocumentFromEmail', function() 
                { 
                  FollowDocumentFromEmail(itemId, listId, docName);
                });
              }

            }, 'SP.init.js');

        });
    })();(function(){

        if (typeof(_spBodyOnLoadFunctions) === 'undefined' || _spBodyOnLoadFunctions === null) {
            return;
        }
        _spBodyOnLoadFunctions.push(function() {

            if (typeof(SPClientTemplates) === 'undefined' || SPClientTemplates === null || (typeof(APD_InAssetPicker) === 'function' && APD_InAssetPicker())) {
                return;
            }

            var renderFollowFooter = function(renderCtx,  calloutActionMenu)
            {
                if (renderCtx.ListTemplateType == 700) 
                    myDocsActionsMenuPopulator(renderCtx, calloutActionMenu);
                else
                    CalloutOnPostRenderTemplate(renderCtx, calloutActionMenu);

                var listItem = renderCtx.CurrentItem;
                if (typeof(listItem) === 'undefined' || listItem === null) {
                    return;
                }
                if (listItem.FSObjType == 0) {
                    calloutActionMenu.addAction(new CalloutAction({
                        text: Strings.STS.L_CalloutFollowAction,
                        tooltip: Strings.STS.L_CalloutFollowAction_Tooltip,
                        onClickCallback: function (calloutActionClickEvent, calloutAction) {
                            var callout = GetCalloutFromRenderCtx(renderCtx);
                            if (!(typeof(callout) === 'undefined' || callout === null))
                                callout.close();
                            SP.SOD.executeFunc('followingcommon.js', 'FollowSelectedDocument', function() { FollowSelectedDocument(renderCtx); });
                        }
                    }));
                }
            };

            var registerOverride = function(id) {
                var followingOverridePostRenderCtx = {};
                followingOverridePostRenderCtx.BaseViewID = 'Callout';
                followingOverridePostRenderCtx.ListTemplateType = id;
                followingOverridePostRenderCtx.Templates = {};
                followingOverridePostRenderCtx.Templates.Footer = function(renderCtx) {
                    var  renderECB;
                    if (typeof(isSharedWithMeView) === 'undefined' || isSharedWithMeView === null) {
                        renderECB = true;
                    } else {
                        var viewCtx = getViewCtxFromCalloutCtx(renderCtx);
                        renderECB = !isSharedWithMeView(viewCtx);
                    }
                    return CalloutRenderFooterTemplate(renderCtx, renderFollowFooter, renderECB);
                };
                SPClientTemplates.TemplateManager.RegisterTemplateOverrides(followingOverridePostRenderCtx);
            }
            registerOverride(101);
            registerOverride(700);
        });
    })();if (typeof(DeferWebFormInitCallback) == 'function') DeferWebFormInitCallback();function WebForm_OnSubmit() {
UpdateFormDigest('\u002fPrograms\u002fCID\u002fDCDC', 1440000);if (typeof(_spFormOnSubmitWrapper) != 'undefined') {return _spFormOnSubmitWrapper();} else {return true;};
return true;
}
//]]>
</script>

<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="FD99F2D9"/>
</div>
            <span id="DeltaSPWebPartManager">
                
            </span>
            <script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$ScriptManager', 'aspnetForm', [], [], [], 90, 'ctl00');
//]]>
</script>

            
            
            
            <div id="s4-workspace">
                <div id="s4-bodyContainer">
                    <header class="masthead ms-dialogHidden">
                        <a name="Top"></a>
                        <div class="header clearfix noindex">
                            <div class="CALogoOuter hidden-xs">
                                <!--Toprow starts-->
                                <div class="container">
                                    <div class="row ">
                                        <div class="col-sm-1">
                                            <a href="https://web.archive.org/web/20210910000328/http://ca.gov/" target="_blank">
                                                <img src="/web/20210910000328im_/https://www.cdph.ca.gov/Style Library/CDPH/images/cagov-logo.png" width="47" height="34" alt="CA.Gov State of California Logo" class="img-responsive"/>
                                            </a>
                                        </div>
                                        <div class="col-sm-11 text-right">
                                            <span id="topNavSecondaryOriginalPosition">
                                                <span class="topNavSecondary" id="topNavSecondary">
                                                    <a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/OPA/Pages/CDPHespanol.aspx" class="txtGreen">en
                                                        Español</a>
                                                    <a href="/web/20210910000328/https://www.cdph.ca.gov/Pages/contact_us.aspx">Contact Us</a>
                                                    <a href="/web/20210910000328/https://www.cdph.ca.gov/Pages/About.aspx">About</a>
                                                    
                                                    <a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/OPA">News &amp; Media</a>
                                                    <a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/HRB">Jobs/Careers </a>
                                                </span>
                                            </span>
                                            <span id="ddlLanguageActualPosition">
                                                <span class="ddl_LanguageBox" id="ddlLanguage">
                                                    <span class="glyphicon glyphicon-globe">
                                                    </span>
                                                    <div data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="cursorPointer" id="google_translate_element">
                                                    </div>
                                                </span>
                                            </span>
                                            <script type="text/javascript">
                                                function googleTranslateElementInit() {
                                                    new google.translate.TranslateElement({ pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE }, 'google_translate_element');
                                                }
                                            </script>
                                            <noscript></noscript>
                                            
                                            <script type="text/javascript" src="//web.archive.org/web/20210910000328js_/https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                                            <noscript></noscript>
                                            <!-- Google Tag Manager -->

                                            <script>(function (w, d, s, l, i) {
                                                    w[l] = w[l] || []; w[l].push({
                                                        'gtm.start':

                                                            new Date().getTime(), event: 'gtm.js'
                                                    }); var f = d.getElementsByTagName(s)[0],

                                                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =

                                                            'https://web.archive.org/web/20210910000328/https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);

                                                })(window, document, 'script', 'dataLayer', 'GTM-KVN6ZCB');</script>

                                            <!-- End Google Tag Manager -->
                                            <!-- Google Tag Manager (noscript) -->

                                            <noscript>
                                                <iframe title="Google Tag Manager" name="Google Tag Manager" src="https://web.archive.org/web/20210910000328if_/https://www.googletagmanager.com/ns.html?id=GTM-KVN6ZCB" height="0" width="0" style="display:none;visibility:hidden">Google Tag Manager
                                                </iframe>
                                            </noscript>

                                            <!-- End Google Tag Manager (noscript) -->
                                            
                                            <span class="topTextSize">
                                                <a onclick="changefontsize('plus')" class="classplus">+
                                                    <span class="sr-only">Increase
                                                    </span>
                                                </a>
                                                <a onclick="changefontsize('0')" class="classrestore">Text Resize
                                                    <span class="sr-only">Reset to Default
                                                    </span>
                                                </a>
                                                <a onclick="changefontsize('minus')" class="classminus">-
                                                    <span class="sr-only">Decrease
                                                    </span>
                                                </a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Toprow ends-->
                            <div class="CDPHLogoOuter" id="headerDiv">
                                <div class="container">
                                    <div class="row hidden-sm hidden-md hidden-lg">
                                        <div class="col-xs-6">
                                            <a href="/web/20210910000328/https://www.cdph.ca.gov/">
                                                <img src="/web/20210910000328im_/https://www.cdph.ca.gov/Style Library/CDPH/images/CDPH-Logo.png" alt="CDPH Site Logo" class="img-responsive siteLogo"/>
                                            </a>
                                        </div>
                                        <div class="col-xs-6 topNavContainer topNavButtons">
                                            <nav class="navbar navbar-default" aria-label="settingsNavbar">
                                                <div class="navbar-header">
                                                    <button class="navbar-toggle topMenuButton " type="button" data-toggle="collapse" data-target="#MainNavigation" onclick="closeothermenu('settingsNavbar')" id="MainNavOpener">
                                                        <span class="sr-only">Toggle navigation
                                                        </span>
                                                        <div class="pull-left">MENU
                                                        </div>
                                                        <div class="pull-right">
                                                            <span class="icon-bar">
                                                            </span>
                                                            <span class="icon-bar">
                                                            </span>
                                                            <span class="icon-bar">
                                                            </span>
                                                        </div>
                                                        <div class="clearBoth">
                                                        </div>
                                                    </button>
                                                    <button class="navbar-toggle topMenuButton " type="button" data-toggle="collapse" data-target="#settingsNavbar" onclick="closeothermenu('MainNavigation')" id="SettingsNavOpener">
                                                        <span class="sr-only">Toggle navigation
                                                        </span>
                                                        <span class="fa fa-gear marginTop5">
                                                        </span>
                                                    </button>
                                                </div>
                                            </nav>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6 col-sm-3 col-md-4 hidden-xs">
                                            <a href="/web/20210910000328/https://www.cdph.ca.gov/">
                                                <img src="/web/20210910000328im_/https://www.cdph.ca.gov/Style Library/CDPH/images/CDPH-Logo.png" alt="CDPH Site Logo" class="img-responsive"/>
                                            </a>
                                        </div>
                                        <div class="col-xs-12 col-sm-9 col-md-8">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 searchBoxContainer paddingLeft0">
                                                    <!--search-->
                                                    <div id="searchoriginalLocation" class="searchoriginalLocation hidden-xs">
                                                        <div id="dv_searchbox" class="dv_searchbox">
                                                            <span class="spn_searchIcon">
                                                                <a onclick="searchResult();" style="cursor: pointer">
                                                                    <em class="fa fa-search">
                                                                    </em>
                                                                </a>
                                                            </span>
                                                            <span class="spn_searchBox">                                                            
                                                                <input type="text" class="txtsearchbox" name="SearchBox" id="sb1" onkeypress="handleKeyPress(event)" title="Search Box" onfocus="this.placeholder = ''" onblur="this.placeholder = 'May we help you find something'">
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="inputgroup hidden">
                                                        <div id="DeltaPlaceHolderSearchArea" class="ms-mpSearchBox">
	
                                                            
            <div id="searchInputBox">
                <div class="ms-webpart-chrome ms-webpart-chrome-fullWidth ">
		<div webpartid="00000000-0000-0000-0000-000000000000" haspers="true" id="WebPartWPQ1" width="100%" class="ms-WPBody noindex " onlyformepart="true" allowdelete="false" style=""><div componentid="ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr" id="ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr"><div id="SearchBox" name="Control"><div class="ms-srch-sb ms-srch-sb-border" id="ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr_sboxdiv"><input type="text" value="Search this site" maxlength="2048" accesskey="S" title="Search this site" id="ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr_sbox" autocomplete="off" autocorrect="off" onkeypress="EnsureScriptFunc('Search.ClientControls.js', 'Srch.U', function() {if (Srch.U.isEnterKey(String.fromCharCode(event.keyCode))) {$find('ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr').search($get('ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr_sbox').value);return Srch.U.cancelEvent(event);}})" onkeydown="EnsureScriptFunc('Search.ClientControls.js', 'Srch.U', function() {var ctl = $find('ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr');ctl.activateDefaultQuerySuggestionBehavior();})" onfocus="EnsureScriptFunc('Search.ClientControls.js', 'Srch.U', function() {var ctl = $find('ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr');ctl.hidePrompt();ctl.setBorder(true);})" onblur="EnsureScriptFunc('Search.ClientControls.js', 'Srch.U', function() {var ctl = $find('ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr'); if (ctl){ ctl.showPrompt(); ctl.setBorder(false);}})" class="ms-textSmall ms-srch-sb-prompt ms-helperText"/><a title="Search" class="ms-srch-sb-searchLink" id="ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr_SearchLink" onclick="EnsureScriptFunc('Search.ClientControls.js', 'Srch.U', function() {$find('ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr').search($get('ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr_sbox').value);})" href="javascript: {}"><img src="/web/20210910000328im_/https://www.cdph.ca.gov/_layouts/15/images/searchresultui.png?rev=23" class="ms-srch-sb-searchImg" id="searchImg" alt="Search"/></a><div class="ms-qSuggest-container ms-shadow" id="AutoCompContainer"><div id="ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr_AutoCompList"></div></div></div></div></div><noscript><div id="ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_noscript">It looks like your browser does not have JavaScript enabled. Please turn on JavaScript and try again.</div></noscript><div id="ctl00_PlaceHolderSearchArea_SmallSearchInputBox1">

		</div><div class="ms-clear"></div></div>
	</div>
                
            </div>
        
                                                        
</div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 topNavContainer padding0">
                                                    <!--New Top Nav-->
                                                    <div class="container-fluid hidden-sm hidden-md hidden-lg">
                                                        <nav class="navbar navbar-default" aria-label="TopNav">
                                                            <div class="collapse navbar-collapse js-navbar-collapse" id="settingsNavbar">
                                                                <ul class="nav navbar-nav MobileLanguageMenu">
                                                                    <li class="dropdown mega-dropdown menuItem1 " id="divLanguageNewPosition">
                                                                    </li>
                                                                </ul>
                                                                <ul class="nav navbar-nav ">
                                                                    <li class="dropdown mega-dropdown menuItem2 ">
                                                                        <a href="#" class="dropdown-toggle">Text Size
                                                                            <span class="fa fa-angle-down">
                                                                            </span>
                                                                        </a>
                                                                        <ul class="dropdown-menu mega-dropdown-menu row menuBorder2 secondmenu">
                                                                            <li class="col-sm-12">
                                                                                <ul>
                                                                                    <li>
                                                                                        <a href="#" onclick="changefontsize('plus')" class="classplus">+ Increase
                                                                                            <span class="sr-only">Increase
                                                                                            </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li>
                                                                                        <a href="#" onclick="changefontsize('minus')" class="classminus">-
                                                                                            Decrease<span class="sr-only">Decrease
                                                                                            </span>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li><a href="#" onclick="changefontsize('0Mobile')" class="classrestore">Reset
                                                                                            to Default
                                                                                            <span class="sr-only">Reset
                                                                                                to Default
                                                                                            </span>
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <!-- /.nav-collapse -->
                                                        </nav>
                                                    </div>
                                                    <div class="container-fluid">
                                                        <nav class="navbar navbar-default" aria-label="MainNavigation">
                                                            <div class="collapse navbar-collapse js-navbar-collapse" id="MainNavigation">
                                                                <div class="searchoriginalLocation hidden-sm hidden-md hidden-lg">
                                                                    <div class="dv_searchbox">
                                                                        <span class="spn_searchIcon">
                                                                            <a onclick="searchResultMobile();" style="cursor: pointer">
                                                                                <em class="fa fa-search">
                                                                                </em>
                                                                            </a>
                                                                        </span>
                                                                        <span class="spn_searchBox">                                                                        
                                                                            <input type="text" id="txtSearchBoxMobile" name="SearchBoxMobile" class="txtsearchbox" onkeypress="handleKeyPressMobile(event)" title="Search Box" onfocus="this.placeholder = ''" onblur="this.placeholder = 'May we help you find something'">
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                
<script src="https://web.archive.org/web/20210910000328js_/https://www.cdph.ca.gov/Programs/CID/DCDC/_layouts/15/CDPH/js/Common.js" type="text/javascript"></script>
<noscript></noscript>
<script src="https://web.archive.org/web/20210910000328js_/https://www.cdph.ca.gov/Programs/CID/DCDC/_layouts/15/CDPH/js/AZIndexSearch.js" type="text/javascript"></script>
<noscript></noscript>

<div id="ctl00_ctl63_divMegaMenu" class="container-fluid"><ul class="nav navbar-nav "><li class="dropdown mega-dropdown menuItem1"><a href="javascript:void();" class="dropdown-toggle">I am looking for<span class="fa fa-angle-down"></span></a><ul class="dropdown-menu mega-dropdown-menu row menuBorder1"><li class="col-sm-4"><ul><li class="dropdown-header"><h1>COVID-19</h1></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/Immunization/ncov2019.aspx">Latest Updates ＆ Information</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/Guidance.aspx">Guidance Documents</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/COVID-19/CovidDataAndTools.aspx">COVID-19 Data ＆ Tools</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/COVID-19/ProtectAndPrevent.aspx">Protect ＆ Prevent</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/COVID-19/Testing.aspx">Testing</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/COVID-19-Contact-Tracing.aspx">Contact Tracing</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/COVID-19/Covid19Vaccines.aspx">COVID-19 Vaccines</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/COVID-19/COVID19CountyMonitoringOverview.aspx">Blueprint for a Safer Economy</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/COVID-19/HealthCareAndTesting.aspx">Health Care ＆ Testing</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/COVID-19/HomeAndCommunity.aspx">Home ＆ Community</a></h2></li></ul></li><li class="col-sm-4"><ul><li class="dropdown-header"><h1>Diseases and Conditions</h1></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Pages/allDiseases.aspx">Diseases and Conditions</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DOA/Pages/OAmain.aspx">HIV/AIDS</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CHCQ/HAI/Pages/HAI_DefinitionsAndInformation.aspx">Healthcare-Associated Infections</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CCDPHP/DCDIC/CDSRB/Pages/Program-Landing2.aspx">Cancer</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/DCDC.aspx">Communicable Diseases</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CCDPHP/DCDIC/CDCB/Pages/DiabetesPrevention.aspx">Diabetes</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/DCDCInformationforLocalHealthDepartments.aspx">Disease Reporting</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CHCQ/HAI/Pages/MRSAMethicillin-ResistantStaphylococcusaureus.aspx">Methicillin-Resistant Staphylococcus Aureus Bloodstream Infection</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/OPG/Pages/opg-landing.aspx">Problem Gambling</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/STD.aspx">Sexually Transmitted Diseases</a></h2></li><li><a href="/web/20210910000328/https://www.cdph.ca.gov/pages/allDiseases.aspx" class="btn btn-seemore">See More</a></li></ul></li><li class="col-sm-4"><ul><li class="dropdown-header"><h1>Licensing, Certification and Other Credentials</h1></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CEH/Pages/CLPR.aspx">Certificates, Licenses, Permits and Registrations</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CHCQ/LCP/Pages/HealthCareFacilities.aspx">Health Care Facility Licenses</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CHCQ/LCP/Pages/HealthCareProfessionals.aspx">Health Care Professionals</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/OSPHLD/Pages/Home.aspx">Laboratory</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CEH/DRSEM/Pages/EMB/MedicalWaste/MedicalWaste.aspx">Medical Waste</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/PetDogandCatImportationandExportation.aspx">Pet Importation and Exportation</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CEH/DRSEM/Pages/RHB.aspx">Radiation and Nuclear</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CEH/DRSEM/Pages/EMB/REHS/REHS.aspx">Registered Environmental Health Specialist</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CHSI/Pages/Vital-Records.aspx">Vital Records</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/WildAnimalImportation.aspx">Wild Animal Importation</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CEH/Pages/CLPR.aspx">Medical Professional Licenses</a></h2></li></ul></li><li class="col-sm-4"><ul><li class="dropdown-header"><h1>Family Health</h1></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CFH/DMCAH/Breastfeeding/Pages/General.aspx">Breastfeeding</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CFH/DGDS/Pages/default.aspx">Genetic Disease Screening</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/OHE/Pages/OfficeHealthEquity.aspx">Mental Health</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CFH/DMCAH/NUPA/Pages/default.aspx">Nutrition and Physical Activity</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CFH/DMCAH/Pages/Topics/Pregnancy-and-Reproductive-Health.aspx">Pregnancy and Reproductive Health</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CFH/DWICSN/Pages/Program-Landing1.aspx">Women, Infants, and Children</a></h2></li></ul></li><li class="col-sm-4"><ul><li class="dropdown-header"><h1>Personal Health and Prevention</h1></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/DO/letstalkcannabis/Pages/LetsTalkCannabis.aspx">Cannabis (Marijuana)</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/immunize.aspx">Immunizations</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CCDPHP/DCDIC/NEOPB/Pages/Nutrition_Education_Obesity_Prevention_Branch.aspx">Nutrition</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CCDPHP/DCDIC/CTCB/Pages/CaliforniaTobaccoControlBranch.aspx">Quit Smoking</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/STD.aspx">Sexually Transmitted Disease Testing</a></h2></li></ul></li><li class="col-sm-4"><ul><li class="dropdown-header"><h1>Health and Safety</h1></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/OHE/Pages/CCHEP.aspx">Climate Change and Health</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/EPO/Pages/Program-Landing1.aspx">Emergency Preparedness</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CCDPHP/DEODC/Pages/Environmental-Health-Topics.aspx">Environmental Health</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/OHE/Pages/HIAP.aspx">Health in all Policies</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CCDPHP/DCDIC/CDCB/Pages/WISEWOMAN.aspx">Health Screening Locations</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/MosquitoDiseaseSurveillanceandControlResources.aspx">Mosquito Disease Surveillance and Control</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CCDPHP/DEODC/OHB/Pages/OHB.aspx">Workplace Health</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CHSI/Pages/Vital-Records.aspx">Vital Records</a></h2></li></ul></li><li class="col-sm-4"><ul><li class="dropdown-header"><h1>Health Facilities</h1></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CHCQ/LCP/CalHealthFind/Pages/Complaint.aspx">File a Complaint</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CHCQ/LCP/calhealthfind/pages/home.aspx">California Health Facilities Information Database</a></h2></li></ul></li><li class="col-sm-4"><ul><li class="dropdown-header"><h1>Administrative</h1></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Pages/BoardsAndAdvisoryCommittees.aspx">Boards and Advisory Committees</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/PSB/Pages/Forms.aspx">Forms</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/HRB/Pages/HumanResourcesDivision.aspx">Jobs and Careers</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Pages/LocalHealthServicesAndOffices.aspx">Local Health Services/Offices</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Pages/Public-Records-Act-Request.aspx">Public Records Act Request</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/OLS/Pages/Office-of-Regulations.aspx">Proposed Regulations</a></h2></li></ul></li><li class="col-sm-4"><ul><li class="dropdown-header"><h1>Briefings</h1></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/OPA/Pages/Stakeholder.aspx">Stakeholder Brief</a></h2></li></ul></li></ul></li></ul><ul class="nav navbar-nav"><li class="dropdown mega-dropdown menuItem2"><a href="javascript:void();" class="dropdown-toggle">I am a<span class="fa fa-angle-down"></span></a><ul class="dropdown-menu mega-dropdown-menu row menuBorder2 secondmenu"><li class="col-sm-12"><ul><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CHCQ/LCP/Pages/CDPH-Clinicians-and-Providers.aspx">Clinician / Healthcare Provider</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/OLGA/Pages/LegislativeAndGovernmentalAffairs.aspx">Legislator / Legislative Staff</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/OPA/Pages/Office-of-Public-Affairs.aspx">Media Representative</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CHSI/Pages/Researchers-and-Statisticians.aspx">Researcher / Statistician</a></h2></li><li class="dropdown-header"><h1>Person Interested In</h1></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CFH/DMCAH/Pages/Audiences/Infant-and-Children.aspx">Infant and Child Health</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/OHE/Pages/Mens_Landing_Page.aspx">Men’s Health</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CCDPHP/DEODC/Pages/Senior-Health.aspx">Senior Health</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/OHE/Pages/Womens_Landing_Page.aspx">Women’s Health</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CFH/DMCAH/Pages/Audiences/Youth-and-Young-Adults.aspx">Youth and Young Adult's Health</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DOA/Pages/LGBT_Health.aspx">LGBT Health</a></h2></li></ul></li></ul></li></ul><ul class="nav navbar-nav"><li class="dropdown mega-dropdown menuItem3"><a href="javascript:void();" class="dropdown-toggle">Programs<span class="fa fa-angle-down"></span></a><ul class="dropdown-menu mega-dropdown-menu row menuBorder3"><li class="col-sm-4"><ul><li class="dropdown-header"><h1><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CCDPHP/Pages/Program-Landing1.aspx">Center for Healthy Communities</a></h1></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CCDPHP/DCDIC/CTCB/Pages/CaliforniaTobaccoControlBranch.aspx">California Tobacco Control</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CCDPHP/DEODC/CLPPB/Pages/CLPPBhome.aspx">Childhood Lead Poisoning Prevention</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CCDPHP/DCDIC/CDCB/Pages/ChronicDiseaseControlBranch.aspx">Chronic Disease Control</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CCDPHP/DCDIC/CDSRB/Pages/Program-Landing2.aspx">Chronic Disease Surveillance and Research</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CCDPHP/DEODC/Pages/Emergency-Preparedness-Team.aspx">Emergency Preparedness</a></h2></li><li><a href="/web/20210910000328/https://www.cdph.ca.gov/pages/CenterDetails.aspx?center=Ix3TrKJkMApzAfDObv%2F0j9OOxY64b3T6pQt%2B4e8O3V4%3D" class="btn btn-seemore">See More</a></li></ul></li><li class="col-sm-4"><ul><li class="dropdown-header"><h1><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CEH/Pages/CEH.aspx">Center for Environmental Health</a></h1></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CEH/DFDCS/Pages/DFDCS.aspx">Division of Food and Drug Safety</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CEH/DRSEM/Pages/DRSEM.aspx">Division of Radiation Safety and Environmental Management</a></h2></li></ul></li><li class="col-sm-4"><ul><li class="dropdown-header"><h1><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CFH/Pages/default.aspx">Center for Family Health</a></h1></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CFH/DGDS/Pages/default.aspx">Genetic Disease Screening Program</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CFH/DMCAH/Pages/default.aspx">Maternal, Child, and Adolescent Health</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CFH/DWICSN/Pages/Program-Landing1.aspx">Women, Infants, and Children</a></h2></li></ul></li><li class="col-sm-4"><ul><li class="dropdown-header"><h1><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CHCQ/Pages/CHCQHome.aspx">Center for Health Care Quality</a></h1></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CHCQ/HAI/Pages/HAIProgramHome.aspx">Healthcare-Associated Infections Program</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CHCQ/LCP/Pages/LandCProgramHome.aspx">Licensing and Certification</a></h2></li></ul></li><li class="col-sm-4"><ul><li class="dropdown-header"><h1><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CHSI/Pages/Program-Landing1.aspx">Center for Health Statistics and Informatics</a></h1></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CHSI/Pages/End-of-Life-Option-Act-.aspx">End of Life Option Act</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CHSI/Pages/MMICP.aspx">Medical Marijuana Identification Card Program</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CHSI/Pages/Vital-Records.aspx">Vital Records</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CHSI/Pages/Data-and-Statistics-.aspx">Vital Records Data and Statistics</a></h2></li></ul></li><li class="col-sm-4"><ul><li class="dropdown-header"><h1><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/Pages/CID.aspx">Center for Infectious Diseases</a></h1></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DOA/Pages/OAmain.aspx">HIV/AIDS</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/OBBH/Pages/OBBHHome.aspx">Binational Border Health</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/DCDC.aspx">Communicable Disease Control</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/CDER.aspx">Communicable Disease Emergency Response</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/ORH/Pages/Home.aspx">Refugee Health</a></h2></li><li><a href="/web/20210910000328/https://www.cdph.ca.gov/pages/CenterDetails.aspx?center=Ix3TrKJkMApcZm2cX3lbubkkdAXdSMloGIzMANTDHzE%3D" class="btn btn-seemore">See More</a></li></ul></li><li class="col-sm-4"><ul><li class="dropdown-header"><h1><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/DO/Pages/CDPH Director Welcome Page.aspx">Director / State Public Health Officer</a></h1></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/OLGA/Pages/LegislativeAndGovernmentalAffairs.aspx">Legislative and Governmental Affairs</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/DO/letstalkcannabis/Pages/LetsTalkCannabis.aspx">Let's Talk Cannabis</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/OHE/Pages/OfficeHealthEquity.aspx">Office of Health Equity</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/OPA/Pages/Office-of-Public-Affairs.aspx">Office of Public Affairs</a></h2></li><li><a href="/web/20210910000328/https://www.cdph.ca.gov/pages/CenterDetails.aspx?center=5ehPbPOt8S6FvpE3rfQSNqPgUtRZfAMw5P4HuCCHmHuOyNIyjdgitQ%3D%3D" class="btn btn-seemore">See More</a></li></ul></li><li class="col-sm-4"><ul><li class="dropdown-header"><h1><a href="javascript:void();">Other CDPH Offices</a></h1></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/EPO/Pages/Program-Landing1.aspx">Emergency Preparedness Office</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/OSPHLD/Pages/Home.aspx">Office of State Public Health Laboratory Director</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/OSPHLD/LFS/Pages/Home.aspx">Laboratory Field Services</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/OLS/Pages/Privacy-Office.aspx">Privacy Office</a></h2></li><li><h2><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/OLS/Pages/Office-of-Regulations.aspx">Office of Regulations</a></h2></li></ul></li></ul></li></ul><ul class="nav navbar-nav"><li class="dropdown mega-dropdown menuItem4"><a href="/web/20210910000328/https://www.cdph.ca.gov/Pages/AtoZIndex.aspx" class="dropdown-toggle">A-Z Index<span class="fa fa-angle-down"></span></a></li></ul></div>


<script async>

    function IndexBtnSearchQuery(param) {

        var queryText = "";
        queryText = param.charAt(0);
        if (param != $("#prevBtn").val()) {
            $('#' + param).removeClass('btn btn-sm btn-menu btn-default');
            $('#' + param).addClass('btn btn-sm btn-menu btn-primary');
            $('#' + $("#prevBtn").val()).removeClass('btn btn-sm btn-menu btn-primary');
            $('#' + $("#prevBtn").val()).addClass('btn btn-sm btn-menu btn-default');
            $('#prevBtn').attr('value', param);
        }
        $("#txtQueryText").val("");
        getSearchData(queryText, 0);
    }

    function KeyPressSearch() {
        var queryText = $("#txtQueryText").val().trim();
        $('#' + $("#prevBtn").val()).removeClass('btn btn-sm btn-menu btn-primary');
        $('#' + $("#prevBtn").val()).addClass('btn btn-sm btn-menu btn-default');
        if (queryText.length > 0) {
            getSearchData(queryText, 0);
        }
        $('#prevBtn').attr('value', '');
    }

    function ImageBtnSearchQuery(param) {
        var queryText = $("#txtQueryText").val().trim();
        $('#' + $("#prevBtn").val()).removeClass('btn btn-sm btn-menu btn-primary');
        $('#' + $("#prevBtn").val()).addClass('btn btn-sm btn-menu btn-default');
        if (queryText.length > 0) {
            getSearchData(queryText, 0);
        }
    }
    function getSearchData(queryText, initialSearch) {
        $('#loadingDiv').show();
        //searchAZIndexByKeyWord(queryText, 'https://web.archive.org/web/20210910000328/https://www.cdph.ca.gov', 'fIJwuNlyfcwm%2BJhn6txU6Q%3D%3D', 'DIqRnmiPEH3XxobvzgLUmA0uyBHD96SomkpI7cMqkk9u9WMa66dixA%3D%3D', 'xT5lHzodTtM%3D', 'Programs');
        if (initialSearch == 1) {
            getAZIndexSearchResult('https://web.archive.org/web/20210910000328/https://www.cdph.ca.gov', 'fIJwuNlyfcwm%2BJhn6txU6Q%3D%3D', 'DIqRnmiPEH3XxobvzgLUmA0uyBHD96SomkpI7cMqkk9u9WMa66dixA%3D%3D', 'xT5lHzodTtM%3D','Windows', 'Programs', queryText, '10.0');
        }
        else {
            writeAZIndexSearchResult(queryText);
        }
    }

    


    function SearchResultData(data, queryText) {
        try {
            var resultCount;
            var itemCount = 0;
            var columnCount = 6;
            var resultHTML = "";
            var title;
            var pageURL;
            if (data != null || data != undefined) {
                var results = data.d.query.PrimaryQueryResult.RelevantResults.Table.Rows.results;
                if (results.length > 0) {
                    $('#noRecordDiv').hide();
                    resultCount = results.length;
                    for (var i = 0; i < resultCount; i++) {
                        title = results[i].Cells.results[3].Value;
                        if (results[i].Cells.results[3].Value.toLowerCase().startsWith(queryText.toLowerCase())) {
                            itemCount = itemCount + 1;
                            if ((itemCount % columnCount) == 1) {
                                resultHTML += "<ul class='topMenuAZindexList'>";
                            }
                            pageURL = results[i].Cells.results[6].Value;
                            resultHTML += "<li>"
                            resultHTML += "<a href=" + pageURL + ">" + showLessMoreText(title, 3) + "</a>"
                            resultHTML += "</li>"
                            if (((itemCount % columnCount) == 0) || (i == resultCount - 1)) {
                                resultHTML += "</ul>";
                            }
                        }
                    }
                }
                if (itemCount == 0) {
                    $('#noRecordDiv').show();
                }
            }
            else {
                $('#noRecordDiv').show();
            }
            $('#topMenuAZindexListContainerDiv').html(resultHTML);
        }
        catch (err) {
            errorSearchHandler('Error populating data: ' + err.message);
        }
        finally {
            $('#loadingDiv').hide();
        }
    }


    //function SearchResult(data) {
    //    try {
    //        var resultCount;
    //        var columnCount = 6;
    //        var resultHTML = "";
    //        var title;
    //        var pageURL;
    //        if (data != null || data != undefined) {
    //            var results = data.d.query.PrimaryQueryResult.RelevantResults.Table.Rows.results;
    //            if (results.length > 0) {
    //                $('#noRecordDiv').hide();
    //                resultCount = results.length;
    //                var fullIterations = Math.floor(resultCount / columnCount);
    //                var mod = (resultCount % columnCount);
    //                var k = 0;
    //                for (var i = 0; i < fullIterations; i++) {
    //                    resultHTML += "<ul class='topMenuAZindexList'>";
    //                    for (var j = 0; j < columnCount; j++) {
    //                        title = results[k].Cells.results[3].Value;
    //                        pageURL = results[k].Cells.results[6].Value;
    //                        resultHTML += "<li>"
    //                        resultHTML += "<a href=" + pageURL + ">" + showLessMoreText(title, 3) + "</a>"
    //                        resultHTML += "</li>"
    //                        k = k + 1;
    //                    }
    //                    resultHTML += "</ul>"
    //                }
    //                if (mod > 0) {
    //                    resultHTML += "<ul class='topMenuAZindexList'>";
    //                    for (var j = 0; j < mod; j++) {
    //                        title = results[k].Cells.results[3].Value;
    //                        pageURL = results[k].Cells.results[6].Value;
    //                        resultHTML += "<li>"
    //                        resultHTML += "<a href=" + pageURL + ">" + showLessMoreText(title, 3) + "</a>"
    //                        resultHTML += "</li>"
    //                        k = k + 1;
    //                    }
    //                    resultHTML += "</ul>"
    //                }
    //            }
    //            else {
    //                $('#noRecordDiv').show();
    //            }
    //        }
    //        $('#topMenuAZindexListContainerDiv').html(resultHTML);
    //    }
    //    catch (err) {
    //        errorSearchHandler('Error populating data: ' + err.message);
    //    }
    //    finally {
    //        $('#loadingDiv').hide();
    //    }
    //}

    function errorSearchHandler(err) {
        $('#errMsgDiv').show();
        $('#errMsgDiv').text(err);
        //alert(err);
    }


    $('li.dropdown.mega-dropdown a').on('click', function (event) {
        $('li.dropdown.mega-dropdown').removeClass('open');
        $('li.dropdown.mega-dropdown').addClass('fadeLinks');

        $(this).parent().toggleClass('open');
        $(this).parent().removeClass('fadeLinks');
    });

    $('li.dropdown.mega-dropdown li ul li a').on('click', function (event) {
        $('li.dropdown.mega-dropdown').removeClass('fadeLinks');
    });

    $('body').on('click', function (e) {
        try{
            if (!$('li.dropdown.mega-dropdown').is(e.target)
                && $('li.dropdown.mega-dropdown').has(e.target).length === 0
                && $('.open').has(e.target).length === 0
            ) {
                $('li.dropdown.mega-dropdown').removeClass('open');
                $('li.dropdown.mega-dropdown').removeClass('fadeLinks');

            }
        }
        catch(err){}
    });

    $(window).load(function () {
        $(".topNavMenuAZcontainerOuter").mCustomScrollbar({
            axis: "x",
            setLeft: "67px",
            theme: "dark-thick",
            documentTouchScroll: true
        });
        setTimeout(setLeftPos, 1000);
    });
    function setLeftPos() {
        $("#mCSB_1_container").css("left", "0px")
    }

    $(document).ready(function () {
        moveSecondaryNav();
       // var queryText = "a";
       // getSearchData(queryText, 1);

    });

    $(window).resize(function () {
        moveSecondaryNav();
    });

    function moveSecondaryNav() {
        var windowWidth = $(window).width();
        if (windowWidth < 768) {
            $("#MainNavigation").append($("#topNavSecondary"));
            //    $("#MainNavigation").prepend($("#dv_searchbox"));
            $("#divLanguageNewPosition").append($("#ddlLanguage"));
        }
        else {
            $("#topNavSecondaryOriginalPosition").append($("#topNavSecondary"));
            //       $("#searchoriginalLocation").append($("#dv_searchbox"));
            $("#ddlLanguageActualPosition").append($("#ddlLanguage"));
        }
    }
</script>
<noscript></noscript>


                                                            </div>
                                                            <!-- /.nav-collapse -->
                                                        </nav>
                                                    </div>
                                                    <!--Top Nav Ends-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="hideMe">
                                <div id="DeltaSiteLogo" class="pull-left">
	
                                    <a id="ctl00_x36d5bee949a7480783b3afc048c15598" class="ms-siteicon-a" href="/web/20210910000328/https://www.cdph.ca.gov/"><img id="ctl00_x74bbee6762f840edad4fe46f496d8c78" class="ms-siteicon-img" name="onetidHeadbnnr0" src="/web/20210910000328im_/https://www.cdph.ca.gov/_layouts/15/images/siteIcon.png?rev=23" alt="Division of Communicable Disease Control"/></a>
                                
</div>
                                <div class="headtitle pull-left">
                                    <h1 id="pageTitle" class="ms-core-pageTitle">
                                        <span id="DeltaPlaceHolderPageTitleInTitleArea">
                                            
            <span><span><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/COVID-19/ThirdVaccineDoseQandA.aspx">ThirdVaccineDoseQandA</a></span></span>
            
            
            Questions and Answers: Additional COVID-19 Vaccine Doses for People Whose Immune Systems are Compromised
            
        
                                        </span>
                                        <div id="DeltaPlaceHolderPageDescription" class="ms-displayInlineBlock ms-normalWrap">
	
                                            <a href="javascript:;" id="ms-pageDescriptionDiv" style="display: none;">
                                                <span id="ms-pageDescriptionImage">&#160;
                                                </span>
                                            </a>
                                            <span class="ms-accessible" id="ms-pageDescription">
                                            </span>
                                            <script type="text/javascript">// <![CDATA[ 

_spBodyOnLoadFunctionNames.push("setupPageDescriptionCallout"); // ]]>
</script>
                                        
</div>
                                    </h1>
                                </div>
                            </div>
                        </div>
                        <div class="navbar navbar-inverse noindex hideMe">
                            <div class="container">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a class="navbar-brand" href="#">Top Menu</a>
                                </div>
                                <div class="ms-breadcrumb-dropdownBox" style="display: none">
                                    <span id="DeltaBreadcrumbDropdown">
                                        
                                    </span>
                                </div>
                                <div id="DeltaTopNavigation" class="ms-displayInline ms-core-navigation" role="navigation">
	
                                    
                                    
            <div id="zz1_TopNavigationMenu" class=" noindex ms-core-listMenu-horizontalBox">
		<ul id="zz2_RootAspMenu" class="root ms-core-listMenu-root static">
			<li class="static selected"><a class="static selected menu-item ms-core-listMenu-item ms-displayInline ms-bold ms-core-listMenu-selected ms-navedit-linkNode" tabindex="0" href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/DCDC.aspx" accesskey="1"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Division of Communicable Disease Control</span><span class="ms-hidden">Currently selected</span></span></a></li>
		</ul>
	</div>
            
        
                                
</div>
                            </div>
                        </div>
                    </header>
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                
                                
                                <div class="noindex" id="breadcrumbSiteMap">
                                    <div class="hidden">
                                        <span class="s4-breadcrumb pull-left"><a href="#ctl00_ctl65_SkipLink"><img alt="Skip Navigation Links" src="/web/20210910000328im_/https://www.cdph.ca.gov/WebResource.axd?d=ZwYFdoO_z1iCltDhVRtGqPPujFA28-01j_20nRiCQmPl-g-DiTimBW_oC2FdRm8m2XzGTwO03dzPegDP1J_HagxvXthIKAJX31bw-IZ4YIg1&amp;t=637453780939909757" width="0" height="0" style="border-width:0px;"/></a><span><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC">Division of Communicable Disease Control</a></span><span> &gt; </span><span><a href="https://web.archive.org/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/_layouts/15/listform.aspx?ListId=%7B6AAD4E4A%2DF5C6%2D484E%2D8D56%2DFD7746CD9D60%7D&amp;PageType=0">Pages</a></span><span> &gt; </span><span><a href="https://web.archive.org/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/_layouts/15/listform.aspx?ListId=%7B6AAD4E4A%2DF5C6%2D484E%2D8D56%2DFD7746CD9D60%7D&amp;PageType=0&amp;RootFolder=%2FPrograms%2FCID%2FDCDC%2FPages%2FCOVID%2D19">COVID-19</a></span><span> &gt; </span><span>ThirdVaccineDoseQandA</span><a id="ctl00_ctl65_SkipLink"></a></span>
                                    </div>
                                    <div id="ctl00_ctl67_customBreadCrumbDiv"><div class="breadCrumbCustomContainer noindex"><ul class="customBreadcrumb"><li><a href="https://web.archive.org/web/20210910000328/https://www.cdph.ca.gov/">Home</a></li><li><a href="https://web.archive.org/web/20210910000328/https://www.cdph.ca.gov/Programs">Programs</a></li><li><a href="https://web.archive.org/web/20210910000328/https://www.cdph.ca.gov/Programs/CID">Center for Infectious Diseases</a></li><li><a href="https://web.archive.org/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC">Division of Communicable Disease Control</a></li><li class="activePage">Questions and Answers: Additional COVID-19 Vaccine Doses for People Whose Immune Systems are Compromised</li></div></ul></div>

                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <section id="centerarea" class="container">
                        <div id="MainRow" class="row">
                            <div class="col-sm-3 col-lg-2 ms-dialogHidden" id="leftNav">
                                <div class="navbar left-navbar navbar-inverse">
                                    <div class="container">
                                        <div class="navbar-header left-navbar">
                                            <button type="button" class="navbar-toggle" id="left-navbar-toggle" data-toggle="collapse" data-target=".left-navbar-collapse">
                                                <span class="icon-bar">
                                                </span>
                                                <span class="icon-bar">
                                                </span>
                                                <span class="icon-bar">
                                                </span>
                                            </button>
                                            <a class="navbar-brand" id="left-navbar-brand" href="#">Left Menu
                                            </a>
                                        </div>
                                        <div id="DeltaPlaceHolderLeftNavBar" class="ms-core-navigation" role="navigation">
	
                                            
            <a id="startNavigation" name="startNavigation" tabindex="-1">
            </a>
            <div>
            </div>
            <div>
            </div>
            <div>
            </div>
            <div>
            </div>
            <div>
            </div>
            <div class="ms-core-sideNavBox-removeLeftMargin">
                <div id="ctl00_PlaceHolderLeftNavBar_QuickLaunchNavigationManager">
		
                
                <div id="zz3_V4QuickLaunchMenu" class=" noindex ms-core-listMenu-verticalBox">
			<ul id="zz4_RootAspMenu" class="root ms-core-listMenu-root static">
				<li class="static selected"><a class="static selected menu-item ms-core-listMenu-item ms-displayInline ms-bold ms-core-listMenu-selected ms-navedit-linkNode" tabindex="0" href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Recent</span><span class="ms-hidden">Currently selected</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/VRDL_About.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">About the Viral and Rickettsial Disease Lab</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" title="This is the landing page for CalREDIE" href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/CalREDIE.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">CalREDIE</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/CalREDIEContactUs.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">CalREDIE Contact Us</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/CDERInformationforHealthProfessionals.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">CDER Information for Health Professionals</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/Chlamydia.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Chlamydia</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" title="This is the landing page for the Communicable Disease Emergency Response Program (CDER)" href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/CDER.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Communicable Disease Emergency Response Program</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" title="This page contains the contact information for DCDC" href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/DCDCContactUs.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">DCDC Contact Us</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/DCDCInformationforLocalHealthDepartments.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">DCDC Information for Local Health Departments</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" title="This is the Resources page for DCDC" href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/DCDCResources.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">DCDC Resources</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/Gonorrhea.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Gonorrhea</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/HepatitisC.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Hepatitis C</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/IDB.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Infectious Diseases Branch</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/InterpretingZikaVirusTestResults.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Interpreting Zika Virus Test Results</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/PelvicInflammatoryDisease.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Pelvic Inflammatory Disease (PID)</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" title="Sexually Transmitted Diseases Control Branch" href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/STD.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Sexually Transmitted Diseases Control Branch</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/Syphilis.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Syphilis</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/Trichomoniasis.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Trichomoniasis</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/VRDL_Contact.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">VRDL Contact Information</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" title="VRDL Guidelines for Specimen Collection and Submission for Pathologic Testing" href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/VRDL_Guidelines_Collection_Submission.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">VRDL Guidelines for Specimen Collection and Submission for Pathologic Testing</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/VRDL_Specimen_Submittal_Forms.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">VRDL Specimen Submittal Forms</span></span></a></li>
			</ul>
		</div>
                
	</div>
                
                <div>
                    <div class="ms-core-listMenu-verticalBox">
                        
                        
                    </div>
                </div>
            </div>
        
                                        
</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-9 col-lg-10 width100PerImp" id="MainContent">
                                <div id="notificationArea" class="ms-notif-box">
                                </div>
                                <div id="DeltaPageStatusBar">
	
                                    <div id="pageStatusBar">
                                    </div>
                                
</div>
                                <div id="DeltaPlaceHolderMain">
	
                                    <a id="mainContent" name="mainContent" tabindex="-1">
                                    </a>
                                    
            <link rel="stylesheet" href="/web/20210910000328cs_/https://www.cdph.ca.gov/_catalogs/masterpage/covid19/css/articlePost.css "/>
            <div class="top-page-wp">
				<div>
				
				<div class="ms-webpart-chrome ms-webpart-chrome-fullWidth ">
		<div webpartid="00000000-0000-0000-0000-000000000000" haspers="true" id="WebPartWPQ2" width="100%" class="ms-WPBody noindex " onlyformepart="true" allowdelete="false" style=""><div class="ms-rtestate-field"><style>
.innerPageBanner2Container {
Height: 40px;
Background-color: green;
}
</style>
<div class="innerPageBanner2Container">
   <span class="innerPageBanner h1PageHeading"></span>
</div></div><div class="ms-clear"></div></div>
	</div>
				</div>           
            </div>
            <section class="article-post-banner article-banner-desktop">
	            <div class="top-page-wp">
					<div class="ms-webpart-chrome ms-webpart-chrome-fullWidth ">
		<div webpartid="00000000-0000-0000-0000-000000000000" haspers="true" id="WebPartWPQ3" width="100%" class="ms-WPBody noindex " onlyformepart="true" allowdelete="false" style=""><div class="ms-rtestate-field"><!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="/web/20210910000328cs_/https://www.cdph.ca.gov/_catalogs/masterpage/covid19/css/subNavigation.css">
    <script src="/web/20210910000328js_/https://www.cdph.ca.gov/_catalogs/masterpage/covid19/js/helpers/SPUtils.js"></script>
    <script src="/web/20210910000328js_/https://www.cdph.ca.gov/_catalogs/masterpage/covid19/js/subNavigation.js"></script>
</head>

<body>
    <nav class="navbar navbar-default sub-navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a href="#" class="navbar-brand">COVID-19 Menu</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul id="covid19-subNavigation" class="nav navbar-nav">
            
          </ul>
        </div>
      </div>
    </nav>  </body>
</html></div><div class="ms-clear"></div></div>
	</div>          
	            </div>
            </section>
            <div class="container article-post">
	            <div class="eyebrow-text">
		            <div data-name="Page Field: Article Eyebrow">
					</div>
	            </div>

                <h1 class="article-post-title">
                    <div data-name="Page Field: Title">
                        
                        
                        Questions and Answers: Additional COVID-19 Vaccine Doses for People Whose Immune Systems are Compromised
                        
                    </div>
                </h1>
                <div class="info-article">
                    <span class="article-date-title">
                        <div data-name="Page Field: Article Date Title">
                            
                            
                            August 23, 2021
                            
                        </div>
                    </span>
                </div>
                <div class="related-materials">
                    <div data-name="Page Field: Related Materials Links">
                        
                        
                        <div id="ctl00_PlaceHolderMain_ctl03__ControlWrapper_SummaryLinkFieldControl" style="display:inline"><div id="slwp_ctl00_PlaceHolderMain_ctl03_ctl00" class="slm-layout-main slwpmarker" xmlns:pcm="urn:PageContentManager"><!--empty--></div></div>
                        
                    </div>
                </div>

                <div class="article-content">
                    <div>
                        
                        
                        <div id="ctl00_PlaceHolderMain_ctl04_label" style="display:none">Page Content</div><div id="ctl00_PlaceHolderMain_ctl04__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_ctl04_label"><h2>Who is eligible for an additional dose of the COVID-19 vaccine?<br></h2><div>On August 12, 2021, the <a href="https://web.archive.org/web/20210910000328/https://www.fda.gov/" target="_blank">U.S. Food and Drug Administration</a> (FDA) expanded the emergency use authorization for both mRNA vaccines, Pfizer-BioNTech and Moderna, for immunocompromised individuals. This authorization covers people ages 12 and up receiving treatments associated with moderate to severe immune compromise. This includes individuals who have&#58;</div><div><ul><li>Been receiving active cancer treatment for tumors or cancers of the blood</li><li>Received an organ transplant and are taking medicine to suppress the immune system</li><li>Received a stem cell transplant within the last 2 years or are taking medicine to suppress the immune system</li><li>Moderate or severe primary immunodeficiency (such as DiGeorge syndrome, Wiskott-Aldrich syndrome)</li><li>Advanced or untreated HIV infection</li><li>Active treatment with high-dose corticosteroids or other drugs that may suppress your immune response</li></ul></div><div>People should talk to their healthcare provider about their medical condition and whether getting an additional dose is appropriate for them.<br></div><h2>Why isn’t everyone eligible for an additional vaccine dose?</h2><div>The FDA and federal Centers for Disease Control and Prevention(CDC) have carefully reviewed data from recent vaccine trials related to antibodies in specific individuals. The results from these trials have shown people who are immunocompromised in a manner similar to those who have undergone solid organ transplantation have a reduced ability to fight infections and other diseases. They are especially vulnerable to infections, including COVID-19. The administration of additional vaccine doses has been shown to increase protection in this population.</div><div><br></div><div>At this time, given the effectiveness of the vaccines, there is no recommendation for those who are fully vaccinated (2 doses of mRNA vaccine or one dose of J&amp;J vaccine) and are NOT moderate or severely immunocompromised to receive an additional dose. However, CDPH has been planning for and is ready to begin administering booster doses in California, once recommended by our federal partners and the Western States Scientific Safety Review Workgroup.<br></div><h2>What if I’m not eligible and still want to get an additional dose because I have other pre-existing medical conditions?</h2><div>At this time, additional vaccine doses are only authorized for individuals with moderate to severe immune compromise. Research has shown that two doses of the mRNA vaccines or one dose of the Johnson &amp; Johnson vaccine still offers protection for&#160;other populations.</div><div><br></div><div>Pending review of the evidence and approval by our federal partners and the Western States Scientific Safety Review Workgroup, booster shots for healthy individuals may become available in the near future&#160;to help maintain immunity to COVID-19 as announced by the U.S. Department of Health and Human Services.<br></div><h2>What is the difference between an additional dose and a booster dose?</h2><div>An “additional dose” of a COVID-19 vaccine is administered to individuals with weakened immune systems who have not responded fully to initial doses.</div><div><br></div><div>A “booster” dose is administered when the response to initial vaccination might have decreased to levels that increase vulnerability.&#160;</div><h2>Q&#58; How many Californians will be eligible?</h2><div>There are approximately 800,000 Californians who will be eligible for additional vaccine doses under the FDA authorization and CDC recommendation. Only two to three percent of people in the United States are eligible for additional doses of COVID-19 vaccines.</div><h2>How is the state defining ‘immunocompromised,’ and will Californians be required to verify they are immunocompromised?</h2><div>California will follow the guidance of the Advisory Committee on Immunization Practices (ACIP) and CDC. ACIP will take the FDA modifications to the vaccine’s Emergency Use Authorizations and create current channels based on current evidence of effectiveness and safety.</div><div><br></div><div>This authorization covers people ages 12 and up who are solid organ transplant recipients or those who are diagnosed with conditions that are considered to have a moderate or severe level of immunocompromise. Verification of immunocompromised status will not be required. Your medical provider can help clarify if you meet the threshold for immunocompromised or not.<br></div><h2>When will additional doses be available for immunocompromised individuals in California?</h2><div>Additional vaccine doses are available as of August 16, 2021.<br></div><h2>How does someone find and receive an additional vaccine dose?</h2><div>Additional vaccine doses will be available through all current vaccination channels, including healthcare providers, clinics, and neighborhood pharmacies. Immunocompromised Californians can visit myturn.ca.gov to make an appointment or find a walk-in clinic.<br></div><h2>When should I receive my additional vaccine dose?</h2><div>The additional dose of mRNA COVID-19 vaccine should be administered at least 28 days after completion of the primary mRNA COVID-19 vaccine series. Whenever possible, mRNA COVID-19 vaccination doses (including the primary series and an additional dose) should be given at least two weeks before initiation of immunosuppressive therapies.<br></div><h2>Which vaccines are covered by the emergency use authorization?</h2><div>The FDA expanded the emergency use authorization for Pfizer-BioNTech and Moderna vaccines only for immunocompromised individuals.<br></div><h2>What if someone received a Johnson &amp; Johnson vaccine initially? Should they still get a Pfizer or Moderna vaccine?</h2><div>The EUA amendment for an additional dose does not apply to the Janssen COVID-19 vaccine or individuals who received Janssen COVID-19 as a primary series. The CDC and FDA are actively engaged to ensure that immunocompromised recipients of the Janssen COVID-19 vaccine have optimal vaccine protection.<br></div><h2>Can a person get an additional vaccine dose from a different vaccine brand, or do they need to stay with the brand for their original supplier?</h2><div>When possible, individuals should receive an additional vaccine dose from the same vaccine manufacturer as their original series. Meaning, if an individual received the Pfizer-BioTech vaccine initially, they should receive an additional dose of Pfizer-BioNTech if available. When the same brand is not available, a person may receive the other brand.<br></div><h2>Why would I want to get an additional vaccine dose? (Are additional doses really necessary?)</h2><div>People who are immunocompromised in a manner similar to those who have undergone solid organ transplantation have a reduced ability to fight infections and other diseases. They are especially vulnerable to infections, including COVID-19. The FDA determined that the administration of additional vaccine doses may increase protection in this population. These patients should be counseled to maintain physical precautions to help prevent COVID-19. In addition, close contacts of immunocompromised persons should get vaccinated, as appropriate for their health status, to provide increased protection to their loved ones.<br></div><h2>Can I get multiple additional doses?</h2><div>Currently, additional vaccine doses are only authorized for immunocompromised individuals by the FDA and recommended by the CDC.<br></div><h2>If a person is eligible for an additional dose, does that mean they are not considered fully vaccinated under current public health guidelines?<br></h2><div>A person is still considered fully vaccinated if they have received their dose(s) prior to the availability of this new dose for certain individuals.<br></div><h2>How are we vaccinating some with additional doses when not everyone has received their two-dose series?</h2><div>mRNA COVID-19 vaccine supply in the United States is sufficient to make additional doses for immunocompromised people feasible. Everyone who wants to get vaccinated will still have vaccines available.<br></div><h2>Will digital vaccine records be updated to show that individuals have received the additional dose of vaccines?</h2><div>Yes, the Digital COVID-19 Vaccine Record has been updated and will show the additional dose.<br></div><h2>If I get an additional dose, will it show on my digital vaccine record?</h2><div>Your digital vaccine record does not automatically update. If you receive an additional dose of the COVID-19 vaccine, you'll have to get a new QR code through the Digital COVID-19 Vaccine Record portal.<br></div><h2>Does this mean California and the United States are going against the World Health Organization’s call to wait on boosters?<br></h2><div>Officials with the WHO emphasized they were not referring to the extra doses that may be needed now for certain groups, like those with weakened immune systems.<br></div><h2>Will an antibody or other test show whether I need an additional dose?</h2><div>We do not recommend using antibodies or any other tests to determine protection from the virus at this time. There is significant variability from one antibody test to another, and we simply do not know enough about what antibody levels really mean in terms of protection. Boosters are an area of intensive research, and we hope to understand them better in the future.<br></div><h2>Does receiving an additional vaccine dose eliminate the need for further harm-reduction precautions?</h2><div>No, even with an extra dose of mRNA vaccine, not everyone with a compromised immune system appears to mount a normal immune response. Since&#160;we are still learning about this, it is prudent to protect yourself by taking extra precautions. Friends, family, and caretakers who you interact with should also continue to take harm-reduction precautions, such as wearing a mask when indoors.<br></div><h2>Is there a test to determine who qualifies as an immunocompromised individual?</h2><div>No, if you think you qualify but are not positive, please check with your medical provider to confirm you are eligible for an additional dose.<br></div><h2>Is the state going to set aside doses specifically for additional doses for immunocompromised persons? Will they be administered from the same amounts allocated for the first doses?</h2><div>California is no longer allocated doses from the CDC but is ordering on an as-needed basis. There are enough doses available on hand or in additional orders, so doses do not need to be set aside.</div><h2>The CDC estimates more than 1 million Americans have received unauthorized additional doses. How many Californians have already received an additional dose?</h2><div>There are approximately 100,000 people who have received three vaccine doses.<br></div><h2>As a person with a weak immune system, once I get an additional dose, can I reduce the use of other precautions such as masking, distancing, and avoiding groups and crowds.</h2><div>No, because even with an extra dose of mRNA vaccine, not everyone with a compromised immune system appears to mount a normal immune response. We are still learning about this, but at this time, we consider it prudent to continue taking extra precautions.<br></div><h3>Provider Questions<br></h3><div><h2>How do providers order an additional supply of vaccine doses?</h2><div>Vaccine providers don’t need specific vaccines for additional doses. It is the same dose as used in the original vaccination.<br></div><h2>How are additional doses recorded in a series?</h2><div>All doses administered to an individual are recorded in CAIR in the same way.<br></div><h2>How are additional doses recorded in CAIR?</h2><div>All doses administered to an individual are recorded in CAIR in the same way.<br></div></div><h3>More Information<br></h3><p>Western States Scientific Safety Review Workgroup&#58;<span class="ms-rteStyle-Accent1"> </span><a href="/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/COVID-19/RecommendationsforAdditionalDoses.aspx" target="_blank"><span class="ms-rteStyle-Accent1">Statement</span></a><br></p><p>CDC&#58;<a href="https://web.archive.org/web/20210910000328/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/recommendations/immuno.html" target="_blank"> <span class="ms-rteStyle-Accent1">COVID-19 Vaccines for Moderately to Severely Immunocompromised People</span></a><br></p><p>CDC&#58; <a href="https://web.archive.org/web/20210910000328/https://www.cdc.gov/media/releases/2021/s0813-additional-mRNA-mrna-dose.html" target="_blank"><span class="ms-rteStyle-Accent1">Statement on Additional COVID-19 vaccine dose for Immunocompromised People</span></a></p><p>FDA&#58; <a href="https://web.archive.org/web/20210910000328/https://www.fda.gov/news-events/press-announcements/coronavirus-covid-19-update-fda-authorizes-additional-vaccine-dose-certain-immunocompromised" target="_blank"><span class="ms-rteStyle-Accent1">Authorization of Additional Vaccine Dose for Certain Immunocompromised Individuals</span></a></p><p>ACIP&#58;&#160;<a href="https://web.archive.org/web/20210910000328/https://www.cdc.gov/vaccines/acip/meetings/slides-2021-08-13.html" target="_blank"><span class="ms-rteStyle-Accent1">Presentation Slides, August 13, 2021 Meeting</span></a><br></p><p><br></p><p><br></p></div>
                        
                    </div>
                </div>
                <div class="article-content">
                    <div data-name="WebPartZone">
                        
                        
                        <div>
                            <menu class="ms-hide">
		<ie:menuitem id="MSOMenu_Help" iconsrc="/_layouts/15/images/HelpIcon.gif" onmenuclick="MSOWebPartPage_SetNewWindowLocation(MenuWebPart.getAttribute('helpLink'), MenuWebPart.getAttribute('helpMode'))" text="Help" type="option" style="display:none">

		</ie:menuitem>
	</menu>
                        </div>
                        
                    </div>
                </div>
                <div class="article-content">
                    <div data-name="Page Field: Article Page Content">
                        
                        
                        <div id="ctl00_PlaceHolderMain_ctl05_label" style="display:none">Article Page Content</div><div id="ctl00_PlaceHolderMain_ctl05__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_ctl05_label"><p>​​​Originally published August&#160;16, 2021<br></p></div>
                        
                    </div>
                </div>
                <div class="article-content">
                    <div data-name="WebPartZone">
                        
                        
                        <div>
                            
                        </div>
                        
                    </div>
                </div>
                <div>
                    <div class="additional-info-links">
                        <div data-name="Page Field: Additional Information Links">
                            
                            
                            <div id="ctl00_PlaceHolderMain_ctl06__ControlWrapper_SummaryLinkFieldControl" style="display:inline"><div id="slwp_ctl00_PlaceHolderMain_ctl06_ctl00" class="slm-layout-main slwpmarker" xmlns:pcm="urn:PageContentManager"><!--empty--></div></div>
                            
                        </div>
                    </div>
                </div>
                <footer>
                    <div class="other-language-links">
                        <div data-name="Page Field: Other Language Links">
                            
                            
                            <div id="ctl00_PlaceHolderMain_ctl07__ControlWrapper_SummaryLinkFieldControl" style="display:inline"><div id="slwp_ctl00_PlaceHolderMain_ctl07_ctl00" class="slm-layout-main slwpmarker" xmlns:pcm="urn:PageContentManager"><!--empty--></div></div>
                            
                        </div>
                    </div>
                </footer>
            </div>
            
            
            
		<div data-name="EditModePanelShowInEdit">
		  
		  
		  
		  
		</div>            
            
            
            <script>//<![CDATA[
            	$(document).ready(function(){
            		$('.eyebrow-text > div').html(function(i,h){
					    return h.replace(/&nbsp;/g,'');
					});
            	});
            //]]></script>
        <div style="display:none" id="hidZone"></div>
                                
</div>
                            </div>
                        </div>
                    </section>
                    <div class="container ms-dialogHidden" id="backToTop">
                        <div class="row ">
                            <div class="col-xs-12">
                                <div class="backtoTop" id="backtoTop">
                                    <a href="javascript:void(0)">
                                        <span class="glyphicon glyphicon-arrow-up">
                                        </span>
                                        <br/>
                                        To Top <span class="sr-only">Back To Top
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <footer class="footer ms-dialogHidden" id="footerDiv">
                        <!--Footer Starts-->
                        <div class="footer-images-bar">
                            <div class="row container">
                                <div class="col-xs-3">
                                    <a href="https://web.archive.org/web/20210910000328/https://ucpi.sco.ca.gov/UCP/" target="_blank">
                                        <img class="img-responsive" alt="Search unclaimed property in California" src="/web/20210910000328im_/https://www.cdph.ca.gov/Style Library/CDPH/images/footer-searchProperty.png">
                                    </a>
                                </div>
                                <div class="col-xs-3">
                                    <a href="https://web.archive.org/web/20210910000328/https://saveourwater.com/" target="_blank">
                                        <img class="img-responsive" alt="Save our Water" src="/web/20210910000328im_/https://www.cdph.ca.gov/Style Library/CDPH/images/saveOurWater.png">
                                    </a>
                                </div>
                                <div class="col-xs-3">
                                    <a href="https://web.archive.org/web/20210910000328/https://energyupgradeca.org/en/" target="_blank">
                                        <img class="img-responsive" alt="Energy Upgrade California" src="/web/20210910000328im_/https://www.cdph.ca.gov/Style Library/CDPH/images/footer-energyUpgrade.png">
                                    </a>
                                </div>
                                <div class="col-xs-3">
                                    <a href="https://web.archive.org/web/20210910000328/https://www.chp.ca.gov/news-alerts/amber-alert" target="_blank">
                                        <img class="img-responsive" alt="Amber Alert California" src="/web/20210910000328im_/https://www.cdph.ca.gov/Style Library/CDPH/images/footer-amberAlert.png">
                                    </a>
                                </div>
                                <!--<div class="col-xs-2">
                                    <a href="https://californiacensus.org/" target="_blank">
                                        <img class="img-responsive" alt="California Census"
                                            src="/Style Library/CDPH/images/footer-cacensus2020.png">
                                    </a>
                                </div>-->
                            </div>
                        </div>
                        <div class="container">
                            <!--      -->
                            <div class="row marginTop10">
                                <div class="col-xs-12 col-sm-3">
                                    <!--GOVERNOR's BANNER START-->
                                    <div class="footerLinkBox">
                                        <div class="profile-banner" style="background-color: #fff !important;">
                                            <div class="inner" style="background: url('https://web.archive.org/web/20210910000328im_/https://california.azureedge.net/cdt/statetemplate/global/images/gov-banner.png') right bottom no-repeat; background-size:250px;">
                                                <div class="banner-subtitle" style="
                                                    font-size: 0.8em;
                                                    /* font-size: .9rem; */
                                                    margin: 0 0 0.2em 0;
                                                    padding: 4px 10px 2px 10px;
                                                    background: linear-gradient(to right,rgba(0,0,0,0.1) 0,rgba(0,0,0,0) 100%);
                                                    ">California Governor
                                                </div>
                                                <div class="banner-title" style="
                                                    font-family: 'Arial Narrow','Helvetica Narrow',Arial,Helvetica,sans-serif;
                                                    font-stretch: condensed;
                                                    font-weight: bold;
                                                    color: #555;
                                                    font-size: 1.1em;           
                                                    /* font-size: 1.2rem; */
                                                    /*padding-right: 105px;*/
                                                    padding-left: 10px;
                                                    ">Gavin Newsom
                                                </div>
                                                <div class="banner-link" style="font-size: 0.9em; padding-left: 12px; margin: 0; padding-top: 10px;">
                                                    <a href="https://web.archive.org/web/20210910000328/https://www.gov.ca.gov/" target="_blank" rel="noopener">Visit Governor&apos;s Website
                                                    </a>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="clearBoth"></div>
                                    </div>
                                    <!--<div class="footerLinkBox">
                                        <div class="footerLnkboxImg">
                                            <img class="img-responsive" alt="The Office of Governor Edmund G. Brown Jr."
                                                src="/Style Library/CDPH/images/footerImg1.png">
                                        </div>
                                        <div class="footerLnkboxTxt">
                                            <span class="text-0dot9em">Office of the Governor
                                                Edmund G. Brown Jr.<br>
                                            </span>
                                            <strong>
                                                <a class="text-0dot85em" href="http://gov.ca.gov/">Visit His Website
                                                </a>
                                            </strong>
                                        </div>
                                        <div class="profile-banner" style="">
                                            <div class="profile-banner" style="background-color: #fff !important;">
                                                <div class="inner"
                                                    style="background: url('https://california.azureedge.net/cdt/statetemplate/global/images/gov-banner.png') right bottom no-repeat; height: 91px;">
                                                    <div class="banner-subtitle" style="
                                                        font-size: 14.4px;
                                                        /* font-size: .9rem; */
                                                        margin: 0 30px .2em 0;
                                                        padding: 4px 35px 2px 10px;
                                                        background: linear-gradient(to right,rgba(0,0,0,0.1) 0,rgba(0,0,0,0) 100%);
                                                        ">California Governor
                                                    </div>
                                                    <div class="banner-title" style="
                                                        font-family: 'Arial Narrow','Helvetica Narrow',Arial,Helvetica,sans-serif;
                                                        font-stretch: condensed;
                                                        font-weight: bold;
                                                        color: #555;
                                                        font-size: 19.2px;
                                                        /* font-size: 1.2rem; */
                                                        padding-right: 70px;
                                                        padding-left: 10px;
                                                        ">Gavin Newsom
                                                    </div>
                                                    <div class="banner-link"
                                                        style="font-size: 11px; padding-left: 12px; margin: 0; padding-top: 10px;">
                                                        <a href="https://www.gov.ca.gov/" target="_blank"
                                                            rel="noopener">Visit Governor's Website
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearBoth">
                                        </div>
                                    </div> -->
                                    <div class="footerLinkBox">
                                        <div class="footerLnkboxImg">
                                            <img class="img-responsive" alt="CDPH Logo" src="/web/20210910000328im_/https://www.cdph.ca.gov/Style Library/CDPH/images/footerImg2.png">
                                        </div>
                                        <div class="footerLnkboxTxt">
                                            <strong><a class="text-0dot85em" href="/web/20210910000328/https://www.cdph.ca.gov/Pages/Welcome%20Message.aspx">Welcome Message</a></strong>
                                        </div>
                                        <div class="clearBoth"></div>
                                    </div>
                                    <!--COVID Image Link Button -->
                                    <div class="footerLinkBox">
                                    <a href="https://web.archive.org/web/20210910000328/https://www.covid19.ca.gov/" target="_blank">
                                        <img class="img-responsive" alt="Covid 19" src="/web/20210910000328im_/https://www.cdph.ca.gov/images/COVID19_Footer_Button.png">
                                    </a>
                                    </div>
                                </div>
                                <!--GOVERNOR's BANNER END-->
                                <div class="col-xs-12 col-sm-2 lineheight1fdot4em" style="color:white;">
                                    <p>
                                        PO Box 997377<br>
                                        MS 0500<br>
                                        Sacramento, CA 95899-7377<br>
                                        
                                    </p>
                                    <p>
                                        <strong>For General Public Information:</strong><br>
                                        <span>(916) 558-1784</span>
                                    </p>
									<p>
                                    <strong>COVID 19 Information Line:</strong><br>
                                  <!-- <span>1-833-544-2374 </span> -->
                                   <span>1-833-4CA4ALL</span><br>
                                   <span>(1-833-422-4255)<span>
                                    </p>
                                </div>
                                <div class="col-xs-12 col-sm-2">
                                    <ul class="footernav">
                                        <li>
                                            <a class="footer-links" href="/web/20210910000328/https://www.cdph.ca.gov/Programs/HRB">Jobs/Careers
                                            </a>
                                        </li>
                                        <li>
                                            <a class="footer-links" href="/web/20210910000328/https://www.cdph.ca.gov/Pages/privacy-policy.aspx">Privacy Policy
                                            </a>
                                        </li>
                                        <li>
                                            <a class="footer-links" href="/web/20210910000328/https://www.cdph.ca.gov/Pages/Conditions-of-Use.aspx">Use Policy
                                            </a>
                                        </li>
                                        <li>
                                            <a class="footer-links" href="/web/20210910000328/https://www.cdph.ca.gov/Pages/Accessibility.aspx">Web Accessibility Certification
                                            </a>
                                        </li>
                                        <li>
                                            <a class="footer-links" href="/web/20210910000328/https://www.cdph.ca.gov/Pages/Sitemap.aspx">Sitemap
                                            </a>
                                        </li>
                                        <li>
                                            <a class="footer-links" href="/web/20210910000328/https://www.cdph.ca.gov/Pages/contact_us.aspx">Feedback
                                            </a>
                                        </li>
                                        
                                        <li><a class="footer-links" href="https://web.archive.org/web/20210910000328/http://get.adobe.com/reader/">Download pdf viewer</a> </li>
                                    </ul>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                   <!-- <h3 class="marginTop0 footer-links">Useful Links</h3>-->
                                   <div class="h3Replace marginTop0 footer-links">Useful Links</div>
                                    <ul class="footernav">
                                        <li>
                                            <a class="footer-links" href="https://web.archive.org/web/20210910000328/https://www.ca.gov/agenciesall/">State Agency Directory
                                            </a>
                                        </li>
                                        <li>
                                            <a class="footer-links" href="https://web.archive.org/web/20210910000328/http://www.chhs.ca.gov/" target="_blank">CA Health &amp; Human Services
                                            </a>
                                        </li>
                                        <li>
                                            <a class="footer-links" target="_blank" href="https://web.archive.org/web/20210910000328/https://www.cdc.gov/">Centers for Disease Control and Prevention
                                            </a>
                                        </li>
                                    </ul>
                                   <div class="socialIcons col-xs-12">
                                        <span class="socialIcon col-sm-3"><a target="_blank" href="https://web.archive.org/web/20210910000328/https://twitter.com/CAPublicHealth">
                                                <i class="fa fa-twitter text-48px"></i>
                                                <span class="sr-only">Twitter</span>
                                            </a>
                                        </span>
                                        <span class="socialIcon col-sm-3"><a target="_blank" href="https://web.archive.org/web/20210910000328/https://www.youtube.com/user/CAPublicHealth">
                                                <i class="fa fa-youtube-play text-48px"></i>
                                                <span class="sr-only">You Tube</span>
                                            </a>
                                        </span>
                                        <span class="socialIcon col-sm-3"><a target="_blank" href="https://web.archive.org/web/20210910000328/https://www.facebook.com/CAPublicHealth">
                                                <i class="fa fa-facebook-f text-48px"></i>
                                                <span class="sr-only">Facebook</span>
                                            </a>
                                            
                                        </span>
                                        <span class="socialIcon col-sm-3"><a target="_blank" href="https://web.archive.org/web/20210910000328/https://instagram.com/capublichealth ">  
 	                                            <i class="fa fa-instagram text-48px"></i>  
 	                                            <span class="sr-only">instagram</span>  
 	                                            </a>  
 	                                    </span>
                                    </div>
                                    <span class="title-bottom">California Department of Public Health</span>
                                </div>
                            </div>
                        </div>
                        <div id="popupdiv"></div>
                    </footer>
                    <!--Footer ends-->
                    <script>
                        //Set swipe
                        function setSwipe() {
                            $(".carousel").swiperight(function () {
                                $(this).carousel('prev');
                            });
                            $(".carousel").swipeleft(function () {
                                $(this).carousel('next');
                            });
                        }

                        setTimeout(setSwipe, 2000);
                        //Search Function
                        function searchResult() {
                            if ($('.txtsearchbox').val() != "") {
                                document.location.href = document.location.protocol + "//" + document.location.host + "/Pages/results.aspx?k=#k=" + $('.txtsearchbox').val();
                            }
                        }

                        //Search on Key press Enter
                        function handleKeyPress(e) {
                            var key = e.keyCode || e.which;
                            if (key == 13) {
                                searchResult();
                            }
                        }
                        var insertButtonHideInterval = "";
                        $(function () {
                            $(".txtsearchbox").on('change keyup paste mouseup', function () {
                                if ($(this).val() === "") {
                                    $('.spn_searchIcon a').css("cursor", "default");
                                }
                                else {
                                    $('.spn_searchIcon a').css("cursor", "pointer");
                                }
                            });
                        });





                        $(function () {
                            $(".txtsearchbox").on('change keyup paste mouseup', function () {
                                if ($(this).val() === "") {
                                    $('.spn_searchIcon a').css("cursor", "default");
                                }
                                else {
                                    $('.spn_searchIcon a').css("cursor", "pointer");
                                }
                            });

                            //////////////////////////////////////////////////////////////////////////////
                            //////////////////////////////////////////////////////////////////////////////
                            //
                            //    Hide another insert tab from ribbon for Content Editor webpart .
                            //
                            //////////////////////////////////////////////////////////////////////////////
                            //////////////////////////////////////////////////////////////////////////////

                            $(".ms-rtestate-write").click(function (e) {

                                try {
                                    if ($('.ms-cui-topBar2 li')[6].id == "Ribbon.WebPartInsert") {
                                        $('.ms-cui-topBar2 li')[6].hidden = true;
                                    }
                                    //$('[id*="Ribbon.EditingTools.CPEditTab.Font"]').find('img').attr('disabled', 'disabled').css('opacity', 0.5);
                                    //$('[id*="Ribbon.EditingTools.CPEditTab.Font"]').find('input').attr('disabled', 'disabled').css('opacity', 0.5);
                                    //$(".ms-rtestate-write").attr({
                                    //    PrefixStyleSheet: 'myprefix-',
                                    //    AllowParagraphFormatting: 'False',
                                    //    AllowFonts: 'False'
                                    //});
                                }
                                catch (err) {

                                    insertButtonHideInterval = setInterval(function () { hideInsertButton(); }, 500);
                                }
                            });
                            function hideInsertButton() {
                                try {
                                    if ($('.ms-cui-topBar2 li')[6].id == "Ribbon.WebPartInsert") {
                                        $('.ms-cui-topBar2 li')[6].hidden = true;
                                        clearInterval(insertButtonHideInterval);
                                    }
                                    //$('[id*="Ribbon.EditingTools.CPEditTab.Font"]').find('img').attr('disabled', 'disabled').css('opacity', 0.5);
                                    //$('[id*="Ribbon.EditingTools.CPEditTab.Font"]').find('input').attr('disabled', 'disabled').css('opacity', 0.5);
                                    //$(".ms-rtestate-write").attr({
                                    //    PrefixStyleSheet: 'myprefix-',
                                    //    AllowParagraphFormatting: 'False',
                                    //    AllowFonts: 'False'
                                    //});
                                }
                                catch (err) { }
                            }

                            //////////////////////////////////////////////////////////////////////////////
                            //////////////////////////////////////////////////////////////////////////////
                            //
                            //    Set Tab Index for each element.
                            //
                            //////////////////////////////////////////////////////////////////////////////
                            //////////////////////////////////////////////////////////////////////////////

                            var tabindex = 1;
                            //$('input,select,div[id*="editableRegion"],div[class*="ms-rtestate-write"],button,span[class*="valid-text"]').each(function () {
                            $('input,select,div[id*="editableRegion"],div[class*="ms-inputuserfield"],div[class*="ms-rtestate-write"],button,textarea[class*="ms-long"],span[class*="valid-text"],textarea[class*="text-field"]').each(function () {
                                if (this.type != "hidden") {
                                    var $input = $(this);
                                    $input.attr("tabindex", tabindex);
                                    tabindex++;
                                }
                            });

                        });


                        //Display add this widget in annonymous mode
                        $(window).load(function () {
                            if ($("#welcomeMenuBox").html() == undefined) {
                                $(".addthis-smartlayers").css("display", "block");
                            }
                            else {
                                $(".addthis-smartlayers").css("display", "none");
                            }
                            //$('img:not([alt])').attr('alt', 'Skip to main content');
                            //$('img[alt=""]').attr('alt', 'Skip to main content');

                        });
                        //Identify the External Links and Open in new window with prompt
                        /*$(window).load(function () {
                            var anchor = $('a');
                            for (var i = 0; i < anchor.length; i++) {

                                if (anchor[i].href.indexOf(location.host) < 0) {

                                    var url = anchor[i].href;
                                    $(anchor[i]).on("click", function (url) {
                                        return confirm("This link will open in new Window");
                                    });

                                    // $(anchor[i])[0].href = "javascript:void(0);";
                                    // $(anchor[i]).removeAttr('href');
                                    // $(anchor[i]).attr('style','cursor:hand');                                   
                                }
                            }
                        });*/
                        //$(window).load(function () {

                        //    $('a').each(function () {
                        //        var a = new RegExp('/' + window.location.host + '/');
                        //        if (!a.test(this.href)) {
                        //            if (this.href.indexOf("javascript:") == -1) {
                        //                $(this).click(function (event) {
                        //                    event.preventDefault();
                        //                    event.stopPropagation();
                        //                    if (confirm("This link will open in new Window")) {
                        //                        window.open(this.href, '_blank');
                        //                    }
                        //                });
                        //            }
                        //        }
                        //    });
                        //});


                        //function redirectToHttps() {
                        //    $('a').each(function () {
                        //        if (this.href.indexOf("contact_us.aspx") > 0 || this.href.indexOf("questions-comments.aspx") > 0 || this.href.indexOf("complaints.aspx") > 0) {
                        //            if (this.href.indexOf("#") == -1) {

                        //                this.href = "https://" + window.location.host + "" + this.href;
                        //                alert(this.href);
                        //            }
                        //        }
                        //        else {
                        //            if (this.href.indexOf("#") == -1) {
                        //                this.href = "http://" + window.location.host + "" + this.href;
                        //            }
                        //        }

                        //    });
                        //}




                        $(document).ready(function () {

                            setBacktoTopPosition();
                            $('#backtoTop a').click(function () {
                                $('#s4-workspace').animate({ scrollTop: 0 }, 'slow');   //this is the main line 
                                $('#backtoTop').addClass('hidden');
                                // alert("hello");
                            });
                            $("a[id*='site_follow_button']").css("display", "none");
                            $("a[id*='_site_share_button']").css("display", "none");

                            //////////////////////////////////////////////////////////////////////////////
                            //////////////////////////////////////////////////////////////////////////////
                            //
                            //    Remeber the screen size for next time.
                            //
                            //////////////////////////////////////////////////////////////////////////////
                            //////////////////////////////////////////////////////////////////////////////

                            $('.topTextSize').click(function () {

                                var fontSize = {};
                                fontSize.font = $("body").css('font-size');
                                localStorage.setItem("FontSize", JSON.stringify(fontSize));
                            });
                            try {
                                var fontSize = JSON.parse(localStorage.getItem("FontSize"));
                                $('body').css("font-size", fontSize.font);

                            }
                            catch (err) { }
                            //End Remeber the screen size for next time.

                            //////////////////////////////////////////////////////////////////////////////
                            //////////////////////////////////////////////////////////////////////////////
                            //
                            //    Increase and Decrease the font size with enable and disable the increse and decrease button
                            //
                            //////////////////////////////////////////////////////////////////////////////
                            //////////////////////////////////////////////////////////////////////////////


                            $('.classminus').addClass('disabled');
                            if (screen.width < 768 && $("body").css('font-size') != "15px") { $('.classminus').removeClass('disabled'); }
                            if ($("body").css('font-size') != "18px") { $('.classminus').removeClass('disabled'); }
                            if (screen.width == 768 && $("body").css('font-size') != "16px") { $('.classminus').removeClass('disabled'); }
                            $('.classplus').click(function () {
                                $('.classminus').removeClass('disabled');
                                if ($("body").css('font-size') === "22px") { $('.classplus').addClass('disabled'); }
                                if (screen.width < 768 && $("body").css('font-size') === "19px") { $('.classplus').addClass('disabled'); }
                                if (screen.width == 768 && $("body").css('font-size') === "20px") { $('.classplus').addClass('disabled'); }
                            });
                            $('.classrestore').click(function () {
                                $('.classminus').addClass('disabled');
                                $('.classplus').removeClass('disabled');


                            });
                            $('.classminus').click(function () {
                                $('.classplus').removeClass('disabled');
                                //$('.classplus').attr( 'class', 'enabled' );
                                if ($("body").css('font-size') === "18px") { $('.classminus').addClass('disabled'); }
                                if (screen.width < 768 && $("body").css('font-size') === "15px") { $('.classminus').addClass('disabled'); }
                                if (screen.width == 768 && $("body").css('font-size') === "16px") { $('.classminus').addClass('disabled'); }

                            });
                            //End of Increase and Decrease the font size with enable and disable the increse and decrease button
                            $('img:not([alt])').attr('alt', 'Skip to main content');
                            $('img[alt=""]').attr('alt', 'Skip to main content');

                            if ($('.txtsearchbox').val() === "") {
                                $('.spn_searchIcon a').css("cursor", "default");
                            }
                            // Remove Tooltip from Contnet Query Web Part
                            $(".js-webpart-titleCell").attr('title', '');


                        });

                        //////////////////////////////////////////////////////////////////////////////
                        //////////////////////////////////////////////////////////////////////////////
                        //
                        // Set Position back to Top
                        //
                        //////////////////////////////////////////////////////////////////////////////
                        //////////////////////////////////////////////////////////////////////////////

                        $("#s4-workspace").scroll(function () {
                            setBacktoTopPosition();
                        });

                        function setBacktoTopPosition() {
                            try {
                                var _cur_top = $('#s4-workspace').scrollTop();
                                //	alert(_cur_top)
                                if (_cur_top == 0)
                                    $("#backtoTop").addClass("hidden");
                                else if (_cur_top > 200)
                                    $("#backtoTop").removeClass("hidden");
                            }
                            catch (err) { }
                        }
                        /***************************************************************************************
                        ********************Enforce Alt Text Functionality*************************************
                        ****************************************************************************************/

                        var form = WebForm_OnSubmit;
                        WebForm_OnSubmit = function () {
                            isValid = checkAltTextForImage();
                            if (isValid) return form.apply(this, arguments);
                            else {
                                jQuery('span[id*=notification_]').remove(); //removes the default 'Saving...' notification 
                                return false;
                            }
                        }

                        function checkAltTextForImage() {
                            var flag = true;
                            var img = document.getElementsByTagName('img')
                            for (var i = 0; i < img.length; i++) {
                                try {
                                    var div = img[i].parentElement.parentElement;
                                    if (div.className.indexOf('ms-rtestate-write') != -1) {
                                        if (img[i].alt == "" || img[i].alt === "Skip to main content") {

                                            var imageName = img[i].src.substring(img[i].src.lastIndexOf('/') + 1, img[i].src.length);
                                            //Create html PouUp
                                            var htmlPopup = '<div class="popup alternatePopup" data-popup="popup-1">';
                                            htmlPopup += '<div class="popup-inner">';
                                            htmlPopup += '<h2>Alternate Text Missing</h2>';
                                            htmlPopup += '<img src="' + img[i].src + '" class="img-responsive popupImage" >';
                                            htmlPopup += '<p>Alternate Text for given Image is missing. To fix this issue please follow the below steps. <br> Please select the Image > Click on Image Tab from Ribbon > Provide the value for ALT Text.</p>';
                                            htmlPopup += '<p><a data-popup-close="popup-1" href="#" onclick="closeMe();">Close</a></p>';
                                            htmlPopup += '<a class="popup-close" data-popup-close="popup-1" href="#" onclick="closeMe();>x</a>';
                                            htmlPopup += '</div>';
                                            htmlPopup += '</div>';

                                            document.getElementById('popupdiv').innerHTML = htmlPopup;
                                            fadeIn(document.getElementsByClassName('alternatePopup')[0], 350);
                                            flag = false;
                                            break;
                                        }
                                    }
                                }
                                catch (err) { }
                            }

                            return flag;
                        }

                        function fadeIn(elem, ms) {
                            if (!elem)
                                return;

                            elem.style.opacity = 0;
                            elem.style.filter = "alpha(opacity=0)";
                            elem.style.display = "inline-block";
                            elem.style.visibility = "visible";

                            if (ms) {
                                var opacity = 0;
                                var timer = setInterval(function () {
                                    opacity += 50 / ms;
                                    if (opacity >= 1) {
                                        clearInterval(timer);
                                        opacity = 1;
                                    }
                                    elem.style.opacity = opacity;
                                    elem.style.filter = "alpha(opacity=" + opacity * 100 + ")";
                                }, 50);
                            }
                            else {
                                elem.style.opacity = 1;
                                elem.style.filter = "alpha(opacity=1)";
                            }
                        }

                        function fadeOut(elem, ms) {
                            if (!elem)
                                return;

                            if (ms) {
                                var opacity = 1;
                                var timer = setInterval(function () {
                                    opacity -= 50 / ms;
                                    if (opacity <= 0) {
                                        clearInterval(timer);
                                        opacity = 0;
                                        elem.style.display = "none";
                                        elem.style.visibility = "hidden";
                                    }
                                    elem.style.opacity = opacity;
                                    elem.style.filter = "alpha(opacity=" + opacity * 100 + ")";
                                }, 50);
                            }
                            else {
                                elem.style.opacity = 0;
                                elem.style.filter = "alpha(opacity=0)";
                                elem.style.display = "none";
                                elem.style.visibility = "hidden";
                            }
                        }

                        $(function () {
                            //----- OPEN
                            $('[data-popup-open]').on('click', function (e) {
                                var targeted_popup_class = jQuery(this).attr('data-popup-open');
                                $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);

                                e.preventDefault();
                            });

                            //----- CLOSE
                            $('[data-popup-close]').on('click', function (e) {
                                var targeted_popup_class = jQuery(this).attr('data-popup-close');
                                $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);

                                e.preventDefault();
                            });


                        });
                        function closeMe() {
                            fadeOut(document.getElementsByClassName('alternatePopup')[0], 350);
                        }
                        /**********************************************************************************************************
                        ***********************End of Enforce Alt Text Functionality*********************************************
                        ***********************************************************************************************************/
                        //////////////////////////////////////////////////////////////////////////////
                        //////////////////////////////////////////////////////////////////////////////
                        //
                        // Set Image in Background using Context menu
                        //
                        //////////////////////////////////////////////////////////////////////////////
                        //////////////////////////////////////////////////////////////////////////////
                        var getSelectedContent = function (editor) {
                            var content = editor.getSelection().getStartElement().getOuterHtml();
                            return (content);
                        }
                      $(document).ready(function () {
                            var img = document.getElementsByTagName('img');
                            for (var i = 0; i < img.length; i++) {
                                try {
                                    var div = img[i].parentElement.parentElement;
                                    if (div.className.indexOf('ms-rtestate-write') != -1) {
                                        var menu = [{
                                            name: 'setBackground',
                                            title: 'Set in background',
                                            className: 'primary',
                                            fun: setBackground(img[i], event)
                                        }];
                                        //img[i].contextMenu(menu);

                                    }
                                    
                                    var altext=img[i].alt;
                                    var imgtitle = img[i].title;
                                    if(altext != "" && imgtitle == "" )
                                    {
                                    	img[i].title = altext;
                                    }
                                }
                                catch (err) { }
                            }

                        });
                        function setBackground(ctrl, e) {
                            degugger;
                        }

                        //////////////////////////////////////////////////////////////////////////////
                        //////////////////////////////////////////////////////////////////////////////
                        //
                        // Disable Fonts from Content Editor Webpart Ribbon
                        //
                        //////////////////////////////////////////////////////////////////////////////
                        //////////////////////////////////////////////////////////////////////////////


                        ExecuteOrDelayUntilScriptLoaded(rte_dialog_pub, "sp.ui.rte.publishing.js"); //for dialogs on publishing pages

                        function rte_dialog_pub() {
                            //$(".ms-rtestate-write").attr('PrefixStyleSheet', 'myprefix-');
                            //$(".ms-rtestate-write").attr('AllowParagraphFormatting', 'False');
                            $(".ms-rtestate-write").attr('AllowFonts', 'False');
                        }

                        //////////////////////////////////////////////////////////////////////////////
                        //////////////////////////////////////////////////////////////////////////////
                        //
                        // Exclude Spell Check from People Picker
                        //
                        //////////////////////////////////////////////////////////////////////////////
                        //////////////////////////////////////////////////////////////////////////////

                        window.onload = function () {
                            try {
                                var a = document.getElementById('excludeContentFromSpellCheck');

                                var b = a.getElementsByTagName('input');
                                for (var i = 0; i < b.length; i++) {
                                    b[i].setAttribute("excludeFromSpellCheck", "true");
                                }

                                var c = a.getElementsByTagName('textarea');
                                for (var i = 0; i < c.length; i++) {
                                    c[i].setAttribute("excludeFromSpellCheck", "true");
                                }
                            }
                            catch (err) {

                            }

                        };

                        ///////////////////////////////////////////////////////////////////////////////////////
                        //////        Open External link in new window
                        //////////////////////////////////////////////////////////////////////////////////////
                        var isRedirectionDone = false;
                        var RedirectionTimer;
                        $(window).load(function () {
                            $('a').each(function () {
                                var a = new RegExp('/' + window.location.host + '/');
                                if (!a.test(this.href)) {
                                    if (this.href != "") {
                                        if (this.href.indexOf("javascript:") == -1) {
                                            if (this.href.indexOf("#") == -1) {
                                                $(this).click(function (event) {
                                                    event.preventDefault();
                                                    event.stopPropagation();
                                                    var url = this.href;
                                                    var htmlPopup = '<div class="popup alternatePopup" data-popup="popup-1">';
                                                    htmlPopup += '<div class="popup-inner">';
                                                    htmlPopup += '<h2>You are now leaving CDPH Site</h2>';
                                                    //htmlPopup += '<img src="' + img[i].src + '" class="img-responsive popupImage" >';
                                                    htmlPopup += '<p>You will be automatically redirected to: <br> <a href="' + this.href + '">' + this.href + '</a></p>';
                                                    htmlPopup += '<p>in 10 seconds or you may click "Continue to this site"</p>';
                                                    htmlPopup += '<p><a data-popup-close="popup-1" href="#" onclick="Cancel();">Cancel</a>';
                                                    htmlPopup += '<a data-popup-close="popup-1" href="#" onclick=Continue("' + this.href + '"); style="float:right;">Continue to this site</a>    </p>';
                                                    htmlPopup += '<a class="popup-close" data-popup-close="popup-1" href="#" onclick="closeMe();>x</a>';
                                                    htmlPopup += '</div>';
                                                    htmlPopup += '</div>';

                                                    document.getElementById('popupdiv').innerHTML = htmlPopup;
                                                    fadeIn(document.getElementsByClassName('alternatePopup')[0], 350);
                                                    isRedirectionDone = false;
                                                    RedirectionTimer = setInterval(function () { redirectPage("" + url + ""); }, 10000);
                                                });
                                            }
                                        }
                                    }
                                }
                                else {
                                    try {
                                        if (this.attributes["aria-rel"].nodeValue == "External") {
                                            $(this).click(function (event) {
                                                event.preventDefault();
                                                event.stopPropagation();
                                                var url = this.href;
                                                var htmlPopup = '<div class="popup alternatePopup" data-popup="popup-1">';
                                                htmlPopup += '<div class="popup-inner">';
                                                htmlPopup += '<h2>This link will open up in New window</h2>';
                                                //htmlPopup += '<img src="' + img[i].src + '" class="img-responsive popupImage" >';
                                                htmlPopup += '<p>You will be automatically redirected to: <br> <a href="' + this.href + '">' + this.href + '</a></p>';
                                                htmlPopup += '<p>in 10 seconds or you may click "Continue to this site"</p>';
                                                htmlPopup += '<p><a data-popup-close="popup-1" href="#" onclick="Cancel();">Cancel</a>';
                                                htmlPopup += '<a data-popup-close="popup-1" href="#" onclick=Continue("' + this.href + '"); style="float:right;">Continue to this site</a>    </p>';
                                                htmlPopup += '<a class="popup-close" data-popup-close="popup-1" href="#" onclick="closeMe();>x</a>';
                                                htmlPopup += '</div>';
                                                htmlPopup += '</div>';

                                                document.getElementById('popupdiv').innerHTML = htmlPopup;
                                                fadeIn(document.getElementsByClassName('alternatePopup')[0], 350);
                                                isRedirectionDone = false;
                                                RedirectionTimer = setInterval(function () { redirectPage("" + url + ""); }, 10000);
                                            });
                                        }
                                    }
                                    catch (err) { }

                                }
                            });
                        });
                        function Continue(url) {
                            fadeOut(document.getElementsByClassName('alternatePopup')[0], 350);
                            isRedirectionDone = true;
                            window.clearInterval(RedirectionTimer);
                            window.open(url, '_blank');
                        }
                        function Cancel() {
                            isRedirectionDone = true;
                            window.clearInterval(RedirectionTimer);
                            fadeOut(document.getElementsByClassName('alternatePopup')[0], 350);
                        }
                        function redirectPage(url) {
                            if (!isRedirectionDone) {
                                isRedirectionDone = true;
                                fadeOut(document.getElementsByClassName('alternatePopup')[0], 350);
                                window.clearInterval(RedirectionTimer);
                                window.open(url, '_blank');
                            }
                        }

                        ///////////////////////////////////////////////////////////////////////////////////////
                        //////        Open External link in new window Code Closed
                        //////////////////////////////////////////////////////////////////////////////////////

                    </script>
                    <noscript></noscript>


                </div>
            </div>
            <!--end workspace-->
            <div id="DeltaFormDigest">
	
                
            <script type="text/javascript">//<![CDATA[
        var formDigestElement = document.getElementsByName('__REQUESTDIGEST')[0];
        if (!((formDigestElement == null) || (formDigestElement.tagName.toLowerCase() != 'input') || (formDigestElement.type.toLowerCase() != 'hidden') ||
            (formDigestElement.value == null) || (formDigestElement.value.length <= 0)))
        {
            formDigestElement.value = '0x051A1F3B8E1207FDDD08F414356C4EA61560BBEABF26123422E7E6B5C7343EC7332E1FCE5B835393FBFE8343048231FB73BA0E9C42A44EE62D96338EABB612C9,10 Sep 2021 00:03:30 -0000';
            g_updateFormDigestPageLoaded = new Date();
        }
        //]]>
        </script>
        
            
</div>
        

<script type="text/javascript">
//<![CDATA[
var _spFormDigestRefreshInterval = 1440000;window.g_updateFormDigestPageLoaded = new Date(); window.g_updateFormDigestPageLoaded.setDate(window.g_updateFormDigestPageLoaded.getDate() -5);
var callBackFrameUrl='/WebResource.axd?d=F8F6KTcBDD_h56O0XzFG4wP9fimzW2y5A-jRkf4j_TR1gY2PF86MZ4mPgBvC2AMoPHZ4okJFLrYCVGXQOVxT9XfWwVPqf_PI7S88M5rHpxM1&t=637453780939909757';
WebForm_InitCallback();var _fV4UI = true;
function _RegisterWebPartPageCUI()
{
    var initInfo = {editable: false,isEditMode: false,allowWebPartAdder: false,listId: "{6aad4e4a-f5c6-484e-8d56-fd7746cd9d60}",itemId: 1133,recycleBinEnabled: true,enableMinorVersioning: true,enableModeration: true,forceCheckout: true,rootFolderUrl: "\u002fPrograms\u002fCID\u002fDCDC\u002fPages",itemPermissions:{High:16,Low:2147680353}};
    SP.Ribbon.WebPartComponent.registerWithPageManager(initInfo);
    var wpcomp = SP.Ribbon.WebPartComponent.get_instance();
    var hid;
    hid = document.getElementById("_wpSelected");
    if (hid != null)
    {
        var wpid = hid.value;
        if (wpid.length > 0)
        {
            var zc = document.getElementById(wpid);
            if (zc != null)
                wpcomp.selectWebPart(zc, false);
        }
    }
    hid = document.getElementById("_wzSelected");
    if (hid != null)
    {
        var wzid = hid.value;
        if (wzid.length > 0)
        {
            wpcomp.selectWebPartZone(null, wzid);
        }
    }
};
function __RegisterWebPartPageCUI() {
ExecuteOrDelayUntilScriptLoaded(_RegisterWebPartPageCUI, "sp.ribbon.js");}
_spBodyOnLoadFunctionNames.push("__RegisterWebPartPageCUI");var __wpmExportWarning='This Web Part Page has been personalized. As a result, one or more Web Part properties may contain confidential information. Make sure the properties contain information that is safe for others to read. After exporting this Web Part, view properties in the Web Part description file (.WebPart) by using a text editor such as Microsoft Notepad.';var __wpmCloseProviderWarning='You are about to close this Web Part.  It is currently providing data to other Web Parts, and these connections will be deleted if this Web Part is closed.  To close this Web Part, click OK.  To keep this Web Part, click Cancel.';var __wpmDeleteWarning='You are about to permanently delete this Web Part.  Are you sure you want to do this?  To delete this Web Part, click OK.  To keep this Web Part, click Cancel.';function _cUpdx74bbee6762f840edad4fe46f496d8c78(){var myd = null; if (typeof(datax74bbee6762f840edad4fe46f496d8c78) != 'undefined') {myd = datax74bbee6762f840edad4fe46f496d8c78;} var myc = document.getElementById('ctl00_x74bbee6762f840edad4fe46f496d8c78');_cUpdcx74bbee6762f840edad4fe46f496d8c78(myd, myc);}function _cUpdcx74bbee6762f840edad4fe46f496d8c78(data, ctrl){SiteLogoImagePageUpdate(ctrl, data);}
            ExecuteOrDelayUntilScriptLoaded(
                function() 
                {                    
                    Srch.ScriptApplicationManager.get_current().states = {"webUILanguageName":"en-US","webDefaultLanguageName":"en-US","contextUrl":"https://web.archive.org/web/20210910000328/https://www.cdph.ca.gov/Programs/CID/DCDC","contextTitle":"Division of Communicable Disease Control","supportedLanguages":[{"id":1025,"label":"Arabic"},{"id":1093,"label":"Bengali"},{"id":1026,"label":"Bulgarian"},{"id":1027,"label":"Catalan"},{"id":2052,"label":"Chinese (Simplified)"},{"id":1028,"label":"Chinese (Traditional)"},{"id":1050,"label":"Croatian"},{"id":1029,"label":"Czech"},{"id":1030,"label":"Danish"},{"id":1043,"label":"Dutch"},{"id":1033,"label":"English"},{"id":1035,"label":"Finnish"},{"id":1036,"label":"French"},{"id":1031,"label":"German"},{"id":1032,"label":"Greek"},{"id":1095,"label":"Gujarati"},{"id":1037,"label":"Hebrew"},{"id":1081,"label":"Hindi"},{"id":1038,"label":"Hungarian"},{"id":1039,"label":"Icelandic"},{"id":1057,"label":"Indonesian"},{"id":1040,"label":"Italian"},{"id":1041,"label":"Japanese"},{"id":1099,"label":"Kannada"},{"id":1042,"label":"Korean"},{"id":1062,"label":"Latvian"},{"id":1063,"label":"Lithuanian"},{"id":1086,"label":"Malay"},{"id":1100,"label":"Malayalam"},{"id":1102,"label":"Marathi"},{"id":1044,"label":"Norwegian"},{"id":1045,"label":"Polish"},{"id":1046,"label":"Portuguese (Brazil)"},{"id":2070,"label":"Portuguese (Portugal)"},{"id":1094,"label":"Punjabi"},{"id":1048,"label":"Romanian"},{"id":1049,"label":"Russian"},{"id":3098,"label":"Serbian (Cyrillic)"},{"id":2074,"label":"Serbian (Latin)"},{"id":1051,"label":"Slovak"},{"id":1060,"label":"Slovenian"},{"id":3082,"label":"Spanish (Spain)"},{"id":2058,"label":"Spanish (Mexico)"},{"id":1053,"label":"Swedish"},{"id":1097,"label":"Tamil"},{"id":1098,"label":"Telugu"},{"id":1054,"label":"Thai"},{"id":1055,"label":"Turkish"},{"id":1058,"label":"Ukrainian"},{"id":1056,"label":"Urdu"},{"id":1066,"label":"Vietnamese"}],"navigationNodes":[{"id":0,"name":"This Site","url":"~site/_layouts/15/osssearchresults.aspx?u={contexturl}","promptString":"Search this site"}],"showAdminDetails":false,"defaultPagesListName":"Pages","isSPFSKU":false,"userAdvancedLanguageSettingsUrl":"?ShowAdvLang=1","defaultQueryProperties":{"culture":1033,"uiLanguage":1033,"summaryLength":180,"desiredSnippetLength":90,"enableStemming":true,"enablePhonetic":false,"enableNicknames":false,"trimDuplicates":true,"bypassResultTypes":false,"enableInterleaving":true,"enableQueryRules":true,"processBestBets":true,"enableOrderingHitHighlightedProperty":false,"hitHighlightedMultivaluePropertyLimit":-1,"processPersonalFavorites":true}};
                    Srch.U.trace(null, 'SerializeToClient', 'ScriptApplicationManager state initialized.');
                }, 'Search.ClientControls.js');var g_clientIdDeltaPlaceHolderMain = "DeltaPlaceHolderMain";
var g_clientIdDeltaPlaceHolderPageTitleInTitleArea = "DeltaPlaceHolderPageTitleInTitleArea";
var g_clientIdDeltaPlaceHolderUtilityContent = "DeltaPlaceHolderUtilityContent";

                    ExecuteOrDelayUntilScriptLoaded(
                        function() 
                        {
                            if ($isNull($find('ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr')))
                            {
                                var sb = $create(Srch.SearchBox, {"delayLoadTemplateScripts":true,"initialPrompt":"Search this site","messages":[],"navigationNodes":[{"id":0,"name":"This Site","url":"~site/_layouts/15/osssearchresults.aspx?u={contexturl}","promptString":"Search this site"}],"queryGroupNames":["MasterPage"],"renderTemplateId":"~sitecollection/_catalogs/masterpage/Display Templates/Search/Control_SearchBox_Compact.js","resultsPageAddress":"~site/_layouts/15/osssearchresults.aspx?u={contexturl}","serverInitialRender":true,"showDataErrors":true,"showNavigation":true,"states":{},"tryInplaceQuery":false}, null, null, $get("ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr"));
                                sb.activate('Search this site', 'ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr_sbox', 'ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr_sboxdiv', 'ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr_NavButton', 'ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr_AutoCompList', 'ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr_NavDropdownList', 'ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr_SearchLink', 'ms-srch-sbprogress', 'ms-srch-sb-prompt ms-helperText');
                            }
                        }, 'Search.ClientControls.js');g_QuickLaunchControlIds.push("zz1_TopNavigationMenu");_spBodyOnLoadFunctionNames.push('QuickLaunchInitDroppable'); var g_zz1_TopNavigationMenu = null; function init_zz1_TopNavigationMenu() { if (g_zz1_TopNavigationMenu == null) g_zz1_TopNavigationMenu = $create(SP.UI.AspMenu, null, null, null, $get('zz1_TopNavigationMenu')); } ExecuteOrDelayUntilScriptLoaded(init_zz1_TopNavigationMenu, 'SP.Core.js');
g_QuickLaunchControlIds.push("zz3_V4QuickLaunchMenu");_spBodyOnLoadFunctionNames.push('QuickLaunchInitDroppable'); var g_zz3_V4QuickLaunchMenu = null; function init_zz3_V4QuickLaunchMenu() { if (g_zz3_V4QuickLaunchMenu == null) g_zz3_V4QuickLaunchMenu = $create(SP.UI.AspMenu, null, null, null, $get('zz3_V4QuickLaunchMenu')); } ExecuteOrDelayUntilScriptLoaded(init_zz3_V4QuickLaunchMenu, 'SP.Core.js');
//]]>
</script>
</form>
        <span id="DeltaPlaceHolderUtilityContent">
            
        </span>
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        <script type="text/javascript" src="//web.archive.org/web/20210910000328js_/https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5900c384ba88d62d"></script>
    </body>
</html><script id="f5_cspm">(function(){var f5_cspm={f5_p:'DGHPJDNKMFIBLAEMFPJEECHOGHLGCJIAEPHKLECDFGAIICLPOAHENAAGAECCCHHGABNBJNPGAADNIGJJODDAIECFAAAFKJCJEHDFIAJBAOJKDCPBNKODEOPNKMNHJOPE',setCharAt:function(str,index,chr){if(index>str.length-1)return str;return str.substr(0,index)+chr+str.substr(index+1);},get_byte:function(str,i){var s=(i/16)|0;i=(i&15);s=s*32;return((str.charCodeAt(i+16+s)-65)<<4)|(str.charCodeAt(i+s)-65);},set_byte:function(str,i,b){var s=(i/16)|0;i=(i&15);s=s*32;str=f5_cspm.setCharAt(str,(i+16+s),String.fromCharCode((b>>4)+65));str=f5_cspm.setCharAt(str,(i+s),String.fromCharCode((b&15)+65));return str;},set_latency:function(str,latency){latency=latency&0xffff;str=f5_cspm.set_byte(str,40,(latency>>8));str=f5_cspm.set_byte(str,41,(latency&0xff));str=f5_cspm.set_byte(str,35,2);return str;},wait_perf_data:function(){try{var wp=window.performance.timing;if(wp.loadEventEnd>0){var res=wp.loadEventEnd-wp.navigationStart;if(res<60001){var cookie_val=f5_cspm.set_latency(f5_cspm.f5_p,res);window.document.cookie='f5avr0285567767aaaaaaaaaaaaaaaa_cspm_='+encodeURIComponent(cookie_val)+';path=/';}
return;}}
catch(err){return;}
setTimeout(f5_cspm.wait_perf_data,100);return;},go:function(){var chunk=window.document.cookie.split(/\s*;\s*/);for(var i=0;i<chunk.length;++i){var pair=chunk[i].split(/\s*=\s*/);if(pair[0]=='f5_cspm'&&pair[1]=='1234')
{var d=new Date();d.setTime(d.getTime()-1000);window.document.cookie='f5_cspm=;expires='+d.toUTCString()+';path=/;';setTimeout(f5_cspm.wait_perf_data,100);}}}}
f5_cspm.go();}());</script><!--
     FILE ARCHIVED ON 00:03:28 Sep 10, 2021 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 05:44:20 Apr 25, 2022.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
-->
<!--
playback timings (ms):
  captures_list: 219.867
  exclusion.robots: 0.135
  exclusion.robots.policy: 0.124
  RedisCDXSource: 3.459
  esindex: 0.01
  LoadShardBlock: 187.07 (3)
  PetaboxLoader3.datanode: 96.294 (4)
  CDXLines.iter: 19.804 (3)
  load_resource: 114.924
  PetaboxLoader3.resolve: 79.926
-->