<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us" dir="ltr" __expr-val-dir="ltr"><!-- InstanceBegin template="/Templates/covid.dwt" codeOutsideHTMLIsLocked="false" --><head><script src="//archive.org/includes/analytics.js?v=cf34f82" type="text/javascript"></script>
<script type="text/javascript">window.addEventListener('DOMContentLoaded',function(){var v=archive_analytics.values;v.service='wb';v.server_name='wwwb-app214.us.archive.org';v.server_ms=259;archive_analytics.send_pageview({});});</script>
<script type="text/javascript" src="/_static/js/bundle-playback.js?v=36gO9Ebf" charset="utf-8"></script>
<script type="text/javascript" src="/_static/js/wombat.js?v=UHAOicsW" charset="utf-8"></script>
<script type="text/javascript">
  __wm.init("https://web.archive.org/web");
  __wm.wombat("http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx","20210626042007","https://web.archive.org/","web","/_static/",
	      "1624681207");
</script>
<link rel="stylesheet" type="text/css" href="/_static/css/banner-styles.css?v=fantwOh2" />
<link rel="stylesheet" type="text/css" href="/_static/css/iconochive.css?v=qtvMKcIJ" />
<!-- End Wayback Rewrite JS Include -->

<!-- InstanceBeginEditable name="doctitle" -->
<title> Alaska Department of Health and Social Services </title>
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="Expires" content="0"/>
<meta http-equiv="x-ua-compatible" content="IE=Edge"/>
<link rel="shortcut icon" href="https://web.archive.org/web/20210626042007im_/http://dhss.alaska.gov/outage/Style Library/DhssZen/Images/favicon.ico" type="image/vnd.microsoft.icon"/>
<link rel="stylesheet" type="text/css" href="https://web.archive.org/web/20210626042007cs_/http://dhss.alaska.gov/outage/Style Library/DhssZen/css/controls.css"/>
<link rel="stylesheet" type="text/css" href="https://web.archive.org/web/20210626042007cs_/http://dhss.alaska.gov/outage/Style Library/DhssZen/css/corev4.css"/>
<link rel="stylesheet" type="text/css" href="https://web.archive.org/web/20210626042007cs_/http://dhss.alaska.gov/outage/Style Library/DhssZen/css/Menu.css"/>
<link rel="stylesheet" type="text/css" href="https://web.archive.org/web/20210626042007cs_/http://dhss.alaska.gov/outage/Style Library/DhssZen/css/page-layouts-21.css"/>
<link rel="stylesheet" type="text/css" href="https://web.archive.org/web/20210626042007cs_/http://dhss.alaska.gov/outage/Style Library/DhssZen/css/reset.css"/>
<link rel="stylesheet" type="text/css" href="https://web.archive.org/web/20210626042007cs_/http://dhss.alaska.gov/outage/Style Library/DhssZen/css/DhssZen.css"/>
<link rel="stylesheet" type="text/css" href="https://web.archive.org/web/20210626042007cs_/http://dhss.alaska.gov/outage/Style Library/DhssZen/css/soa-structure.css"/>
<link rel="stylesheet" type="text/css" href="https://web.archive.org/web/20210626042007cs_/http://dhss.alaska.gov/outage/Style Library/DhssZen/css/dhss-content.css"/>
<link rel="stylesheet" type="text/css" href="https://web.archive.org/web/20210626042007cs_/http://dhss.alaska.gov/outage/Style Library/DhssZen/css/dhss-legacy.css"/>
<script type="text/javascript" src="https://web.archive.org/web/20210626042007js_/http://dhss.alaska.gov/outage/jquery-1.js"></script>
<script type="text/javascript" src="https://web.archive.org/web/20210626042007js_/http://dhss.alaska.gov/outage/menu.js"></script>
<script type="text/javascript" src="https://web.archive.org/web/20210626042007js_/http://dhss.alaska.gov/outage/init.js"></script>
<script type="text/javascript" src="https://web.archive.org/web/20210626042007js_/http://dhss.alaska.gov/outage/msstring.js"></script>
<script type="text/javascript" src="https://web.archive.org/web/20210626042007js_/http://dhss.alaska.gov/outage/ie55up.js"></script>
<script type="text/javascript" src="https://web.archive.org/web/20210626042007js_/http://dhss.alaska.gov/outage/jquery-1_002.js"></script>
<script type="text/javascript" src="https://web.archive.org/web/20210626042007js_/http://dhss.alaska.gov/outage/jquery-noconflict.js"></script>
<script type="text/javascript" src="https://web.archive.org/web/20210626042007js_/http://dhss.alaska.gov/outage/listimport.js"></script>
<script type="text/javascript" src="https://web.archive.org/web/20210626042007js_/http://dhss.alaska.gov/outage/ScriptResource_002.js"></script>
<script type="text/javascript" src="https://web.archive.org/web/20210626042007js_/http://dhss.alaska.gov/outage/blank.js"></script>
<script type="text/javascript" src="https://web.archive.org/web/20210626042007js_/http://dhss.alaska.gov/outage/ScriptResource.js"></script>
<meta name="google-site-verification" content="jPGd8CiqqcBFgCHw2lVALvEzN1eZi3hHuHZGv3s0lW0"/>
<style type="text/css">


</style>
<script type="text/javascript" src="https://web.archive.org/web/20210626042007js_/http://dhss.alaska.gov/outage/core.js"></script>
<script type="text/javascript" src="https://web.archive.org/web/20210626042007js_/http://dhss.alaska.gov/outage/sp.js"></script>
<script type="text/javascript" src="https://web.archive.org/web/20210626042007js_/http://dhss.alaska.gov/outage/ScriptResx.js"></script>
<script type="text/javascript" src="https://web.archive.org/web/20210626042007js_/http://dhss.alaska.gov/outage/cui.js"></script>
<script type="text/javascript" src="https://web.archive.org/web/20210626042007js_/http://dhss.alaska.gov/outage/sp_003.js"></script>
<script type="text/javascript" src="https://web.archive.org/web/20210626042007js_/http://dhss.alaska.gov/outage/sp_005.js"></script>
<script type="text/javascript" src="https://web.archive.org/web/20210626042007js_/http://dhss.alaska.gov/outage/sp_002.js"></script>
<script type="text/javascript" src="https://web.archive.org/web/20210626042007js_/http://dhss.alaska.gov/outage/inplview.js"></script>
<script type="text/javascript" src="https://web.archive.org/web/20210626042007js_/http://dhss.alaska.gov/outage/sp_004.js"></script>
<script type="text/javascript" src="https://web.archive.org/web/20210626042007js_/http://dhss.alaska.gov/outage/ScriptResx_002.js"></script>
<script type="text/javascript" src="https://web.archive.org/web/20210626042007js_/http://dhss.alaska.gov/outage/mdn.js"></script>

<!-- InstanceBeginEditable name="head" -->
	<!-- <link rel="stylesheet" type="text/css" href="http://dhss.alaska.gov/outage/Style%20Library/DhssZen/css/alt_campaign.css" /> --> 
<!-- InstanceEndEditable -->


</head>

<body>

<!-- =====  Start Main Area ============================================================ -->
<div id="s4-bodyContainer">

<a href="#soa-content" id="soa-skip-content">Skip to content</a>
<div id="top-wrapper">
	<div id="soa-bar">
		<a id="soa-title" href="https://web.archive.org/web/20210626042007/http://www.alaska.gov/" name="soa">State of Alaska</a>
		<ul class="menu">
	      <li><a href="https://web.archive.org/web/20210626042007/https://my.alaska.gov/">myAlaska</a></li>
	      <li><a href="https://web.archive.org/web/20210626042007/http://www.alaska.gov/akdir1.html">My Government</a></li>
	      <li><a href="https://web.archive.org/web/20210626042007/http://www.alaska.gov/residentHome.html">Resident</a></li>
	      <li><a href="https://web.archive.org/web/20210626042007/http://www.alaska.gov/businessHome.html">Business in Alaska</a></li>
	      <li><a href="https://web.archive.org/web/20210626042007/http://www.alaska.gov/visitorHome.html">Visiting Alaska</a></li>
	      <li><a href="https://web.archive.org/web/20210626042007/http://www.alaska.gov/employeeHome.html">State Employees</a></li>
		</ul>
	</div>

	<div id="soa-header">
  		<a href="https://web.archive.org/web/20210626042007/http://dhss.alaska.gov/"><img class="soa-banner" src="https://web.archive.org/web/20210626042007im_/http://dhss.alaska.gov/outage/Header_DHSS.jpg" alt="Health and Social Services" style="border-width:0px;"/></a>

	     <div id="soa-search">
                            <input id="soa-searchquery" name="q" title="Search" type="text"/>
                            <input type="button" id="soa-searchsubmit" alt="Search"/>
                            <input value="DHSS" name="client" type="hidden"/>
                            <input value="DHSS" name="proxystylesheet" type="hidden"/>
                            <input value="date:D:L:d1" name="sort" type="hidden"/>
                            <input value="xml_no_dtd" name="output" type="hidden"/>
                            <input value="UTF-8" name="ie" type="hidden"/>
                            <input value="UTF-8" name="oe" type="hidden"/>
                        </div>
	</div>



	<div id="soa-breadcrumbs">
		<span><a href="#ctl00_ctl37_SkipLink"><img alt="Skip Navigation Links" src="https://web.archive.org/web/20210626042007im_/http://dhss.alaska.gov/outage/spacer.gif" style="border-width:0px;" width="0" height="0"/></a>Health and Social Services</span>
	</div>
</div>

<div id="content-wrapper">
    <div id="soa-content">
   

 <!-- s4-ca is the main body div -->
	

<div class="s4-ca">
		
<div id="DhssZenContent">
<div id="menu" class="campaign" style="margin-top:-10px; padding-bottom:15px;">
<ul id="menu_list" style="list-style: outside none none; font-size:120%;">
<li id="first_nav"><a href="https://web.archive.org/web/20210626042007/http://dhss.alaska.gov/dph/epi/id/pages/covid-19/default.aspx"><img src="https://web.archive.org/web/20210626042007im_/http://dhss.alaska.gov/dph/epi/id/PublishingImages/COVID-19/bug_COVID.png" width="125" alt="COVID-19"/> Home</a>
	</li>
<li><a href="vaccineappointments.aspx">Vaccine</a>
<div style="font-size: 80%;">
<ul>
<li><a href="vaccineappointments.aspx#prepmod">Appointments with select providers</a>
</li>

<li><a href="vaccineappointments.aspx#all">All COVID-19 vaccine providers </a>
</li>
	
	<li><a href="vaccineappointments.aspx#call">Call for support</a>
</li>
	
	
<li><a href="sleevesup.aspx">Sleeves Up for Summer</a>
</li>
</ul>
</div>
</li>
	
	
<li><a href="https://web.archive.org/web/20210626042007/https://covid19.alaska.gov/">State of Alaska COVID-19<br/></a></li>
<li><a href="https://web.archive.org/web/20210626042007/http://cdc.gov/covid19">CDC COVID-19</a></li>
</ul>
</div>


	

<div class="grid4 covid" role="main"> <script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
}
</script>
	<!-- InstanceBeginEditable name="covidmain" -->
	
	<a href="Vaccine.aspx"><p class="buttons" style="font-size: 80%; width: 37%; margin-bottom: 20px;">
Return to Alaska COVID-19 Vaccine Homepage
</p></a>

<h1><strong>COVID-19 Vaccine Information for all Alaskans&#160;</strong></h1>

<p><em>Updated May 13, 2021<br/></em></p>
<p></p>
<div class="dz-Element-Box"><ul><li><span><a href="https://web.archive.org/web/20210626042007/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/safety/JJUpdate.html">J&amp;J/Janssen
 update: CDC and FDA have recommended that use of Johnson &amp; 
Johnsonâ€™s Janssen (J&amp;J/Janssen) COVID-19 Vaccine resume in the 
United States, effective April 23, 2021</a></span><span><br/></span></li></ul></div>
<div><em></em></div>
<p></p>
<p>On this page:</p>
<ul><li><a href="#whoandwhen">Who should get vaccinated now</a></li>

<li><a href="#whattoexpectat">Planning for and scheduling your appointment</a></li>
<li><a href="#safe">Safety and efficacy</a></li>

<li><a href="#mandate">Health advisories and mandates (including travel)</a></li>
<li><a href="#vaccinationefforts">About Alaska&#39;s vaccination effort</a></li>

<li><a href="#whattoexpectafter">What changes after you are vaccination</a></li>

<li><a href="#contacts">Contact information</a><br/></li></ul>


<h2 id="whoandwhen">Who  should get vaccinated now</h2>
<ul type="disc">
  <li><a href="#who">Who can get the vaccine now, and when will I be able to?</a></li>
<li><span><a href="#already">Should I get vaccinated if I already had COVID-19?</a><span style="display: inline-block;"></span></span><br/></li>
  
  <li><a href="#Outside">Iâ€™m not currently in Alaska. What should I do to get       vaccinated?</a></li>
  <li><a href="#othervaccines">Can I get a COVID-19 vaccine if Iâ€™ve recently received       the flu vaccine, or any other vaccine?</a></li>
<li><a href="#underlying">If I have an underlying medical condition, can I get vaccinated?</a><br/></li>
  
</ul>

<h3 id="who">Who can get the vaccine now, and when will I be able to?</h3>
<p>The COVID-19 vaccine is now available to anyone living or working in Alaska who is age 12 or older. Pfizer vaccine is authorized for ages 12 and up. Moderna and Johnson &amp; Johnson (Janssen) vaccines are authorized for ages 18 and up. Starting June 1, travelers to Alaska will also be eligible to be vaccinated. </p>
<h3 id="already">Should I get vaccinated if I already had COVID-19?</h3>
<p>Yes. Not enough is yet known about how long immunity from natural COVID-19 illness might last and protect you from becoming re-infected and spreading the illness to others. For that reason, even if you have already been infected, vaccination is an important step to protect yourself and those around you.<br/><strong></strong></p>
<em></em>
<p></p>
<h3 id="Outside"><strong>Iâ€™m not currently in Alaska. What should I do to get  vaccinated?</strong></h3>
<p>Youâ€™ll want to follow the guidelines for where you are  located. States and local jurisdictions will have different guidance than what youâ€™ll  see in Alaska; follow the guidance of the community you are in. Youâ€™ll  want to plan to get both doses of vaccine with the same healthcare provider. </p>


<h3 id="othervaccines">Can I get a COVID-19 vaccine if Iâ€™ve recently received the flu vaccine, or any other vaccine?</h3>
<p>The Centers for Disease Control and Prevention (CDC) does not recommend getting the COVID-19 vaccine within 2 weeks of any other vaccine because insufficient data currently exists on the safety and efficacy of mRNA COVID-19 vaccines administered simultaneously with other vaccines. However, CDC has also recommended not to deny the COVID vaccine to someone who recently had another vaccine and is at high risk for COVID-19 at this time.</p>
<h3 id="underlying">If I have an underlying medical condition, can I get vaccinated?</h3>
<p>Yes, people with underlying medical conditions can receive a COVID-19 vaccine as long as they have not had an immediate or severe allergic reaction to a COVID-19 vaccine or to any of the ingredients in the vaccine. Vaccination is an important consideration for people ages 16 and older with certain underlying medical conditions because they are at increased risk for severe illness from COVID-19.&#160;</p>
<p></p>
<h2 class="dz-Element-h2">Information for parents/guardians, pregnant and breastfeeding people</h2>
<h3 class="dz-Element-h3">Which COVID-19 vaccine can my child get?</h3>
<p class="dz-Element-p">The COVID-19 vaccine is now available to anyone living or working in Alaska who is age 12 or older. Pfizer vaccine is authorized for ages 12 and up. Moderna and Johnson &amp; Johnson (Janssen) vaccines are authorized for ages 18 and up. </p>
<h3 class="dz-Element-h3">Can children get the COVID-19 vaccines at their pediatriciansâ€™ offices, as well as other locations?</h3>
<p class="dz-Element-p">Your pediatrician may provide the vaccine, but not all pediatricians are currently able to offer it. Alaskaâ€™s Vaccine Task Force is working to increase the number of pediatriciansâ€™ and doctorsâ€™ offices able to provide the vaccine to children. Parents and guardians may check visit the vaccine appointment finding page or call 907-646-3322 to find a vaccine provider.</p>
<h3 class="dz-Element-h3">Why should I vaccinate my child? </h3>
<p class="dz-Element-p">Vaccination gets our kids back to the programs, activities and social interactions they need for appropriate academic, social-emotional and physical development. Vaccinated people who have been exposed to the virus also do not need to quarantine or be tested unless they have symptoms or live in a group setting (like a group home).</p>
<p class="dz-Element-p">While most infected children usually experience minimal COVID-19 symptoms, some can develop severe illness. Much is still being learned about â€œlong covidâ€ where symptoms persist in patients previously diagnosed with COVID-19. Asymptomatic infections can still spread the virus through schools and sports teams, affecting not only other children, but also at-risk adults.<br/></p>
<p></p>
<h3 class="dz-Element-h3">Will kids 12 and older receive the same COVID-19 vaccine dose as adults?</h3>
<p></p>
<p class="dz-Element-p">Yes, the clinical trial studies evaluated the safety and efficacy of the vaccine using the same dose as adults.<br/></p>
<p></p>
<h3 class="dz-Element-h3">Can my child receive other vaccinations at the same time?</h3>
<p></p>
<p class="dz-Element-p">COVID-19 vaccines and other vaccines may be given at the same time without regard to timing. This may be discussed with a provider to determine what is best.<br/></p>
<p></p>
<h3 class="dz-Element-h3">If I am pregnant or breastfeeding, can I get vaccinated?</h3>
<p></p>
<p class="dz-Element-p">Alaskans who are pregnant or breastfeeding may choose to get a COVID-19 vaccine when they are eligible to receive it. There are limited data about the safety of COVID-19 vaccines for people who are pregnant. COVID-19 vaccines are unlikely to pose a risk to pregnant people or their babies based on current understanding.<br/></p>
<p></p>
<h3 class="dz-Element-h3">Can the COVID-19 vaccine cause infertility?</h3>
<p></p>
<p class="dz-Element-p">There is no evidence that COVID-19 vaccines cause infertility.<br/></p>
<p></p>
<h3 class="dz-Element-h3">When will Covid-19 vaccines be available for children younger than age 12?</h3>
<p></p>
<p class="dz-Element-p">The best estimate right now is that children younger than 12 may be eligible to receive the Covid-19 vaccines sometime between late fall 2021 and early 2022.</p>
<p></p>
<h2 id="schedulingappts">Planning for and scheduling your appointment </h2>
<ul type="disc">
  <li><a href="#waitlist">If Iâ€™m on a waitlist with a provider, should I work to       seek an appointment, or should I seek an available spot elsewhere?</a></li>
  <li><a href="#appointment">If I am not able to secure an appointment, what       opportunities will we have in the future?</a></li>
  <li><a href="#schedule">What do I need to do to schedule my second dose of       vaccine?</a></li>
</ul>
<h3 id="waitlist"><strong>If Iâ€™m on a waitlist with a provider, should I work to  seek an appointment, or should I seek an available spot elsewhere?</strong></h3>
<p>Itâ€™s okay to look for another spot, but providers are  continuing to follow up and let people know when vaccine is available. If you  do book an appointment elsewhere, you can let the provider youâ€™re waitlisted  with know.</p>

<h3 id="appointment"><strong>If I am not able to secure an appointment, what  opportunities will we have in the future?</strong></h3>
<p>Appointments will be scheduled on a first come, first served  basis. Weâ€™re working to enroll more providers to vaccinate Alaskans, which will  result in more appointment availability. New appointments are added  regularly. </p>

<h3 id="schedule"><strong>What do I need to do to schedule my second dose of  vaccine?</strong></h3>
<p>The healthcare provider who gives you your first dose will give you instructions for scheduling your second dose. If you need assistance making a second dose appointment, please contact the call center at 907-646-3322 or toll-free at 1-833-482-9546 from 9 a.m. - 6:30 p.m. on weekdays, and 9 a.m. - 4:30 p.m. on weekends.</p>




<h2 id="whattoexpectat">What to expect at your appointment</h2>
<ul type="disc">
  <li><a href="#choice">Will I have a choice in which vaccine I can get?</a></li>
<li><a href="#which">Which vaccine should I get?</a></li>
<li><a href="#type">Do I have a choice of which type of vaccine I get?</a></li>

</ul>
<h3 id="choice"><strong>Will I have a choice in which vaccine I can get?</strong></h3>
<p>While vaccine is limited, you may not have options between  vaccines at a specific clinic location. Many vaccination clinics note on the website which vaccine is being offered. You may select a clinic site that has your preferred vaccine.&#160;&#160;</p>
<h3 id="which">Which vaccine should I get?</h3>
<p>It is recommend that you get the first vaccine you are offered, since the sooner a person is vaccinated, the sooner they are protected against COVID-19 illness. Please consult with your health care provider if you have concerns about receiving a specific vaccine.</p>
<h3 id="type">Do I have a choice of which type of vaccine I get?</h3>
<p>While vaccine is limited, you may not have options between vaccines at a specific clinic location. Many vaccination clinics note on the website which vaccine is being offered. It is recommend that you get the first vaccine you are offered, since the sooner a person is vaccinated, the sooner they are protected against COVID-19 illness.&#160; <br/></p>






<h2 id="safe">Safety </h2>
<ul type="disc">
  <li><a href="#safety">Is a COVID-19 vaccine safe?</a></li>
</ul>

<h3 id="safety"><strong>Is a COVID-19 vaccine safe?</strong></h3>
<p>COVID-19  vaccine safety is a top priority. No steps are skipped during the clinical  trial process for COVID-19 vaccine. Vaccine safety checks are in progress and  will continue as long as a vaccine is available. Vaccine safety is complicated  and important, and questions are expected and healthy.</p>
<p>Learn more  about the V-safe after-vaccination health checker and how to report side  effects in the <a href="/web/20210626042007/https://dhss.alaska.gov/dph/Epi/id/SiteAssets/Pages/HumanCoV/v-safe-information-sheet.pdf">Vaccine Safety Application  V-Safe Information Sheet (PDF)</a>.</p>

<h2 id="effective">Efficacy</h2>
<ul type="disc">
  <li><a href="#effective">Will the vaccine be effective?</a></li>
<li><a href="#AlaskaNative">I read an article suggesting that the Johnson and Johnson Janssen vaccine is less effective in Alaska Native people.&#160; Is there reliable scientific evidence to back this up?</a><br/></li>
</ul>

<h3 id="effective"><a name="_1b72l0mkef61"></a><strong>Will the vaccine be effective?</strong></h3>
<p>A safe and  effective COVID vaccine is an important tool for ending the global pandemic.  Vaccines can protect individuals in different ways. Vaccines also protect the  people around you - including Alaskaâ€™s healthcare workers and their  patients.&#160;&#160;&#160;&#160;&#160;&#160;</p>
<p>The Food and  Drug Administration (FDA) authorizes vaccines after they pass several clinical  trials. Scientists are using clinical trials to test the COVID vaccineâ€™s  effectiveness. These clinical trials require thousands of people and months of  data. The vaccine development is faster than normal because some steps are  being done at the same time instead of one after another. Learn more about <a href="https://web.archive.org/web/20210626042007/https://www.fda.gov/emergency-preparedness-and-response/mcm-legal-regulatory-and-policy-framework/emergency-use-authorization">FDAâ€™s Emergency Use  Authorization authority</a> and watch a <a href="https://web.archive.org/web/20210626042007/https://www.youtube.com/watch?v=iGkwaESsGBQ">video on what an EUA is</a>.</p>
<p><a href="https://web.archive.org/web/20210626042007/https://www.fda.gov/news-events/press-announcements/fda-takes-key-action-fight-against-covid-19-issuing-emergency-use-authorization-first-covid-19">The FDA authorized the Pfizer  vaccine</a> on December 11, 2020. The <a href="https://web.archive.org/web/20210626042007/https://www.fda.gov/emergency-preparedness-and-response/coronavirus-disease-2019-covid-19/moderna-covid-19-vaccine">FDA authorized the Moderna vaccine</a> on  December 18, 2020. The <a href="https://web.archive.org/web/20210626042007/https://www.fda.gov/emergency-preparedness-and-response/coronavirus-disease-2019-covid-19/janssen-covid-19-vaccine">FDA authorized the Janssen vaccine</a> on February 27, 2021. All COVID-19 vaccines authorized by the FDA and recommended by the CDC are safe and effective in preventing COVID-19.</p>

<h3 id="AlaskaNative">I read an article suggesting that the Johnson and Johnson Janssen vaccine is less effective in Alaska Native people.&#160; Is there reliable scientific evidence to back this up?</h3>
<p>No. We donâ€™t have any reason to think that the Johnson and Johnson Janssen vaccine is any less effective in Alaska Native people than any other racial group. The Johnson and Johnson Janssen vaccine is safe and effective. The reported vaccine efficacy among people in this racial group in the phase 3 clinical trial for this vaccine included fewer than 200 Alaska Native/American Indian people in the United States. This number is too small to draw any meaningful conclusions about vaccine efficacy.&#160;&#160; <br/></p>


<h2 id="mandate">Health advisories</h2>
<ul type="disc">
  <li><a href="#mandate">Will there be a COVID-19 vaccine mandate?</a></li>
</ul>
<h3 id="mandate"><strong>Will there be a COVID-19 vaccine mandate?</strong></h3>
<p>There are no  plans for a statewide Alaska COVID-19 vaccine mandate. The role of the Alaska  COVID Vaccine Task Force is to provide Alaskans with the information they need  to make safe and healthy decisions about vaccination. State and Tribal leaders  prioritize your trust and safety.</p>





<h2 id="costs">Cost</h2>
<ul type="disc">
  <li><a href="#cost">How much will the vaccine cost?</a></li>
</ul>
<h3 id="cost"><strong>How much will the vaccine cost?</strong></h3>
<p>COVID-19 vaccines are provided at no cost to you, regardless of your insurance policy.</p>
<p>Vaccination providers can be reimbursed for vaccine administration fees by the patientâ€™s public or private insurance company or, for uninsured patients, by the Health Resources and Services Administrationâ€™s Provider Relief Fund. No one can be denied a vaccine if they are unable to pay a vaccine administration fee.</p>







<h2 id="vaccinationefforts">About Alaskaâ€™s vaccination effort</h2>
<ul type="disc">
  <li><a href="#how">How many Alaskans are vaccinated?</a></li>
  <li><a href="#process">What process decides who will get the vaccine first?</a></li>
  <li><a href="#planning">Who is responsible for the planning for the COVID-19       vaccine?</a></li>
</ul>
<h3 id="how"><strong>How many Alaskans are vaccinated?</strong></h3>
<p>View the Alaska dashboard summary below, or see the full vaccine dashboard.<br/></p>
<div class="ms-rtestate-read ms-rte-wpbox" contenteditable="false"><div class="ms-rtestate-notify  ms-rtestate-read 83e9f789-f815-4a58-83a2-8f3ee831f019" id="div_83e9f789-f815-4a58-83a2-8f3ee831f019" unselectable="on"></div>
<div id="vid_83e9f789-f815-4a58-83a2-8f3ee831f019" unselectable="on" style="display: none;"></div></div>

<h3 id="process">What process decides who gets  the vaccine first?</h3>
<p>Learn more about how decisions are made, and  how to provide input, at the <a href="VaccineAllocation.aspx">Alaska Vaccine Allocation Advisory Committee  information page</a>. </p>
<h3 id="planning"><strong>Who is responsible for planning for the  COVID-19 vaccine?</strong></h3>

<p>The State of  Alaskaâ€™s Department of Health and Social Services and Alaska Native Tribal  Health Consortium are working together to plan and distribute COVID-19  vaccines. The Alaska COVID-19 Vaccine Task Force includes eight sub-teams:  planning, operations, software solutions, payers, pharmacy, communications and  education, data and liaisons. On October 16, 2020, an <a href="/web/20210626042007/https://dhss.alaska.gov/dph/Epi/id/SiteAssets/Pages/HumanCoV/AlaskaCOVID-19VaccinationDraftPlan.pdf">Alaska Draft COVID-19  Vaccination Plan</a> was submitted to the CDC for review. This was a requirement for all states. The  goal is to have a safe and effective vaccine available to all Alaskans who want  it.</p>














<h2 id="traveling">Travel</h2>
<ul type="disc">
  <li><a href="#travel">If I get vaccinated do I have to comply with Health       Advisories for travel?</a></li>
</ul>

<h3 id="travel"><strong>If I get vaccinated do I have to comply with  health advisory for travel?</strong></h3>
<p>Yes, you should still comply with the requirements of Health Advisory 3, Intrastate Travel, or Health Advisory 2, International and Interstate Travel. It is unknown whether  vaccinated individuals might still be able to pass the virus on to others.</p>
<p>While the vaccines authorized by the FDA for COVID-19 are safe and highly effective, they have been tested only to find out whether they protect the person getting the vaccine from getting sick with COVID-19. We do not know yet how well they prevent the person from getting infected with the virus and passing it on, only how well they prevent the person from getting sick. We know that getting vaccine helps to protect you, but we do not know yet how much it helps to protect other people.</p>
<p>So, if you are traveling and you have gotten the vaccine, you are much less likely to get very sick with COVID-19. However, because you may still be able to catch the virus and give it to others, the requirements for quarantine and testing do not change. The vaccines do not affect your test results.<br/></p>







<h2 class="dz-Element-h2">What to expect after you are  vaccinated</h2>
<ul type="disc">
  <li><a href="#precautions">After I get vaccinated, do I need to       continue COVID-19 precautions like distancing and wearing a mask?</a></li>
<li><a href="#fullyvaccinated">If I am fully vaccinated, what will change for me?</a></li>
  <li><a href="#documentation">Will I receive documentation that I was vaccinated?</a></li>
<li><a href="#sideeffects">What are the common side effects of COVID-19 vaccines?</a></li>
<li><a href="#howlong">How long does COVID-19 vaccine last?</a><br/></li>
</ul>




<h3 id="precautions">After I get vaccinated, do I need to continue COVID-19 precautions like distancing and wearing a mask?</h3>
<p>CDC has released guidance stating that t<a href="https://web.archive.org/web/20210626042007/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/fully-vaccinated.html">hose who are fully vaccinated may gather with other people who are fully vaccinated in a small private setting without social distancing or wearing a mask</a>. Even if it has been two weeks since your final COVID-19 vaccination, you should still take steps to protect yourself and others in many situations. This includes wearing a mask, staying 6 feet apart, avoiding crowds, and following travel and workplace guidance. These vaccines are excellent at protecting the person who gets immunized, but we are still learning how well COVID-19 vaccines keep people from spreading the disease.&#160;</p>
<p></p>
<h3 id="fullyvaccinated">If I am fully vaccinated, what will change for me?</h3>
<p></p>
<p>You are considered fully vaccinated two weeks after your second dose of the Pfizer or Moderna vaccines, or 2 weeks after your dose of Janssenâ€™s vaccine. When you are fully vaccinated, you can gather indoors with other fully vaccinated people without wearing a mask. If you have been exposed to someone who has COVID-19, you do not need to quarantine or get tested unless you have symptoms or you live in a group setting (like a correctional or detention facility or group home). Review the complete updated guidelines at the CDC website. <br/></p>


<h3 id="documentation"><strong>Will I receive documentation that I was vaccinated?</strong></h3>
<p>You will receive a vaccine record card from your healthcare provider.&#160;</p>
<h3 id="sideeffects">What are the common side effects of COVID-19 vaccines?</h3>
<p>After getting vaccinated, you might have some side effects, which are normal signs that your body is building protection. Common side effects are pain, redness, and swelling in the arm where you received the shot, as well as tiredness, headache, muscle pain, chills, fever, and nausea throughout the rest of the body. These side effects could affect your ability to do daily activities, but they should go away in a few days.</p>
<h3 id="howlong">How long does COVID-19 vaccine last?</h3>
<p>We donâ€™t know how long protection lasts for those who are vaccinated. What we do know is that COVID-19 has caused very serious illness and death for a lot of people. If you get COVID-19, you also risk giving it to loved ones who may get very sick. Getting a COVID-19 vaccine is a safer choice.&#160; <br/></p>





<h2 id="resources"><strong>Resources to support you</strong></h2>
<p>Additional  information will be provided as it is available. </p>
<h3 id="webpages"><strong>Alaska COVID-19 vaccine  information pages</strong></h3>
<ul>
  <li><a href="vaccine.aspx">Alaska COVID-19 Vaccine  Main Page</a></li>
  <li><a href="VaccineProviders.aspx">Healthcare workers</a></li>
</ul>
<h3 id="latest"><strong>Hear the latest about vaccine in  Alaska</strong></h3>
<ul>
  <li><a href="https://web.archive.org/web/20210626042007/https://alaska.zoom.us/webinar/register/WN_i2syaoh3SCCTKY2U289H_Q">COVID Science ECHO</a> Wednesdays, 1:00 pm-2:00 pm</li>
  <li><a href="https://web.archive.org/web/20210626042007/https://chd.app.box.com/s/mtny6d1d9ygssxrrb16afr13hcc39ond/folder/122409363861">Archived COVID-19 Vaccine ECHO  session recordings</a> are available.</li>
</ul>

	
	
	<!-- InstanceEndEditable -->
	
	
	
	
	</div>

    
	</div>
		</div>

    <div id="dhss-footer">
	
		<div id="footer-social">
			<ul>
				<li><a href="https://web.archive.org/web/20210626042007/http://www.facebook.com/pages/Anchorage-AK/Alaska-Department-of-Health-and-Social-Services/99962144928" class="facebook" title="Facebook">&nbsp;</a></li>
				<li><a href="https://web.archive.org/web/20210626042007/http://twitter.com/Alaska_DHSS" class="twitter" title="Twitter">&nbsp;</a></li>
				<li><a href="https://web.archive.org/web/20210626042007/http://vimeo.com/alaskadhss" class="vimeo" title="Vimeo">&nbsp;</a></li>
				<li><a href="https://web.archive.org/web/20210626042007/http://www.youtube.com/alaskadhss" class="youtube" title="YouTube">&nbsp;</a></li>
				<li><a href="https://web.archive.org/web/20210626042007/https://public.govdelivery.com/accounts/AKDHSS/subscriber/new?topic_id=AKDHSS_14" class="govdelivery" title="GovDelivery">&nbsp;</a></li>
			</ul>
	  
		</div>

		<div id="footer-general">
			<ul>
				<li><a href="https://web.archive.org/web/20210626042007/http://dhss.alaska.gov/DHSScontacts.pdf">Contacts</a></li>
				<li><a href="https://web.archive.org/web/20210626042007/http://dhss.alaska.gov/accessibility.htm">Accessibility</a></li>
				<li><a href="https://web.archive.org/web/20210626042007/https://aws.state.ak.us/OnlinePublicNotices/default.aspx">Public Notices</a></li>
				<li class="webmaster"><a href="https://web.archive.org/web/20210626042007/mailto:hsswebmaster@alaska.gov">Webmaster</a></li>
			</ul>
		</div>
	  
		<div id="footer-divisions">&nbsp;
			</div>

   </div>

    </div>

	</div>
</div>
</div>


<div id="footer-wrapper">
	<div id="soa-footer">
		<ul class="menu">
			<li><a href="https://web.archive.org/web/20210626042007/http://www.alaska.gov/">State of Alaska</a></li>
			<li><a href="https://web.archive.org/web/20210626042007/https://myalaska.state.ak.us/home/app">myAlaska</a></li>
			<li><a href="https://web.archive.org/web/20210626042007/http://www.alaska.gov/akdir1.html">My Government</a></li>
			<li><a href="https://web.archive.org/web/20210626042007/http://www.alaska.gov/residentHome.html">Resident</a></li>
			<li><a href="https://web.archive.org/web/20210626042007/http://www.alaska.gov/businessHome.html">Business in Alaska</a></li>
			<li><a href="https://web.archive.org/web/20210626042007/http://www.alaska.gov/visitorHome.html">Visiting Alaska</a></li>
			<li><a href="https://web.archive.org/web/20210626042007/http://www.alaska.gov/employeeHome.html">State Employees</a></li>
		</ul>
		<ul class="footer">
			<li>State of Alaska</li>
			<li>Â© 2021</li>
			<li class="last-child"><a href="https://web.archive.org/web/20210626042007/mailto:hsswebmaster@alaska.gov">Webmaster</a></li>
		</ul>
	</div>
</div>

</div>

</body><!-- InstanceEnd --></html><!--
     FILE ARCHIVED ON 04:20:07 Jun 26, 2021 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 05:38:50 Apr 25, 2022.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
-->
<!--
playback timings (ms):
  captures_list: 63.777
  exclusion.robots: 0.099
  exclusion.robots.policy: 0.091
  RedisCDXSource: 0.655
  esindex: 0.008
  LoadShardBlock: 32.322 (3)
  PetaboxLoader3.datanode: 80.33 (4)
  CDXLines.iter: 22.81 (3)
  load_resource: 179.82
  PetaboxLoader3.resolve: 92.316
-->