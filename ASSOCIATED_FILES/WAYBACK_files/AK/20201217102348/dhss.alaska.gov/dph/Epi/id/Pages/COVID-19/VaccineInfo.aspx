
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" __expr-val-dir="ltr" lang="en-us" dir="ltr">
<head><script src="//archive.org/includes/analytics.js?v=cf34f82" type="text/javascript"></script>
<script type="text/javascript">window.addEventListener('DOMContentLoaded',function(){var v=archive_analytics.values;v.service='wb';v.server_name='wwwb-app211.us.archive.org';v.server_ms=270;archive_analytics.send_pageview({});});</script>
<script type="text/javascript" src="/_static/js/bundle-playback.js?v=36gO9Ebf" charset="utf-8"></script>
<script type="text/javascript" src="/_static/js/wombat.js?v=UHAOicsW" charset="utf-8"></script>
<script type="text/javascript">
  __wm.init("https://web.archive.org/web");
  __wm.wombat("http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx","20201217102348","https://web.archive.org/","web","/_static/",
	      "1608200628");
</script>
<link rel="stylesheet" type="text/css" href="/_static/css/banner-styles.css?v=fantwOh2" />
<link rel="stylesheet" type="text/css" href="/_static/css/iconochive.css?v=qtvMKcIJ" />
<!-- End Wayback Rewrite JS Include -->
<title>
	
	COVID-19: COVID-19 Vaccine Info

</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><meta http-equiv="Expires" content="0"/><meta http-equiv="x-ua-compatible" content="IE=Edge"/><meta property="og:image" content="https://web.archive.org/web/20201217102348im_/http://dhss.alaska.gov/PublishingImages/DHSSlogo.jpg"/><link rel="shortcut icon" href="/web/20201217102348im_/https://dhss.alaska.gov/Style Library/DhssZen/Images/favicon.ico" type="image/vnd.microsoft.icon"/><link rel="stylesheet" type="text/css" href="/web/20201217102348cs_/https://dhss.alaska.gov/Style%20Library/en-US/Themable/Core%20Styles/controls.css"/>
<link rel="stylesheet" type="text/css" href="/web/20201217102348cs_/https://dhss.alaska.gov/_layouts/1033/styles/Themable/corev4.css?rev=cubXjmzpjBGT%2FzzR9ULbAw%3D%3D"/>
<link rel="stylesheet" type="text/css" href="/web/20201217102348cs_/https://dhss.alaska.gov/_layouts/1033/styles/Themable/Menu.css?rev=j46FAsDJga%2BaixwEf8A%2Fgg%3D%3D"/>
<link rel="stylesheet" type="text/css" href="/web/20201217102348cs_/https://dhss.alaska.gov/Style%20Library/en-US/Core%20Styles/page-layouts-21.css"/>
<link rel="stylesheet" type="text/css" href="/web/20201217102348cs_/https://dhss.alaska.gov/Style%20Library/DhssZen/css/alt_campaign.css"/>


    <script type="text/javascript">
	    var _fV4UI = true;
    </script>

    <link rel="stylesheet" type="text/css" href="/web/20201217102348cs_/https://dhss.alaska.gov/Style Library/DhssZen/css/reset.css?v=1.3"/><link rel="stylesheet" type="text/css" href="/web/20201217102348cs_/https://dhss.alaska.gov/Style Library/DhssZen/css/DhssZen.css?v=1.3"/><link rel="stylesheet" type="text/css" href="/web/20201217102348cs_/https://dhss.alaska.gov/Style Library/DhssZen/css/soa-structure.css?v=1.3"/><link rel="stylesheet" type="text/css" href="/web/20201217102348cs_/https://dhss.alaska.gov/Style Library/DhssZen/css/dhss-content.css?v=1.3"/><link rel="stylesheet" type="text/css" href="/web/20201217102348cs_/https://dhss.alaska.gov/Style Library/DhssZen/css/dhss-legacy.css?v=1.3"/><script type="text/javascript">
// <![CDATA[
document.write('<script type="text/javascript" src="/style%20library/dhsszen/scripts/jquery-1.7.2.min.js"></' + 'script>');
document.write('<script type="text/javascript" src="/style%20library/dhsszen/scripts/menu.js"></' + 'script>');
document.write('<script type="text/javascript" src="/_layouts/1033/init.js?rev=mdjxRHK1V7KZ%2BuOTcNNcmQ%3D%3D"></' + 'script>');
document.write('<script type="text/javascript" src="/_layouts/1033/jquery-1.4.4.min.js?rev=c6nDNMXKcdcNCStCBk9kdg%3D%3D"></' + 'script>');
document.write('<script type="text/javascript" src="/_layouts/1033/jquery-noconflict.js?rev=nBDuS1QCCe5SuB%2BmQlVskQ%3D%3D"></' + 'script>');
document.write('<script type="text/javascript" src="/_layouts/listimport/listimport.js?rev=jZKUH8ts44bwUzFbeAf%2F1Q%3D%3D"></' + 'script>');
document.write('<script type="text/javascript" src="/ScriptResource.axd?d=ea23uepthaEvF_TU_KFnyhfDmINentVRWBgrZWYeHp-2_5dpwQPMTO-0VYOlpRzmkTTSHLhJtdKg84IzdzkOPNFtP0l9tJ9WmX6iCOeQr8tVdevmP6W5pprRYRFEv6pzHjANzlNlNgMoFfkHCguA90MY2Xs1&amp;t=3f4a792d"></' + 'script>');
document.write('<script type="text/javascript" src="/_layouts/blank.js?rev=QGOYAJlouiWgFRlhHVlMKA%3D%3D"></' + 'script>');
// ]]>
</script>

	
	<script type="text/javascript">
		function ProcessImn(){}
		function ProcessImnMarkers(){}	
	</script>

	
    
	
<meta name="google-site-verification" content="jPGd8CiqqcBFgCHw2lVALvEzN1eZi3hHuHZGv3s0lW0"/><style type="text/css">
	.ctl00_wpz_0 { border-color:Black;border-width:1px;border-style:Solid; }

</style></head>

<body onload="javascript:_spBodyOnLoadWrapper();">
<form name="aspnetForm" method="post" action="/web/20201217102348/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx" id="aspnetForm">
<input type="hidden" name="MSOWebPartPage_PostbackSource" id="MSOWebPartPage_PostbackSource" value=""/>
<input type="hidden" name="MSOTlPn_SelectedWpId" id="MSOTlPn_SelectedWpId" value=""/>
<input type="hidden" name="MSOTlPn_View" id="MSOTlPn_View" value="0"/>
<input type="hidden" name="MSOTlPn_ShowSettings" id="MSOTlPn_ShowSettings" value="False"/>
<input type="hidden" name="MSOGallery_SelectedLibrary" id="MSOGallery_SelectedLibrary" value=""/>
<input type="hidden" name="MSOGallery_FilterString" id="MSOGallery_FilterString" value=""/>
<input type="hidden" name="MSOTlPn_Button" id="MSOTlPn_Button" value="none"/>
<input type="hidden" name="__REQUESTDIGEST" id="__REQUESTDIGEST" value="noDigest"/>
<input type="hidden" name="MSOSPWebPartManager_DisplayModeName" id="MSOSPWebPartManager_DisplayModeName" value="Browse"/>
<input type="hidden" name="MSOSPWebPartManager_ExitingDesignMode" id="MSOSPWebPartManager_ExitingDesignMode" value="false"/>
<input type="hidden" name="MSOWebPartPage_Shared" id="MSOWebPartPage_Shared" value=""/>
<input type="hidden" name="MSOLayout_LayoutChanges" id="MSOLayout_LayoutChanges" value=""/>
<input type="hidden" name="MSOLayout_InDesignMode" id="MSOLayout_InDesignMode" value=""/>
<input type="hidden" name="_wpSelected" id="_wpSelected" value=""/>
<input type="hidden" name="_wzSelected" id="_wzSelected" value=""/>
<input type="hidden" name="MSOSPWebPartManager_OldDisplayModeName" id="MSOSPWebPartManager_OldDisplayModeName" value="Browse"/>
<input type="hidden" name="MSOSPWebPartManager_StartWebPartEditingName" id="MSOSPWebPartManager_StartWebPartEditingName" value="false"/>
<input type="hidden" name="MSOSPWebPartManager_EndWebPartEditing" id="MSOSPWebPartManager_EndWebPartEditing" value="false"/>
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUBMA9kFgJmD2QWAgIBD2QWBAIBD2QWAmYPZBYCZg9kFgICAQ8WAh4TUHJldmlvdXNDb250cm9sTW9kZQspiAFNaWNyb3NvZnQuU2hhcmVQb2ludC5XZWJDb250cm9scy5TUENvbnRyb2xNb2RlLCBNaWNyb3NvZnQuU2hhcmVQb2ludCwgVmVyc2lvbj0xNC4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj03MWU5YmNlMTExZTk0MjljAWQCAw9kFgYCBw9kFgICAQ9kFgRmD2QWAgIBDxYCHgdWaXNpYmxlaBYCZg9kFgQCAg9kFgYCAQ8WAh8BaGQCAw8WCB4TQ2xpZW50T25DbGlja1NjcmlwdAWEAWphdmFTY3JpcHQ6Q29yZUludm9rZSgnVGFrZU9mZmxpbmVUb0NsaWVudFJlYWwnLDEsIDUzLCAnaHR0cDpcdTAwMmZcdTAwMmZkaHNzLmFsYXNrYS5nb3ZcdTAwMmZkcGhcdTAwMmZFcGlcdTAwMmZpZCcsIC0xLCAtMSwgJycsICcnKR4YQ2xpZW50T25DbGlja05hdmlnYXRlVXJsZB4oQ2xpZW50T25DbGlja1NjcmlwdENvbnRhaW5pbmdQcmVmaXhlZFVybGQeDEhpZGRlblNjcmlwdAUiVGFrZU9mZmxpbmVEaXNhYmxlZCgxLCA1MywgLTEsIC0xKWQCBQ8WAh8BaGQCAw8PFgoeCUFjY2Vzc0tleQUBLx4PQXJyb3dJbWFnZVdpZHRoAgUeEEFycm93SW1hZ2VIZWlnaHQCAx4RQXJyb3dJbWFnZU9mZnNldFhmHhFBcnJvd0ltYWdlT2Zmc2V0WQLrA2RkAgEPZBYEAgMPZBYCAgEPEBYCHwFoZBQrAQBkAgUPZBYCZg9kFgJmDxQrAANkZGRkAg0PZBYCAgIPZBYCAgcPFgIfAAsrBAFkAhEPZBYCAgEPZBYCZg9kFgICAw9kFgICBQ8PFgQeBkhlaWdodBsAAAAAAAB5QAEAAAAeBF8hU0ICgAFkFgICAQ88KwAJAQAPFgQeDVBhdGhTZXBhcmF0b3IECB4NTmV2ZXJFeHBhbmRlZGdkZGTTlVS13ueKNTF5Lj70Iy4bsBJsWg=="/>


<script type="text/javascript">
//<![CDATA[
var MSOWebPartPageFormName = 'aspnetForm';
var g_presenceEnabled = true;
var g_wsaEnabled = false;
var g_wsaLCID = 1033;
var g_wsaSiteTemplateId = 'BLANKINTERNET#2';
var g_wsaListTemplateId = 850;
var _fV4UI=true;var _spPageContextInfo = {webServerRelativeUrl: "\u002fdph\u002fEpi\u002fid", webLanguage: 1033, currentLanguage: 1033, webUIVersion:4,pageListId:"{43e7fa95-79e4-49c4-931c-67a03394dcfd}",pageItemId:250, alertsEnabled:true, siteServerRelativeUrl: "\u002f", allowSilverlightPrompt:'True'};//]]>
</script>
<script type="text/javascript">
<!--
var L_Menu_BaseUrl="/dph/Epi/id";
var L_Menu_LCID="1033";
var L_Menu_SiteTheme="";
//-->
</script>
<script type="text/javascript">
//<![CDATA[
document.onreadystatechange=fnRemoveAllStatus; function fnRemoveAllStatus(){removeAllStatus(true)};var dlc_fvsi = {"DefaultViews":[],"ViewUrls":[],"WebUrl":"\/dph\/Epi\/id"};//]]>
</script>

<script type="text/javascript">
//<![CDATA[
function _spNavigateHierarchy(nodeDiv, dataSourceId, dataPath, url, listInContext, type) {
    CoreInvoke('ProcessDefaultNavigateHierarchy', nodeDiv, dataSourceId, dataPath, url, listInContext, type, document.forms.aspnetForm, "", "\u002fdph\u002fEpi\u002fid\u002fPages\u002fCOVID-19\u002fVaccineInfo.aspx");

}
//]]>
</script>
<script src="/web/20201217102348js_/https://dhss.alaska.gov/_layouts/blank.js?rev=QGOYAJlouiWgFRlhHVlMKA%3D%3D" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
if (typeof(DeferWebFormInitCallback) == 'function') DeferWebFormInitCallback();//]]>
</script>

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="BAB98CB3"/>






<!-- =====  Start Main Area ============================================================ -->
<div id="s4-bodyContainer">

<a href="#soa-content" id="soa-skip-content">Skip to content</a>
<div id="top-wrapper">
	<div id="soa-bar">
		<a id="soa-title" href="https://web.archive.org/web/20201217102348/http://www.alaska.gov/" name="soa">State of Alaska</a>
		<ul class="menu">
	      <li><a href="https://web.archive.org/web/20201217102348/https://my.alaska.gov/">myAlaska</a></li>
	      <li><a href="https://web.archive.org/web/20201217102348/http://www.alaska.gov/akdir1.html">My Government</a></li>
	      <li><a href="https://web.archive.org/web/20201217102348/http://www.alaska.gov/residentHome.html">Resident</a></li>
	      <li><a href="https://web.archive.org/web/20201217102348/http://www.alaska.gov/businessHome.html">Business in Alaska</a></li>
	      <li><a href="https://web.archive.org/web/20201217102348/http://www.alaska.gov/visitorHome.html">Visiting Alaska</a></li>
	      <li><a href="https://web.archive.org/web/20201217102348/http://www.alaska.gov/employeeHome.html">State Employees</a></li>
		</ul>
	</div>

	<div id="soa-header">
  		<a href="/web/20201217102348/https://dhss.alaska.gov/dph/Epi/id/"><img class="soa-banner" src="https://web.archive.org/web/20201217102348im_/http://hss.state.ak.us/css/images/Header_DHSS.jpg" alt="Infectious Disease" border="0"/></a>

	     <div id="soa-search">
                            <input id="soa-searchquery" name="q" title="Search" type="text" value=""/>
                            <input type="button" id="soa-searchsubmit" alt="Search"/>
                            <input value="DHSS" name="client" type="hidden"/>
                            <input value="DHSS" name="proxystylesheet" type="hidden"/>
                            <input value="date:D:L:d1" name="sort" type="hidden"/>
                            <input value="xml_no_dtd" name="output" type="hidden"/>
                            <input value="UTF-8" name="ie" type="hidden"/>
                            <input value="UTF-8" name="oe" type="hidden"/>
                        </div>
	</div>

    <div id="menu">
      <ul id="menu_list">
        <li id="first_nav"><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/">Home</a></li>
        <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/Pages/Services.aspx">Divisions and Agencies</a>
          <div>
            <ul class="left">
                <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/daph">Alaska Pioneer Homes</a></li>
				<li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/api">Alaska Psychiatric Institute</a></li>
		        <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/dbh">Behavioral Health</a></li>
				<li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/ocs">Office of Children's Services</a></li>
				<li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/Commissioner">Office of the Commissioner</a></li>
			    <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/osmap">Office of Substance Misuse and Addiction Prevention</a></li>
				
			</ul>
            <ul class=" left">
                <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/fms">Finance &amp; Management Services</a></li>
                <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/dhcs">Health Care Services</a></li>
				<li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/djj">Juvenile Justice</a></li>
				<li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/dpa">Public Assistance</a></li>
				<li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/dph">Public Health</a></li>
				<li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/dsds">Seniors &amp; Disabilities Services</a></li>

<li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/Commissioner/Pages/Boards/default.aspx">Boards, Councils &amp; Commissions</a></li>            </ul>  
            <br class="clear"/>
          </div>
       </li>
       <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/Pages/Services.aspx">Services</a>
          <div>
            <ul class="left">
                <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/dph/VitalStats">Birth &amp; Marriage Certificates </a></li>
                <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/dpa/Pages/ccare">Child Care </a></li>
                <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/ocs">Child Protection</a></li>
                <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/dpa/Pages/dkc/default.aspx">Denali KidCare</a></li>
                <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/dpa/Pages/SNAP/default.aspx">Supplemental Nutrition Assistance Program (SNAP) </a></li>
                <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/dph/Epi/iz/Pages/default.aspx">Immunization Information </a></li>
                <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/dhcs/Pages/Medicaid_Medicare">Medicaid</a></li>
                <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/dph/Nursing/Pages/locations.aspx">Public Health Centers </a></li>
                <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/dpa/Pages/atap">Temporary &quot;Cash&quot; Assistance </a> </li>
                <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/dpa/Pages/seniorbenefits">Senior Benefits Program</a></li>
                <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/dsds/Pages/Medicare">Medicare</a></li>
                <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/dbh/Pages/TreatmentRecovery/SubstanceAbuse">Substance Abuse Treatment</a></li>
			</ul>
            <ul class="left">
 <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/ocs/Pages/icwa/default.aspx">Alaska Tribal Child Welfare Compact</a></li>
                <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/dph/Director/Pages/LivingWill.aspx">Alaska Directives for Health Care &mdash; Living Will</a></li>
                <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/dph/Chronic/Pages/brfss">Behavioral Risk Factor Survey</a></li>
                <li><a href="https://web.archive.org/web/20201217102348/http://permaudit.alaska.gov/">PERM</a></li>              <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/dph/Epi/Pages/phan/default.aspx">Public Health Alert Network (PHAN)</a></li>
                <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/dhcs/Pages/CertificateOfNeed/default.aspx">Certificate of Need</a></li>
                <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/fms/facilities/Pages/safetyplan.aspx">Department Safety Plan</a></li>
                <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/fms/facilities/Documents/DHSS-Capital-Funding-Allocation-Plan-FY16.pdf">Facilities Funding Allocation Plan</a></li>
            </ul>
            <br class="clear"/>
          </div>
		</li>
        <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/News">News</a>
          <div>
            <ul class=" left">
                <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/News/Pages/2020index.aspx">Press Releases</a></li>
<li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/News/Pages/newsletter/index.aspx">DHSS Newsletter</a></li> 
<li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/News/Pages/newsletter/CMO/CMO_News.aspx">Chief Medical Officer News</a></li> 
<li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/Commissioner/Pages/photos.aspx">Press Photos</a></li>
<li><a href="https://web.archive.org/web/20201217102348/https://aws.state.ak.us/OnlinePublicNotices/default.aspx">Public Notices</a></li>
                <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/Pages/Publications.aspx">Publications</a></li>
                <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/News/Pages/Newsroom.aspx">Newsroom</a></li>
            </ul>
            <br class="clear"/>
          </div>
        </li>
        <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/Commissioner/Pages/Contacts">Contact Us</a>
            <div>
                <ul class="left">
                    <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/Commissioner/Pages/Contacts">Contacts</a></li>
			        <li><a href="https://web.archive.org/web/20201217102348/mailto:hsswebmaster@alaska.gov">Webmaster</a></li>
			        <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/Pages/Accessibility.aspx">Accessibility</a></li>
                    <li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/Pages/Careers.aspx">DHSS Careers</a></li>
                </ul>
                <ul class="left">
                    <li><a href="https://web.archive.org/web/20201217102348/http://www.facebook.com/pages/Anchorage-AK/Alaska-Department-of-Health-and-Social-Services/99962144928">Facebook</a></li>
				    <li><a href="https://web.archive.org/web/20201217102348/http://twitter.com/Alaska_DHSS">Twitter</a></li>
				    <li><a href="https://web.archive.org/web/20201217102348/http://vimeo.com/alaskadhss">Vimeo</a></li>
				    <li><a href="https://web.archive.org/web/20201217102348/http://www.youtube.com/alaskadhss">YouTube</a></li>
				    <li><a href="https://web.archive.org/web/20201217102348/https://public.govdelivery.com/accounts/AKDHSS/subscriber/new?topic_id=AKDHSS_14">GovDelivery</a></li>
                </ul>
                <br class="clear"/>
            </div>
        </li>

<li><a href="https://web.archive.org/web/20201217102348/http://coronavirus.alaska.gov/">COVID-19 Resources</a>
            <div>
                <ul class="left">
<li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/default.aspx">DHSS COVID-19 Website</a></li>                  
<li><a href="https://web.archive.org/web/20201217102348/http://covid19.alaska.gov/">State of Alaska COVID-19 Website</a></li>
<li><a href="https://web.archive.org/web/20201217102348/https://covid19.alaska.gov/health-order/">COVID-19 Health Orders</a></li>
<li><a href="https://web.archive.org/web/20201217102348/https://gov.alaska.gov/home/covid-19-economy/">COVID-19 Economic Stabilization</a></li>
<li><a href="https://web.archive.org/web/20201217102348/https://covid19.alaska.gov/reopen/">Reopen Alaska Responsibly Plan</a></li>
<li><a href="https://web.archive.org/web/20201217102348/https://ready.alaska.gov/covid19">COVID-19 Unified Command</a></li>
<li><a href="https://web.archive.org/web/20201217102348/https://gov.alaska.gov/home/covid19news/">COVID-19 News &amp; Video from Governor Mike Dunleavy</a></li>
                    <li><a href="https://web.archive.org/web/20201217102348/https://public.govdelivery.com/accounts/AKDHSS/subscriber/new?topic_id=AKDHSS_157">Sign up for COVID-19 updates</a></li>
                </ul>
              
                <br class="clear"/>
            </div>
        </li>


      </ul>
    </div>

	<div id="soa-breadcrumbs">
		<span><a href="#ctl00_ctl38_SkipLink"><img alt="Skip Navigation Links" height="0" width="0" src="/web/20201217102348im_/https://dhss.alaska.gov/WebResource.axd?d=Ylnie978DEV5711jLlQRdDn5MusmPX42s1MI0vQZfl06TNXLzB3nmC-pqSSXcz0sHH2njqI71A0oM3BZzO8JaXX1n_01&amp;t=637304273068272736" border="0"/></a><span><a title="Department of Health and Social Services" href="/web/20201217102348/https://dhss.alaska.gov/Pages/default.aspx">Health and Social Services</a></span><span> &gt; </span><span><a title="Division of Public Health" href="/web/20201217102348/https://dhss.alaska.gov/dph/Pages/default.aspx">Public Health</a></span><span> &gt; </span><span><a title="Section of Epidemiology
" href="/web/20201217102348/https://dhss.alaska.gov/DPH/EPI/Pages/default.aspx">Epidemiology</a></span><span> &gt; </span><span><a title="Infectious Disease Program" href="/web/20201217102348/https://dhss.alaska.gov/dph/Epi/id/Pages/default.aspx">Infectious Disease</a></span><span> &gt; </span><span>COVID-19: COVID-19 Vaccine Info</span><a id="ctl00_ctl38_SkipLink"></a></span>
	</div>
</div>

<div id="ctl00_MSO_ContentDiv">
<div id="content-wrapper">
    <div id="soa-content">
    <!-- s4-ca is the main body div -->
	<div class="s4-ca">
		

<div id="menu" class="campaign" style="margin-top:-10px; padding-bottom:15px;">
<ul id="menu_list" style="list-style: outside none none; font-size:120%;">
<li id="first_nav"><a href="/web/20201217102348/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/default.aspx"><img src="https://web.archive.org/web/20201217102348im_/http://dhss.alaska.gov/dph/Epi/id/PublishingImages/COVID-19/bug_COVID.png" width="125px" alt="COVID-19"> Home</a></li>
<li><a href="/web/20201217102348/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/sitemap.aspx">Sitemap</a>
</li>
<li><a href="https://web.archive.org/web/20201217102348/https://covid19.alaska.gov/">State of Alaska COVID-19<br/></a></li>
<li><a href="https://web.archive.org/web/20201217102348/http://cdc.gov/covid19">CDC COVID-19</a></li>
</ul>
</div>


	

<div class="grid4 covid">
<div id="google_translate_element" style="float:right;"></div>

<script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element')
}
</script>

<script type="text/javascript" src="/web/20201217102348js_/https://dhss.alaska.gov/translate_a/element.js?cb=googleTranslateElementInit"></script>


   
    <div id="ctl00_PlaceHolderMain_PageContent_label" style="display:none">Page Content</div><div id="ctl00_PlaceHolderMain_PageContent__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_PageContent_label"><p class="dz-Element-p">
<a href="/web/20201217102348/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/vaccine.aspx">Return to Alaska COVID-19 Vaccine Homepage</a><a href="/web/20201217102348/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/vaccine.aspx"></a>
</p>

<h1><strong>COVID-19 Vaccine Information for all Alaskans </strong></h1>

<p class="dz-Element-p"><span><em>Updated December 14, 2020</em><span><br/></span></span></p>

<p class="dz-Element-p">On this page:</p>

<ul>
<li><span><a href="/web/20201217102348/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx#who">Who can get the vaccine now?</a><a href="/web/20201217102348/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx#who"><span style="display:inline-block"></span></a></span><br/></li>
<li><a href="#when">When       will the vaccine be available to more people?</a></li>
  <li><a href="#how">How       much vaccine will Alaska initially receive?</a></li>
<li><a href="#committee">Who       decides which people will get the vaccine first?</a></li>
  <li><a href="#safe">Is       a COVID-19 vaccine safe?</a></li>
  <li><a href="#effective">Will       the vaccine be effective?</a></li>
  <li><a href="#mandate">Will       there be a COVID-19 vaccine mandate?</a></li>
  <li><a href="#cost">How       much will the vaccine cost?</a></li>
<li><a href="#where">Where will the vaccine be available?</a><br/></li>
  <li><a href="#planning">Who       is responsible for the planning for the COVID-19 vaccine?</a></li>
<li><a href="#resources">Resources you can use</a></li></ul>
<div>

<h2><a name="who"></a><strong>Who can get the vaccine now?</strong></h2>
<p>At first, vaccine will be available to people in these groups:</p>
<ul type="disc">
  <li>Hospital-based       front-line healthcare workers at highest risk for COVID-19 infection;</li>
  <li>Long-term       care facility residents and staff (also includes Skilled Nursing       Facilities, Assisted Living Homes, and Department of Corrections       infirmaries providing care that is similar to assisted living);</li>
  <li>EMS/fire       personnel providing emergency medical services;</li>
  <li>Community       Health Aide/Practitioners (CHA/Ps); and</li>
  <li>Individuals       who are required to perform vaccinations</li>
</ul>
<p>For more background about how decisions about who can get  vaccine are made, learn about the <a href="#planning">Alaska COVID-19 Vaccine Allocation Advisory Committee</a> or <a href="https://web.archive.org/web/20201217102348/https://www.ashnha.com/wp-content/uploads/2020/12/Alaska-Allocation-Committee-Summary-12-3-20.pdf">view  meeting summary</a>.</p>
<h2><a name="when"></a><strong>When will the vaccine be  available to more people?</strong></h2>
<p>At first, vaccine supply will be limited and offered only to certain groups.  Over time, vaccine will be available to all Alaskans. Eventually, the vaccine  will be available to all Alaskans.</p>
<p><em><img src="/web/20201217102348im_/https://dhss.alaska.gov/dph/Epi/id/PublishingImages/COVID-19/Vaccine_WeAreHere.jpg" alt="" style="margin:0px;width:100%;height:349px"/><br/>Image for illustrative purposes only; please refer </em><span><a href="/web/20201217102348/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx#who"><em>Who       can get the vaccine now?</em></a><a href="/web/20201217102348/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx#who"><span style="display:inline-block"></span></a></span><em> </em><em>for information about who the vaccine is  currently available to.</em></p>
<h2><a name="how"></a>How much vaccine will Alaska initially receive?</h2>
<p>According to current federal government estimates,  Alaska’s initial allocations of vaccines could include:</p>
<ul type="disc">
  <li><strong>Pfizer: 35,100 doses</strong></li>
  <li><strong>Moderna: 26,800 doses </strong></li>
</ul>
<p>These doses are intended to be the first of two doses for  61,900 people; the federal government is holding second doses to ensure second  doses are available. These vaccine doses will be distributed statewide among  public, private, and Tribal health systems. Alaska’s distribution process will  not include military service members, who will be vaccinated separately through  a federal allocation. Each person will require two doses of either Pfizer or  Moderna vaccine. Regular shipments of vaccines are expected to continue  throughout 2021.</p>
<h2><a name="committee"></a><a name="committee"></a>Who decides which people will get the  vaccine first?</h2>
<p>On December 1, 2020, the CDC Advisory Committee on Immunization Practice  (ACIP) <a href="https://web.archive.org/web/20201217102348/https://www.cdc.gov/mmwr/volumes/69/wr/mm6949e1.htm?s_cid=mm6949e1_w">released  recommendations</a> for groups to be vaccinated in the initial phase of the  COVID-19 vaccination program (referred to as Phase 1A by planners). States are  able to decide if the recommendations are a good fit, or if adjustments need to  be made. On December 3, 2020 the <a href="/web/20201217102348/https://dhss.alaska.gov/dph/Epi/id/SiteAssets/Pages/HumanCoV/AlaskaAllocationCommitteeMembers.pdf">Alaska COVID-19 Vaccine Allocation Advisory Committee (PDF)</a> met to review the ACIP recommendations and Alaska’s  needs, and determined a portion of the groups to be included in Phase 1A. View a <a href="https://web.archive.org/web/20201217102348/https://www.ashnha.com/wp-content/uploads/2020/12/Alaska-Allocation-Committee-Summary-12-3-20.pdf">summary  of the meeting</a>.</p>

<p>On December 10, the Alaska COVID-19 Vaccine Allocation Advisory Committee reconvened. The committee  clarified the definition of “long-term care facility,” identifying that this  term includes: Skilled Nursing Facilities; Assisted Living Homes; and Dept of  Corrections infirmaries providing care that is similar to assisted living. The  committee will continue to meet to determine additional populations in Phase  1A, as well as later phases of availability.</p>
<p><span>Read about the <a href="/web/20201217102348/https://dhss.alaska.gov/dph/Epi/id/SiteAssets/Pages/HumanCoV/COVID19Vaccine_AllocationCommitteeComments.pdf">public comments opportunities for Alaska COVID-19 Vaccine Allocation Advisory Committee meetings (PDF)</a>.<span style="display:inline-block"></span></span><br/></p>
<h2><a name="safe"></a><strong>Is a  COVID-19 vaccine safe?</strong></h2>
<p>COVID-19 vaccine safety is a top priority. No steps are skipped during the  clinical trial process for COVID-19 vaccine. Vaccine safety checks are in  progress and will continue as long as a vaccine is available. Vaccine safety is  complicated and important, and questions are expected and healthy. </p>
<h2><a name="effective"></a><strong>Will the  vaccine be effective?</strong></h2>
<p>A safe and effective COVID vaccine is an important tool for ending the  global pandemic. Vaccines can protect individuals in different ways. Vaccines  also protect the people around you - including Alaska’s healthcare workers and  their patients.      </p>
<p>The Food and Drug Administration (FDA) authorizes vaccines after they pass several  clinical trials. Scientists are using clinical trials to test the COVID  vaccine’s effectiveness. These clinical trials require thousands of people and  months of data. The vaccine development is faster than normal because some  steps are being done at the same time instead of one after another. Learn more  about <a href="https://web.archive.org/web/20201217102348/https://www.fda.gov/emergency-preparedness-and-response/mcm-legal-regulatory-and-policy-framework/emergency-use-authorization" target="_blank">FDA’s Emergency Use Authorization authority</a> and watch a <a href="https://web.archive.org/web/20201217102348/https://www.youtube.com/watch?v=iGkwaESsGBQ" target="_blank">video on  what an EUA is</a>. <a href="https://web.archive.org/web/20201217102348/https://www.fda.gov/news-events/press-announcements/fda-takes-key-action-fight-against-covid-19-issuing-emergency-use-authorization-first-covid-19">The  FDA authorized the Pfizer vaccine</a> to be made available on December 11,  2020. </p>
<h2><a name="mandate"></a><strong>Will there  be a COVID-19 vaccine mandate?</strong></h2>
<p>There are no plans for a statewide Alaska COVID-19 vaccine mandate. The role  of the Alaska COVID Vaccine Task Force is to provide Alaskans with the  information they need to make safe and healthy decisions about vaccination.  State and Tribal leaders prioritize your trust and safety.</p>
<h2><a name="cost"></a><strong>How much  will the vaccine cost?</strong></h2>
<p>The Alaska COVID Vaccine Task Force is working to ensure COVID vaccines are  available at no charge to the individual. The CDC has stated that cost will not  be an obstacle to getting vaccinated against COVID-19. </p>
<h2><a name="where"></a><strong>Where will  the vaccine be available?</strong></h2>
<p>When the  vaccine is more widely available, a link will be provided on this site to help  Alaskans locate nearby vaccine providers. For now, vaccine providers receiving  vaccine shipments coordinate outreach to people in groups it is available to.</p>

<h2><a name="planning"></a><strong>Who is responsible for  planning for the COVID-19 vaccine?</strong></h2>
<p>The State of Alaska’s Department of Health and Social Services and Alaska  Native Tribal Health Consortium are working together to plan and distribute  COVID-19 vaccines. The Alaska COVID-19 Vaccine Task Force includes eight  sub-teams: planning, operations, software solutions, payers, pharmacy,  communications and education, data and liaisons. On October 16, 2020, an <a href="/web/20201217102348/https://dhss.alaska.gov/dph/Epi/id/SiteAssets/Pages/HumanCoV/AlaskaCOVID-19VaccinationDraftPlan.pdf" target="_blank">Alaska Draft COVID-19 Vaccination Plan</a> was submitted to the  CDC for review. This was a requirement for all states. The goal is to have a  safe and effective vaccine available to all Alaskans who want it.</p>
<div class="box">
<h2>Resources you can  use</h2>
<p>Additional information will be provided as it is available. </p>
<h3>Alaska COVID-19  vaccine information pages</h3>
<ul type="disc">
  <li><a href="/web/20201217102348/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx">All       Alaskans </a> </li>
  <li><a href="/web/20201217102348/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineProviders.aspx">Healthcare       workers </a></li>
  <li><a href="/web/20201217102348/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineCommunityPartners.aspx">Community       partners</a></li>
  <li><a href="/web/20201217102348/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineProviders.aspx">Enrolled       COVID-19 vaccine providers</a></li>
</ul>
<h3>Hear the latest about vaccine in Alaska  </h3>
<ul type="disc">
<li><a href="https://web.archive.org/web/20201217102348/https://echo.zoom.us/meeting/register/tJAufuqqqz0pEtxIsd6tK_4pcTFwKCGCxoRB">COVID       Vaccine ECHO</a> (learn about planning, logistics       and vaccine science) Thursdays, 2:00 pm-3:00 pm</li>
<li><a href="https://web.archive.org/web/20201217102348/https://chd.app.box.com/s/mtny6d1d9ygssxrrb16afr13hcc39ond/folder/122409363861">Archived       COVID-19 Vaccine ECHO session recordings</a> are available.</li></ul>



</div>

<div class="clear">
 <h2><strong>Contact  information</strong></h2>
<p>We can help answer questions about COVID-19  vaccine planning, distribution or availability status. Please speak with your  healthcare provider about any questions you have regarding your health. Our  call center does not answer individual health-related questions.  For emergencies, see your healthcare provider or go to an emergency room. </p>
<ul type="disc">
  <li>General public       questions may       be directed to <a href="https://web.archive.org/web/20201217102348/mailto:covid19vaccine@alaska.gov?subject=Non-Provider COVID-19 Vaccine Question">covid19vaccine@alaska.gov</a> or       1-907-646-3322.</li>
  <li><span>Medical providers may       direct medical questions<span style="display:inline-block"></span></span> to <a href="https://web.archive.org/web/20201217102348/mailto:covid19vaccine@alaska.gov?subject=COVID-19 Vaccine: Provider Question">covid19vaccine@alaska.gov</a> or       833-751-4212.</li></ul></div></div></div>

<h2 class="clear">Can't find what you're looking for?</h2>
<script async src="https://web.archive.org/web/20201217102348js_/https://cse.google.com/cse.js?cx=000435485480808704345:4lgxbxrva15"></script>
<div class="gcse-search" style="padding-bottom: 30px;></div>
</div>


	</div>

	<div id=" developerdashboard" class="ms-developerdashboard">
		
	</div>

    <div id="dhss-footer">
	
		<div id="footer-social">
			<ul>
				<li><a href="https://web.archive.org/web/20201217102348/http://www.facebook.com/pages/Anchorage-AK/Alaska-Department-of-Health-and-Social-Services/99962144928" class="facebook" title="Facebook">&nbsp;</a></li>
				<li><a href="https://web.archive.org/web/20201217102348/http://twitter.com/Alaska_DHSS" class="twitter" title="Twitter">&nbsp;</a></li>
				<li><a href="https://web.archive.org/web/20201217102348/http://vimeo.com/alaskadhss" class="vimeo" title="Vimeo">&nbsp;</a></li>
				<li><a href="https://web.archive.org/web/20201217102348/http://www.youtube.com/alaskadhss" class="youtube" title="YouTube">&nbsp;</a></li>
				<li><a href="https://web.archive.org/web/20201217102348/https://public.govdelivery.com/accounts/AKDHSS/subscriber/new?topic_id=AKDHSS_14" class="govdelivery" title="GovDelivery">&nbsp;</a></li>
			</ul>
	  
		</div>

		<div id="footer-general">
			<ul>
				<li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/Commissioner/Pages/Contacts">Contacts</a></li>
				<li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/Pages/Accessibility.aspx">Accessibility</a></li>
				<li><a href="https://web.archive.org/web/20201217102348/https://aws.state.ak.us/OnlinePublicNotices/default.aspx">Public Notices</a></li>
				<li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/Pages/Staff.aspx">Staff</a></li>
			</ul>
			<ul>
				<li class="webmaster"><a href="https://web.archive.org/web/20201217102348/mailto:hsswebmaster@alaska.gov">Webmaster</a></li>
			</ul>
		</div>
	  
		<div id="footer-divisions">
			<ul>
				<li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/daph">Alaska Pioneer Homes</a></li>
				<li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/api">Alaska Psychiatric Institute</a></li>
				<li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/dbh">Behavioral Health</a></li>

<li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/fms">Finance &amp; Management Services</a></li>

						</ul>
<ul>

<li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/dhcs">Health Care Services</a></li>

<li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/djj">Juvenile Justice</a></li>
	
<li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/ocs">Office of Children's Services</a></li>

<li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/dpa">Public Assistance</a></li>

</ul>

<ul>

<li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/dph">Public Health</a></li>

<li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/dsds">Seniors &amp; Disabilities Services</a></li>

<li><a href="https://web.archive.org/web/20201217102348/http://dhss.alaska.gov/osmap">Substance Misuse and Addiction Prevention</a></li>
</ul>
</div>

    </div>

	</div>
</div>
</div>

<div id="footer-wrapper">
	<div id="soa-footer">
		<ul class="menu">
			<li><a href="https://web.archive.org/web/20201217102348/http://www.alaska.gov/">State of Alaska</a></li>
			<li><a href="https://web.archive.org/web/20201217102348/https://myalaska.state.ak.us/home/app">myAlaska</a></li>
			<li><a href="https://web.archive.org/web/20201217102348/http://www.alaska.gov/akdir1.html">My Government</a></li>
			<li><a href="https://web.archive.org/web/20201217102348/http://www.alaska.gov/residentHome.html">Resident</a></li>
			<li><a href="https://web.archive.org/web/20201217102348/http://www.alaska.gov/businessHome.html">Business in Alaska</a></li>
			<li><a href="https://web.archive.org/web/20201217102348/http://www.alaska.gov/visitorHome.html">Visiting Alaska</a></li>
			<li><a href="https://web.archive.org/web/20201217102348/http://www.alaska.gov/employeeHome.html">State Employees</a></li>
		</ul>
		<ul class="footer">
			<li>State of Alaska</li>
			<li>&copy; 2019</li>
			<li class="last-child"><a href="https://web.archive.org/web/20201217102348/mailto:hsswebmaster@alaska.gov">Webmaster</a></li>
		</ul>
	</div>
</div>
</div>
<!-- =====  End Main ============================================================ -->


	




<div id="ctl00_panelZone">
	<div style="display:none" id="hidZone"><menu class="ms-SrvMenuUI">
		<ie:menuitem id="MSOMenu_Help" iconsrc="/_layouts/images/HelpIcon.gif" onmenuclick="MSOWebPartPage_SetNewWindowLocation(MenuWebPart.getAttribute('helpLink'), MenuWebPart.getAttribute('helpMode'))" text="Help" type="option" style="display:none">

		</ie:menuitem>
	</menu></div>
</div><input type="hidden" id="_wpcmWpid" name="_wpcmWpid" value=""/><input type="hidden" id="wpcmVal" name="wpcmVal" value=""/>

<script type="text/javascript">
//<![CDATA[
var _spFormDigestRefreshInterval = 1440000;window.g_updateFormDigestPageLoaded = new Date(); window.g_updateFormDigestPageLoaded.setDate(window.g_updateFormDigestPageLoaded.getDate() -5);function loadMDN2() { EnsureScript('MDN.js', typeof(loadFilterFn), null); }
function loadMDN1() { ExecuteOrDelayUntilScriptLoaded(loadMDN2, 'sp.ribbon.js'); }
_spBodyOnLoadFunctionNames.push('loadMDN1');
function _spNavigateHierarchyEx(nodeDiv, dataSourceId, dataPath, url, listInContext, type, additionalQString) {
    SetAdditionalNavigateHierarchyQString(additionalQString);
    g_originalSPNavigateFunc(nodeDiv, dataSourceId, dataPath, url, listInContext, type);
}

g_originalSPNavigateFunc = _spNavigateHierarchy;
_spNavigateHierarchy = _spNavigateHierarchyEx;

function EnsureScripts(scriptInfoList, finalFunction)
{
if (scriptInfoList.length == 0)
{
finalFunction();
}
else
{
var scriptInfo = scriptInfoList.shift();
var rest = function () { EnsureScripts(scriptInfoList, finalFunction); };
var defd;
try
{
eval('defd = typeof(' + scriptInfo[1] + ');');
}
catch (e)
{
defd = 'undefined';
}
if (scriptInfo[2])
{
EnsureScript(scriptInfo[0], defd, null);
ExecuteOrDelayUntilScriptLoaded(rest, scriptInfo[0]);
}
else
{
EnsureScript(scriptInfo[0], defd, rest);
}
}
}
function PublishingRibbonUpdateRibbon()
{
var pageManager = SP.Ribbon.PageManager.get_instance();
if (pageManager)
{
pageManager.get_commandDispatcher().executeCommand('appstatechanged', null);
}
}var _fV4UI = true;
function _RegisterWebPartPageCUI()
{
    var initInfo = {editable: false,isEditMode: false,allowWebPartAdder: false,listId: "{43e7fa95-79e4-49c4-931c-67a03394dcfd}",itemId: 250,recycleBinEnabled: true,enableMinorVersioning: true,enableModeration: false,forceCheckout: true,rootFolderUrl: "\u002fdph\u002fEpi\u002fid\u002fPages",itemPermissions:{High:0,Low:196673}};
    SP.Ribbon.WebPartComponent.registerWithPageManager(initInfo);
    var wpcomp = SP.Ribbon.WebPartComponent.get_instance();
    var hid;
    hid = document.getElementById("_wpSelected");
    if (hid != null)
    {
        var wpid = hid.value;
        if (wpid.length > 0)
        {
            var zc = document.getElementById(wpid);
            if (zc != null)
                wpcomp.selectWebPart(zc, false);
        }
    }
    hid = document.getElementById("_wzSelected");
    if (hid != null)
    {
        var wzid = hid.value;
        if (wzid.length > 0)
        {
            wpcomp.selectWebPartZone(null, wzid);
        }
    }
}
ExecuteOrDelayUntilScriptLoaded(_RegisterWebPartPageCUI, "sp.ribbon.js"); var __wpmExportWarning='This Web Part Page has been personalized. As a result, one or more Web Part properties may contain confidential information. Make sure the properties contain information that is safe for others to read. After exporting this Web Part, view properties in the Web Part description file (.WebPart) by using a text editor such as Microsoft Notepad.';var __wpmCloseProviderWarning='You are about to close this Web Part.  It is currently providing data to other Web Parts, and these connections will be deleted if this Web Part is closed.  To close this Web Part, click OK.  To keep this Web Part, click Cancel.';var __wpmDeleteWarning='You are about to permanently delete this Web Part.  Are you sure you want to do this?  To delete this Web Part, click OK.  To keep this Web Part, click Cancel.';//]]>
</script>
<script type="text/javascript">
// <![CDATA[
// ]]>
</script>
<script type="text/javascript">RegisterSod("sp.core.js", "\u002f_layouts\u002fsp.core.js?rev=7ByNlH\u00252BvcgRJg\u00252BRCctdC0w\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.res.resx", "\u002f_layouts\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=SP\u00252ERes\u0026rev=b6\u00252FcRx1a6orhAQ\u00252FcF\u00252B0ytQ\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.runtime.js", "\u002f_layouts\u002fsp.runtime.js?rev=IGffcZfunndj0247nOxKVg\u00253D\u00253D");RegisterSodDep("sp.runtime.js", "sp.core.js");RegisterSodDep("sp.runtime.js", "sp.res.resx");</script>
<script type="text/javascript">RegisterSod("sp.ui.dialog.js", "\u002f_layouts\u002fsp.ui.dialog.js?rev=Tpcmo1\u00252FSu6R0yewHowDl5g\u00253D\u00253D");RegisterSodDep("sp.ui.dialog.js", "sp.core.js");RegisterSodDep("sp.ui.dialog.js", "sp.res.resx");</script>
<script type="text/javascript">RegisterSod("sp.js", "\u002f_layouts\u002fsp.js?rev=\u00252B4ZEyA892P3T0504qi0paw\u00253D\u00253D");RegisterSodDep("sp.js", "sp.core.js");RegisterSodDep("sp.js", "sp.runtime.js");RegisterSodDep("sp.js", "sp.ui.dialog.js");RegisterSodDep("sp.js", "sp.res.resx");</script>
<script type="text/javascript">RegisterSod("cui.js", "\u002f_layouts\u002fcui.js?rev=OOyJv78CADNBeet\u00252FvTvniQ\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("core.js", "\u002f_layouts\u002f1033\u002fcore.js?rev=RiGU6\u00252FvAzNgOjxKFQLw9pw\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("inplview", "\u002f_layouts\u002finplview.js?rev=WB6Gy8a027aeNCq7koVCUg\u00253D\u00253D");RegisterSodDep("inplview", "core.js");RegisterSodDep("inplview", "sp.js");</script>
<script type="text/javascript">RegisterSod("ribbon", "\u002f_layouts\u002fsp.ribbon.js?rev=F\u00252BUEJ66rbXzSvpf7nN69wQ\u00253D\u00253D");RegisterSodDep("ribbon", "core.js");RegisterSodDep("ribbon", "sp.core.js");RegisterSodDep("ribbon", "sp.js");RegisterSodDep("ribbon", "cui.js");RegisterSodDep("ribbon", "sp.res.resx");RegisterSodDep("ribbon", "sp.runtime.js");RegisterSodDep("ribbon", "inplview");</script>
<script type="text/javascript">RegisterSod("sp.ui.policy.resources.resx", "\u002f_layouts\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=SP\u00252EUI\u00252EPolicy\u00252EResources\u0026rev=YhBHGmUAGyJ3lAgSdE4V\u00252Fw\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("mdn.js", "\u002f_layouts\u002fmdn.js?rev=gwmFFJ2\u00252FfFacqXWAqG\u00252FqKg\u00253D\u00253D");RegisterSodDep("mdn.js", "sp.core.js");RegisterSodDep("mdn.js", "sp.runtime.js");RegisterSodDep("mdn.js", "sp.js");RegisterSodDep("mdn.js", "cui.js");RegisterSodDep("mdn.js", "ribbon");RegisterSodDep("mdn.js", "sp.ui.policy.resources.resx");</script>
<script type="text/javascript">RegisterSod("sp.publishing.resources.resx", "\u002f_layouts\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=SP\u00252EPublishing\u00252EResources\u0026rev=q6nxzZIVVXE5X1SPZIMD3A\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.ui.pub.ribbon.js", "\u002f_layouts\u002fsp.ui.pub.ribbon.js?rev=epwnP\u00252FbdljnctbCVld1nnA\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("msstring.js", "\u002f_layouts\u002f1033\u002fmsstring.js?rev=QtiIcPH3HV7LgVSO7vONFg\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("browserScript", "\u002f_layouts\u002f1033\u002fnon_ie.js?rev=EVTj1bu32\u00252FMla6SDN\u00252FsNTA\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSodDep("browserScript", "msstring.js");</script>
<script type="text/javascript">
//<![CDATA[
Sys.Application.initialize();
//]]>
</script>
</form>


</body>
</html>
<!--
     FILE ARCHIVED ON 10:23:48 Dec 17, 2020 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 05:37:36 Apr 25, 2022.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
-->
<!--
playback timings (ms):
  captures_list: 135.399
  exclusion.robots: 0.075
  exclusion.robots.policy: 0.069
  RedisCDXSource: 3.912
  esindex: 0.007
  LoadShardBlock: 108.711 (3)
  PetaboxLoader3.datanode: 115.694 (4)
  CDXLines.iter: 16.5 (3)
  load_resource: 121.338
  PetaboxLoader3.resolve: 90.35
-->