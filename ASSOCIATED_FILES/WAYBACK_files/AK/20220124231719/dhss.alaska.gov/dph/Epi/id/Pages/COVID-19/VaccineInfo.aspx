<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us" dir="ltr" __expr-val-dir="ltr"><!-- InstanceBegin template="/Templates/covid.dwt" codeOutsideHTMLIsLocked="false" --><head><script src="//archive.org/includes/analytics.js?v=cf34f82" type="text/javascript"></script>
<script type="text/javascript">window.addEventListener('DOMContentLoaded',function(){var v=archive_analytics.values;v.service='wb';v.server_name='wwwb-app227.us.archive.org';v.server_ms=5484;archive_analytics.send_pageview({});});</script>
<script type="text/javascript" src="/_static/js/bundle-playback.js?v=36gO9Ebf" charset="utf-8"></script>
<script type="text/javascript" src="/_static/js/wombat.js?v=UHAOicsW" charset="utf-8"></script>
<script type="text/javascript">
  __wm.init("https://web.archive.org/web");
  __wm.wombat("https://dhss.alaska.gov/dph/epi/id/pages/covid-19/vaccineinfo.aspx","20220125073841","https://web.archive.org/","web","/_static/",
	      "1643096321");
</script>
<link rel="stylesheet" type="text/css" href="/_static/css/banner-styles.css?v=fantwOh2" />
<link rel="stylesheet" type="text/css" href="/_static/css/iconochive.css?v=qtvMKcIJ" />
<!-- End Wayback Rewrite JS Include -->
 
<!-- InstanceBeginEditable name="doctitle" -->
<title> Alaska Department of Health and Social Services </title>
<!-- InstanceEndEditable --> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> <meta http-equiv="Expires" content="0"/> <meta http-equiv="x-ua-compatible" content="IE=Edge"/> <link rel="shortcut icon" href="https://web.archive.org/web/20220125073841im_/https://dhss.alaska.gov/outage/Style Library/DhssZen/Images/favicon.ico" type="image/vnd.microsoft.icon"/> <link rel="stylesheet" type="text/css" href="https://web.archive.org/web/20220125073841cs_/https://dhss.alaska.gov/outage/Style Library/DhssZen/css/controls.css"/> <link rel="stylesheet" type="text/css" href="https://web.archive.org/web/20220125073841cs_/https://dhss.alaska.gov/outage/Style Library/DhssZen/css/corev4.css"/> <link rel="stylesheet" type="text/css" href="https://web.archive.org/web/20220125073841cs_/https://dhss.alaska.gov/outage/Style Library/DhssZen/css/Menu.css"/> <link rel="stylesheet" type="text/css" href="https://web.archive.org/web/20220125073841cs_/https://dhss.alaska.gov/outage/Style Library/DhssZen/css/page-layouts-21.css"/> <link rel="stylesheet" type="text/css" href="https://web.archive.org/web/20220125073841cs_/https://dhss.alaska.gov/outage/Style Library/DhssZen/css/reset.css"/> <link rel="stylesheet" type="text/css" href="https://web.archive.org/web/20220125073841cs_/https://dhss.alaska.gov/outage/Style Library/DhssZen/css/DhssZen.css"/> <link rel="stylesheet" type="text/css" href="https://web.archive.org/web/20220125073841cs_/https://dhss.alaska.gov/outage/Style Library/DhssZen/css/soa-structure.css"/> <link rel="stylesheet" type="text/css" href="https://web.archive.org/web/20220125073841cs_/https://dhss.alaska.gov/outage/Style Library/DhssZen/css/dhss-content.css"/> <link rel="stylesheet" type="text/css" href="https://web.archive.org/web/20220125073841cs_/https://dhss.alaska.gov/outage/Style Library/DhssZen/css/dhss-legacy.css"/> <script type="text/javascript" src="https://web.archive.org/web/20220125073841js_/https://dhss.alaska.gov/outage/jquery-1.js"></script> <script type="text/javascript" src="https://web.archive.org/web/20220125073841js_/https://dhss.alaska.gov/outage/menu.js"></script> <script type="text/javascript" src="https://web.archive.org/web/20220125073841js_/https://dhss.alaska.gov/outage/init.js"></script> <script type="text/javascript" src="https://web.archive.org/web/20220125073841js_/https://dhss.alaska.gov/outage/msstring.js"></script> <script type="text/javascript" src="https://web.archive.org/web/20220125073841js_/https://dhss.alaska.gov/outage/ie55up.js"></script> <script type="text/javascript" src="https://web.archive.org/web/20220125073841js_/https://dhss.alaska.gov/outage/jquery-1_002.js"></script> <script type="text/javascript" src="https://web.archive.org/web/20220125073841js_/https://dhss.alaska.gov/outage/jquery-noconflict.js"></script> <script type="text/javascript" src="https://web.archive.org/web/20220125073841js_/https://dhss.alaska.gov/outage/listimport.js"></script> <script type="text/javascript" src="https://web.archive.org/web/20220125073841js_/https://dhss.alaska.gov/outage/ScriptResource_002.js"></script> <script type="text/javascript" src="https://web.archive.org/web/20220125073841js_/https://dhss.alaska.gov/outage/blank.js"></script> <script type="text/javascript" src="https://web.archive.org/web/20220125073841js_/https://dhss.alaska.gov/outage/ScriptResource.js"></script> <meta name="google-site-verification" content="jPGd8CiqqcBFgCHw2lVALvEzN1eZi3hHuHZGv3s0lW0"/> <style type="text/css"> </style> <script type="text/javascript" src="https://web.archive.org/web/20220125073841js_/https://dhss.alaska.gov/outage/core.js"></script> <script type="text/javascript" src="https://web.archive.org/web/20220125073841js_/https://dhss.alaska.gov/outage/sp.js"></script> <script type="text/javascript" src="https://web.archive.org/web/20220125073841js_/https://dhss.alaska.gov/outage/ScriptResx.js"></script> <script type="text/javascript" src="https://web.archive.org/web/20220125073841js_/https://dhss.alaska.gov/outage/cui.js"></script> <script type="text/javascript" src="https://web.archive.org/web/20220125073841js_/https://dhss.alaska.gov/outage/sp_003.js"></script> <script type="text/javascript" src="https://web.archive.org/web/20220125073841js_/https://dhss.alaska.gov/outage/sp_005.js"></script> <script type="text/javascript" src="https://web.archive.org/web/20220125073841js_/https://dhss.alaska.gov/outage/sp_002.js"></script> <script type="text/javascript" src="https://web.archive.org/web/20220125073841js_/https://dhss.alaska.gov/outage/inplview.js"></script> <script type="text/javascript" src="https://web.archive.org/web/20220125073841js_/https://dhss.alaska.gov/outage/sp_004.js"></script> <script type="text/javascript" src="https://web.archive.org/web/20220125073841js_/https://dhss.alaska.gov/outage/ScriptResx_002.js"></script> <script type="text/javascript" src="https://web.archive.org/web/20220125073841js_/https://dhss.alaska.gov/outage/mdn.js"></script> 
	 <!-- InstanceBeginEditable name="head" -->
	<!-- <link rel="stylesheet" type="text/css" href="http://dhss.alaska.gov/outage/Style%20Library/DhssZen/css/alt_campaign.css" /> --> 
<!-- InstanceEndEditable -->  
	</head> 
	<body> <!-- ===== Start Main Area ============================================================ --> <div id="s4-bodyContainer"> <a href="#soa-content" id="soa-skip-content">Skip to content</a> <div id="top-wrapper"> <div id="soa-bar"> <a id="soa-title" href="https://web.archive.org/web/20220125073841/http://www.alaska.gov/" name="soa">State of Alaska</a> <ul class="menu"> <li><a href="https://web.archive.org/web/20220125073841/https://my.alaska.gov/">myAlaska</a></li> <li><a href="https://web.archive.org/web/20220125073841/http://www.alaska.gov/akdir1.html">My Government</a></li> <li><a href="https://web.archive.org/web/20220125073841/http://www.alaska.gov/residentHome.html">Resident</a></li> <li><a href="https://web.archive.org/web/20220125073841/http://www.alaska.gov/businessHome.html">Business in Alaska</a></li> <li><a href="https://web.archive.org/web/20220125073841/http://www.alaska.gov/visitorHome.html">Visiting Alaska</a></li> <li><a href="https://web.archive.org/web/20220125073841/http://www.alaska.gov/employeeHome.html">State Employees</a></li> </ul> </div> <div id="soa-header"> <a href="https://web.archive.org/web/20220125073841/https://dhss.alaska.gov/"><img class="soa-banner" src="https://web.archive.org/web/20220125073841im_/https://dhss.alaska.gov/outage/Header_DHSS.jpg" alt="Health and Social Services" style="border-width:0px;"/></a> <div id="soa-search" style="background: none;"> <script async src="https://web.archive.org/web/20220125073841js_/https://cse.google.com/cse.js?cx=c0fe050eb6ce15f1d"></script> <div class="gcse-searchbox-only" data-resultsurl="https://dhss.alaska.gov/pages/search.aspx"></div> </div> </div> <div id="soa-breadcrumbs"> <span><a href="#ctl00_ctl37_SkipLink"><img alt="Skip Navigation Links" src="https://web.archive.org/web/20220125073841im_/https://dhss.alaska.gov/outage/spacer.gif" style="border-width:0px;" width="0" height="0"/></a><a href="https://web.archive.org/web/20220125073841/http://dhss.alaska.gov/">Health and Social Services</a></span> 
	<!-- InstanceBeginEditable name="breadcrumbs" --><!-- InstanceEndEditable --></div> </div> <div id="content-wrapper"> <div id="soa-content"> <!-- s4-ca is the main body div --> <div class="s4-ca"> <div id="DhssZenContent">
	 
	<div id="menu" class="campaign" style="margin-top:-10px; padding-bottom:15px;"> 
		<ul id="menu_list" style="list-style: outside none none; font-size:120%;"> 
			<li id="first_nav"><a href="https://web.archive.org/web/20220125073841/http://dhss.alaska.gov/dph/epi/id/pages/covid-19/default.aspx"><img src="https://web.archive.org/web/20220125073841im_/http://dhss.alaska.gov/dph/epi/id/PublishingImages/COVID-19/bug_COVID.png" width="125" alt="COVID-19"/> Home</a> <div style="font-size: 80%;"> <ul> <li><a href="https://web.archive.org/web/20220125073841/http://dhss.alaska.gov/dph/epi/id/pages/covid-19/alertlevels.aspx">Alert Levels</a></li> <li><a href="https://web.archive.org/web/20220125073841/http://dhss.alaska.gov/dph/epi/id/pages/covid-19/guidance.aspx">Guidance</a></li> <li><a href="https://web.archive.org/web/20220125073841/http://dhss.alaska.gov/dph/epi/id/pages/covid-19/speakersbureau.aspx">Speakers Bureau</a></li> <li><a href="https://web.archive.org/web/20220125073841/http://dhss.alaska.gov/dph/epi/id/pages/covid-19/echo.aspx">ECHOs</a></li> <li><a href="/web/20220125073841/https://dhss.alaska.gov/dph/epi/id/pages/http:/dhss.alaska.gov/dph/epi/id/pages/covid-19/mentalwellbeing.aspx">Mental Well-being</a></li> <li><a href="https://web.archive.org/web/20220125073841/http://dhss.alaska.gov/dph/epi/id/pages/covid-19/shareables.aspx">Toolkits and Downloads</a></li> <li><a href="https://web.archive.org/web/20220125073841/http://dhss.alaska.gov/dph/epi/id/pages/covid-19/translations.aspx">Multilingual Resources</a></li> <li><a href="https://web.archive.org/web/20220125073841/http://dhss.alaska.gov/dph/epi/id/pages/covid-19/communications.aspx">Newsroom</a></li> <li><a href="">Resources for Health Care Workers</a></li> </ul> </div> </li> <li><a href="https://web.archive.org/web/20220125073841/http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/vaccineappointments.aspx">Vaccine</a> <div style="font-size: 80%;"> <ul> <li><a href="https://web.archive.org/web/20220125073841/http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/vaccineappointments.aspx#prepmod">Appointments with select providers</a> </li> <li><a href="https://web.archive.org/web/20220125073841/http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/vaccineappointments.aspx#all">All COVID-19 vaccine providers </a> </li> <li><a href="https://web.archive.org/web/20220125073841/http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/vaccineappointments.aspx#call">Call for support</a> </li> <li><a href="https://web.archive.org/web/20220125073841/http://dhss.alaska.gov/dph/epi/id/pages/covid-19/getvaccinated.aspx">Reasons To Get Vaccinated</a> </li> </ul> </div> </li> <li><a href="https://web.archive.org/web/20220125073841/http://dhss.alaska.gov/dph/epi/id/pages/covid-19/testing.aspx">Testing</a> <div style="font-size: 80%;"> <ul> <li><a href="https://web.archive.org/web/20220125073841/http://dhss.alaska.gov/dph/epi/id/pages/covid-19/nowwhat.aspx">Feeling Sick or Exposed to COVID-19 &mdash; Now What?</a></li> <li><a href="https://web.archive.org/web/20220125073841/http://dhss.alaska.gov/dph/epi/id/pages/covid-19/contacttracing.aspx">Contact Tracing</a></li> </ul> </div> </li> <li><a href="https://web.archive.org/web/20220125073841/http://dhss.alaska.gov/dph/epi/id/pages/covid-19/travel.aspx">Travel</a> </li> <li><a href="https://web.archive.org/web/20220125073841/https://covid19.alaska.gov/">State of Alaska</a></li> <li><a href="https://web.archive.org/web/20220125073841/https://www.cdc.gov/coronavirus/2019-ncov/index.html">CDC</a></li> </ul> </div> <div class="grid4 covid" role="main"> 
		<script type="text/javascript"> function googleTranslateElementInit() { new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element'); } </script> 
		<!-- InstanceBeginEditable name="covidmain" -->
	
	<a href="Vaccine.aspx"><p class="buttons" style="font-size: 80%; width: 37%; margin-bottom: 20px;">
Return to Alaska COVID-19 Vaccine Homepage
</p></a>

<h1><strong>COVID-19 Vaccine Information for all Alaskans&#160;</strong></h1>

<p><em>Updated December 23, 2021<br/></em></p>

<p>On this page:</p>
<ul>
<li><a href="#whoandwhen">Vaccine administration</a></li>

<li><a href="#whattoexpectat">Planning for and scheduling your appointment</a></li>
<li><a href="#safe">Safety and effectiveness</a></li>

<li><a href="#mandate">Health  mandates</a></li>
<li><a href="#cost">Cost</a></li>
<li><a href="#vaccinationefforts">About Alaska&#39;s vaccination effort</a></li>

<li><a href="#whattoexpectafter">What changes after you are vaccination</a></li>

<li><a href="#contacts">Contact information</a><br/></li></ul>


<h2 id="whoandwhen">Vaccine administration</h2>

<h3 id="who">Who can get the vaccine now?</h3>
<p>The COVID-19 vaccine is available to anyone in Alaska who is age 5 or older. Pfizer vaccine is authorized for ages 12 and up and the pediatric Pfizer vaccine is authorized for ages 5 to 11</span>. The CDC preferentially recommends individuals receive an mRNA COVID-19 vaccine (Pfizer or Moderna) over Johnson &amp; Johnson's COVID-19 vaccine. </p>
<h3>Who should get a booster dose of COVID-19 vaccine? 
</h3>
<p>Booster doses are available for Moderna, Pfizer, and Johnson &amp; Johnson COVID-19 vaccines. If you are 16 or older and received a Pfizer-BioNTech vaccine or 18 or older and received a Moderna COVID-19 vaccine at least six months ago, you are encouraged to get a booster dose. Individuals who are immuno-compromised may also receive a booster dose.</p>
<p>For the Johnson &amp; Johnson COVID-19 vaccine, booster shots are recommended for those who are 18 and older and who were vaccinated two or more months ago. The CDC preferentially recommends individuals receive an mRNA vaccine over the Johnson &amp; Johnson COVID-19 vaccine. Eligible individuals may choose which vaccine they receive as a booster dose.   
</p>
<h3 id="already">Should I get vaccinated if I already had COVID-19?</h3>
<p>Yes. The CDC issued a report on October 29, 2021 stating that both fully-vaccinated individuals and unvaccinated individuals who had been infected with COVID-19 have a low risk of subsequent infection for at least 6 months. However, they also noted that vaccination after infection enhances protection and further reduces the risk of reinfection. Even if you have already been infected, vaccination is an important step to protect yourself and those around you: <a href="https://web.archive.org/web/20220125073841/https://www.cdc.gov/coronavirus/2019-ncov/science/science-briefs/vaccine-induced-immunity.html#anchor_1635539757101">read the full study</a>.</p>



<h3 id="othervaccines">Can I get a COVID-19 vaccine if I've recently received the flu vaccine, or any other vaccine?</h3>
<p>It is safe to receive a COVID-19 vaccine at the same time as other vaccines.  </p>
<h3 id="underlying">If I have an underlying medical condition, can I get vaccinated?</h3>
<p>Yes, people with underlying medical conditions can receive a COVID-19 vaccine as long as they have not had an immediate or severe allergic reaction to a COVID-19 vaccine or to any of the ingredients in the vaccine. Vaccination is an important consideration for people with certain underlying medical conditions because they are at increased risk for severe illness from COVID-19.</p>
<h3 id="immunocompromised">If I am moderately or severely immunocompromised, should I receive an additional dose of COVID vaccine?</h3>
<p>CDC recommends that people who are moderately to severely immunocompromised receive an additional dose of an mRNA COVID-19 Vaccine (Pfizer-BioNTech or Moderna) at least 28 days after completion of the initial mRNA COVID-19 vaccine series. New studies show some people who are  

immunocompromised had a reduced immune response to the initial COVID-19 vaccine series. This includes people who have:  </p>
<ul>
  <li>  Active treatment for solid tumor and hematologic malignancies  
    </li>
  <li>Receipt of a solid-organ transplant and taking immunosuppressive therapy    </li>
  <li>Receipt of CAR-T-cell or hematopoietic stem cell transplant (within 2 years of transplantation or taking immunosuppression therapy)  
    
    Moderate or severe primary immunodeficiency (such as DiGeorge syndrome, Wiskott-Aldrich syndrome) </li>
  <li>Advanced or untreated HIV infection    </li>
  <li>Active treatment with high-dose corticosteroids (&ge;20mg prednisone or equivalent per day), alkylating agents, antimetabolites, transplant-related immunosuppressive drugs, cancer chemotherapeutic agents classified as severely immunosuppressive, tumor-necrosis (TNF) blockers, and other biologic agents that are immunosuppressive or immunomodulatory. </li>
</ul>
<h2 class="dz-Element-h2">Information for families</h2>
<h3 class="dz-Element-h3">Can children get the COVID-19 vaccines at their pediatricians' offices, as well as other locations?</h3>
<p class="dz-Element-p">Many private health care providers do offer COVID-19 vaccines. Check for availability near you at vaccines.gov.  </p>
<h3 class="dz-Element-h3">Why should I vaccinate my child? </h3>
<p class="dz-Element-p">Vaccination protects against severe illness, hospitalization, and death. It gets our kids back to the programs, activities and social interactions they need for appropriate academic, social-emotional and physical development. </p>

<p>Parents can protect their children from needing hospitalization or having long-lasting COVID-19 symptoms. Medical science has proven that other vaccines keep dangerous diseases and viruses away from our children, such as measles, mumps, and polio. The COVID-19 vaccines are no different.</p>
<p></p>
<h3 class="dz-Element-h3">Will children receive the same COVID-19 vaccine dose as adults?</h3>
<p></p>
<p class="dz-Element-p">Clinical trials evaluate the safety and effectiveness of vaccine dosage for different age groups. Children ages 12 and older receive the same vaccine dose as adults.  Children ages 5-11 will receive a smaller dose of the Pfizer vaccine.</p>
<p></p>
<h3 class="dz-Element-h3">If I am pregnant or breastfeeding, can I get vaccinated?</h3>
<p></p>
<p class="dz-Element-p">Alaskans who are pregnant or breastfeeding are<span data-contrast="none" xml:lang="EN-US" lang="EN-US">strongly recommend</span><span data-contrast="none" xml:lang="EN-US" lang="EN-US">ed to get a</span><span data-contrast="none" xml:lang="EN-US" lang="EN-US">Â COVID-19 vaccination either before or during pregnancy</span><span data-contrast="none" xml:lang="EN-US" lang="EN-US">. T</span><span data-contrast="none" xml:lang="EN-US" lang="EN-US">he benefits of vaccination outweigh known or potential risks. Pregnant and recently pregnant people with COVID-19 are at increased risk of severe illness, death, and pregnancy complications.</span> </p>
<p class="dz-Element-p">To speak to someone about COVID-19 vaccination during pregnancy, you can contact MotherToBaby. MotherToBaby experts are available to answer questions in English or Spanish by phone or chat. The free and confidential service is available Monday - Friday 8am - 5pm (local time). To reach <a href="https://web.archive.org/web/20220125073841/https://mothertobaby.org/">MotherToBaby</a>: </p>
	<ul>
		<li>Call 1-866-626-6847 </li>
		<li>Chat live or send an email MotherToBaby </li>
	</ul>
<p></p>
<h3 class="dz-Element-h3">Can the COVID-19 vaccine cause infertility?</h3>
<p></p>
<p class="dz-Element-p">There is no evidence that COVID-19 vaccines cause infertility.</p>
<p></p>
<h2 id="schedulingappts">Planning for and scheduling your appointment </h2>
<h3 id="schedule"><strong>How do I schedule my vaccine appointment? </strong></h3>
<p>To schedule a vaccine appointment, you can visit <a href="https://web.archive.org/web/20220125073841/https://www.vaccines.gov/">vaccines.gov</a>.  If you need assistance, please call  907-646-3322 or 1-833-482-9546 from 9 a.m. - 6:30 p.m. on weekdays, and 9 a.m. - 4:30 p.m. on weekends. </p>

<h3 id="choice"><strong>Will I have a choice in which vaccine I can get? </strong></h3>
<p>Many vaccination clinics note on the website which vaccine is being offered. You may select a clinic site that has your preferred vaccine.</p>
<h2 id="safe">Safety and Effectiveness</h2>

<h3 id="safety"><strong>Are COVID-19 vaccines safe?</strong></h3>
<p>COVID-19 vaccines are safe. The COVID-19 vaccine has been administered to hundreds of millions of people in the United States since December 2020. The vaccines were evaluated in thousands of participants in clinical trials and safety checks are ongoing to ensure the vaccines are safe.  </p>
<p>Learn more about the V-safe after-vaccination health checker and how to report side effects in the  <a href="/web/20220125073841/https://dhss.alaska.gov/dph/epi/id/SiteAssets/Pages/HumanCoV/v-safe-information-sheet.pdf">Vaccine Safety Application  V-Safe Information Sheet (PDF)</a>.<a href="#AlaskaNative"></a><br/>
</p>

<h3 id="effective"><a name="_1b72l0mkef61"></a><strong>Are COVID-19 vaccines effective?</strong></h3>
<p>Yes, the COVID-19 vaccines are effective. A safe and effective COVID vaccine is an important tool for ending the global pandemic. Vaccines help prevent serious illness, hospitalization and death from COVID-19. Vaccines also protect the people around you - including Alaska's healthcare workers and their patients.</p>

<p>The Food and Drug Administration (FDA) authorizes vaccines after they pass several clinical trials. The Pfizer vaccine is FDA approved for ages 16+.</p>
<h3>What are the common side effects of COVID-19 vaccines?  </h3>
<p>After getting vaccinated, you might have some side effects, which are normal signs that your body is building protection. Common side effects are pain, redness, and swelling in the arm where you received the shot, as well as tiredness, headache, muscle pain, chills, fever, and nausea throughout the rest of the body. These side effects could affect your ability to do daily activities, but they should go away in a few days. </p>
<h2 id="mandate">Health mandates</h2>
<h3 id="mandate"><strong>Will there be a COVID-19 vaccine mandate?</strong></h3>
<p>There are no plans for a statewide Alaska COVID-19 vaccine mandate. However, some employers may require it.</p>





<h2 id="costs">Cost</h2>
<h3 id="cost"><strong>How much does the vaccine cost?</strong></h3>
	<p>COVID-19 vaccines are provided at no cost to you, regardless of your insurance policy. </p>

<p>Vaccination providers can be reimbursed for vaccine administration fees by the patient's public or private insurance company or, for uninsured patients, by the Health Resources and Services Administration's Provider Relief Fund. No one can be denied a vaccine if they are unable to pay a vaccine administration fee. </p>







<h2 id="vaccinationefforts">About Alaska's vaccination effort</h2>
<h3 id="how"><strong>How many Alaskans are vaccinated?</strong></h3>
<p>View the <a href="https://web.archive.org/web/20220125073841/https://experience.arcgis.com/experience/a7e8be4adbe740a1bad1393894ee4075/">Alaska COVID-19 Vaccination Dashboard</a> for updated information about vaccination and doses administered in Alaska. </p>















<h2 id="traveling">What to expect after you are  vaccinated<br/>
</h2>


<h3>After I get vaccinated, do I need to continue COVID-19 precautions like distancing and wearing a mask? </h3>
<p>You are considered fully vaccinated two weeks after your second dose in a 2-dose series (Pfizer or Moderna) or two weeks after a single-dose vaccine (Johnson &amp; Johnson). It is advisable to wear a mask in indoor public settings if there is high transmission in your community or if you are at risk for severe disease, regardless of your vaccination status. Review the complete updated <a href="https://web.archive.org/web/20220125073841/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/fully-vaccinated.html">guidelines for when you've been fully vaccinated</a> at the CDC website.</p>
<h3 id="documentation"><strong>Will I receive documentation that I was vaccinated?</strong></h3>
<p>You will receive a vaccine record card from the healthcare provider who administers your vaccination.</p>
	<h3 id="replacement">How do I get a replacement vaccination card? 
</h3>
If you need a copy of your immunization record, including your COVID-19 vaccinations, please print out this <a href="/web/20220125073841/https://dhss.alaska.gov/dph/epi/id/siteassets/pages/HumanCoV/Immunization_Request_Form.pdf">Immunization Request Form (PDF)</a> and mail or fax it to the Alaska Immunization VacTrAK program, which keeps track of the immunization records for Alaskans. Instructions on how to return the form are at the bottom of the form. Please include a copy of a supporting document that identifies the person requesting the immunization record.

<h2 id="resources"><strong>Resources</strong></h2>
<h3 id="webpages"><strong>Alaska COVID-19 vaccine  information pages</strong></h3>
<ul>
  <li><a href="vaccine.aspx">Alaska COVID-19 Vaccine  Main Page</a></li>
  <li><a href="VaccineProviders.aspx">Healthcare workers</a></li>
</ul>
<h3 id="latest"><strong>Hear the latest about vaccine in  Alaska</strong></h3>
<ul>
  <li><a href="https://web.archive.org/web/20220125073841/https://alaska.zoom.us/webinar/register/WN_i2syaoh3SCCTKY2U289H_Q">COVID Science ECHO</a> Wednesdays, 12:00 pm-1:00 pm</li>
  <li><a href="https://web.archive.org/web/20220125073841/https://chd.app.box.com/s/mtny6d1d9ygssxrrb16afr13hcc39ond/folder/122409363861">Archived COVID-19 Vaccine ECHO  session recordings</a> are available.</li>
</ul>

	
	
	<!-- InstanceEndEditable --> </div>  
	</div> </div> 
	<div id="dhss-footer"> <div id="footer-social"> <ul> <li><a href="https://web.archive.org/web/20220125073841/http://www.facebook.com/pages/Anchorage-AK/Alaska-Department-of-Health-and-Social-Services/99962144928" class="facebook" title="Facebook">&nbsp;</a></li> <li><a href="https://web.archive.org/web/20220125073841/http://twitter.com/Alaska_DHSS" class="twitter" title="Twitter">&nbsp;</a></li> <li><a href="https://web.archive.org/web/20220125073841/http://vimeo.com/alaskadhss" class="vimeo" title="Vimeo">&nbsp;</a></li> <li><a href="https://web.archive.org/web/20220125073841/http://www.youtube.com/alaskadhss" class="youtube" title="YouTube">&nbsp;</a></li> <li><a href="https://web.archive.org/web/20220125073841/https://public.govdelivery.com/accounts/AKDHSS/subscriber/new?topic_id=AKDHSS_14" class="govdelivery" title="GovDelivery">&nbsp;</a></li> </ul> </div> <div id="footer-general"> <ul> <li><a href="https://web.archive.org/web/20220125073841/https://dhss.alaska.gov/DHSScontacts.pdf">Contacts</a></li> <li><a href="https://web.archive.org/web/20220125073841/https://dhss.alaska.gov/pages/accessibility.aspx">Accessibility</a></li> <li><a href="https://web.archive.org/web/20220125073841/https://aws.state.ak.us/OnlinePublicNotices/default.aspx">Public Notices</a></li> <li class="webmaster"><a href="https://web.archive.org/web/20220125073841/mailto:hsswebmaster@alaska.gov">Webmaster</a></li> </ul> </div> <div id="footer-divisions">&nbsp; </div> </div> </div> </div> <div id="footer-wrapper"> <div id="soa-footer"> <ul class="menu"> <li><a href="https://web.archive.org/web/20220125073841/http://www.alaska.gov/">State of Alaska</a></li> <li><a href="https://web.archive.org/web/20220125073841/https://myalaska.state.ak.us/home/app">myAlaska</a></li> <li><a href="https://web.archive.org/web/20220125073841/http://www.alaska.gov/akdir1.html">My Government</a></li> <li><a href="https://web.archive.org/web/20220125073841/http://www.alaska.gov/residentHome.html">Resident</a></li> <li><a href="https://web.archive.org/web/20220125073841/http://www.alaska.gov/businessHome.html">Business in Alaska</a></li> <li><a href="https://web.archive.org/web/20220125073841/http://www.alaska.gov/visitorHome.html">Visiting Alaska</a></li> <li><a href="https://web.archive.org/web/20220125073841/http://www.alaska.gov/employeeHome.html">State Employees</a></li> </ul> <ul class="footer"> <li>State of Alaska</li> <li>&copy; 2021</li> <li class="last-child"><a href="https://web.archive.org/web/20220125073841/mailto:hsswebmaster@alaska.gov">Webmaster</a></li> </ul> </div> </div> </div> 
	</body>
<!-- InstanceEnd --></html><!--
     FILE ARCHIVED ON 07:38:41 Jan 25, 2022 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 06:04:15 Apr 25, 2022.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
-->
<!--
playback timings (ms):
  captures_list: 5132.924
  exclusion.robots: 0.109
  exclusion.robots.policy: 0.1
  RedisCDXSource: 0.726
  esindex: 0.01
  LoadShardBlock: 5109.466 (3)
  PetaboxLoader3.datanode: 5260.933 (5)
  CDXLines.iter: 15.702 (3)
  load_resource: 343.126 (2)
  PetaboxLoader3.resolve: 165.685 (2)
-->