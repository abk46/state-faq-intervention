<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us" dir="ltr" __expr-val-dir="ltr"><!-- InstanceBegin template="/Templates/department.dwt" codeOutsideHTMLIsLocked="false" --><head><script src="//archive.org/includes/analytics.js?v=cf34f82" type="text/javascript"></script>
<script type="text/javascript">window.addEventListener('DOMContentLoaded',function(){var v=archive_analytics.values;v.service='wb';v.server_name='wwwb-app215.us.archive.org';v.server_ms=981;archive_analytics.send_pageview({});});</script>
<script type="text/javascript" src="/_static/js/bundle-playback.js?v=36gO9Ebf" charset="utf-8"></script>
<script type="text/javascript" src="/_static/js/wombat.js?v=UHAOicsW" charset="utf-8"></script>
<script type="text/javascript">
  __wm.init("https://web.archive.org/web");
  __wm.wombat("http://dhss.alaska.gov/default.htm?aspxerrorpath=/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx","20210528223318","https://web.archive.org/","web","/_static/",
	      "1622241198");
</script>
<link rel="stylesheet" type="text/css" href="/_static/css/banner-styles.css?v=fantwOh2" />
<link rel="stylesheet" type="text/css" href="/_static/css/iconochive.css?v=qtvMKcIJ" />
<!-- End Wayback Rewrite JS Include -->

<!-- InstanceBeginEditable name="doctitle" -->
<title> Alaska Department of Health and Social Services </title>
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><meta http-equiv="Expires" content="0"/><meta http-equiv="x-ua-compatible" content="IE=Edge"/><link rel="shortcut icon" href="https://web.archive.org/web/20210528223318im_/http://dhss.alaska.gov/outage/Style Library/DhssZen/Images/favicon.ico" type="image/vnd.microsoft.icon"/><link rel="stylesheet" type="text/css" href="https://web.archive.org/web/20210528223318cs_/http://dhss.alaska.gov/outage/Style Library/DhssZen/css/controls.css"/>
<link rel="stylesheet" type="text/css" href="https://web.archive.org/web/20210528223318cs_/http://dhss.alaska.gov/outage/Style Library/DhssZen/css/corev4.css"/>
<link rel="stylesheet" type="text/css" href="https://web.archive.org/web/20210528223318cs_/http://dhss.alaska.gov/outage/Style Library/DhssZen/css/Menu.css"/>
<link rel="stylesheet" type="text/css" href="https://web.archive.org/web/20210528223318cs_/http://dhss.alaska.gov/outage/Style Library/DhssZen/css/page-layouts-21.css"/>



    <link rel="stylesheet" type="text/css" href="https://web.archive.org/web/20210528223318cs_/http://dhss.alaska.gov/outage/Style Library/DhssZen/css/reset.css"/><link rel="stylesheet" type="text/css" href="https://web.archive.org/web/20210528223318cs_/http://dhss.alaska.gov/outage/Style Library/DhssZen/css/DhssZen.css"/><link rel="stylesheet" type="text/css" href="https://web.archive.org/web/20210528223318cs_/http://dhss.alaska.gov/outage/Style Library/DhssZen/css/soa-structure.css"/><link rel="stylesheet" type="text/css" href="https://web.archive.org/web/20210528223318cs_/http://dhss.alaska.gov/outage/Style Library/DhssZen/css/dhss-content.css"/><link rel="stylesheet" type="text/css" href="https://web.archive.org/web/20210528223318cs_/http://dhss.alaska.gov/outage/Style Library/DhssZen/css/dhss-legacy.css"/><script type="text/javascript" src="https://web.archive.org/web/20210528223318js_/http://dhss.alaska.gov/outage/jquery-1.js"></script><script type="text/javascript" src="https://web.archive.org/web/20210528223318js_/http://dhss.alaska.gov/outage/menu.js"></script><script type="text/javascript" src="https://web.archive.org/web/20210528223318js_/http://dhss.alaska.gov/outage/init.js"></script><script type="text/javascript" src="https://web.archive.org/web/20210528223318js_/http://dhss.alaska.gov/outage/msstring.js"></script><script type="text/javascript" src="https://web.archive.org/web/20210528223318js_/http://dhss.alaska.gov/outage/ie55up.js"></script><script type="text/javascript" src="https://web.archive.org/web/20210528223318js_/http://dhss.alaska.gov/outage/jquery-1_002.js"></script><script type="text/javascript" src="https://web.archive.org/web/20210528223318js_/http://dhss.alaska.gov/outage/jquery-noconflict.js"></script><script type="text/javascript" src="https://web.archive.org/web/20210528223318js_/http://dhss.alaska.gov/outage/listimport.js"></script><script type="text/javascript" src="https://web.archive.org/web/20210528223318js_/http://dhss.alaska.gov/outage/ScriptResource_002.js"></script><script type="text/javascript" src="https://web.archive.org/web/20210528223318js_/http://dhss.alaska.gov/outage/blank.js"></script><script type="text/javascript" src="https://web.archive.org/web/20210528223318js_/http://dhss.alaska.gov/outage/ScriptResource.js"></script>

	
<!-- Global site tag (gtag.js) - GA Google Analytics -->

<script async src="https://web.archive.org/web/20210528223318js_/https://www.googletagmanager.com/gtag/js?id=G-Q3JETRJWC4"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-Q3JETRJWC4');
</script>


<!-- end Global site tag (gtag.js) - UA Google Analytics -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://web.archive.org/web/20210528223318js_/https://www.googletagmanager.com/gtag/js?id=UA-185860736-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-185860736-1');
</script>

<!-- end Global site tag (gtag.js) - Google Analytics -->



	
    
	
<meta name="google-site-verification" content="jPGd8CiqqcBFgCHw2lVALvEzN1eZi3hHuHZGv3s0lW0"/><style type="text/css">


</style><script type="text/javascript" src="https://web.archive.org/web/20210528223318js_/http://dhss.alaska.gov/outage/core.js"></script><script type="text/javascript" src="https://web.archive.org/web/20210528223318js_/http://dhss.alaska.gov/outage/sp.js"></script><script type="text/javascript" src="https://web.archive.org/web/20210528223318js_/http://dhss.alaska.gov/outage/ScriptResx.js"></script><script type="text/javascript" src="https://web.archive.org/web/20210528223318js_/http://dhss.alaska.gov/outage/cui.js"></script><script type="text/javascript" src="https://web.archive.org/web/20210528223318js_/http://dhss.alaska.gov/outage/sp_003.js"></script><script type="text/javascript" src="https://web.archive.org/web/20210528223318js_/http://dhss.alaska.gov/outage/sp_005.js"></script><script type="text/javascript" src="https://web.archive.org/web/20210528223318js_/http://dhss.alaska.gov/outage/sp_002.js"></script><script type="text/javascript" src="https://web.archive.org/web/20210528223318js_/http://dhss.alaska.gov/outage/inplview.js"></script><script type="text/javascript" src="https://web.archive.org/web/20210528223318js_/http://dhss.alaska.gov/outage/sp_004.js"></script><script type="text/javascript" src="https://web.archive.org/web/20210528223318js_/http://dhss.alaska.gov/outage/ScriptResx_002.js"></script><script type="text/javascript" src="https://web.archive.org/web/20210528223318js_/http://dhss.alaska.gov/outage/mdn.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body>
<!-- =====  Start Main Area ============================================================ -->
<div id="s4-bodyContainer">

<a href="#soa-content" id="soa-skip-content">Skip to content</a>
<div id="top-wrapper" role="banner">
	<div id="soa-bar">
		<a id="soa-title" href="https://web.archive.org/web/20210528223318/http://www.alaska.gov/" name="soa">State of Alaska</a>
		<ul class="menu">
	      <li><a href="https://web.archive.org/web/20210528223318/https://my.alaska.gov/">myAlaska</a></li>
	      <li><a href="https://web.archive.org/web/20210528223318/http://www.alaska.gov/akdir1.html">My Government</a></li>
	      <li><a href="https://web.archive.org/web/20210528223318/http://www.alaska.gov/residentHome.html">Resident</a></li>
	      <li><a href="https://web.archive.org/web/20210528223318/http://www.alaska.gov/businessHome.html">Business in Alaska</a></li>
	      <li><a href="https://web.archive.org/web/20210528223318/http://www.alaska.gov/visitorHome.html">Visiting Alaska</a></li>
	      <li><a href="https://web.archive.org/web/20210528223318/http://www.alaska.gov/employeeHome.html">State Employees</a></li>
		</ul>
	</div>

	<div id="soa-header">
  		<a href="https://web.archive.org/web/20210528223318/http://dhss.alaska.gov/"><img class="soa-banner" src="https://web.archive.org/web/20210528223318im_/http://dhss.alaska.gov/outage/Header_DHSS.jpg" alt="Health and Social Services" style="border-width:0px;"/></a>

	     <div id="soa-search">
                            <input id="soa-searchquery" name="q" title="Search" type="text"/>
                            <input type="button" id="soa-searchsubmit" alt="Search"/>
                            <input value="DHSS" name="client" type="hidden"/>
                            <input value="DHSS" name="proxystylesheet" type="hidden"/>
                            <input value="date:D:L:d1" name="sort" type="hidden"/>
                            <input value="xml_no_dtd" name="output" type="hidden"/>
                            <input value="UTF-8" name="ie" type="hidden"/>
                            <input value="UTF-8" name="oe" type="hidden"/>
                        </div>
	</div>



	<div id="soa-breadcrumbs">
		<span><a href="#ctl00_ctl37_SkipLink"><img alt="Skip Navigation Links" src="https://web.archive.org/web/20210528223318im_/http://dhss.alaska.gov/outage/spacer.gif" style="border-width:0px;" width="0" height="0"/></a>Health and Social Services
	
		
		
		</span>
	</div>
</div>

<div id="content-wrapper">
    <div id="soa-content">
   

 <!-- s4-ca is the main body div -->
	

<div class="s4-ca">
		
<div id="DhssZenContent"><!-- InstanceBeginEditable name="MainContent" --><div class="grid3" role="main">
<div class="box" style="padding:30px;">
<p><strong>The Department of Health and Social Services website is currently unavailable. </strong> Additional information will be provided as it becomes available.</p>

<ul><li>Critical COVID-19 information – including <a href="appts.htm">vaccine appointment scheduling</a> and the <a href="https://web.archive.org/web/20210528223318/https://alaska-coronavirus-vaccine-outreach-alaska-dhss.hub.arcgis.com/">data dashboards</a> – can still be accessed.</li>
<li>The COVID-19 vaccine helpline is available at 907-646-3322 from 9 a.m. - 6:30 p.m. on weekdays, and 9 a.m. - 4:30 p.m. on weekends. 
</li>

<li>For all other services telephone assistance is available during regular business hours (8 a.m. - 4:30 p.m.) at 907-269-7800. Specific service and office contact numbers are attached.</li>
</ul>

</div>


		  <h1 style="padding-top:20px;">Need help with COVID-19 questions or requests?&nbsp;</h1>

<ul>
<li><a href="appts.htm">Find vaccine appointments</a></li>
<li>Health care providers should direct clinical questions to the Section of Epidemiology at 907-269-8000.</li>
<li>Questions regarding DHSS COVID response, including advisories, can be sent to <a href="https://web.archive.org/web/20210528223318/mailto:covidquestions@alaska.gov">covidquestions@alaska.gov</a>.</li>
<li>If you are a member of a media and have a question, please contact <a href="https://web.archive.org/web/20210528223318/mailto:Clinton.Bennett@alaska.gov">Clinton.Bennett@alaska.gov</a> or 907-269-4996 for resources and contacts.</li>
</ul>


<h2 style="padding-top:20px;">Alaska Careline</h2>
<p>If you are feeling hopeless or having thoughts of self-harm or suicide, reach out for help right away. Call the <a href="https://web.archive.org/web/20210528223318/https://carelinealaska.com/">Alaska Careline</a> at 877-266-4357 or text 4help to 839863. Or call the National Suicide Prevention Lifeline at 800-273-8255.</p>


<h2 id="data" class="clear" style="padding-top:20px;">Vaccine Data<br/></h2>

<iframe width="900" height="150" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="/web/20210528223318if_/http://dhss.alaska.gov/ https:/www.arcgis.com/apps/opsdashboard/index.html#/41b77a1e62fe4587a02e10193307ffb8"></iframe>
<ul>
<li>Visit the <a href="https://web.archive.org/web/20210528223318/https://alaska-coronavirus-vaccine-outreach-alaska-dhss.hub.arcgis.com/">Alaska Coronavirus Response Dashboard</a> to explore the the latest data and maps for the coronavirus response in our local area.</li>
</ul>




		  <h2 style="padding-top:20px;">Contact information for select DHSS programs and services</h2>

  <ul>
 <li><strong><a href="DHSScontacts.pdf">Department Contacts (PDF)</a></strong>
    </li>
<li><strong><a href="https://web.archive.org/web/20210528223318/https://alaska.gov/whitepages/">State of Alaska Employee Directory</a></strong>
    </li>
    <li><strong>Report Child Abuse</strong><br/>
      800-478-4444<br/>
<a href="https://web.archive.org/web/20210528223318/mailto:reportchildabuse@alaska.gov">ReportChildAbuse@alaska.gov</a><br/>
    </li>
    <li><a href="https://web.archive.org/web/20210528223318/https://alaska.wellsky.com/intake/"><strong>Adult Protective Services (Report of Harm)</strong></a><br/>
The link to the Centralized Reporting web form for making reports of harm and critical incident reports is currently unavailable through DHSS webpages. During this outage, please utilize the <a href="https://web.archive.org/web/20210528223318/https://alaska.wellsky.com/intake/">Centralized Reporting web form located at  https://alaska.wellsky.com/intake/</a>. You can also make a report of harm by completing the attached Intake Form and faxing to 907-269-3648 or sending via DSM email to <a href="https://web.archive.org/web/20210528223318/mailto:dsds.mcntake@hss.soa.directak.net">dsds.mcntake@hss.soa.directak.net</a>, or by calling Senior and Disabilities Services at 800-478-9996 or 907-269-3666.
    <li><strong>Alaska Psychiatric Institute</strong><br/>
      800-770-3930  </li>
    <li><strong>Public Assistance Emergency Helplines:</strong>
      <ul>
       
    
        <li><strong>Community Resources</strong><br/>
          2-1-1</li>
        <li><strong>Medicaid Recipient Helpline</strong><br/>
          800-780-9972    </li>
      </ul>
		<li><strong>Public Assistance Programs:</strong>
      <ul>
       
    <li><strong>SNAP (Food Stamps)</strong><br/>
    800-478-7778</li>
        <li><strong>Child Care&nbsp;</strong><br/>
          888-268-4632    </li>
		  <li><strong>Adult Public Assistance</strong><br/>
      800-478-7778</li>
    
    <li><strong>Chronic and Acute Medical Assistance</strong><br/>
      800-478-7778</li>
		    <li><strong>General Relief Assistance</strong><br/>
    800-478-7778</li>
    <li><strong>Heating Assistance</strong><br/>
    800-478-7778</li>
		  <li><strong>Senior Benefits Program </strong><br/>
      800-478-7778</li>
	
	 
    
		  <li><strong><a href="dpa/pages/nutri/default.htm">WIC (Women's Infants, and Children) Program</a>&nbsp;</strong><br/>
          907-465-3100 or 907-500-8714</li>
         <li><strong><a href="dpa/pages/nutri/default.htm">SNAP-Ed</a></strong><br/>
          907-465-3100 or 907-500-8357 </li>
		  <li><strong><a href="dpa/pages/nutri/default.htm">Commodities Supplemental Food Program (CSFP)</a></strong><br/>
		     907-465-3100 or 907-500-8357</li>
	   <li><strong><a href="dpa/pages/nutri/default.htm">Farmers' Market Nutrition Program (FMNP)</a></strong><br/>
	      907-465-3100 or 907-419-4161</li>
		  <li><strong><a href="dpa/pages/nutri/default.htm">Seniors Farmers' Market Nutrition Program (SFMNP)</a></strong><br/>
		     907-465-3100 or 907-419-4161</li>
		  
      </ul>
    </li>

    <li><strong>Adoption &amp; Guardianship </strong><br/>
      800-478-7307</li>
    
	  
    <li><strong>Foster Care</strong><br/>
      800-478-7307</li>
  
    <li><strong>Medicaid </strong><br/>
      800-478-7778</li>
    
    <li><strong>Medicaid Preferred Drug List</strong><br/>
      907-334-2425</li>
    
    <li><strong>Personal Care Services Program </strong><br/>
      800-478-9996</li>
    
    <li><strong>Report Elder Abuse</strong><br/>
      </a>800-478-9996</li>
    
    


    
    <li><a href="https://web.archive.org/web/20210528223318/https://ovr.akleg.gov/"><strong>Victim's Rights</strong> (Juvenile Justice)<br/>
    </a>844-754-3460   </li>
    </ul>

</div>

	<!-- InstanceEndEditable -->
		</div>
	
<div id="DhssZenNav" class="grid1">
	
	<div id="zz1_TopRightNavMenu" class="DhssZenNav" role="navigation">
	<div class="menu vertical menu-vertical">
		<ul class="root static" style="padding: 5px;">
			<li><span class="static menu-item"><span class="additional-background"><span class="menu-item-text">Health and Social Services</span></span></span></li>
			<li><strong>Office of the Commissioner</strong><br/>
			  907-269-7800		</li>
	<li><strong>Alaska Pioneer Homes</strong><br/>
			    907-465-4416
			  </span></span></li>
	<li><strong>Alaska Psychiatric Institute</strong><br/>
			          907-269-7100			  </li>
	<li><strong>Behavioral Health</strong><br/>
			                  800-465-4828			          </li>
<li><strong>Finance and Management Services</strong><br/>
			                          907-465-3030			                 </li>
	<li><a href="/web/20210528223318/http://dhss.alaska.gov/dhcs/"><strong>Health Care Services</strong></a><br/>
			                                  907-465-1617			                          </li>
	<li><a href="/web/20210528223318/http://dhss.alaska.gov/djj/"><strong>Juvenile Justice</strong></a><br/>
			                                          907-465-2212			                                  </li>
<li><a href="/web/20210528223318/http://dhss.alaska.gov/ocs/"><strong>Office of Children's Services</strong></a><br/>
			                                            907-269-4000
			                                          </li>
	<li><a href="/web/20210528223318/http://dhss.alaska.gov/osmap/"><strong>Office of Substance Misuse and Addiction Prevention</strong></a><br/>
			                                            907-334-2602
			                                          </li>
	<li><a href="/web/20210528223318/http://dhss.alaska.gov/dpa/"><strong>Public Assistance</strong></a><br/>
			                                                  888-876-2477			                                          </span></span></li>
	<li><strong>Public Health</strong><br/>
			                                                          907-465-3090			                                                  </li>
	<li><strong>Senior and Disabilities Services</strong><br/>
			                                                          800-478-9996			                                                 </li>
		</ul>
	</div>
</div>
</div>
	
	</div>


    <div id="dhss-footer" role="contentinfo">
	
		<div id="footer-social">
			<ul>
				<li><a href="https://web.archive.org/web/20210528223318/http://www.facebook.com/pages/Anchorage-AK/Alaska-Department-of-Health-and-Social-Services/99962144928" class="facebook" title="Facebook">&nbsp;</a></li>
				<li><a href="https://web.archive.org/web/20210528223318/http://twitter.com/Alaska_DHSS" class="twitter" title="Twitter">&nbsp;</a></li>
				<li><a href="https://web.archive.org/web/20210528223318/http://vimeo.com/alaskadhss" class="vimeo" title="Vimeo">&nbsp;</a></li>
				<li><a href="https://web.archive.org/web/20210528223318/http://www.youtube.com/alaskadhss" class="youtube" title="YouTube">&nbsp;</a></li>
				<li><a href="https://web.archive.org/web/20210528223318/https://public.govdelivery.com/accounts/AKDHSS/subscriber/new?topic_id=AKDHSS_14" class="govdelivery" title="GovDelivery">&nbsp;</a></li>
			</ul>
	  
		</div>

		<div id="footer-general">
			<ul>
				<li><a href="https://web.archive.org/web/20210528223318/http://dhss.alaska.gov/DHSScontacts.pdf">Contacts</a></li>
				<li><a href="https://web.archive.org/web/20210528223318/http://dhss.alaska.gov/accessibility.htm">Accessibility</a></li>
				<li><a href="https://web.archive.org/web/20210528223318/https://aws.state.ak.us/OnlinePublicNotices/default.aspx">Public Notices</a></li>
				<li class="webmaster"><a href="https://web.archive.org/web/20210528223318/mailto:hsswebmaster@alaska.gov">Webmaster</a></li>
			</ul>
		</div>
	  
		<div id="footer-divisions">&nbsp;
			</div>

   </div>

    </div>

	</div>
</div>
</div>


<div id="footer-wrapper" role="contentinfo">
	<div id="soa-footer">
		<ul class="menu">
			<li><a href="https://web.archive.org/web/20210528223318/http://www.alaska.gov/">State of Alaska</a></li>
			<li><a href="https://web.archive.org/web/20210528223318/https://myalaska.state.ak.us/home/app">myAlaska</a></li>
			<li><a href="https://web.archive.org/web/20210528223318/http://www.alaska.gov/akdir1.html">My Government</a></li>
			<li><a href="https://web.archive.org/web/20210528223318/http://www.alaska.gov/residentHome.html">Resident</a></li>
			<li><a href="https://web.archive.org/web/20210528223318/http://www.alaska.gov/businessHome.html">Business in Alaska</a></li>
			<li><a href="https://web.archive.org/web/20210528223318/http://www.alaska.gov/visitorHome.html">Visiting Alaska</a></li>
			<li><a href="https://web.archive.org/web/20210528223318/http://www.alaska.gov/employeeHome.html">State Employees</a></li>
		</ul>
		<ul class="footer">
			<li>State of Alaska</li>
			<li>© 2021</li>
			<li class="last-child"><a href="https://web.archive.org/web/20210528223318/mailto:hsswebmaster@alaska.gov">Webmaster</a></li>
		</ul>
	</div>
</div>

</div>

</body><!-- InstanceEnd --></html><!--
     FILE ARCHIVED ON 22:33:18 May 28, 2021 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 05:38:47 Apr 25, 2022.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
-->
<!--
playback timings (ms):
  captures_list: 758.143 (2)
  exclusion.robots: 0.153 (2)
  exclusion.robots.policy: 0.14 (2)
  RedisCDXSource: 28.398 (2)
  esindex: 0.013 (2)
  LoadShardBlock: 390.298 (6)
  PetaboxLoader3.datanode: 223.037 (8)
  CDXLines.iter: 51.985 (6)
  load_resource: 195.831 (2)
  PetaboxLoader3.resolve: 62.404
-->