
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" __expr-val-dir="ltr" lang="en-us" dir="ltr">
<head><script src="//archive.org/includes/analytics.js?v=cf34f82" type="text/javascript"></script>
<script type="text/javascript">window.addEventListener('DOMContentLoaded',function(){var v=archive_analytics.values;v.service='wb';v.server_name='wwwb-app28.us.archive.org';v.server_ms=224;archive_analytics.send_pageview({});});</script>
<script type="text/javascript" src="/_static/js/bundle-playback.js?v=36gO9Ebf" charset="utf-8"></script>
<script type="text/javascript" src="/_static/js/wombat.js?v=UHAOicsW" charset="utf-8"></script>
<script type="text/javascript">
  __wm.init("https://web.archive.org/web");
  __wm.wombat("http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/vaccineinfo.aspx","20210328200020","https://web.archive.org/","web","/_static/",
	      "1616961620");
</script>
<link rel="stylesheet" type="text/css" href="/_static/css/banner-styles.css?v=fantwOh2" />
<link rel="stylesheet" type="text/css" href="/_static/css/iconochive.css?v=qtvMKcIJ" />
<!-- End Wayback Rewrite JS Include -->



<!-- Global site tag (gtag.js) - GA Google Analytics -->

<script async src="https://web.archive.org/web/20210328200020js_/https://www.googletagmanager.com/gtag/js?id=G-Q3JETRJWC4"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-Q3JETRJWC4');
</script>


<!-- end Global site tag (gtag.js) - UA Google Analytics -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://web.archive.org/web/20210328200020js_/https://www.googletagmanager.com/gtag/js?id=UA-185860736-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-185860736-1');
</script>

<!-- end Global site tag (gtag.js) - Google Analytics -->




    <title>
	
	COVID-19: COVID-19 Vaccine Info

</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><meta http-equiv="Expires" content="0"/><meta http-equiv="x-ua-compatible" content="IE=Edge"/><meta property="og:image" content="https://web.archive.org/web/20210328200020im_/http://dhss.alaska.gov/PublishingImages/DHSSlogo.jpg"/><link rel="shortcut icon" href="/web/20210328200020im_/https://dhss.alaska.gov/Style Library/DhssZen/Images/favicon.ico" type="image/vnd.microsoft.icon"/><link rel="stylesheet" type="text/css" href="/web/20210328200020cs_/https://dhss.alaska.gov/Style%20Library/en-US/Themable/Core%20Styles/controls.css"/>
<link rel="stylesheet" type="text/css" href="/web/20210328200020cs_/https://dhss.alaska.gov/_layouts/1033/styles/Themable/corev4.css?rev=cubXjmzpjBGT%2FzzR9ULbAw%3D%3D"/>
<link rel="stylesheet" type="text/css" href="/web/20210328200020cs_/https://dhss.alaska.gov/_layouts/1033/styles/Themable/Menu.css?rev=j46FAsDJga%2BaixwEf8A%2Fgg%3D%3D"/>
<link rel="stylesheet" type="text/css" href="/web/20210328200020cs_/https://dhss.alaska.gov/Style%20Library/en-US/Core%20Styles/page-layouts-21.css"/>
<link rel="stylesheet" type="text/css" href="/web/20210328200020cs_/https://dhss.alaska.gov/Style%20Library/DhssZen/css/alt_campaign.css"/>


    <script type="text/javascript">
	    var _fV4UI = true;
    </script>

    <link rel="stylesheet" type="text/css" href="/web/20210328200020cs_/https://dhss.alaska.gov/Style Library/DhssZen/css/reset.css?v=1.3"/><link rel="stylesheet" type="text/css" href="/web/20210328200020cs_/https://dhss.alaska.gov/Style Library/DhssZen/css/DhssZen.css?v=1.3"/><link rel="stylesheet" type="text/css" href="/web/20210328200020cs_/https://dhss.alaska.gov/Style Library/DhssZen/css/soa-structure.css?v=1.3"/><link rel="stylesheet" type="text/css" href="/web/20210328200020cs_/https://dhss.alaska.gov/Style Library/DhssZen/css/dhss-content.css?v=1.3"/><link rel="stylesheet" type="text/css" href="/web/20210328200020cs_/https://dhss.alaska.gov/Style Library/DhssZen/css/dhss-legacy.css?v=1.3"/><script type="text/javascript">
// <![CDATA[
document.write('<script type="text/javascript" src="/style%20library/dhsszen/scripts/jquery-1.7.2.min.js"></' + 'script>');
document.write('<script type="text/javascript" src="/style%20library/dhsszen/scripts/menu.js"></' + 'script>');
document.write('<script type="text/javascript" src="/_layouts/1033/init.js?rev=mdjxRHK1V7KZ%2BuOTcNNcmQ%3D%3D"></' + 'script>');
document.write('<script type="text/javascript" src="/_layouts/1033/jquery-1.4.4.min.js?rev=c6nDNMXKcdcNCStCBk9kdg%3D%3D"></' + 'script>');
document.write('<script type="text/javascript" src="/_layouts/1033/jquery-noconflict.js?rev=nBDuS1QCCe5SuB%2BmQlVskQ%3D%3D"></' + 'script>');
document.write('<script type="text/javascript" src="/_layouts/listimport/listimport.js?rev=jZKUH8ts44bwUzFbeAf%2F1Q%3D%3D"></' + 'script>');
document.write('<script type="text/javascript" src="/ScriptResource.axd?d=ea23uepthaEvF_TU_KFnyhfDmINentVRWBgrZWYeHp-2_5dpwQPMTO-0VYOlpRzmkTTSHLhJtdKg84IzdzkOPNFtP0l9tJ9WmX6iCOeQr8tVdevmP6W5pprRYRFEv6pzHjANzlNlNgMoFfkHCguA90MY2Xs1&amp;t=3f4a792d"></' + 'script>');
document.write('<script type="text/javascript" src="/_layouts/blank.js?rev=QGOYAJlouiWgFRlhHVlMKA%3D%3D"></' + 'script>');
// ]]>
</script>

	
	<script type="text/javascript">
		function ProcessImn(){}
		function ProcessImnMarkers(){}	
	</script>

	
    
	
<meta name="google-site-verification" content="jPGd8CiqqcBFgCHw2lVALvEzN1eZi3hHuHZGv3s0lW0"/><style type="text/css">
	.ctl00_wpz_0 { border-color:Black;border-width:1px;border-style:Solid; }

</style></head>

<body onload="javascript:_spBodyOnLoadWrapper();">
<form name="aspnetForm" method="post" action="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/vaccineinfo.aspx" id="aspnetForm">
<input type="hidden" name="MSOWebPartPage_PostbackSource" id="MSOWebPartPage_PostbackSource" value=""/>
<input type="hidden" name="MSOTlPn_SelectedWpId" id="MSOTlPn_SelectedWpId" value=""/>
<input type="hidden" name="MSOTlPn_View" id="MSOTlPn_View" value="0"/>
<input type="hidden" name="MSOTlPn_ShowSettings" id="MSOTlPn_ShowSettings" value="False"/>
<input type="hidden" name="MSOGallery_SelectedLibrary" id="MSOGallery_SelectedLibrary" value=""/>
<input type="hidden" name="MSOGallery_FilterString" id="MSOGallery_FilterString" value=""/>
<input type="hidden" name="MSOTlPn_Button" id="MSOTlPn_Button" value="none"/>
<input type="hidden" name="__REQUESTDIGEST" id="__REQUESTDIGEST" value="noDigest"/>
<input type="hidden" name="MSOSPWebPartManager_DisplayModeName" id="MSOSPWebPartManager_DisplayModeName" value="Browse"/>
<input type="hidden" name="MSOSPWebPartManager_ExitingDesignMode" id="MSOSPWebPartManager_ExitingDesignMode" value="false"/>
<input type="hidden" name="MSOWebPartPage_Shared" id="MSOWebPartPage_Shared" value=""/>
<input type="hidden" name="MSOLayout_LayoutChanges" id="MSOLayout_LayoutChanges" value=""/>
<input type="hidden" name="MSOLayout_InDesignMode" id="MSOLayout_InDesignMode" value=""/>
<input type="hidden" name="_wpSelected" id="_wpSelected" value=""/>
<input type="hidden" name="_wzSelected" id="_wzSelected" value=""/>
<input type="hidden" name="MSOSPWebPartManager_OldDisplayModeName" id="MSOSPWebPartManager_OldDisplayModeName" value="Browse"/>
<input type="hidden" name="MSOSPWebPartManager_StartWebPartEditingName" id="MSOSPWebPartManager_StartWebPartEditingName" value="false"/>
<input type="hidden" name="MSOSPWebPartManager_EndWebPartEditing" id="MSOSPWebPartManager_EndWebPartEditing" value="false"/>
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUBMA9kFgJmD2QWAgIBD2QWBAIBD2QWAgIBD2QWAmYPZBYCAgEPFgIeE1ByZXZpb3VzQ29udHJvbE1vZGULKYgBTWljcm9zb2Z0LlNoYXJlUG9pbnQuV2ViQ29udHJvbHMuU1BDb250cm9sTW9kZSwgTWljcm9zb2Z0LlNoYXJlUG9pbnQsIFZlcnNpb249MTQuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49NzFlOWJjZTExMWU5NDI5YwFkAgMPZBYIAgMPZBYCBSZnXzgzZTlmNzg5X2Y4MTVfNGE1OF84M2EyXzhmM2VlODMxZjAxOQ9kFgRmDxYCHgdWaXNpYmxlaGQCAQ8WAh8BaGQCBw9kFgICAQ9kFgRmD2QWAgIBDxYCHwFoFgJmD2QWBAICD2QWBgIBDxYCHwFoZAIDDxYIHhNDbGllbnRPbkNsaWNrU2NyaXB0BYQBamF2YVNjcmlwdDpDb3JlSW52b2tlKCdUYWtlT2ZmbGluZVRvQ2xpZW50UmVhbCcsMSwgNTMsICdodHRwOlx1MDAyZlx1MDAyZmRoc3MuYWxhc2thLmdvdlx1MDAyZmRwaFx1MDAyZkVwaVx1MDAyZmlkJywgLTEsIC0xLCAnJywgJycpHhhDbGllbnRPbkNsaWNrTmF2aWdhdGVVcmxkHihDbGllbnRPbkNsaWNrU2NyaXB0Q29udGFpbmluZ1ByZWZpeGVkVXJsZB4MSGlkZGVuU2NyaXB0BSJUYWtlT2ZmbGluZURpc2FibGVkKDEsIDUzLCAtMSwgLTEpZAIFDxYCHwFoZAIDDw8WCh4JQWNjZXNzS2V5BQEvHg9BcnJvd0ltYWdlV2lkdGgCBR4QQXJyb3dJbWFnZUhlaWdodAIDHhFBcnJvd0ltYWdlT2Zmc2V0WGYeEUFycm93SW1hZ2VPZmZzZXRZAusDZGQCAQ9kFgQCAw9kFgICAQ8QFgIfAWhkFCsBAGQCBQ9kFgJmD2QWAmYPFCsAA2RkZGQCDQ9kFgICAg9kFgICFw8WAh8ACysEAWQCEQ9kFgICAQ9kFgJmD2QWAgIDD2QWAgIFDw8WBB4GSGVpZ2h0GwAAAAAAAHlAAQAAAB4EXyFTQgKAAWQWAgIBDzwrAAkBAA8WBB4NUGF0aFNlcGFyYXRvcgQIHg1OZXZlckV4cGFuZGVkZ2RkZLBf0xSsaG2NIl37vUfusF85MzBj"/>


<script type="text/javascript">
//<![CDATA[
var MSOWebPartPageFormName = 'aspnetForm';
var g_presenceEnabled = true;
var g_wsaEnabled = false;
var g_wsaLCID = 1033;
var g_wsaSiteTemplateId = 'BLANKINTERNET#2';
var g_wsaListTemplateId = 850;
var _fV4UI=true;var _spPageContextInfo = {webServerRelativeUrl: "\u002fdph\u002fEpi\u002fid", webLanguage: 1033, currentLanguage: 1033, webUIVersion:4,pageListId:"{43e7fa95-79e4-49c4-931c-67a03394dcfd}",pageItemId:250, alertsEnabled:true, siteServerRelativeUrl: "\u002f", allowSilverlightPrompt:'True'};//]]>
</script>
<script type="text/javascript">
<!--
var L_Menu_BaseUrl="/dph/Epi/id";
var L_Menu_LCID="1033";
var L_Menu_SiteTheme="";
//-->
</script>
<script type="text/javascript">
//<![CDATA[
document.onreadystatechange=fnRemoveAllStatus; function fnRemoveAllStatus(){removeAllStatus(true)};var dlc_fvsi = {"DefaultViews":[],"ViewUrls":[],"WebUrl":"\/dph\/Epi\/id"};//]]>
</script>

<script type="text/javascript">
//<![CDATA[
function _spNavigateHierarchy(nodeDiv, dataSourceId, dataPath, url, listInContext, type) {
    CoreInvoke('ProcessDefaultNavigateHierarchy', nodeDiv, dataSourceId, dataPath, url, listInContext, type, document.forms.aspnetForm, "", "\u002fdph\u002fEpi\u002fid\u002fPages\u002fCOVID-19\u002fvaccineinfo.aspx");

}
//]]>
</script>
<script type="text/javascript">
//<![CDATA[
var _spWebPartComponents = new Object();//]]>
</script>

<script src="/web/20210328200020js_/https://dhss.alaska.gov/_layouts/blank.js?rev=QGOYAJlouiWgFRlhHVlMKA%3D%3D" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
if (typeof(DeferWebFormInitCallback) == 'function') DeferWebFormInitCallback();//]]>
</script>

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="BAB98CB3"/>






<!-- =====  Start Main Area ============================================================ -->
<div id="s4-bodyContainer">

<a href="#soa-content" id="soa-skip-content">Skip to content</a>
<div id="top-wrapper">
	<div id="soa-bar">
		<a id="soa-title" href="https://web.archive.org/web/20210328200020/http://www.alaska.gov/" name="soa">State of Alaska</a>
		<ul class="menu">
	      <li><a href="https://web.archive.org/web/20210328200020/https://my.alaska.gov/">myAlaska</a></li>
	      <li><a href="https://web.archive.org/web/20210328200020/http://www.alaska.gov/akdir1.html">My Government</a></li>
	      <li><a href="https://web.archive.org/web/20210328200020/http://www.alaska.gov/residentHome.html">Resident</a></li>
	      <li><a href="https://web.archive.org/web/20210328200020/http://www.alaska.gov/businessHome.html">Business in Alaska</a></li>
	      <li><a href="https://web.archive.org/web/20210328200020/http://www.alaska.gov/visitorHome.html">Visiting Alaska</a></li>
	      <li><a href="https://web.archive.org/web/20210328200020/http://www.alaska.gov/employeeHome.html">State Employees</a></li>
		</ul>
	</div>

	<div id="soa-header">
  		<a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/"><img class="soa-banner" src="https://web.archive.org/web/20210328200020im_/http://hss.state.ak.us/css/images/Header_DHSS.jpg" alt="Infectious Disease" border="0"/></a>

	     <div id="soa-search" style="background: none;">
                         
<script async src="https://web.archive.org/web/20210328200020js_/https://cse.google.com/cse.js?cx=c0fe050eb6ce15f1d"></script>
<div class="gcse-searchbox-only" data-resultsurl="http://dhss.alaska.gov/pages/search.aspx"></div>
</div>
	</div>

    <div id="menu">
      <ul id="menu_list">
        <li id="first_nav"><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/">Home</a></li>
        <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/Pages/Services.aspx">Divisions and Agencies</a>
          <div>
            <ul class="left">
                <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/daph">Alaska Pioneer Homes</a></li>
				<li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/api">Alaska Psychiatric Institute</a></li>
		        <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/dbh">Behavioral Health</a></li>

			<li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/Commissioner">Commissioner's Office</a></li>
	
			   
	<li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/fms">Finance &amp; Management Services</a></li>	

<li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/dhcs">Health Care Services</a></li>	


	
			</ul>
            <ul class="left">
                
<li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/djj">Juvenile Justice</a></li>                
				
<li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/ocs">Office of Children's Services</a></li>
				<li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/dpa">Public Assistance</a></li>
				<li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/dph">Public Health</a></li>
				<li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/dsds">Seniors &amp; Disabilities Services</a></li>

<li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/Commissioner/Pages/Boards/default.aspx">Boards, Councils &amp; Commissions</a></li>            </ul>  
            <br class="clear"/>
          </div>
       </li>
       <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/Pages/Services.aspx">Services</a>
          <div>
            <ul class="left">
                <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/dph/VitalStats">Birth &amp; Marriage Certificates </a></li>
                <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/dpa/Pages/ccare">Child Care </a></li>
                <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/ocs">Child Protection</a></li>
                <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/dpa/Pages/dkc/default.aspx">Denali KidCare</a></li>
                <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/dpa/Pages/SNAP/default.aspx">Supplemental Nutrition Assistance Program (SNAP) </a></li>
                <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/dph/Epi/iz/Pages/default.aspx">Immunization Information </a></li>
                <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/dhcs/Pages/Medicaid_Medicare">Medicaid</a></li>
                <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/dph/Nursing/Pages/locations.aspx">Public Health Centers </a></li>
                <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/dpa/Pages/atap">Temporary &quot;Cash&quot; Assistance </a> </li>
                <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/dpa/Pages/seniorbenefits">Senior Benefits Program</a></li>
                <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/dsds/Pages/Medicare">Medicare</a></li>
                <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/dbh/Pages/TreatmentRecovery/SubstanceAbuse">Substance Abuse Treatment</a></li>
			</ul>
            <ul class="left">
 <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/ocs/Pages/icwa/default.aspx">Alaska Tribal Child Welfare Compact</a></li>
                <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/dph/Director/Pages/LivingWill.aspx">Alaska Directives for Health Care &mdash; Living Will</a></li>
                <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/dph/Chronic/Pages/brfss">Behavioral Risk Factor Survey</a></li>
                <li><a href="https://web.archive.org/web/20210328200020/http://permaudit.alaska.gov/">PERM</a></li>              <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/dph/Epi/Pages/phan/default.aspx">Public Health Alert Network (PHAN)</a></li>
                <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/dhcs/Pages/CertificateOfNeed/default.aspx">Certificate of Need</a></li>
                <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/fms/facilities/Pages/safetyplan.aspx">Department Safety Plan</a></li>
                <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/Pages/Publications.aspx#CAP">Facilities Funding Allocation Plan</a></li>
            </ul>
            <br class="clear"/>
          </div>
		</li>
        <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/News">News</a>
          <div>
            <ul class=" left">
                <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/News/Pages/2021index.aspx">Press Releases</a></li>
<li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/blog">DHSS Insights</a></li> 
<li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/News/Pages/newsletter/CMO/CMO_News.aspx">Chief Medical Officer News</a></li> 
<li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/Commissioner/Pages/photos.aspx">Press Photos</a></li>
<li><a href="https://web.archive.org/web/20210328200020/https://aws.state.ak.us/OnlinePublicNotices/default.aspx">Public Notices</a></li>
                <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/Pages/Publications.aspx">Publications</a></li>
                <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/News/Pages/Newsroom.aspx">Newsroom</a></li>
            </ul>
            <br class="clear"/>
          </div>
        </li>
        <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/Commissioner/Pages/Contacts">Contact Us</a>
            <div>
                <ul class="left">
                    <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/Commissioner/Pages/Contacts">Contacts</a></li>
			        <li><a href="https://web.archive.org/web/20210328200020/mailto:hsswebmaster@alaska.gov">Webmaster</a></li>
			        <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/Pages/Accessibility.aspx">Accessibility</a></li>
                    <li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/Pages/Careers.aspx">DHSS Careers</a></li>
                </ul>
                <ul class="left">
                    <li><a href="https://web.archive.org/web/20210328200020/http://www.facebook.com/pages/Anchorage-AK/Alaska-Department-of-Health-and-Social-Services/99962144928">Facebook</a></li>
				    <li><a href="https://web.archive.org/web/20210328200020/http://twitter.com/Alaska_DHSS">Twitter</a></li>
				    <li><a href="https://web.archive.org/web/20210328200020/http://vimeo.com/alaskadhss">Vimeo</a></li>
				    <li><a href="https://web.archive.org/web/20210328200020/http://www.youtube.com/alaskadhss">YouTube</a></li>
				    <li><a href="https://web.archive.org/web/20210328200020/https://public.govdelivery.com/accounts/AKDHSS/subscriber/new?topic_id=AKDHSS_14">GovDelivery</a></li>
                </ul>
                <br class="clear"/>
            </div>
        </li>

<li><a href="https://web.archive.org/web/20210328200020/http://coronavirus.alaska.gov/">COVID-19 Resources</a>
            <div>
                <ul class="left">
<li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/default.aspx">DHSS COVID-19 Website</a></li>                  
<li><a href="https://web.archive.org/web/20210328200020/http://covid19.alaska.gov/">State of Alaska COVID-19 Website</a></li>
<li><a href="https://web.archive.org/web/20210328200020/https://covid19.alaska.gov/health-advisories/">COVID-19 Health Advisories</a></li>
<li><a href="https://web.archive.org/web/20210328200020/https://gov.alaska.gov/home/covid-19-economy/">COVID-19 Economic Stabilization</a></li>
<li><a href="https://web.archive.org/web/20210328200020/https://covid19.alaska.gov/reopen/">Reopen Alaska Responsibly Plan</a></li>
<li><a href="https://web.archive.org/web/20210328200020/https://ready.alaska.gov/covid19">COVID-19 Unified Command</a></li>
<li><a href="https://web.archive.org/web/20210328200020/https://gov.alaska.gov/home/covid19news/">COVID-19 News &amp; Video from Governor Mike Dunleavy</a></li>
                    <li><a href="https://web.archive.org/web/20210328200020/https://public.govdelivery.com/accounts/AKDHSS/subscriber/new?topic_id=AKDHSS_157">Sign up for COVID-19 updates</a></li>
                </ul>
              
                <br class="clear"/>
            </div>
        </li>


      </ul>
    </div>

	<div id="soa-breadcrumbs">
		<span><a href="#ctl00_ctl38_SkipLink"><img alt="Skip Navigation Links" height="0" width="0" src="/web/20210328200020im_/https://dhss.alaska.gov/WebResource.axd?d=Ylnie978DEV5711jLlQRdDn5MusmPX42s1MI0vQZfl06TNXLzB3nmC-pqSSXcz0sHH2njqI71A0oM3BZzO8JaXX1n_01&amp;t=637304273068272736" border="0"/></a><span><a title="Department of Health and Social Services" href="/web/20210328200020/https://dhss.alaska.gov/Pages/default.aspx">Health and Social Services</a></span><span> &gt; </span><span><a title="Division of Public Health" href="/web/20210328200020/https://dhss.alaska.gov/dph/Pages/default.aspx">Public Health</a></span><span> &gt; </span><span><a title="Section of Epidemiology
" href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/Pages/default.aspx">Epidemiology</a></span><span> &gt; </span><span><a title="Infectious Disease Program" href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/default.aspx">Infectious Disease</a></span><span> &gt; </span><span>COVID-19: COVID-19 Vaccine Info</span><a id="ctl00_ctl38_SkipLink"></a></span>
	</div>
</div>

<div id="ctl00_MSO_ContentDiv">
<div id="content-wrapper">
    <div id="soa-content">
    <!-- s4-ca is the main body div -->
	<div class="s4-ca">
		

<div id="menu" class="campaign" style="margin-top:-10px; padding-bottom:15px;">
<ul id="menu_list" style="list-style: outside none none; font-size:120%;">
<li id="first_nav"><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/default.aspx"><img src="https://web.archive.org/web/20210328200020im_/http://dhss.alaska.gov/dph/Epi/id/PublishingImages/COVID-19/bug_COVID.png" width="125px" alt="COVID-19"> Home</a></li>
<li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/sitemap.aspx">Sitemap</a>
</li>
<li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/vaccine.aspx">Vaccine</a>
<div style="font-size: 80%;">
<ul>
<li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/vaccine.aspx">COVID-19 Vaccine Homepage</a>
</li>
<li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/vaccineinfo.aspx">FAQs</a>
</li>
<li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/vaccineavailability.aspx">Vaccine Eligibility</a>
</li>
<li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/vaccineappointments.aspx">Find a COVID-19 Vaccine Provider</a>
</li>
<li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/vaccineallocation.aspx">Allocation Planning</a>
</li>
<li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/vaccineproviders.aspx">Healthcare worker information</a>
</li>
<li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/vaccineresources.aspx">Resources</a>
</li>
</ul>
</div>
</li>
<li><a href="https://web.archive.org/web/20210328200020/https://covid19.alaska.gov/">State of Alaska COVID-19<br/></a></li>
<li><a href="https://web.archive.org/web/20210328200020/http://cdc.gov/covid19">CDC COVID-19</a></li>
</ul>
</div>


	

<div class="grid4 covid">
<div id="google_translate_element"></div>

<script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
}
</script>

<script type="text/javascript" src="/web/20210328200020js_/https://dhss.alaska.gov/translate_a/element.js?cb=googleTranslateElementInit"></script>


   
    <div id="ctl00_PlaceHolderMain_PageContent_label" style="display:none">Page Content</div><div id="ctl00_PlaceHolderMain_PageContent__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_PageContent_label"><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/vaccine.aspx"><p class="buttons" style="font-size:80%;width:37%;margin-bottom:20px">
Return to Alaska COVID-19 Vaccine Homepage
</p></a>

<h1><strong>COVID-19 Vaccine Information for all Alaskans </strong></h1>

<p><em>Updated March 18, 2021</em></p>
<p>On this page:</p>
<ul><li><a href="#whoandwhen">Who should get vaccinated, and when</a></li>
<li><a href="#schedulingappts">Scheduling an appointment</a></li>
<li><a href="#whattoexpectat">What to expect at your appointment</a></li>
<li><a href="#safe">Safety</a></li>
<li><a href="#effective">Efficacy</a></li>
<li><a href="#mandate">Health advisories</a></li>
<li><a href="#costs">Cost</a></li>
<li><a href="#vaccinationefforts">About Alaska's vaccination effort</a></li>
<li><a href="#traveling">Travel</a></li>
<li><a href="#whattoexpectafter">What to expect after you are vaccinated</a></li>

<li><a href="#contacts">Contact information</a><br/></li></ul>


<h2 id="whoandwhen">Who  should get vaccinated, and when</h2>
<ul type="disc">
  <li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx#who">Who can get the vaccine now, and when will I       be able to?</a></li>
  <li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx#pregnant">If I am pregnant or breastfeeding, can I get vaccinated?</a></li>
  <li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx#Outside">I’m not currently in Alaska. What should I do to get       vaccinated?</a></li>
  <li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx#othervaccines">Can I get a COVID-19 vaccine if I’ve recently received       the flu vaccine, or any other vaccine?</a></li>
<li>If I have an underlying medical condition, can I get vaccinated?<br/></li>
  
</ul>

<h3 id="who">Who can get the vaccine now, and when will I be able to?</h3>
<p><strong>The COVID-19 vaccine is now available anyone living or working in Alaska who is age 16 or older.</strong><br/><strong></strong><strong>Appointments are limited, but <a href="https://web.archive.org/web/20210328200020/http://appointments.covidvax.alaska.gov/"><u>more appointments open regularly</u></a>. New appointments are often added on Thursdays. <a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/vaccine.aspx#contacts" style="text-decoration:underline">Phone assistance</a> is available. </strong></p>
<p>The U.S. Food &amp; Drug Administration (FDA) has authorized the Pfizer, Moderna, and Janssen COVID-19 vaccines for emergency use in the United States. Supply is limited at this time.</p>
<p><em><img src="/web/20210328200020im_/https://dhss.alaska.gov/dph/Epi/id/PublishingImages/COVID-19/WeAreHere.jpg" alt="" style="margin:0px;width:100%;height:349px"/><br/>Image for illustrative purposes only; please refer </em><span><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx#who"><em>Who       can get the vaccine now?</em></a><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx#who"><span style="display:inline-block"></span></a></span><em> </em><em>for information about who the vaccine is  currently available to.<br/></em></p>
<p></p>
<h3 id="pregnant">If I am pregnant or breastfeeding, can I get vaccinated?</h3>
<p></p>
<p>Alaskans who are pregnant or breastfeeding may choose to get a COVID-19 vaccine when they are eligible to receive it. There are limited data about the safety of COVID-19 vaccines for people who are pregnant. COVID-19 vaccines are unlikely to pose a risk to pregnant people or their babies based on current understanding.<br/></p>
<h3 id="Outside"><strong>I’m not currently in Alaska. What should I do to get  vaccinated?</strong></h3>
<p>You’ll want to follow the guidelines for where you are  located. States and local jurisdictions will have different guidance than what you’ll  see in Alaska. You’ll follow the guidance of the community you are in. You’ll  want to plan to get both doses of vaccine with the same healthcare provider. </p>


<h3 id="othervaccines">Can I get a COVID-19 vaccine if I’ve recently received the flu vaccine, or any other vaccine?</h3>
<p>The Centers for Disease Control (CDC) does not recommend getting the COVID-19 vaccine within 2 weeks of any other vaccine because insufficient data currently exists on the safety and efficacy of mRNA COVID-19 vaccines administered simultaneously with other vaccines. However, CDC has also recommended not to deny the COVID vaccine to someone who recently had another vaccine and is at high risk for COVID-19 at this time.</p>
<h3 class="dz-Element-h3">If I have an underlying medical condition, can I get vaccinated?</h3>
<p>Yes, people with underlying medical conditions can receive a COVID-19 vaccine as long as they have not had an immediate or severe allergic reaction to a COVID-19 vaccine or to any of the ingredients in the vaccine. Vaccination is an important consideration for people ages 16 and older with certain underlying medical conditions because they are at increased risk for severe illness from COVID-19. <br/></p>
<h2 id="schedulingappts">Scheduling an appointment </h2>
<ul type="disc">
  <li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx#waitlist">If I’m on a waitlist with a provider, should I work to       seek an appointment, or should I seek an available spot elsewhere?</a></li>
  <li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx#appointment">If I am not able to secure an appointment, what       opportunities will we have in the future?</a></li>
  <li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx#schedule">What do I need to do to schedule my second dose of       vaccine?</a></li>
</ul>
<h3 id="waitlist"><strong>If I’m on a waitlist with a provider, should I work to  seek an appointment, or should I seek an available spot elsewhere?</strong></h3>
<p>It’s okay to look for another spot, but providers are  continuing to follow up and let people know when vaccine is available. If you  do book an appointment elsewhere, you can let the provider you’re waitlisted  with know.</p>

<h3 id="appointment"><strong>If I am not able to secure an appointment, what  opportunities will we have in the future?</strong></h3>
<p>Appointments will be scheduled on a first come, first served  basis. We’re working to enroll more providers to vaccinate Alaskans, which will  result in more appointment availability. New appointments are added  regularly. </p>

<h3 id="schedule"><strong>What do I need to do to schedule my second dose of  vaccine?</strong></h3>
<p>The healthcare provider who gives you a your first dose will  give you instructions for scheduling your second dose. If you receive the Janssen Johnson &amp; Johnson vaccine, you will only need to receive one dose.</p>




<h2 id="whattoexpectat">What to expect at your appointment</h2>
<ul type="disc">
  <li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx#choice">Will I have a choice in which vaccine I can get?</a></li>
<li>Which vaccine should I get?</li>
<li>Do I have a choice of which type of vaccine I get?</li>

</ul>
<h3 id="choice"><strong>Will I have a choice in which vaccine I can get?</strong></h3>
<p>While vaccine is limited, you may not have options between  vaccines at a specific clinic location. Many vaccination clinics note on the website which vaccine is being offered. You may select a clinic site that has your preferred vaccine.  </p>
<h3 class="dz-Element-h3">Which vaccine should I get?</h3>
<p>It is recommend that you get the first vaccine you are offered, since the sooner a person is vaccinated, the sooner they are protected against COVID-19 illness. Please consult with your health care provider if you have concerns about receiving a specific vaccine.</p>
<h3 class="dz-Element-h3">Do I have a choice of which type of vaccine I get?</h3>
<p>While vaccine is limited, you may not have options between vaccines at a specific clinic location. Many vaccination clinics note on the website which vaccine is being offered. It is recommend that you get the first vaccine you are offered, since the sooner a person is vaccinated, the sooner they are protected against COVID-19 illness.  <br/></p>






<h2 id="safe">Safety </h2>
<ul type="disc">
  <li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx#safety">Is a COVID-19 vaccine safe?</a></li>
</ul>

<h3 id="safety"><strong>Is a COVID-19 vaccine safe?</strong></h3>
<p>COVID-19  vaccine safety is a top priority. No steps are skipped during the clinical  trial process for COVID-19 vaccine. Vaccine safety checks are in progress and  will continue as long as a vaccine is available. Vaccine safety is complicated  and important, and questions are expected and healthy.</p>
<p>Learn more  about the V-safe after-vaccination health checker and how to report side  effects in the <a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/SiteAssets/Pages/HumanCoV/v-safe-information-sheet.pdf">Vaccine Safety Application  V-Safe Information Sheet (PDF)</a>.</p>

<h2 id="effective">Efficacy</h2>
<ul type="disc">
  <li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx#effective">Will the vaccine be effective?</a></li>
<li>I read an article suggesting that the Johnson and Johnson Janssen vaccine is less effective in Alaska Native people.  Is there reliable scientific evidence to back this up?<br/></li>
</ul>

<h3 id="effective"><a name="_1b72l0mkef61"></a><strong>Will the vaccine be effective?</strong></h3>
<p>A safe and  effective COVID vaccine is an important tool for ending the global pandemic.  Vaccines can protect individuals in different ways. Vaccines also protect the  people around you - including Alaska’s healthcare workers and their  patients.      </p>
<p>The Food and  Drug Administration (FDA) authorizes vaccines after they pass several clinical  trials. Scientists are using clinical trials to test the COVID vaccine’s  effectiveness. These clinical trials require thousands of people and months of  data. The vaccine development is faster than normal because some steps are  being done at the same time instead of one after another. Learn more about <a href="https://web.archive.org/web/20210328200020/https://www.fda.gov/emergency-preparedness-and-response/mcm-legal-regulatory-and-policy-framework/emergency-use-authorization">FDA’s Emergency Use  Authorization authority</a> and watch a <a href="https://web.archive.org/web/20210328200020/https://www.youtube.com/watch?v=iGkwaESsGBQ">video on what an EUA is</a>.</p>
<p><a href="https://web.archive.org/web/20210328200020/https://www.fda.gov/news-events/press-announcements/fda-takes-key-action-fight-against-covid-19-issuing-emergency-use-authorization-first-covid-19">The FDA authorized the Pfizer  vaccine</a> on December 11, 2020. The <a href="https://web.archive.org/web/20210328200020/https://www.fda.gov/emergency-preparedness-and-response/coronavirus-disease-2019-covid-19/moderna-covid-19-vaccine">FDA authorized the Moderna vaccine</a> on  December 18, 2020. The <a href="https://web.archive.org/web/20210328200020/https://www.fda.gov/emergency-preparedness-and-response/coronavirus-disease-2019-covid-19/janssen-covid-19-vaccine">FDA authorized the Janssen vaccine</a> on February 27, 2021. All COVID-19 vaccines authorized by the FDA and recommended by the CDC are safe and effective in preventing COVID-19.</p>
<h3 class="dz-Element-h3">I read an article suggesting that the Johnson and Johnson Janssen vaccine is less effective in Alaska Native people.  Is there reliable scientific evidence to back this up?</h3>
<p>No. We don’t have any reason to think that the Johnson and Johnson Janssen vaccine is any less effective in Alaska Native people than any other racial group. The Johnson and Johnson Janssen vaccine is safe and effective. The reported vaccine efficacy among people in this racial group in the phase 3 clinical trial for this vaccine included fewer than 200 Alaska Native/American Indian people in the United States. This number is too small to draw any meaningful conclusions about vaccine efficacy.   <br/></p>
<h2 id="mandate">Health advisories</h2>
<ul type="disc">
  <li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx#mandate">Will there be a COVID-19 vaccine mandate?</a></li>
</ul>
<h3 id="mandate"><strong>Will there be a COVID-19 vaccine mandate?</strong></h3>
<p>There are no  plans for a statewide Alaska COVID-19 vaccine mandate. The role of the Alaska  COVID Vaccine Task Force is to provide Alaskans with the information they need  to make safe and healthy decisions about vaccination. State and Tribal leaders  prioritize your trust and safety.</p>





<h2 id="costs">Cost</h2>
<ul type="disc">
  <li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx#cost">How much will the vaccine cost?</a></li>
</ul>
<h3 id="cost"><strong>How much will the vaccine cost?</strong></h3>
<p>COVID-19 vaccines are provided at no cost to you, regardless of your insurance policy.</p>
<p>Vaccination providers can be reimbursed for vaccine administration fees by the patient’s public or private insurance company or, for uninsured patients, by the Health Resources and Services Administration’s Provider Relief Fund. No one can be denied a vaccine if they are unable to pay a vaccine administration fee.</p>







<h2 id="vaccinationefforts">About Alaska’s vaccination effort</h2>
<ul type="disc">
  <li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx#how">How many Alaskans are vaccinated?</a></li>
  <li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx#process">What process decides who will get the vaccine first?</a></li>
  <li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx#planning">Who is responsible for the planning for the COVID-19       vaccine?</a></li>
</ul>
<h3 id="how"><strong>How many Alaskans are vaccinated?</strong></h3>
<p>View the Alaska dashboard summary below, or see the full vaccine dashboard.<br/></p>
<div class="ms-rtestate-read ms-rte-wpbox"><div class="ms-rtestate-notify  ms-rtestate-read 83e9f789-f815-4a58-83a2-8f3ee831f019" id="div_83e9f789-f815-4a58-83a2-8f3ee831f019" unselectable="on"><table style="width:100%" cellpadding="0" cellspacing="0"><tr><td id="MSOZoneCell_WebPartWPQ1" valign="top" class="s4-wpcell-plain"><table class="s4-wpTopTable" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td><table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr class="ms-WPHeader">
				<td align="left" class="ms-wpTdSpace">&#160;</td><td title="Dashboard Summary - Allows authors to enter rich text content." id="WebPartTitleWPQ1" class="ms-WPHeaderTd"><h3 style="text-align:justify;" class="ms-standardheader ms-WPTitle"><nobr><span>Dashboard Summary</span><span id="WebPartCaptionWPQ1"></span></nobr></h3></td><td class="ms-WPHeaderTdSelection"><span class="ms-WPHeaderTdSelSpan"><input type="checkbox" id="SelectionCbxWebPartWPQ1" class="ms-WPHeaderCbxHidden" title="Select or deselect Dashboard Summary Web Part" onblur="this.className='ms-WPHeaderCbxHidden'" onfocus="this.className='ms-WPHeaderCbxVisible'" onkeyup="WpCbxKeyHandler(event);" onmouseup="WpCbxSelect(event); return false;" onclick="TrapMenuClick(event); return false;"/></span></td><td align="left" class="ms-wpTdSpace">&#160;</td>
			</tr>
		</table></td>
	</tr><tr>
		<td class="" valign="top"><div webpartid="86c1a4ee-914d-4f7a-9d9c-293fbcfe1c1c" webpartid2="83e9f789-f815-4a58-83a2-8f3ee831f019" haspers="false" id="WebPartWPQ1" width="100%" class="ms-WPBody ms-wpContentDivSpace" allowremove="false" allowdelete="false" style=""><iframe width="900" height="150" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="/web/20210328200020if_/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/ https:/www.arcgis.com/apps/opsdashboard/index.html#/41b77a1e62fe4587a02e10193307ffb8"></iframe>







</div></td>
	</tr>
</table></td></tr></table></div>
<div id="vid_83e9f789-f815-4a58-83a2-8f3ee831f019" unselectable="on" style="display:none"></div></div>

<h3 id="process">What process decides who gets  the vaccine first?</h3>
<p>Learn more about how decisions are made, and  how to provide input, at the <a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineAllocation.aspx">Alaska Vaccine Allocation Advisory Committee  information page</a>. </p>
<h3 id="planning"><strong>Who is responsible for planning for the  COVID-19 vaccine?</strong></h3>

<p>The State of  Alaska’s Department of Health and Social Services and Alaska Native Tribal  Health Consortium are working together to plan and distribute COVID-19  vaccines. The Alaska COVID-19 Vaccine Task Force includes eight sub-teams:  planning, operations, software solutions, payers, pharmacy, communications and  education, data and liaisons. On October 16, 2020, an <a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/SiteAssets/Pages/HumanCoV/AlaskaCOVID-19VaccinationDraftPlan.pdf">Alaska Draft COVID-19  Vaccination Plan</a> was submitted to the CDC for review. This was a requirement for all states. The  goal is to have a safe and effective vaccine available to all Alaskans who want  it.</p>














<h2 id="traveling">Travel</h2>
<ul type="disc">
  <li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx#travel">If I get vaccinated do I have to comply with Health       Advisories for travel?</a></li>
</ul>

<h3 id="travel"><strong>If I get vaccinated do I have to comply with  health advisory for travel?</strong></h3>
<p>Yes, you should still comply with the requirements of Health Advisory 3, Intrastate Travel, or Health Advisory 2, International and Interstate Travel. It is unknown whether  vaccinated individuals might still be able to pass the virus on to others.</p>
<p>While the vaccines authorized by the FDA for COVID-19 are safe and highly effective, they have been tested only to find out whether they protect the person getting the vaccine from getting sick with COVID-19. We do not know yet how well they prevent the person from getting infected with the virus and passing it on, only how well they prevent the person from getting sick. We know that getting vaccine helps to protect you, but we do not know yet how much it helps to protect other people.</p>
<p>So, if you are traveling and you have gotten the vaccine, you are much less likely to get very sick with COVID-19. However, because you may still be able to catch the virus and give it to others, the requirements for quarantine and testing do not change. The vaccines do not affect your test results.<br/></p>







<h2 class="dz-Element-h2">What to expect after you are  vaccinated</h2>
<ul type="disc">
  <li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx#precautions">After I get vaccinated, do I need to       continue COVID-19 precautions like distancing and wearing a mask?</a></li>
<li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx#fullyvaccinated">If I am fully vaccinated, what will change for me?</a></li>
  <li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx#documentation">Will I receive documentation that I was vaccinated?</a></li>
<li>What are the common side effects of COVID-19 vaccines?</li>
<li>How long does COVID-19 vaccine last?<br/></li>
</ul>




<h3 id="precautions">After I get vaccinated, do I need to continue COVID-19 precautions like distancing and wearing a mask?</h3>
<p>CDC has released guidance stating that t<a href="https://web.archive.org/web/20210328200020/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/fully-vaccinated.html">hose who are fully vaccinated may gather with other people who are fully vaccinated in a small private setting without social distancing or wearing a mask</a>. Even if it has been two weeks since your final COVID-19 vaccination, you should still take steps to protect yourself and others in many situations. This includes wearing a mask, staying 6 feet apart, avoiding crowds, and following travel and workplace guidance. These vaccines are excellent at protecting the person who gets immunized, but we are still learning how well COVID-19 vaccines keep people from spreading the disease. </p>
<p></p>
<h3 id="fullyvaccinated">If I am fully vaccinated, what will change for me?</h3>
<p></p>
<p>You are considered fully vaccinated two weeks after your second dose of the Pfizer or Moderna vaccines, or 2 weeks after your dose of Janssen’s vaccine. When you are fully vaccinated, you can gather indoors with other fully vaccinated people without wearing a mask. If you have been exposed to someone who has COVID-19, you do not need to quarantine or get tested unless you have symptoms or you live in a group setting (like a correctional or detention facility or group home). Review the complete updated guidelines at the CDC website. <br/></p>


<h3 id="documentation"><strong>Will I receive documentation that I was vaccinated?</strong></h3>
<p>You will receive a vaccine record card from your healthcare provider. </p>
<h3 class="dz-Element-h3">What are the common side effects of COVID-19 vaccines?</h3>
<p>After getting vaccinated, you might have some side effects, which are normal signs that your body is building protection. Common side effects are pain, redness, and swelling in the arm where you received the shot, as well as tiredness, headache, muscle pain, chills, fever, and nausea throughout the rest of the body. These side effects could affect your ability to do daily activities, but they should go away in a few days.</p>
<h3 class="dz-Element-h3">How long does COVID-19 vaccine last?</h3>
<p>We don’t know how long protection lasts for those who are vaccinated. What we do know is that COVID-19 has caused very serious illness and death for a lot of people. If you get COVID-19, you also risk giving it to loved ones who may get very sick. Getting a COVID-19 vaccine is a safer choice.  <br/></p>





<h2 id="resources"><strong>Resources to support you</strong></h2>
<p>Additional  information will be provided as it is available. </p>
<h3><a name="_7d3vxjfsf5m8"></a><strong>Alaska COVID-19 vaccine  information pages</strong></h3>
<ul>
  <li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/vaccine.aspx">Alaska COVID-19 Vaccine  Main Page</a></li>
  <li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx">All Alaskans</a></li>
  <li><a href="/web/20210328200020/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineProviders.aspx">Healthcare workers</a></li>
</ul>
<h3><a name="_5p6yzk9l8yx0"></a><strong>Hear the latest about vaccine in  Alaska</strong></h3>
<ul>
  <li><a href="https://web.archive.org/web/20210328200020/https://alaska.zoom.us/webinar/register/WN_i2syaoh3SCCTKY2U289H_Q">COVID Science ECHO</a> Wednesdays, 1:00 pm-2:00 pm</li>
  <li><a href="https://web.archive.org/web/20210328200020/https://chd.app.box.com/s/mtny6d1d9ygssxrrb16afr13hcc39ond/folder/122409363861">Archived COVID-19 Vaccine ECHO  session recordings</a> are available.</li>
</ul>
</div>

<h2 class="clear">Can't find what you're looking for?</h2>
<script async src="https://web.archive.org/web/20210328200020js_/https://cse.google.com/cse.js?cx=000435485480808704345:4lgxbxrva15"></script>
<div class="gcse-search" style="padding-bottom: 30px;></div>
</div>


	</div>

	<div id=" developerdashboard" class="ms-developerdashboard">
		
	</div>

    <div id="dhss-footer">
	
		<div id="footer-social">
			<ul>
				<li><a href="https://web.archive.org/web/20210328200020/http://www.facebook.com/pages/Anchorage-AK/Alaska-Department-of-Health-and-Social-Services/99962144928" class="facebook" title="Facebook">&nbsp;</a></li>
				<li><a href="https://web.archive.org/web/20210328200020/http://twitter.com/Alaska_DHSS" class="twitter" title="Twitter">&nbsp;</a></li>
				<li><a href="https://web.archive.org/web/20210328200020/http://vimeo.com/alaskadhss" class="vimeo" title="Vimeo">&nbsp;</a></li>
				<li><a href="https://web.archive.org/web/20210328200020/http://www.youtube.com/alaskadhss" class="youtube" title="YouTube">&nbsp;</a></li>
				<li><a href="https://web.archive.org/web/20210328200020/https://public.govdelivery.com/accounts/AKDHSS/subscriber/new?topic_id=AKDHSS_14" class="govdelivery" title="GovDelivery">&nbsp;</a></li>
			</ul>
	  
		</div>

		<div id="footer-general">
			<ul>
				<li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/Commissioner/Pages/Contacts">Contacts</a></li>
				<li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/Pages/Accessibility.aspx">Accessibility</a></li>
				<li><a href="https://web.archive.org/web/20210328200020/https://aws.state.ak.us/OnlinePublicNotices/default.aspx">Public Notices</a></li>
				<li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/Pages/Staff.aspx">Staff</a></li>
			</ul>
			<ul>
				<li class="webmaster"><a href="https://web.archive.org/web/20210328200020/mailto:hsswebmaster@alaska.gov">Webmaster</a></li>
			</ul>
		</div>
	  
		<div id="footer-divisions" style="font-size: 85%;">
			<ul>
				<li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/daph">Alaska Pioneer Homes</a></li>
				<li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/api">Alaska Psychiatric Institute</a></li>
				<li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/dbh">Behavioral Health</a></li>

<li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/Commissioner">Commissioner's Office</a></li>

</ul>
<ul>

<li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/fms">Finance &amp; Management Services</a></li>


<li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/dhcs">Health Care Services</a></li>

<li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/djj">Juvenile Justice</a></li>

<li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/ocs">Office of Children's Services</a></li>

</ul>

<ul>




<li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/dpa">Public Assistance</a></li>


<li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/dph">Public Health</a></li>

<li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/dsds">Seniors &amp; Disabilities Services</a></li>


<li><a href="https://web.archive.org/web/20210328200020/http://dhss.alaska.gov/Commissioner/Pages/Boards">Boards, Councils &amp; Commissions</a></li>

</ul>
</div>

    </div>

	</div>
</div>
</div>

<div id="footer-wrapper">
	<div id="soa-footer">
		<ul class="menu">
			<li><a href="https://web.archive.org/web/20210328200020/http://www.alaska.gov/">State of Alaska</a></li>
			<li><a href="https://web.archive.org/web/20210328200020/https://myalaska.state.ak.us/home/app">myAlaska</a></li>
			<li><a href="https://web.archive.org/web/20210328200020/http://www.alaska.gov/akdir1.html">My Government</a></li>
			<li><a href="https://web.archive.org/web/20210328200020/http://www.alaska.gov/residentHome.html">Resident</a></li>
			<li><a href="https://web.archive.org/web/20210328200020/http://www.alaska.gov/businessHome.html">Business in Alaska</a></li>
			<li><a href="https://web.archive.org/web/20210328200020/http://www.alaska.gov/visitorHome.html">Visiting Alaska</a></li>
			<li><a href="https://web.archive.org/web/20210328200020/http://www.alaska.gov/employeeHome.html">State Employees</a></li>
		</ul>
		<ul class="footer">
			<li>State of Alaska</li>
			<li>&copy; 2021</li>
			<li class="last-child"><a href="https://web.archive.org/web/20210328200020/mailto:hsswebmaster@alaska.gov">Webmaster</a></li>
		</ul>
	</div>
</div>
</div>
<!-- =====  End Main ============================================================ -->


	




<div id="ctl00_panelZone">
	<div style="display:none" id="hidZone"><table width="100%" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td id="MSOZoneCell_WebPartWPQ1" valign="top" class="s4-wpcell-plain"></td>
		</tr>
	</table></div>
</div><input type="hidden" id="_wpcmWpid" name="_wpcmWpid" value=""/><input type="hidden" id="wpcmVal" name="wpcmVal" value=""/>

<script type="text/javascript">
//<![CDATA[
var _spFormDigestRefreshInterval = 1440000;window.g_updateFormDigestPageLoaded = new Date(); window.g_updateFormDigestPageLoaded.setDate(window.g_updateFormDigestPageLoaded.getDate() -5);function loadMDN2() { EnsureScript('MDN.js', typeof(loadFilterFn), null); }
function loadMDN1() { ExecuteOrDelayUntilScriptLoaded(loadMDN2, 'sp.ribbon.js'); }
_spBodyOnLoadFunctionNames.push('loadMDN1');
function _spNavigateHierarchyEx(nodeDiv, dataSourceId, dataPath, url, listInContext, type, additionalQString) {
    SetAdditionalNavigateHierarchyQString(additionalQString);
    g_originalSPNavigateFunc(nodeDiv, dataSourceId, dataPath, url, listInContext, type);
}

g_originalSPNavigateFunc = _spNavigateHierarchy;
_spNavigateHierarchy = _spNavigateHierarchyEx;

function EnsureScripts(scriptInfoList, finalFunction)
{
if (scriptInfoList.length == 0)
{
finalFunction();
}
else
{
var scriptInfo = scriptInfoList.shift();
var rest = function () { EnsureScripts(scriptInfoList, finalFunction); };
var defd;
try
{
eval('defd = typeof(' + scriptInfo[1] + ');');
}
catch (e)
{
defd = 'undefined';
}
if (scriptInfo[2])
{
EnsureScript(scriptInfo[0], defd, null);
ExecuteOrDelayUntilScriptLoaded(rest, scriptInfo[0]);
}
else
{
EnsureScript(scriptInfo[0], defd, rest);
}
}
}
function PublishingRibbonUpdateRibbon()
{
var pageManager = SP.Ribbon.PageManager.get_instance();
if (pageManager)
{
pageManager.get_commandDispatcher().executeCommand('appstatechanged', null);
}
}var _fV4UI = true;
function _RegisterWebPartPageCUI()
{
    var initInfo = {editable: false,isEditMode: false,allowWebPartAdder: false,listId: "{43e7fa95-79e4-49c4-931c-67a03394dcfd}",itemId: 250,recycleBinEnabled: true,enableMinorVersioning: true,enableModeration: false,forceCheckout: true,rootFolderUrl: "\u002fdph\u002fEpi\u002fid\u002fPages",itemPermissions:{High:0,Low:196673}};
    SP.Ribbon.WebPartComponent.registerWithPageManager(initInfo);
    var wpcomp = SP.Ribbon.WebPartComponent.get_instance();
    var hid;
    hid = document.getElementById("_wpSelected");
    if (hid != null)
    {
        var wpid = hid.value;
        if (wpid.length > 0)
        {
            var zc = document.getElementById(wpid);
            if (zc != null)
                wpcomp.selectWebPart(zc, false);
        }
    }
    hid = document.getElementById("_wzSelected");
    if (hid != null)
    {
        var wzid = hid.value;
        if (wzid.length > 0)
        {
            wpcomp.selectWebPartZone(null, wzid);
        }
    }
}
ExecuteOrDelayUntilScriptLoaded(_RegisterWebPartPageCUI, "sp.ribbon.js"); var __wpmExportWarning='This Web Part Page has been personalized. As a result, one or more Web Part properties may contain confidential information. Make sure the properties contain information that is safe for others to read. After exporting this Web Part, view properties in the Web Part description file (.WebPart) by using a text editor such as Microsoft Notepad.';var __wpmCloseProviderWarning='You are about to close this Web Part.  It is currently providing data to other Web Parts, and these connections will be deleted if this Web Part is closed.  To close this Web Part, click OK.  To keep this Web Part, click Cancel.';var __wpmDeleteWarning='You are about to permanently delete this Web Part.  Are you sure you want to do this?  To delete this Web Part, click OK.  To keep this Web Part, click Cancel.';//]]>
</script>
<script type="text/javascript">
// <![CDATA[
// ]]>
</script>
<script type="text/javascript">RegisterSod("sp.core.js", "\u002f_layouts\u002fsp.core.js?rev=7ByNlH\u00252BvcgRJg\u00252BRCctdC0w\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.res.resx", "\u002f_layouts\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=SP\u00252ERes\u0026rev=b6\u00252FcRx1a6orhAQ\u00252FcF\u00252B0ytQ\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.runtime.js", "\u002f_layouts\u002fsp.runtime.js?rev=IGffcZfunndj0247nOxKVg\u00253D\u00253D");RegisterSodDep("sp.runtime.js", "sp.core.js");RegisterSodDep("sp.runtime.js", "sp.res.resx");</script>
<script type="text/javascript">RegisterSod("sp.ui.dialog.js", "\u002f_layouts\u002fsp.ui.dialog.js?rev=Tpcmo1\u00252FSu6R0yewHowDl5g\u00253D\u00253D");RegisterSodDep("sp.ui.dialog.js", "sp.core.js");RegisterSodDep("sp.ui.dialog.js", "sp.res.resx");</script>
<script type="text/javascript">RegisterSod("sp.js", "\u002f_layouts\u002fsp.js?rev=\u00252B4ZEyA892P3T0504qi0paw\u00253D\u00253D");RegisterSodDep("sp.js", "sp.core.js");RegisterSodDep("sp.js", "sp.runtime.js");RegisterSodDep("sp.js", "sp.ui.dialog.js");RegisterSodDep("sp.js", "sp.res.resx");</script>
<script type="text/javascript">RegisterSod("cui.js", "\u002f_layouts\u002fcui.js?rev=OOyJv78CADNBeet\u00252FvTvniQ\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("core.js", "\u002f_layouts\u002f1033\u002fcore.js?rev=RiGU6\u00252FvAzNgOjxKFQLw9pw\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("inplview", "\u002f_layouts\u002finplview.js?rev=WB6Gy8a027aeNCq7koVCUg\u00253D\u00253D");RegisterSodDep("inplview", "core.js");RegisterSodDep("inplview", "sp.js");</script>
<script type="text/javascript">RegisterSod("ribbon", "\u002f_layouts\u002fsp.ribbon.js?rev=F\u00252BUEJ66rbXzSvpf7nN69wQ\u00253D\u00253D");RegisterSodDep("ribbon", "core.js");RegisterSodDep("ribbon", "sp.core.js");RegisterSodDep("ribbon", "sp.js");RegisterSodDep("ribbon", "cui.js");RegisterSodDep("ribbon", "sp.res.resx");RegisterSodDep("ribbon", "sp.runtime.js");RegisterSodDep("ribbon", "inplview");</script>
<script type="text/javascript">RegisterSod("sp.ui.policy.resources.resx", "\u002f_layouts\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=SP\u00252EUI\u00252EPolicy\u00252EResources\u0026rev=YhBHGmUAGyJ3lAgSdE4V\u00252Fw\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("mdn.js", "\u002f_layouts\u002fmdn.js?rev=gwmFFJ2\u00252FfFacqXWAqG\u00252FqKg\u00253D\u00253D");RegisterSodDep("mdn.js", "sp.core.js");RegisterSodDep("mdn.js", "sp.runtime.js");RegisterSodDep("mdn.js", "sp.js");RegisterSodDep("mdn.js", "cui.js");RegisterSodDep("mdn.js", "ribbon");RegisterSodDep("mdn.js", "sp.ui.policy.resources.resx");</script>
<script type="text/javascript">RegisterSod("sp.publishing.resources.resx", "\u002f_layouts\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=SP\u00252EPublishing\u00252EResources\u0026rev=q6nxzZIVVXE5X1SPZIMD3A\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.ui.pub.ribbon.js", "\u002f_layouts\u002fsp.ui.pub.ribbon.js?rev=epwnP\u00252FbdljnctbCVld1nnA\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("msstring.js", "\u002f_layouts\u002f1033\u002fmsstring.js?rev=QtiIcPH3HV7LgVSO7vONFg\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("browserScript", "\u002f_layouts\u002f1033\u002fnon_ie.js?rev=EVTj1bu32\u00252FMla6SDN\u00252FsNTA\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSodDep("browserScript", "msstring.js");</script>
<script type="text/javascript">
//<![CDATA[
Sys.Application.initialize();
//]]>
</script>
</form>


</body>
</html>
<!--
     FILE ARCHIVED ON 20:00:20 Mar 28, 2021 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 05:38:35 Apr 25, 2022.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
-->
<!--
playback timings (ms):
  captures_list: 97.145
  exclusion.robots: 0.131
  exclusion.robots.policy: 0.122
  cdx.remote: 0.144
  esindex: 0.01
  LoadShardBlock: 54.687 (3)
  PetaboxLoader3.datanode: 66.531 (4)
  CDXLines.iter: 23.188 (3)
  load_resource: 107.929
  PetaboxLoader3.resolve: 76.768
-->