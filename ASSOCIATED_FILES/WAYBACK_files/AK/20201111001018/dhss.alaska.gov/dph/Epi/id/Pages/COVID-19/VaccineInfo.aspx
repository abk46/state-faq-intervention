
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" __expr-val-dir="ltr" lang="en-us" dir="ltr">
<head><script src="//archive.org/includes/analytics.js?v=cf34f82" type="text/javascript"></script>
<script type="text/javascript">window.addEventListener('DOMContentLoaded',function(){var v=archive_analytics.values;v.service='wb';v.server_name='wwwb-app214.us.archive.org';v.server_ms=283;archive_analytics.send_pageview({});});</script>
<script type="text/javascript" src="/_static/js/bundle-playback.js?v=36gO9Ebf" charset="utf-8"></script>
<script type="text/javascript" src="/_static/js/wombat.js?v=UHAOicsW" charset="utf-8"></script>
<script type="text/javascript">
  __wm.init("https://web.archive.org/web");
  __wm.wombat("http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx","20201111001018","https://web.archive.org/","web","/_static/",
	      "1605053418");
</script>
<link rel="stylesheet" type="text/css" href="/_static/css/banner-styles.css?v=fantwOh2" />
<link rel="stylesheet" type="text/css" href="/_static/css/iconochive.css?v=qtvMKcIJ" />
<!-- End Wayback Rewrite JS Include -->
<title>
	
	COVID-19: COVID-19 Vaccine Info

</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><meta http-equiv="Expires" content="0"/><meta http-equiv="x-ua-compatible" content="IE=Edge"/><meta property="og:image" content="https://web.archive.org/web/20201111001018im_/http://dhss.alaska.gov/PublishingImages/DHSSlogo.jpg"/><link rel="shortcut icon" href="/web/20201111001018im_/https://dhss.alaska.gov/Style Library/DhssZen/Images/favicon.ico" type="image/vnd.microsoft.icon"/><link rel="stylesheet" type="text/css" href="/web/20201111001018cs_/https://dhss.alaska.gov/Style%20Library/en-US/Themable/Core%20Styles/controls.css"/>
<link rel="stylesheet" type="text/css" href="/web/20201111001018cs_/https://dhss.alaska.gov/_layouts/1033/styles/Themable/corev4.css?rev=cubXjmzpjBGT%2FzzR9ULbAw%3D%3D"/>
<link rel="stylesheet" type="text/css" href="/web/20201111001018cs_/https://dhss.alaska.gov/_layouts/1033/styles/Themable/Menu.css?rev=j46FAsDJga%2BaixwEf8A%2Fgg%3D%3D"/>
<link rel="stylesheet" type="text/css" href="/web/20201111001018cs_/https://dhss.alaska.gov/Style%20Library/en-US/Core%20Styles/page-layouts-21.css"/>
<link rel="stylesheet" type="text/css" href="/web/20201111001018cs_/https://dhss.alaska.gov/Style%20Library/DhssZen/css/alt_campaign.css"/>


    <script type="text/javascript">
	    var _fV4UI = true;
    </script>

    <link rel="stylesheet" type="text/css" href="/web/20201111001018cs_/https://dhss.alaska.gov/Style Library/DhssZen/css/reset.css?v=1.3"/><link rel="stylesheet" type="text/css" href="/web/20201111001018cs_/https://dhss.alaska.gov/Style Library/DhssZen/css/DhssZen.css?v=1.3"/><link rel="stylesheet" type="text/css" href="/web/20201111001018cs_/https://dhss.alaska.gov/Style Library/DhssZen/css/soa-structure.css?v=1.3"/><link rel="stylesheet" type="text/css" href="/web/20201111001018cs_/https://dhss.alaska.gov/Style Library/DhssZen/css/dhss-content.css?v=1.3"/><link rel="stylesheet" type="text/css" href="/web/20201111001018cs_/https://dhss.alaska.gov/Style Library/DhssZen/css/dhss-legacy.css?v=1.3"/><script type="text/javascript">
// <![CDATA[
document.write('<script type="text/javascript" src="/style%20library/dhsszen/scripts/jquery-1.7.2.min.js"></' + 'script>');
document.write('<script type="text/javascript" src="/style%20library/dhsszen/scripts/menu.js"></' + 'script>');
document.write('<script type="text/javascript" src="/_layouts/1033/init.js?rev=mdjxRHK1V7KZ%2BuOTcNNcmQ%3D%3D"></' + 'script>');
document.write('<script type="text/javascript" src="/_layouts/1033/jquery-1.4.4.min.js?rev=c6nDNMXKcdcNCStCBk9kdg%3D%3D"></' + 'script>');
document.write('<script type="text/javascript" src="/_layouts/1033/jquery-noconflict.js?rev=nBDuS1QCCe5SuB%2BmQlVskQ%3D%3D"></' + 'script>');
document.write('<script type="text/javascript" src="/_layouts/listimport/listimport.js?rev=jZKUH8ts44bwUzFbeAf%2F1Q%3D%3D"></' + 'script>');
document.write('<script type="text/javascript" src="/ScriptResource.axd?d=ea23uepthaEvF_TU_KFnyhfDmINentVRWBgrZWYeHp-2_5dpwQPMTO-0VYOlpRzmkTTSHLhJtdKg84IzdzkOPNFtP0l9tJ9WmX6iCOeQr8tVdevmP6W5pprRYRFEv6pzHjANzlNlNgMoFfkHCguA90MY2Xs1&amp;t=3f4a792d"></' + 'script>');
document.write('<script type="text/javascript" src="/_layouts/blank.js?rev=QGOYAJlouiWgFRlhHVlMKA%3D%3D"></' + 'script>');
// ]]>
</script>

	
	<script type="text/javascript">
		function ProcessImn(){}
		function ProcessImnMarkers(){}	
	</script>

	
    
	
<meta name="google-site-verification" content="jPGd8CiqqcBFgCHw2lVALvEzN1eZi3hHuHZGv3s0lW0"/><style type="text/css">
	.ctl00_wpz_0 { border-color:Black;border-width:1px;border-style:Solid; }

</style></head>

<body onload="javascript:_spBodyOnLoadWrapper();">
<form name="aspnetForm" method="post" action="/web/20201111001018/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx" id="aspnetForm">
<input type="hidden" name="MSOWebPartPage_PostbackSource" id="MSOWebPartPage_PostbackSource" value=""/>
<input type="hidden" name="MSOTlPn_SelectedWpId" id="MSOTlPn_SelectedWpId" value=""/>
<input type="hidden" name="MSOTlPn_View" id="MSOTlPn_View" value="0"/>
<input type="hidden" name="MSOTlPn_ShowSettings" id="MSOTlPn_ShowSettings" value="False"/>
<input type="hidden" name="MSOGallery_SelectedLibrary" id="MSOGallery_SelectedLibrary" value=""/>
<input type="hidden" name="MSOGallery_FilterString" id="MSOGallery_FilterString" value=""/>
<input type="hidden" name="MSOTlPn_Button" id="MSOTlPn_Button" value="none"/>
<input type="hidden" name="__REQUESTDIGEST" id="__REQUESTDIGEST" value="noDigest"/>
<input type="hidden" name="MSOSPWebPartManager_DisplayModeName" id="MSOSPWebPartManager_DisplayModeName" value="Browse"/>
<input type="hidden" name="MSOSPWebPartManager_ExitingDesignMode" id="MSOSPWebPartManager_ExitingDesignMode" value="false"/>
<input type="hidden" name="MSOWebPartPage_Shared" id="MSOWebPartPage_Shared" value=""/>
<input type="hidden" name="MSOLayout_LayoutChanges" id="MSOLayout_LayoutChanges" value=""/>
<input type="hidden" name="MSOLayout_InDesignMode" id="MSOLayout_InDesignMode" value=""/>
<input type="hidden" name="_wpSelected" id="_wpSelected" value=""/>
<input type="hidden" name="_wzSelected" id="_wzSelected" value=""/>
<input type="hidden" name="MSOSPWebPartManager_OldDisplayModeName" id="MSOSPWebPartManager_OldDisplayModeName" value="Browse"/>
<input type="hidden" name="MSOSPWebPartManager_StartWebPartEditingName" id="MSOSPWebPartManager_StartWebPartEditingName" value="false"/>
<input type="hidden" name="MSOSPWebPartManager_EndWebPartEditing" id="MSOSPWebPartManager_EndWebPartEditing" value="false"/>
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUBMA9kFgJmD2QWAgIBD2QWBAIBD2QWAmYPZBYCZg9kFgICAQ8WAh4TUHJldmlvdXNDb250cm9sTW9kZQspiAFNaWNyb3NvZnQuU2hhcmVQb2ludC5XZWJDb250cm9scy5TUENvbnRyb2xNb2RlLCBNaWNyb3NvZnQuU2hhcmVQb2ludCwgVmVyc2lvbj0xNC4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj03MWU5YmNlMTExZTk0MjljAWQCAw9kFgYCBw9kFgICAQ9kFgRmD2QWAgIBDxYCHgdWaXNpYmxlaBYCZg9kFgQCAg9kFgYCAQ8WAh8BaGQCAw8WCB4TQ2xpZW50T25DbGlja1NjcmlwdAWEAWphdmFTY3JpcHQ6Q29yZUludm9rZSgnVGFrZU9mZmxpbmVUb0NsaWVudFJlYWwnLDEsIDUzLCAnaHR0cDpcdTAwMmZcdTAwMmZkaHNzLmFsYXNrYS5nb3ZcdTAwMmZkcGhcdTAwMmZFcGlcdTAwMmZpZCcsIC0xLCAtMSwgJycsICcnKR4YQ2xpZW50T25DbGlja05hdmlnYXRlVXJsZB4oQ2xpZW50T25DbGlja1NjcmlwdENvbnRhaW5pbmdQcmVmaXhlZFVybGQeDEhpZGRlblNjcmlwdAUiVGFrZU9mZmxpbmVEaXNhYmxlZCgxLCA1MywgLTEsIC0xKWQCBQ8WAh8BaGQCAw8PFgoeCUFjY2Vzc0tleQUBLx4PQXJyb3dJbWFnZVdpZHRoAgUeEEFycm93SW1hZ2VIZWlnaHQCAx4RQXJyb3dJbWFnZU9mZnNldFhmHhFBcnJvd0ltYWdlT2Zmc2V0WQLrA2RkAgEPZBYEAgMPZBYCAgEPEBYCHwFoZBQrAQBkAgUPZBYCZg9kFgJmDxQrAANkZGRkAg0PZBYCAgIPZBYCAgcPFgIfAAsrBAFkAhEPZBYCAgEPZBYCZg9kFgICAw9kFgICBQ8PFgQeBkhlaWdodBsAAAAAAAB5QAEAAAAeBF8hU0ICgAFkFgICAQ88KwAJAQAPFgQeDVBhdGhTZXBhcmF0b3IECB4NTmV2ZXJFeHBhbmRlZGdkZGTTlVS13ueKNTF5Lj70Iy4bsBJsWg=="/>


<script type="text/javascript">
//<![CDATA[
var MSOWebPartPageFormName = 'aspnetForm';
var g_presenceEnabled = true;
var g_wsaEnabled = false;
var g_wsaLCID = 1033;
var g_wsaSiteTemplateId = 'BLANKINTERNET#2';
var g_wsaListTemplateId = 850;
var _fV4UI=true;var _spPageContextInfo = {webServerRelativeUrl: "\u002fdph\u002fEpi\u002fid", webLanguage: 1033, currentLanguage: 1033, webUIVersion:4,pageListId:"{43e7fa95-79e4-49c4-931c-67a03394dcfd}",pageItemId:250, alertsEnabled:true, siteServerRelativeUrl: "\u002f", allowSilverlightPrompt:'True'};//]]>
</script>
<script type="text/javascript">
<!--
var L_Menu_BaseUrl="/dph/Epi/id";
var L_Menu_LCID="1033";
var L_Menu_SiteTheme="";
//-->
</script>
<script type="text/javascript">
//<![CDATA[
document.onreadystatechange=fnRemoveAllStatus; function fnRemoveAllStatus(){removeAllStatus(true)};var dlc_fvsi = {"DefaultViews":[],"ViewUrls":[],"WebUrl":"\/dph\/Epi\/id"};//]]>
</script>

<script type="text/javascript">
//<![CDATA[
function _spNavigateHierarchy(nodeDiv, dataSourceId, dataPath, url, listInContext, type) {
    CoreInvoke('ProcessDefaultNavigateHierarchy', nodeDiv, dataSourceId, dataPath, url, listInContext, type, document.forms.aspnetForm, "", "\u002fdph\u002fEpi\u002fid\u002fPages\u002fCOVID-19\u002fVaccineInfo.aspx");

}
//]]>
</script>
<script src="/web/20201111001018js_/https://dhss.alaska.gov/_layouts/blank.js?rev=QGOYAJlouiWgFRlhHVlMKA%3D%3D" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
if (typeof(DeferWebFormInitCallback) == 'function') DeferWebFormInitCallback();//]]>
</script>

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="BAB98CB3"/>






<!-- =====  Start Main Area ============================================================ -->
<div id="s4-bodyContainer">

<a href="#soa-content" id="soa-skip-content">Skip to content</a>
<div id="top-wrapper">
	<div id="soa-bar">
		<a id="soa-title" href="https://web.archive.org/web/20201111001018/http://www.alaska.gov/" name="soa">State of Alaska</a>
		<ul class="menu">
	      <li><a href="https://web.archive.org/web/20201111001018/https://my.alaska.gov/">myAlaska</a></li>
	      <li><a href="https://web.archive.org/web/20201111001018/http://www.alaska.gov/akdir1.html">My Government</a></li>
	      <li><a href="https://web.archive.org/web/20201111001018/http://www.alaska.gov/residentHome.html">Resident</a></li>
	      <li><a href="https://web.archive.org/web/20201111001018/http://www.alaska.gov/businessHome.html">Business in Alaska</a></li>
	      <li><a href="https://web.archive.org/web/20201111001018/http://www.alaska.gov/visitorHome.html">Visiting Alaska</a></li>
	      <li><a href="https://web.archive.org/web/20201111001018/http://www.alaska.gov/employeeHome.html">State Employees</a></li>
		</ul>
	</div>

	<div id="soa-header">
  		<a href="/web/20201111001018/https://dhss.alaska.gov/dph/Epi/id/"><img class="soa-banner" src="https://web.archive.org/web/20201111001018im_/http://hss.state.ak.us/css/images/Header_DHSS.jpg" alt="Infectious Disease" border="0"/></a>

	     <div id="soa-search">
                            <input id="soa-searchquery" name="q" title="Search" type="text" value=""/>
                            <input type="button" id="soa-searchsubmit" alt="Search"/>
                            <input value="DHSS" name="client" type="hidden"/>
                            <input value="DHSS" name="proxystylesheet" type="hidden"/>
                            <input value="date:D:L:d1" name="sort" type="hidden"/>
                            <input value="xml_no_dtd" name="output" type="hidden"/>
                            <input value="UTF-8" name="ie" type="hidden"/>
                            <input value="UTF-8" name="oe" type="hidden"/>
                        </div>
	</div>

    <div id="menu">
      <ul id="menu_list">
        <li id="first_nav"><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/">Home</a></li>
        <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/Pages/Services.aspx">Divisions and Agencies</a>
          <div>
            <ul class="left">
                <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/daph">Alaska Pioneer Homes</a></li>
				<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/api">Alaska Psychiatric Institute</a></li>
		        <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dbh">Behavioral Health</a></li>
				<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/ocs">Office of Children's Services</a></li>
				<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/Commissioner">Office of the Commissioner</a></li>
			    <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/osmap">Office of Substance Misuse and Addiction Prevention</a></li>
				
			</ul>
            <ul class=" left">
                <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/fms">Finance &amp; Management Services</a></li>
                <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dhcs">Health Care Services</a></li>
				<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/djj">Juvenile Justice</a></li>
				<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dpa">Public Assistance</a></li>
				<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dph">Public Health</a></li>
				<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dsds">Seniors &amp; Disabilities Services</a></li>

<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/Commissioner/Pages/Boards/default.aspx">Boards, Councils &amp; Commissions</a></li>            </ul>  
            <br class="clear"/>
          </div>
       </li>
       <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/Pages/Services.aspx">Services</a>
          <div>
            <ul class="left">
                <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dph/VitalStats">Birth &amp; Marriage Certificates </a></li>
                <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dpa/Pages/ccare">Child Care </a></li>
                <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/ocs">Child Protection</a></li>
                <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dpa/Pages/dkc/default.aspx">Denali KidCare</a></li>
                <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dpa/Pages/SNAP/default.aspx">Supplemental Nutrition Assistance Program (SNAP) </a></li>
                <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dph/Epi/iz/Pages/default.aspx">Immunization Information </a></li>
                <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dhcs/Pages/Medicaid_Medicare">Medicaid</a></li>
                <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dph/Nursing/Pages/locations.aspx">Public Health Centers </a></li>
                <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dpa/Pages/atap">Temporary &quot;Cash&quot; Assistance </a> </li>
                <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dpa/Pages/seniorbenefits">Senior Benefits Program</a></li>
                <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dsds/Pages/Medicare">Medicare</a></li>
                <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dbh/Pages/TreatmentRecovery/SubstanceAbuse">Substance Abuse Treatment</a></li>
			</ul>
            <ul class="left">
 <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/ocs/Pages/icwa/default.aspx">Alaska Tribal Child Welfare Compact</a></li>
                <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dph/Director/Pages/LivingWill.aspx">Alaska Directives for Health Care &mdash; Living Will</a></li>
                <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dph/Chronic/Pages/brfss">Behavioral Risk Factor Survey</a></li>
                <li><a href="https://web.archive.org/web/20201111001018/http://permaudit.alaska.gov/">PERM</a></li>              <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dph/Epi/Pages/phan/default.aspx">Public Health Alert Network (PHAN)</a></li>
                <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dhcs/Pages/CertificateOfNeed/default.aspx">Certificate of Need</a></li>
                <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/fms/facilities/Pages/safetyplan.aspx">Department Safety Plan</a></li>
                <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/fms/facilities/Documents/DHSS-Capital-Funding-Allocation-Plan-FY16.pdf">Facilities Funding Allocation Plan</a></li>
            </ul>
            <br class="clear"/>
          </div>
		</li>
        <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/News">News</a>
          <div>
            <ul class=" left">
                <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/News/Pages/2020index.aspx">Press Releases</a></li>
<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/News/Pages/newsletter/index.aspx">DHSS Newsletter</a></li> 
<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/News/Pages/newsletter/CMO/CMO_News.aspx">Chief Medical Officer News</a></li> 
<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/Commissioner/Pages/photos.aspx">Press Photos</a></li>
<li><a href="https://web.archive.org/web/20201111001018/https://aws.state.ak.us/OnlinePublicNotices/default.aspx">Public Notices</a></li>
                <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/Pages/Publications.aspx">Publications</a></li>
                <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/News/Pages/Newsroom.aspx">Newsroom</a></li>
            </ul>
            <br class="clear"/>
          </div>
        </li>
        <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/Commissioner/Pages/Contacts">Contact Us</a>
            <div>
                <ul class="left">
                    <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/Commissioner/Pages/Contacts">Contacts</a></li>
			        <li><a href="https://web.archive.org/web/20201111001018/mailto:hsswebmaster@alaska.gov">Webmaster</a></li>
			        <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/Pages/Accessibility.aspx">Accessibility</a></li>
                    <li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/Pages/Careers.aspx">DHSS Careers</a></li>
                </ul>
                <ul class="left">
                    <li><a href="https://web.archive.org/web/20201111001018/http://www.facebook.com/pages/Anchorage-AK/Alaska-Department-of-Health-and-Social-Services/99962144928">Facebook</a></li>
				    <li><a href="https://web.archive.org/web/20201111001018/http://twitter.com/Alaska_DHSS">Twitter</a></li>
				    <li><a href="https://web.archive.org/web/20201111001018/http://vimeo.com/alaskadhss">Vimeo</a></li>
				    <li><a href="https://web.archive.org/web/20201111001018/http://www.youtube.com/alaskadhss">YouTube</a></li>
				    <li><a href="https://web.archive.org/web/20201111001018/https://public.govdelivery.com/accounts/AKDHSS/subscriber/new?topic_id=AKDHSS_14">GovDelivery</a></li>
                </ul>
                <br class="clear"/>
            </div>
        </li>

<li><a href="https://web.archive.org/web/20201111001018/http://coronavirus.alaska.gov/">COVID-19 Resources</a>
            <div>
                <ul class="left">
<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/default.aspx">DHSS COVID-19 Website</a></li>                  
<li><a href="https://web.archive.org/web/20201111001018/http://covid19.alaska.gov/">State of Alaska COVID-19 Website</a></li>
<li><a href="https://web.archive.org/web/20201111001018/https://covid19.alaska.gov/health-mandates/">COVID-19 Health Mandates</a></li>
<li><a href="https://web.archive.org/web/20201111001018/https://gov.alaska.gov/home/covid-19-economy/">COVID-19 Economic Stabilization</a></li>
<li><a href="https://web.archive.org/web/20201111001018/https://covid19.alaska.gov/reopen/">Reopen Alaska Responsibly Plan</a></li>
<li><a href="https://web.archive.org/web/20201111001018/https://ready.alaska.gov/covid19">COVID-19 Unified Command</a></li>
<li><a href="https://web.archive.org/web/20201111001018/https://gov.alaska.gov/home/covid19news/">COVID-19 News &amp; Video from Governor Mike Dunleavy</a></li>
                    <li><a href="https://web.archive.org/web/20201111001018/https://public.govdelivery.com/accounts/AKDHSS/subscriber/new?topic_id=AKDHSS_157">Sign up for COVID-19 updates</a></li>
                </ul>
              
                <br class="clear"/>
            </div>
        </li>


      </ul>
    </div>

	<div id="soa-breadcrumbs">
		<span><a href="#ctl00_ctl38_SkipLink"><img alt="Skip Navigation Links" height="0" width="0" src="/web/20201111001018im_/https://dhss.alaska.gov/WebResource.axd?d=Ylnie978DEV5711jLlQRdDn5MusmPX42s1MI0vQZfl06TNXLzB3nmC-pqSSXcz0sHH2njqI71A0oM3BZzO8JaXX1n_01&amp;t=637304273068272736" border="0"/></a><span><a title="Department of Health and Social Services" href="/web/20201111001018/https://dhss.alaska.gov/Pages/default.aspx">Health and Social Services</a></span><span> &gt; </span><span><a title="Division of Public Health" href="/web/20201111001018/https://dhss.alaska.gov/dph/Pages/default.aspx">Public Health</a></span><span> &gt; </span><span><a title="Section of Epidemiology
" href="/web/20201111001018/https://dhss.alaska.gov/dph/Epi/Pages/default.aspx">Epidemiology</a></span><span> &gt; </span><span><a title="Infectious Disease Program" href="/web/20201111001018/https://dhss.alaska.gov/dph/Epi/id/Pages/default.aspx">Infectious Disease</a></span><span> &gt; </span><span>COVID-19: COVID-19 Vaccine Info</span><a id="ctl00_ctl38_SkipLink"></a></span>
	</div>
</div>

<div id="ctl00_MSO_ContentDiv">
<div id="content-wrapper">
    <div id="soa-content">
    <!-- s4-ca is the main body div -->
	<div class="s4-ca">
		
<div>

<img src="https://web.archive.org/web/20201111001018im_/http://dhss.alaska.gov/dph/Epi/id/PublishingImages/COVID-19/CovidDHSSpage-banner.jpg" alt="COVID-19" style="width: 100%; margin-top: -10px;"/>
<div id="menu" class="campaign">
<ul id="menu_list" style="list-style: outside none none;">
<li id="first_nav"><a href="/web/20201111001018/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/default.aspx">Home</a></li>
<li><a href="/web/20201111001018/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/sitemap.aspx">Sitemap</a>
</li>
<li><a href="https://web.archive.org/web/20201111001018/https://covid19.alaska.gov/">State of Alaska COVID-19<br/></a></li>
<li><a href="https://web.archive.org/web/20201111001018/https://covid19.alaska.gov/unified-command/">Unified Command</a></li>
<li><a href="https://web.archive.org/web/20201111001018/http://cdc.gov/covid19">CDC COVID-19</a></li>
</ul>
</div>

<div class="grid1"><div class="box"><h2>Coronavirus Disease 2019 </h2>
<h3>About COVID-19</h3>
<ul><li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/default.aspx">COVID-19 Homepage</a></li>
<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/testing.aspx">COVID-19 Testing</a></li>
<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/monitoring.aspx">Case Counts</a></li>
<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dph/Epi/id/Pages/covid-19/communications.aspx">Media &amp; Communications</a></li></ul>
<h3>What to do</h3>
<ul><li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/Sick.aspx">If you are sick</a></li>
<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/exposed.aspx">If you may have been exposed</a></li>
<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/highrisk.aspx">Precautions for high risk groups</a></li>
<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/washyourhands.aspx">Wash your hands</a></li>
<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/stayhome.aspx">Stay home, save lives</a></li>
<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/socialdistancing.aspx">Social distancing</a></li>
<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/bubble.aspx">Keep your social circle small</a></li>
<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/coveryourface.aspx">Cloth face coverings</a></li>
<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/coping.aspx">Coping with stress</a></li>
<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/wellbeing.aspx">Child and family well-being</a></li>
</ul>

<h3>Resources</h3>
<ul><li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/general.aspx">General Public</a></li>
<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/healthcare.aspx">Health Care Professionals</a></li>
<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/publichealth.aspx">Public Health</a></li>
<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/populations.aspx">Specific Groups</a></li>
<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/translations.aspx">Multi-lingual Resources</a></li>
</ul>
<h3>Stay informed</h3>
<p><em>Subscribe to DHSS's updates by email or text:</em></p>

<ul>
<li><a href="https://web.archive.org/web/20201111001018/https://public.govdelivery.com/accounts/AKDHSS/subscriber/new?topic_id=AKDHSS_157">COVID-19 Health Alerts</a><br/></li>
<li><a href="https://web.archive.org/web/20201111001018/https://public.govdelivery.com/accounts/AKDHSS/subscriber/new?topic_id=12">DHSS Press Releases</a></li></ul>
<h3>Need help?</h3>
<p><a href="https://web.archive.org/web/20201111001018/https://alaska211.org/"><img alt="Alaska 2-1-1: Get connected. Get answers." src="https://web.archive.org/web/20201111001018im_/http://dhss.alaska.gov/dph/Epi/id/PublishingImages/COVID-19/logo_211.png" style="margin:0px;width:195px;height:106px"/></a><br/>For current information related to COVID-19, dial 2-1-1 or 1-800-478-2221. Available 8:30am-6pm, Monday-Friday.</p>
<p><img alt="Careline logo" src="https://web.archive.org/web/20201111001018im_/http://dhss.alaska.gov/dbh/PublishingImages/prevention/Carelinewebad.jpg" style="margin:5px 0px;width:190px;height:129px"/><br/></p>
</div>
<h2><br/>CDC guidance</h2>
<h3>Topics for the<br/>General Public</h3>
<ul>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/need-extra-precautions/asthma.html">Asthma</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/need-extra-precautions/pregnancy-breastfeeding.html">Breastfeeding</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/community/organizations/businesses-employers.html">Businesses/Employers</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/community/schools-childcare/index.html">Childcare</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/daily-life-coping/children.html">Children</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/travelers/returning-cruise-voyages.html">Cruise  Ships/Returning Passengers</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/hcp/dental-settings.html">Dental  Settings</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/faq.html">FAQs</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/community/organizations/index.html">Faith-based  Organizations</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/php/water.html">Feces</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/hcp/index.html">Health Care</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/healthcare-facilities/dialysis.html">Hemodialysis Facilities</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/need-extra-precautions/index.html">High Risk Groups</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/need-extra-precautions/hiv.html">HIV</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/community/homeless-shelters/index.html">Homeless</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/php/water.html">Hot Tubs and Pools</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/if-you-are-sick/index.html">Isolation</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/healthcare-facilities/prevent-spread-in-long-term-care-facilities.html">Long Term Care Facilities/Nursing Homes</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/hcp/ppe-strategy/index.html">Masks/Personal  Protective Equipment</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/need-extra-precautions/older-adults.html">Older  Adults</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/daily-life-coping/animals.html">Pets and Other Animals</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/need-extra-precautions/pregnancy-breastfeeding.html">Pregnant Women</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/php/pandemic-preparedness-resources.html">Preparedness</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/need-extra-precautions/get-ready.html">Prevention</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/prevent-getting-sick/index.html">Quarantine</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/community/retirement/faq.html">Retirement  Communities and Independent Living Facilities</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/community/schools-childcare/index.html">Schools</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/symptoms-testing/symptoms.html">Symptoms</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/symptoms-testing/testing.html">Testing  for COVID-19</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/travelers/index.html">Travel</a></li>
  <li><a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-ncov/php/water.html">Water/Wastewater</a></li>
</ul>
<p>Note: this is not an exhaustive list of the currently available CDC guidance.  For more information, please visit the <a href="https://web.archive.org/web/20201111001018/https://www.cdc.gov/coronavirus/2019-nCoV/index.html">CDC COVID-19  homepage</a>. </p>
</div>
	

<div class="grid3" style="width:690px">
   
    <div id="ctl00_PlaceHolderMain_PageContent_label" style="display:none">Page Content</div><div id="ctl00_PlaceHolderMain_PageContent__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_PageContent_label"><p class="dz-Element-p">
<a href="/web/20201111001018/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/vaccine.aspx">Return to Alaska COVID-19 Vaccine Homepage</a><a href="/web/20201111001018/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/vaccine.aspx"></a>
</p>
<h1 style="padding-bottom:10px"><strong>COVID-19 Vaccine Information for all Alaskans </strong></h1>
<div>
</div>
<h2 class="dz-Element-h2">How  is the state of Alaska planning for a COVID-19 vaccine?</h2>
<div>
</div>
<p>The State of Alaska’s Department of  Health and Social Services and Alaska Native Tribal Health Consortium are  working jointly to plan for distribution of COVID-19 vaccine. While no vaccine  is currently available, we are reviewing federal guidance and are following  progress on COVID-19 vaccine trials and potential Food and Drug Administration  (FDA) authorized vaccine(s).</p>
<div>
</div>
<p>On October 16 an <a href="/web/20201111001018/https://dhss.alaska.gov/dph/Epi/id/SiteAssets/Pages/HumanCoV/AlaskaCOVID-19VaccinationDraftPlan.pdf"><img class="ms-asset-icon ms-rtePosition-4" src="/web/20201111001018im_/https://dhss.alaska.gov/_layouts/images/pdficon_small.png" alt=""/>Alaska Draft COVID-19 Vaccination Plan</a> was submitted to the CDC for review. This plan will be  adjusted as we learn more. </p>
<div>
</div>
<h2 class="dz-Element-h2">When  will the vaccine be available?</h2>
<div>
</div>
<p>An exact date the vaccine will be  made available is not yet known. We are planning so we will have a clear course  of action when a vaccine is available. A limited supply of COVID-19 vaccine may  be available as soon as November 2020.</p>
<div>
</div>
<h2 class="dz-Element-h2">Will  the vaccine be available to everyone in Alaska?</h2>
<div>
</div>
<p>At first, vaccine would be available  in limited supply and would be offered to certain groups. We are receiving  ongoing guidance from federal agencies on how to prioritize availability of  vaccine. Some of the prioritized populations may include:</p>
<div>
</div>
<ul type="disc">
  <li>Healthcare workers</li>
  <li>Staff and residents in long-term care and assisted       living facilities</li>
  <li>Critical worker infrastructure workforce</li>
  <li>People at increased risk for severe COVID-19 illness </li>
  <li>People at increased risk of acquiring or transmitting       COVID-19 </li>
</ul>
<div>
</div>
<p>The COVID-19 vaccine may not be  recommended for children at first. This is because early <a href="https://web.archive.org/web/20201111001018/http://www.clinicaltrials.gov/">clinical trials</a> for various COVID-19  vaccines only included non-pregnant adults. The groups recommended to receive  the vaccines could change in the future as clinical trials expand their  participants.</p>
<div>
</div>
<h2 class="dz-Element-h2">Will  there be a COVID-19 vaccine mandate?</h2>
<div>
</div>
<p>There are no plans for a State of  Alaska COVID-19 vaccine mandate.</p>
<div>
</div>
<h2 class="dz-Element-h2">What  will be the cost of the COVID-19 vaccine?</h2>
<div>
</div>
<p>We are reviewing options for making  the vaccine available to Alaskans at no charge, or at reduced cost.  </p>
<div>
</div>
<p>COVID-19  vaccine and ancillary supplies will be distributed by the federal government at  no cost to enrolled COVID-19 vaccination providers. It’s expected that  providers will be able to bill insurance an administration fee. </p>
<div>
</div>
<h2 class="dz-Element-h2">Will  the COVID-19 vaccine be safe and effective?</h2>
<div>
</div>
<p>Covid-19 vaccine safety is a top  priority.</p>
<div>
</div>
<p>There are many vaccines currently  undergoing clinical trials. We are closely following the progress of these  trials. Any vaccine would need to be authorized by the Food and Drug  Administration (FDA), the agency responsible for making sure vaccines are safe  and effective. We will learn all we can about any authorized vaccine, and we’ll  work to share what we learn. </p>
<div>
</div>
<h2 class="dz-Element-h2">What  is the process for a vaccine to be authorized by the FDA?</h2>
<div>
</div>
<p>The Food and Drug Administration  (FDA) is responsible for evaluating safety and efficacy of medical products,  such as vaccines. When certain public health emergencies arise, such as the  COVID-19 pandemic, the FDA can use an Emergency Use Authorization (EUA) to  authorize life-saving medications or other medical products. Learn more  about <a href="https://web.archive.org/web/20201111001018/https://www.fda.gov/emergency-preparedness-and-response/mcm-legal-regulatory-and-policy-framework/emergency-use-authorization">FDA’s  Emergency Use Authorization authority</a> and watch a <a href="https://web.archive.org/web/20201111001018/https://www.youtube.com/watch?v=iGkwaESsGBQ">video on what an EUA is</a>. </p>
<div>
</div>
<p>While the EUA process is shortened  from the traditional license process, the FDA will still be responsible for a  review of risks and benefits for the potential products. In the case of  COVID-19 vaccines, the FDA previously released guidance to support the  development of data needed to weigh risks and benefits for a vaccine. In the  guidance document, <a href="https://web.archive.org/web/20201111001018/https://www.fda.gov/regulatory-information/search-fda-guidance-documents/development-and-licensure-vaccines-prevent-covid-19" title="Development and Licensure of Vaccines to Prevent COVID-19">Development and Licensure of Vaccines to Prevent COVID-19</a>, manufacturers were presented an outline of expectations  for the design of clinical trials, trial populations, safety and efficacy  considerations and the information that would be needed to assess manufacturing  and facility information, as well as recommendations to support safety. The  guidelines also outline circumstances where an EUA would be considered.</p>
<div>
</div>
<h2 class="dz-Element-h2">Who  is responsible for planning for the COVID-19 vaccine?</h2>
<div>
</div>
<p>The State of Alaska’s Department of  Health and Social Services and Alaska Native Tribal Health Consortium are  working jointly. The COVID-19 Vaccine Task Force includes eight sub-teams  including planning, operations, software solutions, payers, pharmacy,  communications and education, data and a liaison team. On October 16 an <a href="/web/20201111001018/https://dhss.alaska.gov/dph/Epi/id/SiteAssets/Pages/HumanCoV/AlaskaCOVID-19VaccinationDraftPlan.pdf"><img class="ms-asset-icon ms-rtePosition-4" src="/web/20201111001018im_/https://dhss.alaska.gov/_layouts/images/pdficon_small.png" alt=""/>Alaska Draft  COVID-19 Vaccination Plan</a> was submitted to the CDC for review, as required  of all states. </p>
<div>
</div>
<p>Local planning is essential to the  effective distribution of the COVID-19 vaccine. The planning team anticipates  that Local-level emergency operations centers and healthcare workers will have  flexibility to consider ways vaccination efforts may need to be tailored to fit  their community. Communities play a critical role by assembling local vaccine  planning teams with representation from a variety of stakeholders, reviewing  the capabilities and challenges for equitable distribution of the vaccine in  their communities, and utilizing local resources such as pharmacies, vaccine  providers, and Point of Dispensing plans to ensure that individuals have access  to the vaccine in all phases of the distribution process. More information for  local planners is available at the <a href="/web/20201111001018/https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineCommunityPartners.aspx">COVID-19 Vaccine Information for Community Planners</a> webpage. </p>
<div>
</div>
<p><em>Updated  October 22, 2020</em></p></div>
</div>

	</div>

	<div id="DeveloperDashboard" class="ms-developerdashboard">
		
	</div>

    <div id="dhss-footer">
	
		<div id="footer-social">
			<ul>
				<li><a href="https://web.archive.org/web/20201111001018/http://www.facebook.com/pages/Anchorage-AK/Alaska-Department-of-Health-and-Social-Services/99962144928" class="facebook" title="Facebook">&nbsp;</a></li>
				<li><a href="https://web.archive.org/web/20201111001018/http://twitter.com/Alaska_DHSS" class="twitter" title="Twitter">&nbsp;</a></li>
				<li><a href="https://web.archive.org/web/20201111001018/http://vimeo.com/alaskadhss" class="vimeo" title="Vimeo">&nbsp;</a></li>
				<li><a href="https://web.archive.org/web/20201111001018/http://www.youtube.com/alaskadhss" class="youtube" title="YouTube">&nbsp;</a></li>
				<li><a href="https://web.archive.org/web/20201111001018/https://public.govdelivery.com/accounts/AKDHSS/subscriber/new?topic_id=AKDHSS_14" class="govdelivery" title="GovDelivery">&nbsp;</a></li>
			</ul>
	  
		</div>

		<div id="footer-general">
			<ul>
				<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/Commissioner/Pages/Contacts">Contacts</a></li>
				<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/Pages/Accessibility.aspx">Accessibility</a></li>
				<li><a href="https://web.archive.org/web/20201111001018/https://aws.state.ak.us/OnlinePublicNotices/default.aspx">Public Notices</a></li>
				<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/Pages/Staff.aspx">Staff</a></li>
			</ul>
			<ul>
				<li class="webmaster"><a href="https://web.archive.org/web/20201111001018/mailto:hsswebmaster@alaska.gov">Webmaster</a></li>
			</ul>
		</div>
	  
		<div id="footer-divisions">
			<ul>
				<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/daph">Alaska Pioneer Homes</a></li>
				<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/api">Alaska Psychiatric Institute</a></li>
				<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dbh">Behavioral Health</a></li>

<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/fms">Finance &amp; Management Services</a></li>

						</ul>
<ul>

<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dhcs">Health Care Services</a></li>

<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/djj">Juvenile Justice</a></li>
	
<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/ocs">Office of Children's Services</a></li>

<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dpa">Public Assistance</a></li>

</ul>

<ul>

<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dph">Public Health</a></li>

<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/dsds">Seniors &amp; Disabilities Services</a></li>

<li><a href="https://web.archive.org/web/20201111001018/http://dhss.alaska.gov/osmap">Substance Misuse and Addiction Prevention</a></li>
</ul>
</div>

    </div>

	</div>
</div>
</div>

<div id="footer-wrapper">
	<div id="soa-footer">
		<ul class="menu">
			<li><a href="https://web.archive.org/web/20201111001018/http://www.alaska.gov/">State of Alaska</a></li>
			<li><a href="https://web.archive.org/web/20201111001018/https://myalaska.state.ak.us/home/app">myAlaska</a></li>
			<li><a href="https://web.archive.org/web/20201111001018/http://www.alaska.gov/akdir1.html">My Government</a></li>
			<li><a href="https://web.archive.org/web/20201111001018/http://www.alaska.gov/residentHome.html">Resident</a></li>
			<li><a href="https://web.archive.org/web/20201111001018/http://www.alaska.gov/businessHome.html">Business in Alaska</a></li>
			<li><a href="https://web.archive.org/web/20201111001018/http://www.alaska.gov/visitorHome.html">Visiting Alaska</a></li>
			<li><a href="https://web.archive.org/web/20201111001018/http://www.alaska.gov/employeeHome.html">State Employees</a></li>
		</ul>
		<ul class="footer">
			<li>State of Alaska</li>
			<li>&copy; 2019</li>
			<li class="last-child"><a href="https://web.archive.org/web/20201111001018/mailto:hsswebmaster@alaska.gov">Webmaster</a></li>
		</ul>
	</div>
</div>
</div>
<!-- =====  End Main ============================================================ -->


	




<div id="ctl00_panelZone">
	<div style="display:none" id="hidZone"><menu class="ms-SrvMenuUI">
		<ie:menuitem id="MSOMenu_Help" iconsrc="/_layouts/images/HelpIcon.gif" onmenuclick="MSOWebPartPage_SetNewWindowLocation(MenuWebPart.getAttribute('helpLink'), MenuWebPart.getAttribute('helpMode'))" text="Help" type="option" style="display:none">

		</ie:menuitem>
	</menu></div>
</div><input type="hidden" id="_wpcmWpid" name="_wpcmWpid" value=""/><input type="hidden" id="wpcmVal" name="wpcmVal" value=""/>

<script type="text/javascript">
//<![CDATA[
var _spFormDigestRefreshInterval = 1440000;window.g_updateFormDigestPageLoaded = new Date(); window.g_updateFormDigestPageLoaded.setDate(window.g_updateFormDigestPageLoaded.getDate() -5);function loadMDN2() { EnsureScript('MDN.js', typeof(loadFilterFn), null); }
function loadMDN1() { ExecuteOrDelayUntilScriptLoaded(loadMDN2, 'sp.ribbon.js'); }
_spBodyOnLoadFunctionNames.push('loadMDN1');
function _spNavigateHierarchyEx(nodeDiv, dataSourceId, dataPath, url, listInContext, type, additionalQString) {
    SetAdditionalNavigateHierarchyQString(additionalQString);
    g_originalSPNavigateFunc(nodeDiv, dataSourceId, dataPath, url, listInContext, type);
}

g_originalSPNavigateFunc = _spNavigateHierarchy;
_spNavigateHierarchy = _spNavigateHierarchyEx;

function EnsureScripts(scriptInfoList, finalFunction)
{
if (scriptInfoList.length == 0)
{
finalFunction();
}
else
{
var scriptInfo = scriptInfoList.shift();
var rest = function () { EnsureScripts(scriptInfoList, finalFunction); };
var defd;
try
{
eval('defd = typeof(' + scriptInfo[1] + ');');
}
catch (e)
{
defd = 'undefined';
}
if (scriptInfo[2])
{
EnsureScript(scriptInfo[0], defd, null);
ExecuteOrDelayUntilScriptLoaded(rest, scriptInfo[0]);
}
else
{
EnsureScript(scriptInfo[0], defd, rest);
}
}
}
function PublishingRibbonUpdateRibbon()
{
var pageManager = SP.Ribbon.PageManager.get_instance();
if (pageManager)
{
pageManager.get_commandDispatcher().executeCommand('appstatechanged', null);
}
}var _fV4UI = true;
function _RegisterWebPartPageCUI()
{
    var initInfo = {editable: false,isEditMode: false,allowWebPartAdder: false,listId: "{43e7fa95-79e4-49c4-931c-67a03394dcfd}",itemId: 250,recycleBinEnabled: true,enableMinorVersioning: true,enableModeration: false,forceCheckout: true,rootFolderUrl: "\u002fdph\u002fEpi\u002fid\u002fPages",itemPermissions:{High:0,Low:196673}};
    SP.Ribbon.WebPartComponent.registerWithPageManager(initInfo);
    var wpcomp = SP.Ribbon.WebPartComponent.get_instance();
    var hid;
    hid = document.getElementById("_wpSelected");
    if (hid != null)
    {
        var wpid = hid.value;
        if (wpid.length > 0)
        {
            var zc = document.getElementById(wpid);
            if (zc != null)
                wpcomp.selectWebPart(zc, false);
        }
    }
    hid = document.getElementById("_wzSelected");
    if (hid != null)
    {
        var wzid = hid.value;
        if (wzid.length > 0)
        {
            wpcomp.selectWebPartZone(null, wzid);
        }
    }
}
ExecuteOrDelayUntilScriptLoaded(_RegisterWebPartPageCUI, "sp.ribbon.js"); var __wpmExportWarning='This Web Part Page has been personalized. As a result, one or more Web Part properties may contain confidential information. Make sure the properties contain information that is safe for others to read. After exporting this Web Part, view properties in the Web Part description file (.WebPart) by using a text editor such as Microsoft Notepad.';var __wpmCloseProviderWarning='You are about to close this Web Part.  It is currently providing data to other Web Parts, and these connections will be deleted if this Web Part is closed.  To close this Web Part, click OK.  To keep this Web Part, click Cancel.';var __wpmDeleteWarning='You are about to permanently delete this Web Part.  Are you sure you want to do this?  To delete this Web Part, click OK.  To keep this Web Part, click Cancel.';//]]>
</script>
<script type="text/javascript">
// <![CDATA[
// ]]>
</script>
<script type="text/javascript">RegisterSod("sp.core.js", "\u002f_layouts\u002fsp.core.js?rev=7ByNlH\u00252BvcgRJg\u00252BRCctdC0w\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.res.resx", "\u002f_layouts\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=SP\u00252ERes\u0026rev=b6\u00252FcRx1a6orhAQ\u00252FcF\u00252B0ytQ\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.runtime.js", "\u002f_layouts\u002fsp.runtime.js?rev=IGffcZfunndj0247nOxKVg\u00253D\u00253D");RegisterSodDep("sp.runtime.js", "sp.core.js");RegisterSodDep("sp.runtime.js", "sp.res.resx");</script>
<script type="text/javascript">RegisterSod("sp.ui.dialog.js", "\u002f_layouts\u002fsp.ui.dialog.js?rev=Tpcmo1\u00252FSu6R0yewHowDl5g\u00253D\u00253D");RegisterSodDep("sp.ui.dialog.js", "sp.core.js");RegisterSodDep("sp.ui.dialog.js", "sp.res.resx");</script>
<script type="text/javascript">RegisterSod("sp.js", "\u002f_layouts\u002fsp.js?rev=\u00252B4ZEyA892P3T0504qi0paw\u00253D\u00253D");RegisterSodDep("sp.js", "sp.core.js");RegisterSodDep("sp.js", "sp.runtime.js");RegisterSodDep("sp.js", "sp.ui.dialog.js");RegisterSodDep("sp.js", "sp.res.resx");</script>
<script type="text/javascript">RegisterSod("cui.js", "\u002f_layouts\u002fcui.js?rev=OOyJv78CADNBeet\u00252FvTvniQ\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("core.js", "\u002f_layouts\u002f1033\u002fcore.js?rev=RiGU6\u00252FvAzNgOjxKFQLw9pw\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("inplview", "\u002f_layouts\u002finplview.js?rev=WB6Gy8a027aeNCq7koVCUg\u00253D\u00253D");RegisterSodDep("inplview", "core.js");RegisterSodDep("inplview", "sp.js");</script>
<script type="text/javascript">RegisterSod("ribbon", "\u002f_layouts\u002fsp.ribbon.js?rev=F\u00252BUEJ66rbXzSvpf7nN69wQ\u00253D\u00253D");RegisterSodDep("ribbon", "core.js");RegisterSodDep("ribbon", "sp.core.js");RegisterSodDep("ribbon", "sp.js");RegisterSodDep("ribbon", "cui.js");RegisterSodDep("ribbon", "sp.res.resx");RegisterSodDep("ribbon", "sp.runtime.js");RegisterSodDep("ribbon", "inplview");</script>
<script type="text/javascript">RegisterSod("sp.ui.policy.resources.resx", "\u002f_layouts\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=SP\u00252EUI\u00252EPolicy\u00252EResources\u0026rev=YhBHGmUAGyJ3lAgSdE4V\u00252Fw\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("mdn.js", "\u002f_layouts\u002fmdn.js?rev=gwmFFJ2\u00252FfFacqXWAqG\u00252FqKg\u00253D\u00253D");RegisterSodDep("mdn.js", "sp.core.js");RegisterSodDep("mdn.js", "sp.runtime.js");RegisterSodDep("mdn.js", "sp.js");RegisterSodDep("mdn.js", "cui.js");RegisterSodDep("mdn.js", "ribbon");RegisterSodDep("mdn.js", "sp.ui.policy.resources.resx");</script>
<script type="text/javascript">RegisterSod("sp.publishing.resources.resx", "\u002f_layouts\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=SP\u00252EPublishing\u00252EResources\u0026rev=q6nxzZIVVXE5X1SPZIMD3A\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.ui.pub.ribbon.js", "\u002f_layouts\u002fsp.ui.pub.ribbon.js?rev=epwnP\u00252FbdljnctbCVld1nnA\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("msstring.js", "\u002f_layouts\u002f1033\u002fmsstring.js?rev=QtiIcPH3HV7LgVSO7vONFg\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("browserScript", "\u002f_layouts\u002f1033\u002fnon_ie.js?rev=EVTj1bu32\u00252FMla6SDN\u00252FsNTA\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSodDep("browserScript", "msstring.js");</script>
<script type="text/javascript">
//<![CDATA[
Sys.Application.initialize();
//]]>
</script>
</form>


</body>
</html>
<!--
     FILE ARCHIVED ON 00:10:18 Nov 11, 2020 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 05:37:27 Apr 25, 2022.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
-->
<!--
playback timings (ms):
  captures_list: 108.891
  exclusion.robots: 0.094
  exclusion.robots.policy: 0.086
  RedisCDXSource: 0.57
  esindex: 0.009
  LoadShardBlock: 80.554 (3)
  PetaboxLoader3.datanode: 54.505 (4)
  CDXLines.iter: 20.066 (3)
  load_resource: 156.874
  PetaboxLoader3.resolve: 102.329
-->