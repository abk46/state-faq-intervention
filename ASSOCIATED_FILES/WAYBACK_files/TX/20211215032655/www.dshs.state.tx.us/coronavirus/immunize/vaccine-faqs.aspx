
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head><script src="//archive.org/includes/analytics.js?v=cf34f82" type="text/javascript"></script>
<script type="text/javascript">window.addEventListener('DOMContentLoaded',function(){var v=archive_analytics.values;v.service='wb';v.server_name='wwwb-app226.us.archive.org';v.server_ms=340;archive_analytics.send_pageview({});});</script>
<script type="text/javascript" src="/_static/js/bundle-playback.js?v=36gO9Ebf" charset="utf-8"></script>
<script type="text/javascript" src="/_static/js/wombat.js?v=UHAOicsW" charset="utf-8"></script>
<script type="text/javascript">
  __wm.init("https://web.archive.org/web");
  __wm.wombat("https://www.dshs.state.tx.us/coronavirus/immunize/vaccine-faqs.aspx","20211215032655","https://web.archive.org/","web","/_static/",
	      "1639538815");
</script>
<link rel="stylesheet" type="text/css" href="/_static/css/banner-styles.css?v=fantwOh2" />
<link rel="stylesheet" type="text/css" href="/_static/css/iconochive.css?v=qtvMKcIJ" />
<!-- End Wayback Rewrite JS Include -->
<script type="text/javascript" src="/web/20211215032655js_/https://www.dshs.state.tx.us/ruxitagentjs_ICA23Vfghjqrtux_10229211201102017.js" data-dtconfig="rid=RID_-1764846130|rpid=-349945408|domain=state.tx.us|reportUrl=/rb_bf35299gib|app=fb61825848b339be|rcdec=1209600000|featureHash=ICA23Vfghjqrtux|rdnt=1|uxrgce=1|bp=2|srmcrv=10|cuc=2tgs02ew|mdl=mdcc2=20|mel=100000|dpvc=1|md=mdcc1=bdocument.referrer,mdcc2=bnavigator.userAgent,mdcc3=dutm_source,mdcc4=dutm_medium,mdcc5=dutm_term,mdcc6=dutm_campaign,mdcc7=dutm_content|ssv=4|lastModification=1638798468693|dtVersion=10229211201102017|srmcrl=1|tp=500,50,0,1|uxdcw=1500|tal=3|agentUri=/ruxitagentjs_ICA23Vfghjqrtux_10229211201102017.js"></script><link rel="stylesheet" type="text/css" href="/web/20211215032655cs_/https://www.dshs.state.tx.us/WorkArea/FrameworkUI/css/ektron.stylesheet.ashx?id=-1759591071+-300771134+1985268503"/><script type="text/javascript" src="/web/20211215032655js_/https://www.dshs.state.tx.us/WorkArea/FrameworkUI/js/ektron.javascript.ashx?id=-569449246+-1939951303+-1080527330+-1687560804+-1388997516+2009761168+27274999+1979897163+-422906301+-1818005853+-1008700845+-991739241+-1793043690"></script><title>COVID-19 Vaccine Frequently Asked Questions (FAQs)</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><meta name="Author" content="Texas Department of State Health Services"/>
    <!--These style sheets apply to IE7 and IE8 Compatibility mode -->
    <!--[if lt IE 8]><link rel="stylesheet" href="/css/blueprint/ie.css" type="text/css" media="screen, projection" /><![endif]-->
    <link href="/web/20211215032655cs_/https://www.dshs.state.tx.us/css/blueprint/print.css" rel="stylesheet" type="text/css" media="print"/><link href="/web/20211215032655cs_/https://www.dshs.state.tx.us/css/dshsPrint.css" rel="stylesheet" type="text/css" media="print"/>
    <!-- import several css files in one to avoid IE limit on @ of css files -->
    <link rel="stylesheet" href="/web/20211215032655cs_/https://www.dshs.state.tx.us/css/DSHS.css" type="text/css"/>
    <!--[if lt IE 8]><link rel="stylesheet" href="/css/DSHSie.css" type="text/css" media="screen, projection" /><![endif]-->
    <!--[if lt IE 7.0000]>
      <link rel="stylesheet" href="/css/DSHSIE6.css" type="text/css" />
    <![endif]-->
    <link href="/web/20211215032655cs_/https://www.dshs.state.tx.us/css/editorStyles.css" rel="stylesheet" type="text/css"/><link href="/web/20211215032655cs_/https://www.dshs.state.tx.us/css/internet-styles.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="/web/20211215032655js_/https://www.dshs.state.tx.us/js/master-min.js"></script>

    <!-- superfish jQuery extension -->
    <script type="text/javascript">
        // initialize plugins
        $ektron().ready(function () {
            $ektron('ul#mainNav').superfish();

            $ektron(".sort_color_off").click(function () {
                $ektron("#srchloading").show();
            });
        });
    </script>
    <!-- SiteImprove -->
    <script type="text/javascript">
        /*<![CDATA[*/
        (function () {
            var sz = document.createElement('script'); sz.type = 'text/javascript'; sz.async = true;
            sz.src = '//web.archive.org/web/20211215032655/https://siteimproveanalytics.com/js/siteanalyze_28641.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(sz, s);
        })();
        /*]]>*/
    </script>

    

    <link href="/web/20211215032655cs_/https://www.dshs.state.tx.us/css/glossy.css" type="text/css" rel="stylesheet"/>
    <script src="/web/20211215032655js_/https://www.dshs.state.tx.us/js/ddaccordion.js" type="text/javascript">
      /***********************************************
      * Accordion Content script- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
      * Visit http://www.dynamicDrive.com for hundreds of DHTML scripts
      * This notice must stay intact for legal use
      ***********************************************/
    </script>
    <script src="/web/20211215032655js_/https://www.dshs.state.tx.us/js/locNav.js" type="text/javascript"></script>
    <script type="text/javascript" src="/web/20211215032655js_/https://www.dshs.state.tx.us/WorkArea/java/thickbox.js" id="EktronThickBoxJS"></script>
   <link rel="stylesheet" type="text/css" href="/web/20211215032655cs_/https://www.dshs.state.tx.us/WorkArea/csslib/box.css" id="EktronThickBoxCss"/>

<link rel="canonical" href="https://web.archive.org/web/20211215032655/https://www.dshs.state.tx.us/coronavirus/immunize/vaccine-faqs.aspx"/></head>
<body id="bodymain">
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
                m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://web.archive.org/web/20211215032655/https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-76662241-1', 'auto');
        ga('send', 'pageview');

    </script>
    <form method="post" action="/web/20211215032655/https://www.dshs.state.tx.us/coronavirus/immunize/vaccine-faqs.aspx" id="aspnetForm">
<div class="aspNetHidden">
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value=""/>
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value=""/>
<input type="hidden" name="EktronClientManager" id="EktronClientManager" value="-1759591071,-569449246,-1939951303,-1080527330,-1687560804,-1388997516,2009761168,27274999,1979897163,-422906301,-1818005853,-1008700845,-991739241,-1793043690,-300771134,1985268503"/>
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTE2MjIzNTA4NjQPZBYCZg9kFgICARBkZBYEAgcPZBYCZg9kFgICAQ9kFgJmDxYCHgdWaXNpYmxlZ2QCDw9kFgYCAw9kFgICAw9kFgJmD2QWBGYPZBYCAgMPZBYCZg9kFgICAQ8WAh4FY2xhc3MFEGRyb3B6b25lIFBCQ2xlYXJkAgEPZBYCAgEPZBYCZg9kFgICAQ9kFgICAQ8WAh4LXyFJdGVtQ291bnQCARYCZg9kFgZmDxUCEyBzdHlsZT0nd2lkdGg6MTAwJScAZAIBDxYCHwBoZAIDDxYCHwJmZAIFDxYCHwEFDHNwYW4tMjAgbGFzdBYIAgEPZBYCZg9kFgICBQ8WAh4JaW5uZXJodG1sBTJDT1ZJRC0xOSBWYWNjaW5lIEZyZXF1ZW50bHkgQXNrZWQgUXVlc3Rpb25zIChGQVFzKWQCBQ9kFgJmD2QWBAIBDxYCHwBoZAIDDxYCHwBoZAIHDw8WAh8AZ2RkAgkPZBYCZg9kFgRmD2QWAgIDD2QWAmYPZBYCAgEPFgIfAQUQZHJvcHpvbmUgUEJDbGVhcmQCAQ9kFgICAQ9kFgJmD2QWAgIBD2QWAgIBDxYCHwICARYCZg9kFgZmDxUCEyBzdHlsZT0nd2lkdGg6MTAwJScAZAIBDxYCHwBoZAIDDxYCHwICAxYCAgIPZBYCAgEPZBYCZg9kFgICAQ9kFgICAQ9kFgJmD2QWAgIBD2QWAmYPZBYCAgEPZBYCAgEPZBYCAgEPEGRkFgBkAgkPZBYCZg9kFgRmD2QWAgIDD2QWAmYPZBYCAgEPFgIfAQUQZHJvcHpvbmUgUEJDbGVhcmQCAQ9kFgICAQ9kFgJmD2QWAgIBD2QWAgIBDxYCHwICARYCZg9kFgZmDxUCEyBzdHlsZT0nd2lkdGg6MTAwJScAZAIBDxYCHwBoZAIDDxYCHwJmZBgMBS5jdGwwMCRDb250ZW50UGxhY2VIb2xkZXIxJFBhZ2VIb3N0MSR1eFVYU3dpdGNoDw9kAgFkBRNjdGwwMCRVeEhlYWRlckxpbmtzDw9kZmQFZ2N0bDAwJENvbnRlbnRQbGFjZUhvbGRlcjEkRHJvcFpvbmUyJHV4Q29sdW1uRGlzcGxheSRjdGwwMCR1eENvbnRyb2xDb2x1bW4kY3RsMDIkdXhXaWRnZXRIb3N0JHV4VVhTd2l0Y2gPD2QCAWQFZ2N0bDAwJENvbnRlbnRQbGFjZUhvbGRlcjEkRHJvcFpvbmUyJHV4Q29sdW1uRGlzcGxheSRjdGwwMCR1eENvbnRyb2xDb2x1bW4kY3RsMDAkdXhXaWRnZXRIb3N0JHV4VVhTd2l0Y2gPD2QCAWQFeGN0bDAwJENvbnRlbnRQbGFjZUhvbGRlcjEkRHJvcFpvbmUyJHV4Q29sdW1uRGlzcGxheSRjdGwwMCR1eENvbnRyb2xDb2x1bW4kY3RsMDIkdXhXaWRnZXRIb3N0JHV4V2lkZ2V0SG9zdF93aWRnZXQkVmlld1NldA8PZGZkBXhjdGwwMCRDb250ZW50UGxhY2VIb2xkZXIxJERyb3Bab25lMiR1eENvbHVtbkRpc3BsYXkkY3RsMDAkdXhDb250cm9sQ29sdW1uJGN0bDAxJHV4V2lkZ2V0SG9zdCR1eFdpZGdldEhvc3Rfd2lkZ2V0JFZpZXdTZXQPD2RmZAVnY3RsMDAkQ29udGVudFBsYWNlSG9sZGVyMSREcm9wWm9uZTIkdXhDb2x1bW5EaXNwbGF5JGN0bDAwJHV4Q29udHJvbENvbHVtbiRjdGwwMSR1eFdpZGdldEhvc3QkdXhVWFN3aXRjaA8PZAIBZAUuY3RsMDAkQ29udGVudFBsYWNlSG9sZGVyMSREcm9wWm9uZTMkdXhVWFN3aXRjaA8PZAIBZAUuY3RsMDAkQ29udGVudFBsYWNlSG9sZGVyMSREcm9wWm9uZTIkdXhVWFN3aXRjaA8PZAIBZAV4Y3RsMDAkQ29udGVudFBsYWNlSG9sZGVyMSREcm9wWm9uZTIkdXhDb2x1bW5EaXNwbGF5JGN0bDAwJHV4Q29udHJvbENvbHVtbiRjdGwwMCR1eFdpZGdldEhvc3QkdXhXaWRnZXRIb3N0X3dpZGdldCRWaWV3U2V0Dw9kZmQFLmN0bDAwJENvbnRlbnRQbGFjZUhvbGRlcjEkRHJvcFpvbmUxJHV4VVhTd2l0Y2gPD2QCAWQFEGN0bDAwJFV4Tm9TY3JpcHQPD2RmZNzERQhFKV2KV1e1YYmKoINzITYPdETLlsDU0WJqQamM"/>
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['aspnetForm'];
if (!theForm) {
    theForm = document.aspnetForm;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/web/20211215032655js_/https://www.dshs.state.tx.us/WebResource.axd?d=KKq73DFr4fCLH8w5NB0cZDb8c8jrqXWdK0Nzb1PywUyBh2uBO-u-KA0GQYmDU4mvFlZH-cYxD4FvhJllQpSczeHsGHuAieS-0kj7SCHussQ1&amp;t=635875293181218729" type="text/javascript"></script>


<script src="/web/20211215032655js_/https://www.dshs.state.tx.us/ScriptResource.axd?d=o_opkTyIdu5UJ7uXytBDxlym-ZjB7J0PquyrsMNc_AZV6RnBGnpRWCGPFVPQnVRJDjTy8poRT__OPZkKMQ7ccYpbAEPdIk8q7WqwyYTF4GACEpdOe1UdgroLMVQb5ueZpTkRnvG5VDw5mnmvTUnnST4y4mLKklBhAylRyXjxHss1&amp;t=ffffffffeea0dba9" type="text/javascript"></script>
<script src="/web/20211215032655js_/https://www.dshs.state.tx.us/ScriptResource.axd?d=NOo1NzXkSpPVu3fs8p-hBwmYtZfx-QeieUDDfDU0Eyah4kzeU7dxMZvptODreJHJs0locQVh7C9j-3AVkondt3dI_tv-81cer7U-E1kipJaehs0M3L5aU7qQeOcwCs8ycLIW6luMK9DV9AyYYiP0tvOISrwX6UHoIPKMwwiDfshK1nqaPb7UXnR1gc-7bigk0&amp;t=ffffffffeea0dba9" type="text/javascript"></script>
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="87D830E2"/>
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAAWomVnMWcICo8J6wy9CxOx11WUX9yiin76Y35vEtoINXGZe8aAeuzuY1Cab543wfs0VPrrZgUp04R/CwoNGhOgDcieQ/WfpLZhvHgCegzlLdBIdQQoKcpxLcuwVD/jXxncXSGaqTu0NmUU7DxFyvdyi"/>
</div>
        
                <noscript>
                    Note: Javascript is disabled or is not supported by your browser. All 
            content is viewable but it will not display as intended.
           
                </noscript>
            



        <div id="header">
            <div id="headerTop">
            </div>
            <div class="container">
                <div class="span-10">
                    <div id="logo">
                        <a href="/web/20211215032655/https://www.dshs.state.tx.us/" accesskey="1">
                            <img src="/web/20211215032655im_/https://www.dshs.state.tx.us/images/HHSDSHS-Logo.png" alt="Texas Department of State Health Services"/>
                        </a>

                    </div>
                </div>
                <a class="skiplink" href="#globalmenu" accesskey="5">Skip to global menu 5</a>
                <a class="skiplink" href="#startpage" accesskey="2">Skip to local menu 2</a>
                <a class="skiplink" href="#startcontent" accesskey="3">Skip to content 3</a>
                <a class="skiplink" href="#footermenu" accesskey="6">Skip to footer 6</a>
                <div class="span-12">
                    <div id="search">
                        <div id="ctl00_srchBox" onkeypress="javascript:return WebForm_FireDefaultButton(event, 'ctl00_UxSearch')">
	
                            <div class="searchBoxes">
                                <input name="ctl00$txtSearchText" type="text" id="ctl00_txtSearchText" accesskey="4" class="searchBox" aria-label="Search"/>
                                <input type="button" name="ctl00$UxSearch" value="" onclick="javascript:__doPostBack('ctl00$UxSearch','')" id="ctl00_UxSearch" class="searchButton" aria-label="Run Search"/>
                                
                            </div>
                        
</div>
                        <div class="span-11">
                            
<ul class="alphabet">
    <li>Topics:</li>
    <li><a href="/web/20211215032655/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=A">A</a></li>
    <li><a href="/web/20211215032655/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=B">B</a> </li>
    <li><a href="/web/20211215032655/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=C">C</a> </li>
    <li><a href="/web/20211215032655/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=D">D</a> </li>
    <li><a href="/web/20211215032655/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=E">E</a> </li>
    <li><a href="/web/20211215032655/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=F">F</a> </li>
    <li><a href="/web/20211215032655/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=G">G</a> </li>
    <li><a href="/web/20211215032655/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=H">H</a> </li>
    <li><a href="/web/20211215032655/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=I">I</a> </li>
    <li><a href="/web/20211215032655/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=J">J</a> </li>
    <li><a href="/web/20211215032655/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=K">K</a> </li>
    <li><a href="/web/20211215032655/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=L">L</a> </li>
    <li><a href="/web/20211215032655/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=M">M</a> </li>
    <li><a href="/web/20211215032655/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=N">N</a> </li>
    <li><a href="/web/20211215032655/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=O">O</a> </li>
    <li><a href="/web/20211215032655/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=P">P</a> </li>
    <li><a href="/web/20211215032655/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=Q">Q</a> </li>
    <li><a href="/web/20211215032655/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=R">R</a> </li>
    <li><a href="/web/20211215032655/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=S">S</a> </li>
    <li><a href="/web/20211215032655/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=T">T</a> </li>
    <li><a href="/web/20211215032655/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=U">U</a> </li>
    <li><a href="/web/20211215032655/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=V">V</a> </li>
    <li><a href="/web/20211215032655/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=W">W</a> </li>
    <li><a href="/web/20211215032655/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=X">X</a> </li>
    <li><a href="/web/20211215032655/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=Y">Y</a> </li>
    <li><a href="/web/20211215032655/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=Z">Z</a> </li>
    <li><a href="/web/20211215032655/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=all">All</a> </li>
</ul>

                        </div>
                    </div>
                </div>
                <div class="span-3 last">
                    <div class="headerLinks pull-1">
                        
                                
                                <span id="ctl00_MobileLogic1_UxMobileLink"><a href="/web/20211215032655/https://www.dshs.state.tx.us/Mobile/Mobile.aspx">Mobile</a> | </span>
                                <a id="ctl00_UxSpanish" href="javascript:__doPostBack('ctl00$UxSpanish','')">Inicio en español</a>
                                <br/>
                                Text Size:
                       
                            
                        
<span id="resize">
<a id="increase" class="textLg" href="#" title="Font Larger">Font Larger</a>
<a id="decrease" class="textSm" href="#" title="Font Smaller">Font Smaller</a>
</span>
                        <br/>
                        <span id="ctl00_uxLoginMenu_lblWelcomeMessage"></span>
<span id="ctl00_uxLoginMenu_logoutBar"></span>

<span id="ctl00_uxLoginMenu_accountlinkbar"></span>


                    </div>
                </div>
                <div class="span-25 last">
                    <a name="globalmenu" id="globalmenu"></a>
                    <div id="globalNav"><ul id="mainNav"><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/" target="_self">Home</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/coronavirus/">COVID-19</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/about-DSHS.shtm">About DSHS</a><ul><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/orgchart/contact_list.shtm">Administrative Contacts</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/Advisory-Committees.aspx">Advisory Committees</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/contracts/default.shtm">Contracts and Budgets</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/policy/compact.shtm#customerservice">Customer Service</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/datareports.shtm">Data and Reports</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/contractor/">Doing Business with DSHS</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/exec-team.aspx">Executive Team</a></li><li><a href="https://web.archive.org/web/20211215032655/https://hhs.texas.gov/laws-regulations/policies-rules/health-human-services-rulemaking" target="_self">HHS Rulemaking</a></li><li><a href="https://web.archive.org/web/20211215032655/https://hhs.texas.gov/about-hhs/leadership/councils/health-human-services-commission-executive-council" target="_self">HHSC Executive Council</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/legislative/default.shtm">Legislative Information</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/orgchart/default.shtm">Organization Chart</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/about-DSHS/campaigns/">Public Health Campaigns</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/visitor/default.shtm">Visitor Information</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/volunteer/default.shtm">Volunteer with DSHS</a></li><li class="menuCornerTabbed"><div class="menuCorners"></div></li></ul></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/news/">News</a><ul><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/WorkArea/linkit.aspx?LinkIdentifier=ID&amp;ItemID=34370">News Releases</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/news/alerts.aspx">Health Alerts &amp; Advisories</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/news/updates.shtm">News Updates</a></li><li class="menuCornerTabbed"><div class="menuCorners"></div></li></ul></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/I-am-a.shtm">I am a...</a><ul><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/texas-resident/">Citizen</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/contractor/">Contractor</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/grant-writer/">Grant Writer</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/health-professional/">Health Professional</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/texas-resident/">Individual or Family</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/news/">Journalist</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/Licensee-Registrant-Permittee">Licensee or Business</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/texas-resident/">Parent/Guardian</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/public-servant/">Public Servant</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/chs/">Researcher</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/texas-resident/#student">Student</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/texas-resident/">Texas Resident</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/volunteer/default.shtm">Volunteer</a></li><li class="menuCornerTabbed"><div class="menuCorners"></div></li></ul></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/Most-Popular/">Most Popular</a><ul><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/coronavirus/">Coronavirus Disease 2019</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/vs/reqproc/certified_copy.shtm">Birth Certificates</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/vs/reqproc/deathcert.shtm">Death Certificates</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/emstraumasystems/default.shtm">EMS Certification and Licensing</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/flu/surveillance.aspx">Flu Surveillance</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/Search-Jobs.aspx">Jobs at DSHS</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/immunize/">Immunizations</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/Vital_Statistics/Verification_of_a_Marriage_or_Divorce.aspx">Marriage/Divorce Verification</a></li><li><a href="https://web.archive.org/web/20211215032655/https://vo.ras.dshs.state.tx.us/" target="_self">Online Licenses</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/txever">TxEVER</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/Im-looking-for.aspx">All Most Popular...</a></li><li class="menuCornerTabbed"><div class="menuCorners"></div></li></ul></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/Resources.htm">Resources</a><ul><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/Advisory-Committees.aspx">Advisory Committees</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/avlib/default.shtm">Audiovisual Library</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/idcu/investigation/conditions/">Disease Reporting</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/library/DSHSauthors.shtm">DSHS Research Articles</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/eGrants/">eGrants System</a></li><li><a href="https://web.archive.org/web/20211215032655/http://hhsc.pinnaclecart.com/dshs/" target="_self">Forms and Publications</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/fic/default.shtm">Funding Information Center</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/library/">Medical and Research Library</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/open/default.shtm">Open Meetings</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/policy/openrecords.shtm">Open Records Requests</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/regions/default.shtm">Public Health Regions</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/links/default.shtm">Related Websites</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/about/rules-regs.aspx">Rules and Regulations</a></li><li><a href="https://web.archive.org/web/20211215032655/http://www.texashealthlibrary.com/" target="_self">Texas Health Library</a></li></ul></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/Online-Services.aspx">Online Services</a><ul><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/vs/reqproc/Ordering-Birth-Certificates-Online.shtm">Birth Certificates</a></li><li><a href="https://web.archive.org/web/20211215032655/https://vo.ras.dshs.state.tx.us/">Business/Professional Licenses</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/vs/reqproc/Ordering-Death-Certificates-Online/">Death Certificates</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/eGrants/">eGrants System</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/emstraumasystems/newcert.shtm">EMS Certification &amp; Licenses</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/food-handlers/training/online.aspx">Food Handler Training</a></li><li><a href="https://web.archive.org/web/20211215032655/http://hhsc.pinnaclecart.com/dshs/">Forms and Publications</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/fic/default.shtm">Funding Opportunities</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/Search-Jobs.aspx">Job Opportunities</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/vs/reqproc/Ordering-Marriage-and-Divorce-Verifications-Online.shtm">Marriage/Divorce Verifications</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/immunize/immtrac/default.shtm">Texas Immunization Registry</a></li><li class="menuCornerTabbed"><div class="menuCorners"></div></li></ul></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/contact.shtm">Contact Us</a></li></ul></div>
                </div>
            </div>
        </div>
        <div id="body">
            <div class="container">
                <a name="startpage" id="startpage"></a>
                
    <a class="skiplink" href="#startcontent" accesskey="3">Skip to content 3</a>
    <script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$ContentPlaceHolder1$PageHost1$ctl03', 'aspnetForm', ['tctl00$ContentPlaceHolder1$DropZone1$uxUpdatePanel','','tctl00$ContentPlaceHolder1$SectionTitleUpdater','','tctl00$ContentPlaceHolder1$DropZone2$uxUpdatePanel','','tctl00$ContentPlaceHolder1$DropZone3$uxUpdatePanel','','tctl00$ContentPlaceHolder1$DropZone2$uxColumnDisplay$ctl00$uxControlColumn$ctl00$uxWidgetHost$uxUpdatePanel','','tctl00$ContentPlaceHolder1$DropZone2$uxColumnDisplay$ctl00$uxControlColumn$ctl01$uxWidgetHost$uxUpdatePanel','','tctl00$ContentPlaceHolder1$DropZone2$uxColumnDisplay$ctl00$uxControlColumn$ctl02$uxWidgetHost$uxUpdatePanel',''], [], [], 90, 'ctl00');
//]]>
</script>


    <script type="text/javascript">
        Ektron.PBSettings = { 'dontClose': false }
    </script>




        <div class="ektron-ux-UITheme ux-app-siteApppageBuilder-setSizeTemplate">
            
        </div>
        <script id="EktronScriptBlocklgbtw" type="text/javascript">

Ektron.ready(function(event, eventName){

                $ektron(".ux-app-siteApppageBuilder-setSizeTemplate .ui-dialog-buttonpane a").button();
                $ektron(".ux-app-siteApppageBuilder-setSizeTemplate .framework").accordion({
                    heightStyle: "content",
                    activate: function(event, ui){
                        $ektron(ui).closest(".ui-accordion").accordion("refresh");
                    }
                });
                $ektron(".ux-app-siteApppageBuilder-setSizeTemplate").draggable({ handle: "div.ui-dialog-titlebar ", containment: $ektron("body") });
            
});


</script>
		
        
    

    <div id="ctl00_ContentPlaceHolder1_leftFlex" class="span-5" style="float:left !Important">
        <div class="leftMargin">
            <div class="glossymenu"><a class="menuitem" href="/web/20211215032655/https://www.dshs.state.tx.us/coronavirus/immunize/vaccine.aspx" target="_self">COVID-19 Vaccine Home</a><a class="menuitem submenuheader" href="#" target="_self">Find Vaccine</a><div class="submenu"><ul><li><a href="https://web.archive.org/web/20211215032655/https://www.vaccines.gov/" target="_self">National Vaccine Finder</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/coronavirus/immunize/vaccine.aspx#mobile" target="_self">Mobile Vaccine Program</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/coronavirus/immunize/vaccine.aspx#phone" target="_self">Find by Phone</a></li><li><a href="https://web.archive.org/web/20211215032655/https://getthevaccine.dshs.texas.gov/" target="_self">Texas Public Health Vaccine Scheduler</a></li></ul></div><a class="menuitem" href="/web/20211215032655/https://www.dshs.state.tx.us/coronavirus/immunize/vaccine-faqs.aspx" target="_self">COVID-19 Vaccine FAQs</a><a class="menuitem submenuheader" href="#" target="_self">Information for Vaccination Providers</a><div class="submenu"><ul><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/coronavirus/immunize/vaccination-providers.aspx" target="_self">Information for Providers</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/coronavirus/immunize/provider-enrollment.aspx" target="_self">Provider Enrollment</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/coronavirus/immunize/vaccine-manage-resources.aspx" target="_self">Vaccine Management</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/immunize/covid19/COVID-19-Vaccine-Provider-Webinars/" target="_self">Webinars</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/coronavirus/immunize/vaccine/providerfaq.aspx" target="_self">Provider FAQs</a></li></ul></div><a class="menuitem submenuheader" href="#" target="_self">Data</a><div class="submenu"><ul><li><a href="https://web.archive.org/web/20211215032655/https://tabexternal.dshs.texas.gov/t/THD/views/COVID-19VaccineinTexasDashboard/Summary?:origin=card_share_link&amp;:embed=n" target="_self">COVID-19 Vaccination in Texas</a></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/immunize/covid19/data/vaccination-status.aspx" target="_self">Cases and Deaths by Vaccination Status</a></li></ul></div><a class="menuitem" href="/web/20211215032655/https://www.dshs.state.tx.us/coronavirus/immunize/vaccineallocations.aspx" target="_self">COVID-19 Vaccine Allocations</a><a class="menuitem" href="/web/20211215032655/https://www.dshs.state.tx.us/coronavirus/immunize/evap.aspx" target="_self">Expert Vaccine Allocation Panel</a><a class="menuitem" href="/web/20211215032655/https://www.dshs.state.tx.us/immunize/covid19/Public-Health-Entity-Forums/" target="_self">Public Health Entity Forums</a><a class="menuitem" href="/web/20211215032655/https://www.dshs.state.tx.us/immunize/safety/" target="_self">Vaccine Safety</a><a class="menuitem" href="/web/20211215032655/https://www.dshs.state.tx.us/coronavirus/tools/vaccine-comm.aspx" target="_self">COVID-19 Vaccine Communication Tools</a></div>
      
        
        <div id="ctl00_ContentPlaceHolder1_DropZone1_uxUpdatePanel" class="">
	
                <div id="ctl00_ContentPlaceHolder1_DropZone1_uxDropZone" data-ux-pagebuilder="DropZone">
                    
                            <div data-ux-pagebuilder="Column" style="width:100%">
                                
                                <ul>
                                    
                                </ul>
                            </div>
                        
                </div>
            
</div>
    
        </div>
          </div>
     <div id="ctl00_ContentPlaceHolder1_ContentPageColumnCenter" class="span-20 last">
        <ul id="ctl00_ContentPlaceHolder1_UxBreadCrumb_uxBreadcrumb" class="breadcrumb"><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/">Home</a><span> &gt; </span></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/coronavirus/default.aspx">COVID-19</a><span> &gt; </span></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/immunize/">Immunizations</a><span> &gt; </span></li><li><a href="/web/20211215032655/https://www.dshs.state.tx.us/coronavirus/immunize/vaccine.aspx">COVID-19 Vaccine</a><span> &gt; </span></li>
    
   <li id="ctl00_ContentPlaceHolder1_UxBreadCrumb_uxLastli" class="selected">COVID-19 Vaccine Frequently Asked Questions (FAQs)</li>
</ul>

    
        <div id="ctl00_ContentPlaceHolder1_SectionTitleUpdater">
	
                   
        <a name="startcontent" id="startcontent"></a>
                
        
</div>
        <div id="ctl00_ContentPlaceHolder1_pnlContent">
	
        <!--PAGEWATCH-->
        <h1 class="contentTitle">
            COVID-19 Vaccine Frequently Asked Questions (FAQs)
        </h1>
        <div class="content editorStyles">
            <div id="ctl00_ContentPlaceHolder1_uxContent">
		<style type="text/css">
ul.onThisPage {
    margin: 0;
    padding: 0;
    list-style-type: none;
    display: inline-block;
    width: 100%;
}
ul.onThisPage li {
    margin: 0;
    padding: .75em;
    width: calc(50% - 2em);
    float: left;
    border-bottom: 1px dashed #dddddd;
}
ul.onThisPage li:nth-child(odd) {
    margin-right: 1em;
}
a.ctaButton {
   margin: .25em 0; 
}
</style>
<div class="commButtons" style="margin: 0 0 .5em 0;"><a href="/web/20211215032655/https://www.dshs.state.tx.us/Workarea/linkit.aspx?LinkIdentifier=id&amp;ItemID=12884945069&amp;Langtype=1034" class="langButton" lang="es" role="button">en español</a><a href="https://web.archive.org/web/20211215032655/https://www.facebook.com/TexasDSHS" class="fbButton" aria-label="DSHS on Facebook" title="DSHS on Facebook" role="button"><span>Facebook</span></a><a href="https://web.archive.org/web/20211215032655/https://www.instagram.com/TexasDSHS" class="igButton" aria-label="DSHS on Instagram" title="DSHS on Instagram" role="button"><span>Instagram</span></a><a href="https://web.archive.org/web/20211215032655/https://twitter.com/TexasDSHS" class="twButton" aria-label="DSHS on Twitter" title="DSHS on Twitter" role="button"><span><span>Twitter</span></span></a><a href="https://web.archive.org/web/20211215032655/https://www.youtube.com/TexasDSHS" class="ytButton" aria-label="DSHS on YouTube" title="DSHS on YouTube" role="button"><span>YouTube</span></a></div>
<p> <img src="/web/20211215032655im_/https://www.dshs.state.tx.us/uploadedImages/Content/Prevention_and_Preparedness/immunize/covid19/banner-interior-full.png" alt="COVID-19 header image" class="bannerTop"/> </p>
<p style="clear: left;">On this page are frequently asked questions (FAQs) about COVID-19 vaccines authorized and available across Texas.</p>
<p style="background-color: #eeeeee; border-top: 1px solid #dddddd; border-bottom: 2px solid #435363; margin-bottom: 0; padding: .75em;"><strong>On this page:</strong> </p>
<ul class="onThisPage">
    <li><a href="#addboost">Boosters &amp; Additional Doses</a></li>
    <li><a href="#safety">Safety</a></li>
    <li><a href="#preg">Pregnant &amp; Recently Pregnant People</a></li>
    <li><a href="#basics">Basics</a></li>
    <li><a href="#children">Children &amp; Teens</a></li>
    <li><a href="#effective">Effectiveness &amp; Immunity</a></li>
    <li><a href="#carditis">Myocarditis and Pericarditis after Vaccination</a></li>
    <li><a href="#moreinfo">More Information</a></li>
    <li><a href="#jj">Johnson &amp; Johnson Safety Information</a></li>
    <li><a href="#provider">Vaccine Provider FAQs</a></li>
    <li><a href="#availability">Vaccine Availability in Texas</a></li>
    <li><a href="#general">General COVID-19 FAQs</a></li>
    <li><a href="#getvax">Getting Vaccinated</a></li>
    <li><a href="#delta">Delta Variant FAQs</a></li>
</ul>
<hr style="height: 2px; background: #435363; color: #435363;"/>


<h2 id="addboost" style="margin-bottom: .35em !important;">Boosters &amp; Additional Doses</h2>
<h3>What is the difference between a “booster dose" and an “additional dose”?</h3>
<p>To understand the difference between a “booster dose" and an “additional dose,” it’s important to understand each of the following terms.</p>
<p>A “primary series” is the initial dose(s) of a COVID-19 vaccine. For Pfizer and Moderna mRNA vaccines, the primary series is two vaccine doses. For the Johnson &amp; Johnson (J&amp;J) COVID-19 vaccine, the primary series is a single vaccine dose.</p>
<p>A “<a href="#boost">booster dose</a>” is a supplemental vaccine dose given to people when the immune response to a primary vaccine series is likely to have waned over time. CDC has issued recommendations for a single vaccine booster dose in some populations. See the <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/booster-shot.html">COVID-19 Vaccine Booster Shot</a> page on the CDC website for more information.</p>
<p>An “<a href="#add">additional dose</a>” is a subsequent dose given after a primary mRNA vaccine series (Pfizer or Moderna). This is recommended only for people who are moderately to severely immunocompromised, because they may not have received adequate protection from their initial 2-dose series. These people should receive their additional dose at least 28 days after a second dose of Pfizer or Moderna vaccine. See the <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/recommendations/immuno.html">COVID-19 Vaccines for Moderately to Severely Immunocompromised People</a> page on the CDC website for more information.</p>


<h2 style="font-variant: small-caps; margin-bottom: .25em !important;" id="boost">Booster Doses</h2>

<h3>Are booster shots being offered?</h3>
<p>A booster shot is recommended for all three available COVID-19 vaccines in the U.S. Eligible individuals may mix and match which vaccine they receive as a booster dose.</p>
<p>For people who have received an mRNA vaccine (Pfizer or Moderna) as their primary series, a booster shot is recommended for people 18 years old and older at least 6 months after their second dose.</p>
<p>For people who received the Johnson &amp; Johnson (J&amp;J) COVID-19 vaccine, a booster shot is also recommended for those who are 18 years old and older and were vaccinated at least two months before the booster is administered.</p>
<p>If you have questions about whether you should get a COVID-19 booster shot, talk to your healthcare provider. For more information, see the <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/booster-shot.html">Who Is Eligible for a COVID-19 Vaccine Booster Shot?</a> page on the CDC website.</p>

<h3>What does “mix and match” vaccine mean?</h3>
<p>Eligible individuals may choose which vaccine they receive as a booster dose. Some people may have a preference for the vaccine type that they originally received, and others may prefer to get a different booster. CDC’s recommendations now allow for this type of mix-and-match dosing for booster shots.</p>


<h3>Are the booster doses the same as the primary series?</h3>
<ul>
    <li>The Pfizer COVID-19 booster dose vaccine is exactly the same vaccine as the primary series and the dose is the same.</li>
    <li>The Moderna COVID-19 booster dose vaccine is the same vaccine but <strong>half the dose</strong> of the primary series.</li>
    <li>The J&amp;J booster dose vaccine is exactly the same vaccine as the primary series and the dose is the same.</li>
</ul>

<h3>Am I still considered “fully vaccinated” if I don’t get a booster shot? </h3>
<p>Yes. Everyone is still considered fully vaccinated two weeks after their second dose in a 2-shot series, such as the Pfizer or Moderna vaccines, or two weeks after a single-dose vaccine, such as the J&amp;J vaccine.</p>

<h3>If I am immunocompromised and have received my additional dose of an mRNA COVID-19 vaccine (Pfizer or Moderna), can I get a booster dose?</h3>
<p>Yes. Moderately and severely immunocompromised people 18 years old and older who completed an mRNA primary vaccine series (Pfizer or Moderna) and received an additional mRNA vaccine dose may receive a single booster dose (Pfizer, Moderna, or J&amp;J) <strong>at least 6 months after receiving their third mRNA vaccine dose</strong>.</p>
<p>In other words, people who are immunocompromised may receive a total of four COVID-19 vaccine doses according to the timeline described above.</p>

<h3>If I am immunocompromised and have received a single J&amp;J vaccine dose as my primary series, can I get a booster dose?</h3>
<p>Yes. Moderately and severely immunocompromised people 18 years old and older who received a single dose J&amp;J primary vaccine series <strong>should</strong> receive a single booster dose of any of the three available vaccines (Pfizer, Moderna, or J&amp;J) at least 2 months (8 weeks) after receiving their initial J&amp;J primary dose.</p>
<p>A person who received one primary dose of J&amp;J vaccine should not receive more than two COVID-19 vaccine doses.</p>
<p>If you have questions, talk to your healthcare provider about the best timing for your vaccination.</p>

<h2 style="font-variant: small-caps; margin-bottom: .25em !important;" id="add">Additional Doses</h2>

<h3>Who is considered moderately to severely immunocompromised and recommended to receive the additional dose?</h3>
<p>The additional dose of mRNA vaccine should be considered for people who are immunocompromised due to certain medical conditions or certain immunosuppressive medications or treatments. This includes people who have:</p>
<ul>
    <li>Been receiving active cancer treatment for tumors or cancers of the blood</li>
    <li>Received an organ transplant and are taking medicine to suppress the immune system</li>
    <li>Received a stem cell transplant within the last 2 years or are taking medicine to suppress the immune system</li>
    <li>Moderate or severe primary immunodeficiency (such as DiGeorge syndrome, Wiskott-Aldrich syndrome)</li>
    <li>Advanced or untreated HIV infection</li>
    <li>Active treatment with high-dose corticosteroids or other drugs that may suppress your immune response</li>
</ul>
<p>Other medical conditions may also make a person moderately or severely immunocompromised. For the most up-to-date information, see the <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/recommendations/immuno.html">COVID-19 Vaccines for Moderately to Severely Immunocompromised People</a> page of the CDC website. If you have questions about whether you are eligible for an additional dose based on your medical condition or medical treatments, you can talk to your healthcare provider.</p>


<h3>Should my additional dose of the mRNA vaccine come from the same brand (same manufacturer) as my initial vaccine series? Or, can I mix and match?</h3>
<p>Your additional dose should be from the same vaccine brand (manufacturer) as your initial vaccine series. So, if your first 2-dose series was from Moderna, locate your additional dose from Moderna. If your initial 2-dose series was from Pfizer, locate your additional dose from Pfizer.</p>
<p>However, if the mRNA vaccine you got for the first two doses is not available, you can get the other mRNA vaccine. </p>

<h3>What age groups are eligible for an additional dose of an mRNA vaccine?</h3>
<p>It depends on which vaccine brand (manufacturer) you received for your primary series.</p>
<ul>
    <li>Pfizer additional dose: available for moderately to severely immunocompromised people 12 years old and older.</li>
    <li>Moderna additional dose: available for moderately to severely immunocompromised people 18 years old and older.</li>
</ul>

<h3>Do I need a doctor’s note or referral to get an additional dose if I am immunocompromised? Do I have to get the additional dose from the same provider who gave me my initial mRNA vaccine series?</h3>
<p>If you are immunocompromised, you may talk to your healthcare provider about whether an additional dose is appropriate for you, but you are not required to do so. You do not need a doctor’s note or referral and can “self-attest” to receive the additional dose wherever mRNA vaccines are offered. However, it is best that your additional dose is the same brand (Pfizer or Moderna) as your initial vaccine series. </p>

<h3>I got the Johnson &amp; Johnson/Janssen (J&amp;J) single-dose vaccine. Can I get the mRNA additional dose?</h3>
<p>Moderately and severely immunocompromised people 18 years old and older who received a single J&amp;J primary series <strong>should receive a single COVID-19 booster dose</strong> (Pfizer, Moderna, or J&amp;J) as their additional dose at least 2 months (8 weeks) after receiving their initial J&amp;J primary dose. </p>
<p>A person who received one primary dose of J&amp;J vaccine should not receive more than two COVID-19 vaccine doses.</p>
<p>If you have questions, talk to your healthcare provider about the best timing for your vaccination.</p>


<p style="text-align:right;"> <a href="#top">▲ Top</a> </p>
<hr/>
<h2 id="preg" style="margin-bottom: .35em !important;">Pregnant &amp; Recently Pregnant People</h2>
<h3>Can pregnant people get the vaccine?</h3>
<p>Yes. COVID-19 vaccination is recommended for all people 5 years old and older, including <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/recommendations/pregnancy.html">people who are pregnant, breastfeeding, trying to get pregnant now, or might become pregnant in the future</a>.</p>
<p>Pregnant and recently pregnant people are <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/recommendations/pregnancy.html#anchor_1628692463325">more likely to get severely ill with COVID-19 compared with non-pregnant people</a>. Getting a COVID-19 vaccine can protect you from severe illness from COVID-19. Severe illness includes illness that requires hospitalization, intensive care, need for a ventilator or special equipment to breathe, or illness that results in death. Additionally, pregnant people with COVID-19 are at increased risk of preterm birth and might be at increased risk of other adverse pregnancy outcomes, compared with pregnant women without COVID-19.</p>
<p>Evidence suggests the benefits of receiving a COVID-19 vaccine outweigh the risks.</p>
<p>Discuss your options and any concerns with your healthcare provider if you have any reservations.</p>
<p>For more information about COVID-19 vaccines and pregnancy, visit the <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/recommendations/pregnancy.html">COVID-19 Vaccines While Pregnant or Breastfeeding</a> and <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/need-extra-precautions/pregnant-people.html">Pregnant and Recently Pregnant People</a> sections of the CDC website.</p>

<h3>Are there any special precautions pregnant people should take after getting the COVID-19 vaccine?</h3>
<p>Yes. If you experience fever following vaccination, you should take acetaminophen (Tylenol). Fever—for any reason—has been associated with adverse pregnancy outcomes.</p>
<p>Women younger than 50 years old who received the Johnson &amp; Johnson (J&amp;J) COVID-19 vaccine should especially be aware of the rare risk of blood clots with low platelets after vaccination. There are other COVID-19 vaccines available for which this risk has not been seen. If you received a J&amp;J COVID-19 vaccine, you can <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/safety/JJUpdate.html#symptoms-list-question">find out more about this rare but serious adverse event, including what symptoms to look out for</a>, on the CDC website.</p>
<p>If you are pregnant and have received a COVID-19 vaccine, we encourage you to enroll in <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/safety/vsafe.html"><strong>v-safe</strong></a>. V-safe is CDC’s smartphone-based tool that provides personalized health check-ins after vaccination. CDC established a <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/safety/vsafepregnancyregistry.html">v-safe pregnancy registry</a> to gather information on the health of pregnant people who have received a COVID-19 vaccine. Participation is voluntary. You may opt out at any time.</p>

<h3>Who can I speak with if I am pregnant and have questions about COVID-19 vaccination?</h3>
<p>If you have questions or concerns about COVID-19 vaccination, talk to your doctor, nurse, or other healthcare provider. Alternatively, you can contact MotherToBaby, a free and confidential service. MotherToBaby experts are available to answer questions in English or Spanish, Monday–Friday 8am–5pm (local time). To reach MotherToBaby:</p>
<ul>
    <li>Call 1-866-626-6847</li>
    <li>Text 855-999-8525</li>
    <li>Chat live or send a message to <a href="https://web.archive.org/web/20211215032655/https://mothertobaby.org/ask-an-expert/">MotherToBaby</a></li>
</ul>
<p>MotherToBaby is a nonprofit service of the Organization of Teratology Information Specialists (OTIS).</p>

<p style="text-align:right;"> <a href="#top">▲ Top</a> </p>
<hr/>
<h2 id="children" style="margin-bottom: .35em !important;">Children &amp; Teens</h2>
<h3>Can my child get vaccinated for COVID-19?</h3>
<p>It depends. Children 5 years old and older are eligible to get the COVID-19 vaccine. At this time, only the Pfizer vaccine is authorized for people ages 5 years to 17 years.</p>
<p>The Pfizer COVID-19 vaccine for children 5-11 years of age is a different vaccine product than for children 12 years and older. The vaccine for children 5-11 years of age comes in an orange cap vial. The orange cap vial is the only vaccine authorized for this age group. The Pfizer vaccine for 12 years old and older comes in a purple cap vial and must not be administered to children younger than 12 years of age.</p>
<p>Additionally, the dose of the vaccine for children 5-11 years of age is one-third the dose of adolescent/adult dose (10 mcg versus 30 mcg).</p>
<p>Children 5-11 years of age are still required to get two doses, three weeks apart, to be considered fully vaccinated.</p>

<h3>Why should I get my child vaccinated against COVID-19?</h3>
<p>COVID-19 vaccination can help protect your child from getting COVID-19. </p>
<p>Although fewer children have been sick with COVID-19 compared to adults, children can be infected with the virus that causes COVID-19, can get sick from COVID-19, and can spread the virus that causes COVID-19 to others. Getting your child vaccinated helps to protect your child and your family. Vaccination is now recommended for everyone 5 years old and older. Currently, the <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/different-vaccines/Pfizer-BioNTech.html">Pfizer-BioNTech COVID-19 Vaccine</a> is the only one available to children 5 years old and older.</p>
<p>Promptly vaccinating children ages 5 years and up is another valuable tool that will help end the COVID-19 pandemic and have a direct and positive effect on schools being open for classroom learning and extracurricular activities.</p>
<p>For more information, visit the <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/recommendations/adolescents.html">COVID-19 Vaccines for Children and Teens</a> and <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/daily-life-coping/children/symptoms.html">COVID-19 in Children and Teens</a> sections of the CDC website.</p>

<h3>Can my child get vaccinated at any clinic?</h3>
<p>No. At this time, only the Pfizer vaccine is authorized for children and adolescents 5 to 17 years of age. It’s also important to note that the Pfizer COVID-19 vaccine for children 5-11 years of age is a different vaccine product and dosage than that for children 12 years old and older.</p>
<p>However, many pharmacies, Federally Qualified Health Centers, Local Health Departments, Rural Health Clinics, Community Health Centers, school-based clinics, and pediatric provider offices across the state now have pediatric vaccine available. Just be sure to contact your vaccine provider to ensure they are offering the appropriate Pfizer vaccine formulation for your child or adolescent before making an appointment or attending a walk-up vaccine clinic.</p>

<h3>Does my child need parental consent to receive the COVID-19 vaccine?</h3>
<p>Yes. Parental consent is required for the vaccination of children in this age group. Consent may be given verbally if a parent is present, or in writing if a parent is not present.</p>

<h3>Is it safe for my child to get a COVID-19 vaccine?</h3>
<p>Yes. Studies show that COVID-19 vaccines are <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/safety/safety-of-vaccines.html">safe</a> and <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/effectiveness.html">effective</a>. Children 5 years old and older are now eligible to get vaccinated against COVID-19. Like adults, children may have some <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/expect/after.html">side effects</a> after COVID-19 vaccination, which are normal signs that their body is building protection. These side effects may affect their ability to do daily activities, but they should go away in a few days. Your child cannot get COVID-19 from any COVID-19 vaccine.</p>
<p>It is important to note that COVID-19 vaccines have undergone—and will continue to undergo—the most intensive safety monitoring in U.S. history. Robust clinical trials featuring thousands of children were conducted to evaluate the safety and immune response to a COVID-19 vaccine in this population. Because young children are still growing and developing, researchers assessed the need for different doses of vaccines already used for adolescents and adults. As a result, children ages 5 through 11 years will receive an age-appropriate dose and formulation of the Pfizer COVID-19 vaccine. Smaller needles, specifically designed for children, will also be used to give the vaccine to children. </p>
<p>Cases of <a href="#carditis">myocarditis (inflammation of the heart muscle) and pericarditis (inflammation of the outer lining of the heart)</a> have been reported following COVID-19 vaccination in children ages 12 through 17 years. While these conditions are rare, the available evidence suggests a link with mRNA COVID-19 vaccination. In general, people who developed these conditions following COVID-19 vaccination respond well to medical treatment and rest and recover. Because the known and potential benefits of COVID-19 vaccination outweigh the known and potential risks, including the possible small risk of myocarditis or pericarditis, CDC and DSHS continue to recommend COVID-19 vaccination for everyone 5 years of age and older.</p>
<p>Parents/caregivers can enroll their child in <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/safety/vsafe.html">v-safe</a>, a free, easy-to-use smartphone-based tool that uses text messaging and web surveys to provide personalized health check-ins. Through v-safe, you can report how your child is feeling after vaccination.</p>
<p>Additionally, patients, caregivers, and vaccine providers are also asked to report adverse events after vaccination to the <a href="https://web.archive.org/web/20211215032655/https://vaers.hhs.gov/">Vaccine Adverse Event Reporting System (VAERS)</a>, even if it is not clear that the vaccine caused the adverse event. CDC reviews all of the information and reports any serious adverse reactions.</p>
<p>For more information about the safety of COVID-19 vaccines, see the frequently asked questions in the <a href="#safety">Safety</a> section of this page.</p>
<p>Discuss your options and any concerns with your healthcare provider if you have any reservations.</p>

<h3>My child is behind on other vaccines. Can my child get other vaccinations along with the COVID-19 vaccine?</h3>
<p>Yes. In addition to approving the vaccine’s use for children and adolescents, the Centers for Disease Control and Prevention (CDC) updated its clinical guidance to allow COVID-19 vaccines to be administered at the same time as other routine vaccines, including live attenuated vaccines. </p>

<h3>Is my child required to receive the COVID-19 vaccine?</h3>
<p>The COVID-19 vaccine is NOT required at this time for people enrolled in child-care/Pre-K facilities, K-12 schools, colleges, or universities.</p>
<p style="text-align:right;"> <a href="#top">▲ Top</a> </p>
<hr/>
<h2 id="carditis">Myocarditis and Pericarditis after Vaccination</h2>
<p>According to the Centers of Disease Control and Prevention (CDC), rare cases of inflammation of the heart within a week of vaccination with an mRNA COVID-19 vaccine have been reported to the Vaccine Adverse Event Reporting System (VAERS). The reports are rare, given the number of people in the United States vaccinated with one of the mRNA vaccines manufactured by Pfizer and Moderna. More than 165 million people in the US and more than 11 million people in Texas have received these COVID-19 vaccines.</p>
<p>Most patients who received care responded well to medicine and rest and quickly felt better. The CDC and its partners are working to see if there is a relationship between vaccination and the inflammation, called myocarditis and pericarditis. To help with this, healthcare providers should report any cases of myocarditis and pericarditis after vaccination to <a href="https://web.archive.org/web/20211215032655/https://vaers.hhs.gov/">VAERS</a>.</p>
<h3>What do we know about the cases?</h3>
<ul>
    <li>The few reported cases of myocarditis and pericarditis were mostly in young men ages 16 years and older.</li>
    <li>Symptoms typically began a few days to a week after receiving a COVID-19 vaccination.</li>
    <li>They occurred more often following the second dose than the first.</li>
    <li>Most patients saw a prompt improvement after receiving the standard medical care.</li>
</ul>
<h3>What are myocarditis and pericarditis?</h3>
<p><strong>Myocarditis</strong> is inflammation of the heart muscle, and <strong>pericarditis</strong> is inflammation of the outer lining of the heart. In both cases, the body’s immune system causes inflammation in response to an infection or some other trigger. </p>
<p>Many different things can cause these types of inflammation, most commonly infections with a virus, including the flu, common cold viruses, and the virus that causes COVID-19. Most cases of myocarditis and pericarditis are minor, and many times don’t cause symptoms at all.</p>
<h3>What symptoms should I look out for?</h3>
<p>Symptoms include:</p>
<ul>
    <li>Chest pain</li>
    <li>Shortness of breath</li>
    <li>Feelings of having a fast-beating, fluttering, or pounding heart</li>
</ul>
<p>Please seek medical care if you have any of these symptoms within a week after COVID-19 vaccination.</p>
<h3>What are the outcomes of these cases of myocarditis and pericarditis?</h3>
<p>Most patients who received care responded well to medicine and rest and quickly felt better.</p>
<p>Patients can usually return to normal activity after their symptoms improve. However, patients should consult with a healthcare provider and may be advised not to participate in vigorous activity for a period of time while their heart recovers.</p>
<h3>Should I still get myself or my child vaccinated?</h3>
<p>Yes. DSHS and the CDC continue to recommend COVID-19 vaccination for everyone 5 years of age and older.</p>
<p>The known and potential benefits of COVID-19 vaccination outweigh the known and potential risks, including the possible small risk of myocarditis or pericarditis. Also, most patients with myocarditis and pericarditis who received care responded well to medicine and rest and quickly felt better.</p>
<p>If you have concerns about COVID-19 vaccination, talk with your or your child’s doctor, nurse, or other healthcare provider.</p>
<h3>Recommendations to Clinicians:</h3>
<ul>
    <li>Report all cases of myocarditis, pericarditis, and any other adverse events post COVID-19 vaccination to VAERS: <a href="https://web.archive.org/web/20211215032655/https://vaers.hhs.gov/reportevent.html" title="Report an Adverse Event to VAERS">https://vaers.hhs.gov/reportevent.html</a></li>
    <li>For diagnosis, treatment and other clinical recommendations, please visit the CDC website: <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/vaccines/covid-19/clinical-considerations/myocarditis.html" title="Clinical Considerations: Myocarditis and Pericarditis after Receipt of mRNA COVID-19 Vaccines Among Adolescents and Young Adults">https://www.cdc.gov/vaccines/covid-19/clinical-considerations/myocarditis.html</a></li>
</ul>
<p style="text-align:right;"> <a href="#top">▲ Top</a> </p>
<hr/>
<h2 id="jj" style="margin-bottom: .35em !important;">Johnson &amp; Johnson Safety Information</h2>
<h3>Why was use of the Johnson &amp; Johnson/Janssen vaccine paused?</h3>
<p>On April 13, the CDC, FDA, and DSHS recommended a pause in the use of the vaccine after reports of six cases of extremely rare but serious cases of blood clots with low platelet count were reported in women who had received the Johnson &amp; Johnson/Janssen (J&amp;J) vaccine. This pause allowed public health professionals and regulators to conduct an extensive safety review, alert the public to the issue and give healthcare professionals information on how to treat these rare side effects.</p>
<h3>What did health authorities decide after the safety review?</h3>
<p>On April 23, a CDC advisory committee determined that the vaccine is safe and effective and its benefits outweigh the potential risks. The committee recommends the vaccine for anyone age 18 years and older in the United States. The CDC, FDA, and DSHS agree with that recommendation and are asking providers to resume administering it.</p>
<p>The safety review identified a total of 15 cases of rare but serious blood clots in combination with low platelets out of the more than seven million people who have received the Johnson &amp; Johnson vaccine in the U.S. Most were in women between the ages of 18 and 49 who experienced the first symptoms one to two weeks after vaccination.</p>
<p>The CDC estimates that using the vaccine in the United States will prevent more than 2,200 intensive care admissions and 1,400 deaths over the next six months. While the risk of these side effects is very low, public health and regulators will continue to monitor COVID-19 vaccines for safety.</p>
<h3>What should I do if I have received the Johnson &amp; Johnson COVID-19 vaccine?</h3>
<p>After getting the J&amp;J vaccine, it is a good idea to monitor your health and watch for symptoms that may occur.</p>
<p>It is important to remember that mild side effects from COVID-19 vaccines are common, particularly in the first two to three days of vaccination. They are a sign that your immune system is responding to the vaccine. Many people have pain, redness and swelling in the arm where they got the shot. They also may experience tiredness, mild headache, muscle pain, chills, fever, and nausea. These side effects usually start within a day or two of getting the vaccine and usually go away within a few days.</p>
<p>However, you should contact a healthcare provider if you experience any of these symptoms within three weeks of receiving the J&amp;J vaccine:</p>
<ul>
    <li>Shortness of breath</li>
    <li>Chest pain</li>
    <li>Leg swelling</li>
    <li>Persistent abdominal pain</li>
    <li>Severe or persistent headaches or blurred vision</li>
    <li>Easy bruising or tiny blood spots under the skin beyond the site of the injection</li>
</ul>
<h3>If I am a vaccine provider and have Johnson &amp; Johnson/Janssen vaccine from before the pause, can it be used now?</h3>
<p>Yes, as long as your supply of J&amp;J vaccine has been properly stored, the vials were not punctured, and the vaccine is not expired as per the manufacturer’s date.</p>
<h3>Are there restrictions on using the J&amp;J vaccine in certain patient populations?</h3>
<p>No, advisory committee did not recommend any restrictions on the use of the J&amp;J vaccine in specific patient populations. Please see <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/vaccines/covid-19/clinical-considerations/covid-19-vaccines-us.html#janssen-vaccine-certain-populations">CDC’s considerations for use of the Janssen COVID-19 vaccine in certain populations</a>.</p>
<p style="text-align:right;"> <a href="#top">▲ Top</a> </p>

<hr/>
<h2 id="availability" style="margin-bottom: .35em !important;">Vaccine Availability in Texas</h2>
<h3>Who can get the vaccine now?</h3>
<p>As of Wednesday, November 3, 2021, <strong>everyone 5 years old and older is now eligible to receive a COVID-19 vaccine in Texas.</strong></p>
<p>The state’s Expert Vaccine Allocation Panel recommends vaccination for everyone who falls under the current Food and Drug Administration (FDA) emergency use authorizations and approvals:</p>
<ul>
    <li>All vaccines are authorized for people 18 years old and older.</li>
    <li>The Pfizer pediatric vaccine is authorized for people 5 through 11 years of age.</li>
    <li>The Pfizer vaccine is authorized for people 12 through 15 years of age.</li>
    <li>The Pfizer vaccine, marketed under the name COMIRNATY, is fully approved by the FDA for people 16 years old and older.</li>
</ul>
<h3>If I’m eligible for vaccine now, how do I get one?</h3>
<p>There are many ways to get fully vaccinated in Texas—you don’t need health insurance and the vaccine is always free. Please visit or call any of the vaccine resources below.</p>
<h4 style="font-size: 1.15em;">National Vaccine Finder</h4>
 <p><a href="https://web.archive.org/web/20211215032655/https://www.vaccines.gov/" class="ctaButton floatLeft" style="text-transform: none; font-size: 1.15em;">Vaccines.gov <span class="arrow"></span></a></p>
<p><a href="https://web.archive.org/web/20211215032655/https://www.vaccines.gov/">Vaccines.gov</a> is the CDC website that helps people find vaccines in their area.  </p>
<p><a href="https://web.archive.org/web/20211215032655/https://wa.link/z5kihm"><strong>WhatsApp</strong> (in Spanish only)</a> – Choose from a menu to find vaccine locations near you, learn how to get free rides and childcare for your vaccine appointment, and find out more about the COVID-19 vaccine.</p>
<h4 style="font-size: 1.15em;">Local Pharmacies</h4>
<p>Check your local pharmacy’s website to see if vaccine appointments or walk-ins are available. <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/vaccines/covid-19/retail-pharmacy-program/participating-pharmacies.html">See a list of retail pharmacies providing vaccinations.</a></p>
<h4 style="font-size: 1.15em;">Mobile Vaccine Program</h4>
<p>The state mobile program provides a way for Texas businesses and people who are homebound to schedule free mobile vaccinations.</p>
<ul>
    <li><strong><em>Texas businesses, groups, or civic organizations</em></strong> with five or more individuals who voluntarily choose to be vaccinated can call <strong>844-90-TEXAS</strong> (844-908-3927) and select <strong>Option 4</strong> to schedule a visit.</li>
    <li><strong><em>Texans who are homebound</em></strong> can call <strong>844-90-TEXAS</strong> (844-908-3927) and select <strong>Option 2</strong> to request a state mobile vaccination team to come to their home.</li>
</ul>
<h4 style="font-size: 1.15em;">Find Vaccine by Phone</h4>
<ul>
    <li style="margin-bottom: .5em;"><strong><span style="font-size: larger;">Text</span></strong> your ZIP code to find vaccine, childcare, and free rides to clinics to
        <ul>
            <li><strong>GETVAX</strong> (438829) for English</li>
            <li><strong>VACUNA</strong> (822862) for Spanish</li>
        </ul></li>
    <li style="margin-bottom: .5em;"><strong><span style="font-size: larger;">Call</span></strong> 1-833-832-7067 (toll free) for referral to a local vaccine provider
        <ul>
            <li>Call center is open Monday–Friday 8:00am⁠–⁠6:00pm, and Saturday 8:00am–5:00pm.</li>
            <li>Spanish language and other translators are available to help callers.</li>
        </ul></li>
    <li><strong><span style="font-size: larger;">Call</span></strong> the national vaccine finder hotline toll free at 1-800-232-0233 (TTY 1-888-720-7489)</li>
</ul>
<h4 style="font-size: 1.15em;">Texas Public Health Vaccine Scheduler</h4>
<p>The Texas Vaccine Scheduler helps Texans get scheduled for a COVID-19 vaccine at clinics hosted by participating Texas public health entities.</p>
<p>Register online at <a href="https://web.archive.org/web/20211215032655/https://getthevaccine.dshs.texas.gov/">GetTheVaccine.dshs.texas.gov</a>. You will be notified by email or text when and where to get the vaccine. If there’s not a public health clinic near you, you will be directed to other places to get your vaccine.</p>
<p>Call (833) 832-7067 if you don’t have internet or need help signing up.</p>
<h4 style="font-size: 1.15em;">Vaccination Services for People with Disabilities</h4>
<p>People with disabilities needing assistance getting vaccinated can contact the Disability Rights Texas Hotline (DRTx Vaccine Hotline) by phone or email, at 1-800-880-8401 or <a href="https://web.archive.org/web/20211215032655/mailto:vaccine@DRTx.org">vaccine@DRTx.org</a>.</p>
<p>You can also contact the national Disability Information and Access Line (DIAL) at 888-677-1199 or <a href="https://web.archive.org/web/20211215032655/mailto:DIAL@n4a.org">DIAL@n4a.org</a> for vaccine help. </p>
<h3>Who can provide vaccines, and how does that happen?</h3>
<p>Any facility, organization or healthcare provider licensed to possess or administer vaccine or provide vaccination services is eligible to enroll as a COVID-19 vaccine provider. Each facility or location, including those that are part of a hospital system or clinic network, must register at <a href="https://web.archive.org/web/20211215032655/https://enrolltexasiz.dshs.texas.gov/emrlogin.asp">EnrollTexasIZ.dshs.texas.gov/emrlogin.asp</a> and complete the CDC COVID-19 Vaccination Program Provider Agreement.</p>
<h3>How can a long-term care facility get residents, staff and providers vaccinated now that the federal partnership sign-up has closed?</h3>
<p>A long-term care facility that has not already signed up for the federal partnership program has other options to get their residents, staff and providers vaccinated. See <a href="/web/20211215032655/https://www.dshs.state.tx.us/immunize/covid19/LTCOptions-COVID19Vaccination.pdf">Long-Term Care Options for COVID-19 Vaccination</a> (PDF) for additional information.</p>
<h3>I am hearing that the DSHS Pharmacy Branch has vaccines available. Can I get my vaccine there?</h3>
<p>No. The DSHS Pharmacy Branch is not a public pharmacy and does not vaccinate people. It receives and distributes medications to providers across the state. Please do not call or visit the DSHS Pharmacy Branch, as they do not vaccinate anyone at this location.</p>

<h3>What should I do to protect myself and others before I'm fully vaccinated? </h3>
<p><strong>Vaccination is the best tool</strong> we have to protect people and communities from COVID-19. There are many ways to get fully vaccinated in Texas—you don’t need health insurance and the vaccine is always free. Please see the resources in the <a href="/web/20211215032655/https://www.dshs.state.tx.us/coronavirus/immunize/vaccine.aspx#where">How to Find a Vaccine</a> section of this website to find out how to get fully vaccinated.</p>
<p>Until you are fully vaccinated, it is critical that you practice the same safety habits you’ve been doing to prevent the spread of COVID-19. Take the following precautions to limit exposure for yourself and others:</p>
<ul>
  <li>Wear a mask. Vaccinated or not, wearing a mask in indoor public spaces can help protect you and everyone close to you.</li>
  <li>Practice social distancing and avoid close contact with others:
    <ul>
      <li><strong>Outside your home:</strong> Stay at least 6 feet away from others and avoid crowded places.</li>
      <li><strong>Inside your home:</strong> Avoid close contact with household members who are sick. Avoid sharing personal items and use a separate room and bathroom for sick household members, if possible.</li>
    </ul>
  </li>
  <li>Wash your hands often with soap and water for at least 20 seconds, especially after going to the bathroom; before eating; and after blowing your nose, coughing, or sneezing. If soap and water are not readily available, you can use an alcohol-based hand sanitizer that contains at least 60% alcohol.</li>
  <li>Clean frequently-touched objects and surfaces using a household cleaner. You should also use a disinfectant on <a href="https://web.archive.org/web/20211215032655/https://www.epa.gov/pesticide-registration/list-n-disinfectants-coronavirus-covid-19">List N: Disinfectants for COVID-19</a> when someone is sick or if someone who is positive for COVID-19 has been in your home within the last 24 hours.</li>
  <li>Avoid touching your eyes, nose, and mouth with unwashed hands.</li>
  <li>Cover your cough or sneeze with a tissue, then throw the tissue in the trash and wash your hands.</li>
  <li>Stay home when you are sick.</li>
</ul>
<p style="text-align:right;"> <a href="#top">▲ Top</a> </p>


<hr/>
<h2 id="getvax" style="margin-bottom: .35em !important;">Getting Vaccinated</h2>
<h3>Do I need to get vaccinated if I’ve already recovered from COVID-19?</h3>
<p>Yes. You should be vaccinated regardless of whether you already had COVID-19. That’s because experts do not yet know how long you are protected from getting sick again after recovering from COVID-19. Immunity from the COVID-19 vaccine may last longer than the natural immunity you get if you’ve already had COVID-19.</p>
<p>People who currently have COVID-19 should not be vaccinated while they are sick.</p>
<h3>Will the COVID-19 vaccine be one or two shots? How long after the first dose do I take the second one?</h3>
<p>The number of doses needed depends on which vaccine you receive:</p>
<ul>
    <li>It’s best to get the <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/different-vaccines/Pfizer-BioNTech.html">Pfizer</a> second dose 3 to 6 weeks after the Pfizer first dose.</li>
    <li>It’s best to get the <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/different-vaccines/Moderna.html">Moderna</a> second dose 4 to 6 weeks after the Moderna first dose.</li>
    <li><a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/different-vaccines/Janssen.html">J&amp;J/Janssen</a> COVID-19 vaccine requires only one dose.</li>
</ul>
<h3>Can I just take one of the two doses?</h3>
<p>If you choose to get only one dose of a two-dose vaccine, the amount of protection you may have is not known.</p>
<p>When you get the vaccine, you will receive information about what kind of vaccine you got and when you need to come back for your second dose (for two-dose vaccines). You can register and use the new V-safe After Vaccination Health Checker to receive health check-ins after you receive a COVID-19 vaccination, as well as reminders to get your second dose if you need one.</p>
<h3>Do I have to get the second dose from the same location I got the first dose? My provider doesn't know when they'll get another vaccine shipment.</h3>
<p>You do not have to get your second dose from the same location as you got the first dose. But for two-dose vaccines, please try to get both doses from the same vaccine provider. However, if you need to locate a second dose, be sure it’s from the same manufacturer and is in the recommended dose interval. For more information, refer to the vaccination materials you received from your provider when you received your first dose. Those may include a vaccination fact sheet and/or record card.</p>
<h3>If I got the first of a two-dose vaccine but I'm unable to get the second dose within the recommended timeframe, do I have to start all over?</h3>
<p>No, you do not have to start all over. Missing the suggested interval delays full protection. But you can still get the second dose later if you have difficulty getting it within the recommended time. Just don’t get it earlier than recommended.</p>
<p>According to CDC, if you need help scheduling your vaccine appointment for your second shot, contact the location that set up your appointment for assistance. Two-dose vaccines will need two shots to get the most protection.</p>
<p>The timing between your first and second shot depends on which vaccine you received. You should get your second shot:</p>
<ul>
    <li>It’s best to get the <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/different-vaccines/Pfizer-BioNTech.html">Pfizer</a> second dose 3 to 6 weeks after the Pfizer first dose.</li>
    <li>It’s best to get the <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/different-vaccines/Moderna.html">Moderna</a> second dose 4 to 6 weeks after the Moderna first dose.</li>
    <li><a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/different-vaccines/Janssen.html">J&amp;J/Janssen</a> COVID-19 vaccine requires only one dose.</li>
</ul>
<p>For two-dose vaccines, you should <strong>get your second shot as close to the recommended 3-week or 1-month interval as possible</strong>. However, there is no maximum interval between the first and second doses for either vaccine. You should not get the second dose earlier than the recommended interval.</p>
<h3>Which vaccine should I get for COVID-19? Do I have a choice?</h3>
<p>You always have a choice about your health care. Talk to a healthcare provider to get information specific to your situation.</p>
<h3>Does the vaccine I choose depend on my age or underlying conditions?</h3>
<p>Any currently authorized COVID-19 vaccine can be administered to people with underlying medical conditions who have no contraindications to vaccination or their ingredients. Your age and/or underlying conditions may affect which vaccine you are eligible to get. The Advisory Committee on Immunization Practices (ACIP) does not state a product preference.</p>
<p>The Pfizer vaccine is recommended for people 5 years old and older.
<br/>The Moderna vaccine is recommended for people 18 years old and older.
<br/>The J&amp;J vaccine is recommended for people 18 years old and older.</p>
<p>Talk to a healthcare provider to get information specific to you and the COVID-19 vaccines currently available.</p>
<h3>Can I get the COVID-19 vaccine if I have COVID-19?</h3>
<p>No. People with COVID-19 who have symptoms should wait to be vaccinated until they have recovered from their illness and have met the <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/your-health/quarantine-isolation.html">criteria for discontinuing isolation</a>; those without symptoms should also wait until they meet the criteria before getting vaccinated. This guidance also applies to people who get COVID-19 before getting their second dose of vaccine.</p>
<h3>What are some side effects from the vaccines for COVID-19?</h3>
<p>COVID-19 vaccines are associated with a number of side effects, but almost all of them are mild. They include pain and redness at the injection site, fatigue, headache, body aches and even fever. </p>
<p>Having symptoms like fever after you get a vaccine is normal and a sign your immune system is building protection against the virus. The side effects from COVID-19 vaccination may feel like flu, but they should go away in a few days. </p>
<p>If you get the vaccine and experience severe side effects or ones that do not go away in a couple of days, contact your healthcare provider for further instructions on how to take care of yourself.</p>
<p>You can register and use the new <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/safety/vsafe.html">V-safe After Vaccination Health Checker</a> to receive health check-ins after you receive a COVID-19 vaccination, as well as reminders to get your second dose if you need one.</p>
<p>To learn what side effects to expect and get helpful tips on how to reduce pain and discomfort after your vaccination, visit the <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/expect/after.html">Possible Side Effects After Getting a COVID-19 Vaccine</a> section of the CDC website.</p>
<h3>Does the vaccine react poorly with any medications, or do the prescriptions I'm taking preclude me from being able to get a vaccine?</h3>
<p>You will need to check with your healthcare provider about whether your medication will interfere with being vaccinated.</p>
<p style="text-align:right;"> <a href="#top">▲ Top</a> </p>

<hr/>
<h2 id="safety" style="margin-bottom: .35em !important;">Safety</h2>
<h3>How do I know whether the COVID-19 vaccine is safe?</h3>
<p>Safety is a top priority while federal partners work to make COVID-19 vaccines available. The new COVID-19 vaccines have been evaluated in tens of thousands of volunteers during clinical trials. The vaccines are only authorized for use if they are found to be safe.</p>
<p>For the most up-to-date information, see the <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/safety.html">Vaccine Safety</a> section of the CDC website.</p>
<p>To learn about CDC’s new vaccine safety monitoring system, see the <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/safety/vsafe.html">V-safe After Vaccination Health Checker</a> section of the CDC website.</p>
<h3>How do I report it if I have a bad reaction to a vaccine?</h3>
<p>Patients, caregivers, and vaccine providers are asked to report serious side effects (called adverse events) to the <a href="https://web.archive.org/web/20211215032655/https://vaers.hhs.gov/">Vaccine Adverse Event Reporting System (VAERS)</a>, even if it is not clear that the vaccine caused the adverse event. CDC reviews all of the information and reports any serious adverse reactions.</p>
<p>CDC also has a new smartphone-based tool called <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/safety/vsafe.html">v-safe</a>. This tool helps CDC check in on people’s health after they receive a COVID-19 vaccine. When you get your vaccine, you should also receive a v-safe information sheet telling you how to enroll in v-safe. If you enroll, you will get regular text messages directing you to surveys. Use these surveys to report any problems or adverse reactions you have after receiving a COVID-19 vaccine.</p>
<p>For more information about the difference between a vaccine side effect and an adverse event, visit the <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/vaccinesafety/ensuringsafety/sideeffects/index.html">Understanding Side Effects and Adverse Events</a> section of the CDC website.</p>


<h3>Can the COVID-19 vaccine make me sick or give me COVID-19?</h3>
<p>No. COVID-19 vaccines cannot give you COVID-19. The vaccine does not alter your DNA. COVID-19 vaccination will help protect you by creating an immune response without having to experience sickness. Sometimes after vaccination, the process of building immunity can cause symptoms, such as fever. These symptoms are normal and are signs that the body is building immunity.</p>
<p>To learn about COVID-19 vaccines, visit the <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/different-vaccines.html">Different COVID-19 Vaccines</a> section of the CDC website.</p>

<h3>Can children get the vaccine, or will they rely on their natural immune system to protect them?</h3>
<p>Children 5 years old and older are currently eligible to get the vaccine in Texas. Vaccines are currently in clinical trials for children 4 years old and younger. For more information, see the frequently asked questions in the <a href="#children">Children &amp; Teens</a> section of this page.</p>
<p style="text-align:right;"> <a href="#top">▲ Top</a> </p>

<hr/>
<h2 id="basics" style="margin-bottom: .35em !important;">Basics</h2>
<h3>How are the COVID-19 vaccines different from other vaccines?</h3>
<p>Different types of vaccines work in different ways to offer protection. But every type of vaccine works by teaching our bodies how to recognize a germ and trigger an immune response. That immune response, which produces antibodies, is what protects us from getting infected if the real virus enters our bodies.</p>
<p>Currently, there are three main types of COVID-19 vaccines that are authorized and recommended, or undergoing large-scale (Phase 3) clinical trials in the United States:</p>
<ul>
  <li>mRNA vaccines</li>
  <li>Protein subunit vaccines</li>
  <li>Vector vaccines</li>
</ul>
<p>The Pfizer and Moderna vaccines are both mRNA vaccines. Johnson &amp; Johnson’s (J&amp;J) Janssen vaccine is a vector vaccine. </p>
<p>COVID-19 vaccines do not use the live virus and cannot give you COVID-19. The vaccine does not alter your DNA. COVID-19 vaccination will help protect you by creating an immune response without having to experience sickness.</p>
<p>Learn more about how COVID-19 vaccines work on the <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/different-vaccines/how-they-work.html">Understanding How COVID-19 Vaccines Work</a> section of the CDC website.</p>
<h3>Why should I take the COVID-19 vaccine?</h3>
<p>Getting vaccinated will help keep you from getting COVID-19. But no vaccine is 100% effective. If you do get COVID-19, your vaccine can prevent you from getting seriously ill.</p>
<p>Getting a COVID-19 vaccine once it is available to you represents one step that you can take to get the Texas economy, and our day-to-day lives, back to normal.</p>
<h3>Will vaccines prevent people from getting and spreading COVID-19?</h3>
<p>Studies have shown that the available vaccines are effective against disease and hospitalization caused by COVID-19 and its variants, including the Delta variant. Unvaccinated people are most at risk of contracting COVID-19, including any of its variants. The now predominant Delta variant is more aggressive than other known variants and spreads most rapidly in communities with fewer fully vaccinated people.</p>
<p>To learn more about the Delta variant, visit the <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/variants/delta-variant.html">Delta Variant: What We Know About the Science</a> page on the CDC website.</p>
<p>The absolute best protection for yourself and those close to you is getting fully vaccinated. The vaccine is proven to safely protect you from COVID-19’s worst effects and lowers your chances of spreading the virus. Greatly increasing the number of fully vaccinated Texans is the only way to prevent a devastating rise in the spread of the pandemic virus.</p>
<h3 id="fully">When am I considered fully vaccinated?</h3>
<p>You are considered fully vaccinated two weeks after your second dose on two-dose vaccines and 2 weeks after your single dose on one-dose vaccines.</p>
<p>For the most up-to-date information, see the <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/fully-vaccinated.html">When You’ve Been Fully Vaccinated</a> section of the CDC website.</p>
<p style="text-align:right;"> <a href="#top">▲ Top</a> </p>

<hr/>
<h2 id="effective" style="margin-bottom: .35em !important;">Effectiveness &amp; Immunity</h2>
<h3>How effective will the vaccine be against COVID-19, and for how long?</h3>
<p>All vaccines currently authorized for use in the U.S. are effective at protecting against severe COVID-19 that can lead to hospitalization and death. The best protection against COVID-19 and any of its variants is getting fully vaccinated.</p>
<p>    Data show that administration of a booster shot may result in increases in antibody levels and effectiveness compared to primary vaccination.</p>
<p>The Centers for Disease Control and Prevention’s Advisory Committee on Immunization Practices (ACIP) recommends the use of booster shots for recipients of all three COVID-19 vaccines. To learn more, see the frequently asked questions in the <a href="#addboost">Boosters &amp; Additional Doses</a> section of this page.</p>
<p>To learn about efficacy rates for specific vaccines, see the CDC page on <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/different-vaccines.html">Different COVID-19 Vaccines</a>.</p>

<h3>Will the immunity after getting COVID-19 last longer than the protection provided by the vaccine?</h3>
<p>We are still learning about how long a recovered person is protected by “natural immunity.” We are also learning how long the vaccines’ protection, called “vaccine-induced immunity,” lasts. However, a <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/mmwr/volumes/70/wr/mm7044e1.htm?s_cid=mm7044e1_w">recently published study</a> showed that vaccines provided better protection than previous infection. Our Texas antibody project also found that the levels of antibody were higher in people who were fully vaccinated when compared to unvaccinated people with previous infection.</p>
<p>Getting the COVID-19 vaccine is a safer way to gain immunity than by getting the infection itself. So, it is important for every eligible person to get fully vaccinated. And, <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/booster-shot.html">for those people who qualify</a>, it’s important to get the booster dose, too.</p>

<h3>Will we ever achieve “herd immunity” in Texas?</h3>
<p>Herd immunity may not be achievable. We may never eliminate the threat of COVID-19. Instead, SARS-CoV-2 may become endemic, meaning that it would be added to the list of respiratory viruses that usually circulate in Texas. </p>
<p>But we know that increasing the number of Texans who are vaccinated will help protect our communities, whether or not we reach herd immunity.</p>
<p style="text-align:right;"> <a href="#top">▲ Top</a> </p>

<hr/>
<h2 id="moreinfo" style="margin-bottom: .35em !important;">More Information</h2>
<h3>Where can I get reliable information about vaccines for COVID-19?</h3>
<p>Three excellent sources of reliable information are the Texas Department of State Health Services (DSHS), Centers for Disease Control and Prevention (CDC), and the Food and Drug Administration (FDA).</p>
<h4>DSHS</h4>
<p> <a href="/web/20211215032655/https://www.dshs.state.tx.us/coronavirus/immunize/vaccine.aspx">COVID-19 Vaccine Information</a> </p>
<h4>CDC</h4>
<p> <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/index.html">COVID-19 Vaccines</a> </p>
<p> <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/safety.html">COVID-19 Vaccine Safety</a> </p>
<p> <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/keythingstoknow.html">Key Things to Know About COVID-19 Vaccines</a> </p>
<p> <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/your-vaccination.html">Your COVID-19 Vaccination</a> </p>
<p><a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/expect/after.html">Possible Side Effects After Getting a COVID-19 Vaccine</a></p>
<p> <a href="https://web.archive.org/web/20211215032655/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/safety/vsafe.html">V-safe After Vaccination Health Checker</a> </p>
<h4>FDA</h4>
<p> <a href="https://web.archive.org/web/20211215032655/https://www.fda.gov/emergency-preparedness-and-response/coronavirus-disease-2019-covid-19/covid-19-vaccines">COVID-19 Vaccines</a> </p>
<p> <a href="https://web.archive.org/web/20211215032655/https://www.fda.gov/">FDA Homepage</a> </p>
<p style="text-align:right;"> <a href="#top">▲ Top</a> </p>
<hr/>
<h2 id="provider">Vaccine Provider FAQs</h2>
<p>See the <a href="/web/20211215032655/https://www.dshs.state.tx.us/coronavirus/immunize/vaccine/providerfaq.aspx">COVID-19 Vaccine Provider FAQs</a> for answers to common questions for vaccinators in Texas.</p>
<p style="text-align:right;"> <a href="#top">▲ Top</a> </p>
<hr/>
<h2 id="general">General COVID-19 FAQs</h2>
<p>See the <a href="/web/20211215032655/https://www.dshs.state.tx.us/coronavirus/faq.aspx">COVID-19 FAQs</a> for answers to general questions about COVID-19.</p>
<p style="text-align:right;"> <a href="#top">▲ Top</a> </p>
<hr/>
<h2 id="delta">Delta Variant FAQs</h2>
<p>See the <a href="/web/20211215032655/https://www.dshs.state.tx.us/coronavirus/variant-faqs.aspx">Delta Variant FAQs</a> for answers to common questions about the variants of COVID-19, including the Delta variant.</p>
<p style="text-align:right;"> <a href="#top">▲ Top</a> </p>
<hr/>
	</div>
        </div>
        
<script type="text/javascript">
    $ektron('table.zebra tr:nth-child(even)')
        .add('table.zebraBorder tr:nth-child(even)')
        .addClass('zebraEven');
 </script>
        <!--/PAGEWATCH-->
        
</div>
        
        <div id="ctl00_ContentPlaceHolder1_DropZone2_uxUpdatePanel" class="">
	
                <div id="ctl00_ContentPlaceHolder1_DropZone2_uxDropZone" data-ux-pagebuilder="DropZone">
                    
                            <div data-ux-pagebuilder="Column" style="width:100%">
                                
                                <ul>
                                    
                                            <li>
                                                
        <div id="ctl00_ContentPlaceHolder1_DropZone2_uxColumnDisplay_ctl00_uxControlColumn_ctl00_uxWidgetHost_uxUpdatePanel">
		
                <div data-ux-pagebuilder="Widget">
                    
                    <div class="widgetBody">
                        
        
        
        <span id="ctl00_ContentPlaceHolder1_DropZone2_uxColumnDisplay_ctl00_uxControlColumn_ctl00_uxWidgetHost_uxWidgetHost_widget_errorLb"></span>
    

                    </div>
                </div>
            
	</div>
    
                                            </li>
                                        
                                            <li>
                                                
        <div id="ctl00_ContentPlaceHolder1_DropZone2_uxColumnDisplay_ctl00_uxControlColumn_ctl01_uxWidgetHost_uxUpdatePanel">
		
                <div data-ux-pagebuilder="Widget">
                    
                    <div class="widgetBody">
                        
        <div id="ctl00_ContentPlaceHolder1_DropZone2_uxColumnDisplay_ctl00_uxControlColumn_ctl01_uxWidgetHost_uxWidgetHost_widget_CB">
			<script>(function () {

var mppAttachPixel = function () {

    //startexchange

	var dbmTagManager = document.createElement("script");

	dbmTagManager.async = true;

	dbmTagManager.src = "//web.archive.org/web/20211215032655/https://www.googletagmanager.com/gtm.js?id=GTM-T5387JV";

	document.head.appendChild(dbmTagManager);

	window.dataLayer = window.dataLayer || [];

	function gtag(){dataLayer.push(arguments);}

	gtag('js', new Date());

	gtag('config', 'GTM-T5387JV');

	//endexchange

}

if (document.readyState === "complete") {

	mppAttachPixel();

} else {

	document.addEventListener("readystatechange", function(e) {

		if (document.readyState === "complete") {

			mppAttachPixel();

		}

	});

}

}())
</script>
		</div>
        
        <span id="ctl00_ContentPlaceHolder1_DropZone2_uxColumnDisplay_ctl00_uxControlColumn_ctl01_uxWidgetHost_uxWidgetHost_widget_errorLb"></span>
    

                    </div>
                </div>
            
	</div>
    
                                            </li>
                                        
                                            <li>
                                                
        <div id="ctl00_ContentPlaceHolder1_DropZone2_uxColumnDisplay_ctl00_uxControlColumn_ctl02_uxWidgetHost_uxUpdatePanel">
		
                <div data-ux-pagebuilder="Widget">
                    
                    <div class="widgetBody">
                        
	<script type="text/javascript">
	$ektron().ready(function(){				
		 $ektron('#navigation').accordion({
			    active: false,
			    header: '.head',
			    navigation: true,
			    event: 'mouseover',
			    fillSpace: true,
			    animated: 'slide'
		    });
		    $ektron('.navigation1').accordion({
			    active: false,
			    header: '.head',
			    navigation: true,
			    event: 'mouseover',
			    fillSpace: true,
			    animated: 'slide'
		    });
	});	
	</script>
	
    <style type="text/css">
	#navigation ul
        {
	      height: auto !important;	      
        }
	</style>

        
    

                    </div>
                </div>
            
	</div>
    
                                            </li>
                                        
                                </ul>
                            </div>
                        
                </div>
            
</div>
    
        
        <span id="ctl00_ContentPlaceHolder1_UxLastUpdatedDate_UxLastUpdated" class="lastUpdated">Last updated </span><span class="lastUpdatedDate"> December 3, 2021</span>
        
    </div>
        <div class="span-5 last">
            <div class="rightMargin page">
                <div class="suppNav"><ul></ul></div>
                
        <div id="ctl00_ContentPlaceHolder1_DropZone3_uxUpdatePanel" class="">
	
                <div id="ctl00_ContentPlaceHolder1_DropZone3_uxDropZone" data-ux-pagebuilder="DropZone">
                    
                            <div data-ux-pagebuilder="Column" style="width:100%">
                                
                                <ul>
                                    
                                </ul>
                            </div>
                        
                </div>
            
</div>
    
            </div>
        </div>

            </div>
        </div>
        <div id="footer">
            <div id="footerShadow">
                <div class="container">
                    <div id="footerContent">
                        <a name="footermenu" id="footermenu"></a>
                        <a href="/web/20211215032655/https://www.dshs.state.tx.us/contact.shtm">Contact Us</a> | <a href="/web/20211215032655/https://www.dshs.state.tx.us/visitor/default.shtm">Visitor Information</a> | <a href="/web/20211215032655/https://www.dshs.state.tx.us/policy/compact.shtm">Compact with Texans</a> | <a href="/web/20211215032655/https://www.dshs.state.tx.us/viewing.shtm">File Viewing Information</a> | <a href="/web/20211215032655/https://www.dshs.state.tx.us/policy.shtm">Site Policies</a> | <a href="https://web.archive.org/web/20211215032655/https://hhs.texas.gov/">Texas HHS</a> | <a href="/web/20211215032655/https://www.dshs.state.tx.us/Search-Jobs.aspx">Jobs at DSHS</a><p><a href="https://web.archive.org/web/20211215032655/http://governor.state.tx.us/homeland">Texas Homeland Security</a> | <a href="https://web.archive.org/web/20211215032655/https://www.tsl.texas.gov/trail/index.html">Statewide Search</a> | <a href="https://web.archive.org/web/20211215032655/http://www.texas.gov/">Texas.gov</a> | <a href="https://web.archive.org/web/20211215032655/https://veterans.portal.texas.gov/">Texas Veterans Portal</a> | <a href="/web/20211215032655/https://www.dshs.state.tx.us/privacypractices.aspx">Privacy Practices</a> | <a href="https://web.archive.org/web/20211215032655/https://oig.hhsc.texas.gov/report-fraud">Report Fraud, Waste, and Abuse</a></p>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="/web/20211215032655js_/https://www.dshs.state.tx.us/js/TrackDownloads.js"></script>

        
        <a id="ctl00_jshack" alt="jshack" href="javascript:__doPostBack('ctl00$jshack','')" style="display: none;">link added for javascript postback</a>
    </form>
</body>
</html>
<!--
     FILE ARCHIVED ON 03:26:55 Dec 15, 2021 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 06:09:27 Apr 25, 2022.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
-->
<!--
playback timings (ms):
  captures_list: 73.735
  exclusion.robots: 0.079
  exclusion.robots.policy: 0.072
  RedisCDXSource: 0.566
  esindex: 0.007
  LoadShardBlock: 49.32 (3)
  PetaboxLoader3.datanode: 134.456 (4)
  CDXLines.iter: 17.859 (3)
  load_resource: 252.793
  PetaboxLoader3.resolve: 146.294
-->