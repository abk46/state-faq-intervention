
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head><script src="//archive.org/includes/analytics.js?v=cf34f82" type="text/javascript"></script>
<script type="text/javascript">window.addEventListener('DOMContentLoaded',function(){var v=archive_analytics.values;v.service='wb';v.server_name='wwwb-app215.us.archive.org';v.server_ms=303;archive_analytics.send_pageview({});});</script>
<script type="text/javascript" src="/_static/js/bundle-playback.js?v=36gO9Ebf" charset="utf-8"></script>
<script type="text/javascript" src="/_static/js/wombat.js?v=UHAOicsW" charset="utf-8"></script>
<script type="text/javascript">
  __wm.init("https://web.archive.org/web");
  __wm.wombat("https://www.dshs.state.tx.us/coronavirus/immunize/vaccine-faqs.aspx","20210417152729","https://web.archive.org/","web","/_static/",
	      "1618673249");
</script>
<link rel="stylesheet" type="text/css" href="/_static/css/banner-styles.css?v=fantwOh2" />
<link rel="stylesheet" type="text/css" href="/_static/css/iconochive.css?v=qtvMKcIJ" />
<!-- End Wayback Rewrite JS Include -->
<script type="text/javascript" src="/web/20210417152729js_/https://www.dshs.state.tx.us/ruxitagentjs_ICA23SVfgjqrtux_10213210407103252.js" data-dtconfig="rid=RID_-1764846130|rpid=404934039|domain=state.tx.us|reportUrl=/rb_bf35299gib|app=fb61825848b339be|rcdec=1209600000|featureHash=ICA23SVfgjqrtux|rdnt=1|uxrgce=1|srcss=1|bp=2|srmcrv=10|cuc=2tgs02ew|mel=100000|dpvc=1|ssv=4|lastModification=1618321661334|dtVersion=10213210407103252|srmcrl=1|tp=500,50,0,1|uxdcw=1500|vs=2|tal=3|agentUri=/ruxitagentjs_ICA23SVfgjqrtux_10213210407103252.js"></script><link rel="stylesheet" type="text/css" href="/web/20210417152729cs_/https://www.dshs.state.tx.us/WorkArea/FrameworkUI/css/ektron.stylesheet.ashx?id=-1759591071+-300771134+1985268503"/><script type="text/javascript" src="/web/20210417152729js_/https://www.dshs.state.tx.us/WorkArea/FrameworkUI/js/ektron.javascript.ashx?id=-569449246+-1939951303+-1080527330+-1687560804+-1388997516+2009761168+27274999+1979897163+-422906301+-1818005853+-1008700845+-991739241+-1793043690"></script><title>COVID-19 Vaccine Frequently Asked Questions (FAQs)</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/><meta name="Author" content="Texas Department of State Health Services"/><meta property="og:site_name" content="Texas Department of State Health Services"/><meta name="twitter:card" content="summary"/><meta name="twitter:site" content="@TexasDSHS"/><meta property="og:type" content="article"/>
    <!--These style sheets apply to IE7 and IE8 Compatibility mode -->
    <!--[if lt IE 8]><link rel="stylesheet" href="/css/blueprint/ie.css" type="text/css" media="screen, projection" /><![endif]-->
    <link href="/web/20210417152729cs_/https://www.dshs.state.tx.us/css/blueprint/print.css" rel="stylesheet" type="text/css" media="print"/><link href="/web/20210417152729cs_/https://www.dshs.state.tx.us/css/dshsPrint.css" rel="stylesheet" type="text/css" media="print"/>
    <!-- import several css files in one to avoid IE limit on @ of css files -->
    <link rel="stylesheet" href="/web/20210417152729cs_/https://www.dshs.state.tx.us/css/DSHS.css" type="text/css"/>
    <!--[if lt IE 8]><link rel="stylesheet" href="/css/DSHSie.css" type="text/css" media="screen, projection" /><![endif]-->
    <!--[if lt IE 7.0000]>
      <link rel="stylesheet" href="/css/DSHSIE6.css" type="text/css" />
    <![endif]-->
    <link href="/web/20210417152729cs_/https://www.dshs.state.tx.us/css/editorStyles.css" rel="stylesheet" type="text/css"/><link href="/web/20210417152729cs_/https://www.dshs.state.tx.us/css/internet-styles.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="/web/20210417152729js_/https://www.dshs.state.tx.us/js/master-min.js"></script>

    <!-- superfish jQuery extension -->
    <script type="text/javascript">
        // initialize plugins
        $ektron().ready(function () {
            $ektron('ul#mainNav').superfish();

            $ektron(".sort_color_off").click(function () {
                $ektron("#srchloading").show();
            });
        });
    </script>

    

    <link href="/web/20210417152729cs_/https://www.dshs.state.tx.us/css/glossy.css" type="text/css" rel="stylesheet"/>
    <script src="/web/20210417152729js_/https://www.dshs.state.tx.us/js/ddaccordion.js" type="text/javascript">
      /***********************************************
      * Accordion Content script- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
      * Visit http://www.dynamicDrive.com for hundreds of DHTML scripts
      * This notice must stay intact for legal use
      ***********************************************/
    </script>
    <script src="/web/20210417152729js_/https://www.dshs.state.tx.us/js/locNav.js" type="text/javascript"></script>
    <script type="text/javascript" src="/web/20210417152729js_/https://www.dshs.state.tx.us/WorkArea/java/thickbox.js" id="EktronThickBoxJS"></script>
   <link rel="stylesheet" type="text/css" href="/web/20210417152729cs_/https://www.dshs.state.tx.us/WorkArea/csslib/box.css" id="EktronThickBoxCss"/>

<link rel="canonical" href="https://web.archive.org/web/20210417152729/https://www.dshs.state.tx.us/coronavirus/immunize/vaccine-faqs.aspx"/></head>
<body id="bodymain">
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
                m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://web.archive.org/web/20210417152729/https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-76662241-1', 'auto');
        ga('send', 'pageview');

    </script>
    <form method="post" action="/web/20210417152729/https://www.dshs.state.tx.us/coronavirus/immunize/vaccine-faqs.aspx" id="aspnetForm">
<div class="aspNetHidden">
<input type="hidden" name="EktronClientManager" id="EktronClientManager" value="-1759591071,-569449246,-1939951303,-1080527330,-1687560804,-1388997516,2009761168,27274999,1979897163,-422906301,-1818005853,-1008700845,-991739241,-1793043690,-300771134,1985268503"/>
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTE2MjIzNTA4NjQPZBYCZg9kFgICARBkZBYEAgcPZBYCZg9kFgICAQ9kFgJmDxYCHgdWaXNpYmxlZ2QCDw9kFgYCAw9kFgICAw9kFgJmD2QWBGYPZBYCAgMPZBYCZg9kFgICAQ8WAh4FY2xhc3MFEGRyb3B6b25lIFBCQ2xlYXJkAgEPZBYCAgEPZBYCZg9kFgICAQ9kFgICAQ8WAh4LXyFJdGVtQ291bnQCARYCZg9kFgZmDxUCEyBzdHlsZT0nd2lkdGg6MTAwJScAZAIBDxYCHwBoZAIDDxYCHwJmZAIFDxYCHwEFDHNwYW4tMjAgbGFzdBYIAgEPZBYCZg9kFgICBQ8WAh4JaW5uZXJodG1sBTJDT1ZJRC0xOSBWYWNjaW5lIEZyZXF1ZW50bHkgQXNrZWQgUXVlc3Rpb25zIChGQVFzKWQCBQ9kFgJmD2QWBAIBDxYCHwBoZAIDDxYCHwBoZAIHDw8WAh8AZ2RkAgkPZBYCZg9kFgRmD2QWAgIDD2QWAmYPZBYCAgEPFgIfAQUQZHJvcHpvbmUgUEJDbGVhcmQCAQ9kFgICAQ9kFgJmD2QWAgIBD2QWAgIBDxYCHwICARYCZg9kFgZmDxUCEyBzdHlsZT0nd2lkdGg6MTAwJScAZAIBDxYCHwBoZAIDDxYCHwICAxYCAgIPZBYCAgEPZBYCZg9kFgICAQ9kFgICAQ9kFgJmD2QWAgIBD2QWAmYPZBYCAgEPZBYCAgEPZBYCAgEPEGRkFgBkAgkPZBYCZg9kFgRmD2QWAgIDD2QWAmYPZBYCAgEPFgIfAQUQZHJvcHpvbmUgUEJDbGVhcmQCAQ9kFgICAQ9kFgJmD2QWAgIBD2QWAgIBDxYCHwICARYCZg9kFgZmDxUCEyBzdHlsZT0nd2lkdGg6MTAwJScAZAIBDxYCHwBoZAIDDxYCHwJmZBgMBS5jdGwwMCRDb250ZW50UGxhY2VIb2xkZXIxJFBhZ2VIb3N0MSR1eFVYU3dpdGNoDw9kAgFkBRNjdGwwMCRVeEhlYWRlckxpbmtzDw9kZmQFZ2N0bDAwJENvbnRlbnRQbGFjZUhvbGRlcjEkRHJvcFpvbmUyJHV4Q29sdW1uRGlzcGxheSRjdGwwMCR1eENvbnRyb2xDb2x1bW4kY3RsMDIkdXhXaWRnZXRIb3N0JHV4VVhTd2l0Y2gPD2QCAWQFZ2N0bDAwJENvbnRlbnRQbGFjZUhvbGRlcjEkRHJvcFpvbmUyJHV4Q29sdW1uRGlzcGxheSRjdGwwMCR1eENvbnRyb2xDb2x1bW4kY3RsMDAkdXhXaWRnZXRIb3N0JHV4VVhTd2l0Y2gPD2QCAWQFeGN0bDAwJENvbnRlbnRQbGFjZUhvbGRlcjEkRHJvcFpvbmUyJHV4Q29sdW1uRGlzcGxheSRjdGwwMCR1eENvbnRyb2xDb2x1bW4kY3RsMDIkdXhXaWRnZXRIb3N0JHV4V2lkZ2V0SG9zdF93aWRnZXQkVmlld1NldA8PZGZkBXhjdGwwMCRDb250ZW50UGxhY2VIb2xkZXIxJERyb3Bab25lMiR1eENvbHVtbkRpc3BsYXkkY3RsMDAkdXhDb250cm9sQ29sdW1uJGN0bDAxJHV4V2lkZ2V0SG9zdCR1eFdpZGdldEhvc3Rfd2lkZ2V0JFZpZXdTZXQPD2RmZAVnY3RsMDAkQ29udGVudFBsYWNlSG9sZGVyMSREcm9wWm9uZTIkdXhDb2x1bW5EaXNwbGF5JGN0bDAwJHV4Q29udHJvbENvbHVtbiRjdGwwMSR1eFdpZGdldEhvc3QkdXhVWFN3aXRjaA8PZAIBZAUuY3RsMDAkQ29udGVudFBsYWNlSG9sZGVyMSREcm9wWm9uZTMkdXhVWFN3aXRjaA8PZAIBZAUuY3RsMDAkQ29udGVudFBsYWNlSG9sZGVyMSREcm9wWm9uZTIkdXhVWFN3aXRjaA8PZAIBZAV4Y3RsMDAkQ29udGVudFBsYWNlSG9sZGVyMSREcm9wWm9uZTIkdXhDb2x1bW5EaXNwbGF5JGN0bDAwJHV4Q29udHJvbENvbHVtbiRjdGwwMCR1eFdpZGdldEhvc3QkdXhXaWRnZXRIb3N0X3dpZGdldCRWaWV3U2V0Dw9kZmQFLmN0bDAwJENvbnRlbnRQbGFjZUhvbGRlcjEkRHJvcFpvbmUxJHV4VVhTd2l0Y2gPD2QCAWQFEGN0bDAwJFV4Tm9TY3JpcHQPD2RmZNzERQhFKV2KV1e1YYmKoINzITYPdETLlsDU0WJqQamM"/>
</div>


<script src="/web/20210417152729js_/https://www.dshs.state.tx.us/ScriptResource.axd?d=1tK-jKVHmtLw7td9etlJn6gKL9POT1Y-BFev7F_wwd_GCP-56c3MAqY6px18FmgMTv68r68FGzktqoBJca8J9WKBNkxj9J_s8jem3hSqCaKB2oO1sS8JeBugCbaOwpeT63CNuQHtQon_M1ipEnBu9epkhhiOetkX812CZs9knsY1&amp;t=3d6efc1f" type="text/javascript"></script>
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="87D830E2"/>
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAAWomVnMWcICo8J6wy9CxOx11WUX9yiin76Y35vEtoINXGZe8aAeuzuY1Cab543wfs0VPrrZgUp04R/CwoNGhOgDcieQ/WfpLZhvHgCegzlLdBIdQQoKcpxLcuwVD/jXxncXSGaqTu0NmUU7DxFyvdyi"/>
</div>
        
                <noscript>
                    Note: Javascript is disabled or is not supported by your browser. All 
            content is viewable but it will not display as intended.
           
                </noscript>
            



        <div id="header">
            <div id="headerTop">
            </div>
            <div class="container">
                <div class="span-10">
                    <div id="logo">
                        <a href="/web/20210417152729/https://www.dshs.state.tx.us/" accesskey="1">
                            <img src="/web/20210417152729im_/https://www.dshs.state.tx.us/images/HHSDSHS-Logo.png" alt="Texas Department of State Health Services"/>
                        </a>

                    </div>
                </div>
                <a class="skiplink" href="#globalmenu" accesskey="5">Skip to global menu 5</a>
                <a class="skiplink" href="#startpage" accesskey="2">Skip to local menu 2</a>
                <a class="skiplink" href="#startcontent" accesskey="3">Skip to content 3</a>
                <a class="skiplink" href="#footermenu" accesskey="6">Skip to footer 6</a>
                <div class="span-12">
                    <div id="search">
                        <div id="ctl00_srchBox">
	
                            <div class="searchBoxes">
                                <input name="ctl00$txtSearchText" type="text" id="ctl00_txtSearchText" accesskey="4" class="searchBox" aria-label="Search"/>
                                <input type="button" name="ctl00$UxSearch" value="" onclick="javascript:__doPostBack('ctl00$UxSearch','')" id="ctl00_UxSearch" class="searchButton" aria-label="Run Search"/>
                                
                            </div>
                        
</div>
                        <div class="span-11">
                            
<ul class="alphabet">
    <li>Topics:</li>
    <li><a href="/web/20210417152729/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=A">A</a></li>
    <li><a href="/web/20210417152729/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=B">B</a> </li>
    <li><a href="/web/20210417152729/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=C">C</a> </li>
    <li><a href="/web/20210417152729/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=D">D</a> </li>
    <li><a href="/web/20210417152729/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=E">E</a> </li>
    <li><a href="/web/20210417152729/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=F">F</a> </li>
    <li><a href="/web/20210417152729/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=G">G</a> </li>
    <li><a href="/web/20210417152729/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=H">H</a> </li>
    <li><a href="/web/20210417152729/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=I">I</a> </li>
    <li><a href="/web/20210417152729/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=J">J</a> </li>
    <li><a href="/web/20210417152729/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=K">K</a> </li>
    <li><a href="/web/20210417152729/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=L">L</a> </li>
    <li><a href="/web/20210417152729/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=M">M</a> </li>
    <li><a href="/web/20210417152729/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=N">N</a> </li>
    <li><a href="/web/20210417152729/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=O">O</a> </li>
    <li><a href="/web/20210417152729/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=P">P</a> </li>
    <li><a href="/web/20210417152729/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=Q">Q</a> </li>
    <li><a href="/web/20210417152729/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=R">R</a> </li>
    <li><a href="/web/20210417152729/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=S">S</a> </li>
    <li><a href="/web/20210417152729/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=T">T</a> </li>
    <li><a href="/web/20210417152729/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=U">U</a> </li>
    <li><a href="/web/20210417152729/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=V">V</a> </li>
    <li><a href="/web/20210417152729/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=W">W</a> </li>
    <li><a href="/web/20210417152729/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=X">X</a> </li>
    <li><a href="/web/20210417152729/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=Y">Y</a> </li>
    <li><a href="/web/20210417152729/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=Z">Z</a> </li>
    <li><a href="/web/20210417152729/https://www.dshs.state.tx.us/HealthTopics.aspx?alp=all">All</a> </li>
</ul>

                        </div>
                    </div>
                </div>
                <div class="span-3 last">
                    <div class="headerLinks pull-1">
                        
                                
                                <span id="ctl00_MobileLogic1_UxMobileLink"><a href="/web/20210417152729/https://www.dshs.state.tx.us/Mobile/Mobile.aspx">Mobile</a> | </span>
                                <a id="ctl00_UxSpanish" href="javascript:__doPostBack('ctl00$UxSpanish','')">Inicio en español</a>
                                <br/>
                                Text Size:
                       
                            
                        
<span id="resize">
<a id="increase" class="textLg" href="#" title="Font Larger">Font Larger</a>
<a id="decrease" class="textSm" href="#" title="Font Smaller">Font Smaller</a>
</span>
                        <br/>
                        <span id="ctl00_uxLoginMenu_lblWelcomeMessage"></span>
<span id="ctl00_uxLoginMenu_logoutBar"></span>

<span id="ctl00_uxLoginMenu_accountlinkbar"></span>


                    </div>
                </div>
                <div class="span-25 last">
                    <a name="globalmenu" id="globalmenu"></a>
                    <div id="globalNav"><ul id="mainNav"><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/" target="_self">Home</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/coronavirus/">COVID-19</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/about-DSHS.shtm">About DSHS</a><ul><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/orgchart/contact_list.shtm">Administrative Contacts</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/Advisory-Committees.aspx">Advisory Committees</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/contracts/default.shtm">Contracts and Budgets</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/policy/compact.shtm#customerservice">Customer Service</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/datareports.shtm">Data and Reports</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/contractor/">Doing Business with DSHS</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/exec-team.aspx">Executive Team</a></li><li><a href="https://web.archive.org/web/20210417152729/https://hhs.texas.gov/laws-regulations/policies-rules/health-human-services-rulemaking" target="_self">HHS Rulemaking</a></li><li><a href="https://web.archive.org/web/20210417152729/https://hhs.texas.gov/about-hhs/leadership/councils/health-human-services-commission-executive-council" target="_self">HHSC Executive Council</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/legislative/default.shtm">Legislative Information</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/closures/covid19.aspx">Office Closures</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/orgchart/default.shtm">Organization Chart</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/about-DSHS/campaigns/">Public Health Campaigns</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/visitor/default.shtm">Visitor Information</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/volunteer/default.shtm">Volunteer with DSHS</a></li><li class="menuCornerTabbed"><div class="menuCorners"></div></li></ul></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/news/">News</a><ul><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/WorkArea/linkit.aspx?LinkIdentifier=ID&amp;ItemID=34370">News Releases</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/news/alerts.aspx">Health Alerts &amp; Advisories</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/news/updates.shtm">News Updates</a></li><li class="menuCornerTabbed"><div class="menuCorners"></div></li></ul></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/I-am-a.shtm">I am a...</a><ul><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/texas-resident/">Citizen</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/contractor/">Contractor</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/grant-writer/">Grant Writer</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/health-professional/">Health Professional</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/texas-resident/">Individual or Family</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/news/">Journalist</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/Licensee-Registrant-Permittee">Licensee or Business</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/texas-resident/">Parent/Guardian</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/public-servant/">Public Servant</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/chs/">Researcher</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/texas-resident/#student">Student</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/texas-resident/">Texas Resident</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/volunteer/default.shtm">Volunteer</a></li><li class="menuCornerTabbed"><div class="menuCorners"></div></li></ul></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/Most-Popular/">Most Popular</a><ul><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/coronavirus/">Coronavirus Disease 2019</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/vs/reqproc/certified_copy.shtm">Birth Certificates</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/vs/reqproc/deathcert.shtm">Death Certificates</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/emstraumasystems/default.shtm">EMS Certification and Licensing</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/flu/surveillance.aspx">Flu Surveillance</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/Search-Jobs.aspx">Jobs at DSHS</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/immunize/">Immunizations</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/Vital_Statistics/Verification_of_a_Marriage_or_Divorce.aspx">Marriage/Divorce Verification</a></li><li><a href="https://web.archive.org/web/20210417152729/https://vo.ras.dshs.state.tx.us/" target="_self">Online Licenses</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/txever">TxEVER</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/Im-looking-for.aspx">All Most Popular...</a></li><li class="menuCornerTabbed"><div class="menuCorners"></div></li></ul></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/Resources.htm">Resources</a><ul><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/Advisory-Committees.aspx">Advisory Committees</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/avlib/default.shtm">Audiovisual Library</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/idcu/investigation/conditions/">Disease Reporting</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/library/DSHSauthors.shtm">DSHS Research Articles</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/eGrants/">eGrants System</a></li><li><a href="https://web.archive.org/web/20210417152729/http://hhsc.pinnaclecart.com/dshs/" target="_self">Forms and Publications</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/fic/default.shtm">Funding Information Center</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/library/">Medical and Research Library</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/open/default.shtm">Open Meetings</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/policy/openrecords.shtm">Open Records Requests</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/regions/default.shtm">Public Health Regions</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/links/default.shtm">Related Websites</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/about/rules-regs.aspx">Rules and Regulations</a></li><li><a href="https://web.archive.org/web/20210417152729/http://www.texashealthlibrary.com/" target="_self">Texas Health Library</a></li></ul></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/Online-Services.aspx">Online Services</a><ul><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/vs/reqproc/Ordering-Birth-Certificates-Online.shtm">Birth Certificates</a></li><li><a href="https://web.archive.org/web/20210417152729/https://vo.ras.dshs.state.tx.us/">Business/Professional Licenses</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/vs/reqproc/Ordering-Death-Certificates-Online/">Death Certificates</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/eGrants/">eGrants System</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/emstraumasystems/newcert.shtm">EMS Certification &amp; Licenses</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/food-handlers/training/online.aspx">Food Handler Training</a></li><li><a href="https://web.archive.org/web/20210417152729/http://hhsc.pinnaclecart.com/dshs/">Forms and Publications</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/fic/default.shtm">Funding Opportunities</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/Search-Jobs.aspx">Job Opportunities</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/vs/reqproc/Ordering-Marriage-and-Divorce-Verifications-Online.shtm">Marriage/Divorce Verifications</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/immunize/immtrac/default.shtm">Texas Immunization Registry</a></li><li class="menuCornerTabbed"><div class="menuCorners"></div></li></ul></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/contact.shtm">Contact Us</a></li></ul></div>
                </div>
            </div>
        </div>
        <div id="body">
            <div class="container">
                <a name="startpage" id="startpage"></a>
                
    <a class="skiplink" href="#startcontent" accesskey="3">Skip to content 3</a>
    

    <script type="text/javascript">
        Ektron.PBSettings = { 'dontClose': false }
    </script>




        <div class="ektron-ux-UITheme ux-app-siteApppageBuilder-setSizeTemplate">
            
        </div>
        <script id="EktronScriptBlockystna" type="text/javascript">

Ektron.ready(function(event, eventName){

                $ektron(".ux-app-siteApppageBuilder-setSizeTemplate .ui-dialog-buttonpane a").button();
                $ektron(".ux-app-siteApppageBuilder-setSizeTemplate .framework").accordion({
                    heightStyle: "content",
                    activate: function(event, ui){
                        $ektron(ui).closest(".ui-accordion").accordion("refresh");
                    }
                });
                $ektron(".ux-app-siteApppageBuilder-setSizeTemplate").draggable({ handle: "div.ui-dialog-titlebar ", containment: $ektron("body") });
            
});


</script>
		
        
    

    <div id="ctl00_ContentPlaceHolder1_leftFlex" class="span-5" style="float:left !Important">
        <div class="leftMargin">
            <div class="glossymenu"><a class="menuitem" href="/web/20210417152729/https://www.dshs.state.tx.us/coronavirus/immunize/vaccine.aspx" target="_self">COVID-19 Vaccine Home</a><a class="menuitem submenuheader" href="#" target="_self">Find Vaccine</a><div class="submenu"><ul><li><a href="https://web.archive.org/web/20210417152729/https://tdem.maps.arcgis.com/apps/webappviewer/index.html?id=3700a84845c5470cb0dc3ddace5c376b" target="_self">Vaccine Availability Map</a></li><li><a href="https://web.archive.org/web/20210417152729/http://www.vaccinefinder.org/" target="_self">National Vaccine Finder</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/coronavirus/immunize/vaccine-hubs.aspx" target="_self">Hub Providers</a></li><li><a href="https://web.archive.org/web/20210417152729/https://getthevaccine.dshs.texas.gov/" target="_self">Texas Public Health Vaccine Scheduler</a></li><li><a href="https://web.archive.org/web/20210417152729/https://genesis.soc.texas.gov/files/accessibility/vaccineprovideraccessibilitydata.csv" target="_self">Accessible List of Vaccine Providers</a></li></ul></div><a class="menuitem" href="/web/20210417152729/https://www.dshs.state.tx.us/coronavirus/immunize/vaccine-faqs.aspx" target="_self">COVID-19 Vaccine FAQs</a><a class="menuitem submenuheader" href="#" target="_self">Information for Vaccination Providers</a><div class="submenu"><ul><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/coronavirus/immunize/vaccination-providers.aspx" target="_self">Information for Providers</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/coronavirus/immunize/provider-enrollment.aspx" target="_self">Provider Enrollment</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/coronavirus/immunize/vaccine-manage-resources.aspx" target="_self">Vaccine Management</a></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/coronavirus/immunize/vaccine/providerfaq.aspx" target="_self">Provider FAQs</a></li></ul></div><a class="menuitem" href="/web/20210417152729/https://www.dshs.state.tx.us/coronavirus/immunize/vaccineallocations.aspx" target="_self">COVID-19 Vaccine Allocations</a><a class="menuitem" href="/web/20210417152729/https://www.dshs.state.tx.us/coronavirus/immunize/evap.aspx" target="_self">Expert Vaccine Allocation Panel</a><a class="menuitem" href="/web/20210417152729/https://www.dshs.state.tx.us/immunize/covid19/Public-Health-Entity-Forums/" target="_self">Public Health Entity Forums</a><a class="menuitem" href="/web/20210417152729/https://www.dshs.state.tx.us/immunize/safety/" target="_self">Vaccine Safety</a><a class="menuitem" href="/web/20210417152729/https://www.dshs.state.tx.us/coronavirus/tools/vaccine-comm.aspx" target="_self">COVID-19 Vaccine Communication Tools</a></div>
      
        
        <div id="ctl00_ContentPlaceHolder1_DropZone1_uxUpdatePanel" class="">
	
                <div id="ctl00_ContentPlaceHolder1_DropZone1_uxDropZone" data-ux-pagebuilder="DropZone">
                    
                            <div data-ux-pagebuilder="Column" style="width:100%">
                                
                                <ul>
                                    
                                </ul>
                            </div>
                        
                </div>
            
</div>
    
        </div>
          </div>
     <div id="ctl00_ContentPlaceHolder1_ContentPageColumnCenter" class="span-20 last">
        <ul id="ctl00_ContentPlaceHolder1_UxBreadCrumb_uxBreadcrumb" class="breadcrumb"><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/">Home</a><span> &gt; </span></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/coronavirus/default.aspx">COVID-19</a><span> &gt; </span></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/immunize/">Immunizations</a><span> &gt; </span></li><li><a href="/web/20210417152729/https://www.dshs.state.tx.us/coronavirus/immunize/vaccine.aspx">COVID-19 Vaccine</a><span> &gt; </span></li>
    
   <li id="ctl00_ContentPlaceHolder1_UxBreadCrumb_uxLastli" class="selected">COVID-19 Vaccine Frequently Asked Questions (FAQs)</li>
</ul>

    
        <div id="ctl00_ContentPlaceHolder1_SectionTitleUpdater">
	
                   
        <a name="startcontent" id="startcontent"></a>
                
        
</div>
        <div id="ctl00_ContentPlaceHolder1_pnlContent">
	
        <!--PAGEWATCH-->
        <h1 class="contentTitle">
            COVID-19 Vaccine Frequently Asked Questions (FAQs)
        </h1>
        <div class="content editorStyles">
            <div id="ctl00_ContentPlaceHolder1_uxContent">
		<style type="text/css">
ul.onThisPage {
    margin: 0;
    padding: 0;
    list-style-type: none;
    display: inline-block;
    width: 100%;
}
ul.onThisPage li {
    margin: 0;
    padding: .75em;
    width: calc(50% - 2em);
    float: left;
    border-bottom: 1px dashed #dddddd;
}
ul.onThisPage li:nth-child(odd) {
    margin-right: 1em;
}
a.ctaButton {
   margin: .25em 0; 
}
</style>
<div class="commButtons" style="margin: 0 0 .5em 0;"><a href="/web/20210417152729/https://www.dshs.state.tx.us/Workarea/linkit.aspx?LinkIdentifier=id&amp;ItemID=12884945069&amp;Langtype=1034" class="langButton" lang="es" role="button">en español</a><a href="https://web.archive.org/web/20210417152729/https://www.facebook.com/TexasDSHS" class="fbButton" aria-label="DSHS on Facebook" title="DSHS on Facebook" role="button"><span>Facebook</span></a><a href="https://web.archive.org/web/20210417152729/https://www.instagram.com/TexasDSHS" class="igButton" aria-label="DSHS on Instagram" title="DSHS on Instagram" role="button"><span>Instagram</span></a><a href="https://web.archive.org/web/20210417152729/https://twitter.com/TexasDSHS" class="twButton" aria-label="DSHS on Twitter" title="DSHS on Twitter" role="button"><span><span>Twitter</span></span></a><a href="https://web.archive.org/web/20210417152729/https://www.youtube.com/TexasDSHS" class="ytButton" aria-label="DSHS on YouTube" title="DSHS on YouTube" role="button"><span>YouTube</span></a></div>
<p> <img src="/web/20210417152729im_/https://www.dshs.state.tx.us/uploadedImages/Content/Prevention_and_Preparedness/immunize/covid19/banner-interior-full.png" alt="COVID-19 header image" class="bannerTop"/> </p>
<p>On this page are frequently asked questions (FAQs) about COVID-19 vaccines in development and their distribution across Texas.</p>
<p style="background-color: #eeeeee; border-top: 1px solid #dddddd; border-bottom: 2px solid #435363; margin-bottom: 0; padding: .75em;"><strong>On this page:</strong> </p>
<ul class="onThisPage">
    <li><a href="#jj">Johnson &amp; Johnson Safety Information</a></li>
    <li><a href="#getvax">Getting Vaccinated</a> </li>
    <li><a href="#basics">Basics</a> </li>
    <li><a href="#safety">Safety</a> </li>
    <li><a href="#availability">Vaccine Availability in Texas</a> </li>
    <li><a href="#moreinfo">More Information</a> </li>
    <li><a href="#effective">Effectiveness</a> </li>
    <li><a href="#provider">Vaccine Provider FAQs</a> </li>
    <li><a href="#immunity">Immunity</a> </li>
    <li><a href="#general">General COVID-19 FAQs</a> </li>
</ul>
<hr style="height: 2px; background: #435363; color: #435363;"/>
<h2 id="jj" style="margin-bottom: .35em !important;">Johnson &amp; Johnson Safety Information</h2>
<h3>What do we know now?</h3>
<p>Six people out of the millions who have received the Johnson &amp; Johnson/Janssen COVID-19 vaccine in the United States developed a rare and severe type of blood clot between 6 and 13 days after vaccination. Right now, these adverse events appear to be extremely rare and are being further evaluated to ensure vaccine safety.</p>
<p>The Centers for Disease Control and Prevention (CDC), the Food and Drug Administration (FDA) and DSHS are recommending providers temporarily pause administering the Johnson &amp; Johnson COVID-19 vaccine as that evaluation is done.</p>
<p>There is no evidence of a similar concern with the Pfizer and Moderna COVID-19 vaccines. DSHS strongly encourages people to get the Pfizer and Moderna vaccines since vaccination is the best way to control cases of COVID-19 and end the pandemic.</p>
<h3>How many people in the US have been vaccinated with the Johnson &amp; Johnson COVID-19 vaccine?</h3>
<p>As of April 12, 2021, more than 6.8 million doses of this vaccine have been administered.</p>
<h3>How many people in Texas have been vaccinated with Johnson &amp; Johnson COVID-19 vaccine?</h3>
<p>As of April 12, 2021, approximately 560,000 doses of this vaccine have been administered in Texas.</p>
<h3>Are any of the six reported cases in Texas?</h3>
<p>None of the six initial cases involve Texans.</p>
<h3>What should I do if I have received the Johnson &amp; Johnson COVID-19 vaccine?</h3>
<p>After getting the Johnson &amp; Johnson vaccine, it is a good idea to monitor your health and watch for symptoms that may occur.</p>
<p>It is important to remember that mild side effects from COVID-19 vaccines are common, particularly in the first two to three days of vaccination. They are a sign that your immune system is responding to the vaccine. Many people have pain, redness and swelling in the arm where they got the shot. They also may experience tiredness, mild headache, muscle pain, chills, fever, and nausea. These side effects usually start within a day or two of getting the vaccine and usually go away within a few days.</p>
<p>If you experience severe headache, abdominal pain, leg pain, or shortness of breath within three weeks after vaccination, please contact your healthcare provider for information on what you should do next.</p>
<p>If you do not have a healthcare provider, you can call 2-1-1 and be referred to a provider near you. If you are experiencing a medical emergency, call 9-1-1 or go to your nearest emergency department.</p>
<h3>What is known about these six cases?</h3>
<p>The CDC and FDA are reviewing six reported U.S. cases of a rare and severe type of blood clot in individuals after receiving the Johnson &amp; Johnson vaccine. In these cases, a type of blood clot called cerebral venous sinus thrombosis (CVST) was seen in combination with low levels of blood platelets (thrombocytopenia). All six cases occurred among women between the ages of 18 and 48, and symptoms occurred 6 to 13 days after vaccination.</p>
<p>This specific type of blood clot needs different treatment than other types of blood clots. Usually, an anticoagulant drug called heparin is used to treat blood clots. In this situation, heparin may be dangerous, and so alternative treatments should be considered.</p>
<h3>Why is vaccination being paused with only six cases?</h3>
<p>Vaccine safety is incredibly important and is something public health organizations take very seriously. The CDC and the FDA recommended pausing the Johnson &amp; Johnson vaccine to give the agencies more time to evaluate the possible link between the vaccine and this type of blood clot.</p>
<p>Additionally, the pause will help ensure that the public and health care providers are aware that this rare type of adverse event is possible and that providers can recognize and treat the blood clots if they do occur.</p>
<h3>How long will this pause be in-effect?</h3>
<p>Both the CDC and the FDA are reviewing and analyzing these cases to determine their significance and make further recommendations about vaccination. Until that process is complete, DSHS recommends a pause in the use of this vaccine.</p>
<h3>What about the Moderna and Pfizer COVID-19 vaccines? Should I get one of these vaccines?</h3>
<p>Yes. As of April 13, approximately 190 million doses of the Moderna and Pfizer COVID-19 vaccines have been administered in the United States, including 13 million in Texas, and these kinds of events have not been seen with either vaccine.</p>
<p>If you have a vaccine appointment, please proceed with the appointment and get a Moderna or Pfizer COVID-19 vaccine.</p>
<p style="text-align:right;"> <a href="#top">▲ Top</a> </p>
<hr/>
<h2 id="basics" style="margin-bottom: .35em !important;">Basics</h2>
<h3>How are the COVID-19 vaccines different from other vaccines?</h3>
<p>Different types of vaccines work in different ways to offer protection. But every type of vaccine works by teaching our bodies how to make cells that trigger an immune response. That immune response, which produces antibodies, is what protects us from getting infected if the real virus enters our bodies.</p>
<p>Currently, there are three main types of COVID-19 vaccines that are authorized and recommended, or undergoing large-scale (Phase 3) clinical trials in the United States:</p>
<ul>
  <li>mRNA vaccines</li>
  <li>Protein subunit vaccines</li>
  <li>Vector vaccines</li>
</ul>
<p>The Pfizer and Moderna vaccines are both mRNA vaccines. Johnson &amp; Johnson’s (J&amp;J) Janssen vaccine is a vector vaccine. </p>
<p>As of April 13, 2021, the J&amp;J/Janssen vaccine was temporarily paused because six people experienced a rare and severe type of blood clot. For more information, see the <a href="#jj">Johnson &amp; Johnson Safety Information</a> section.</p>
<p>COVID-19 vaccines do not use the live virus and cannot give you COVID-19. The vaccine does not alter your DNA. COVID-19 vaccination will help protect you by creating an immune response without having to experience sickness.</p>
<p>Learn more about how COVID-19 vaccines work on the <a href="https://web.archive.org/web/20210417152729/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/different-vaccines/how-they-work.html">Understanding How COVID-19 Vaccines Work</a> section of the Centers for Disease Control and Prevention (CDC) website.</p>
<h3>Why should I take the COVID-19 vaccine?</h3>
<p>Getting vaccinated will help you from getting COVID-19. But no vaccine is 100% effective. If you do get COVID-19, your vaccine will fight the virus and prevent you from getting seriously ill.</p>
<p>Getting a COVID-19 vaccine once it is available to you represents one step that you can take to get the Texas economy, and our day-to-day lives, back to normal.</p>
<p style="text-align:right;"> <a href="#top">▲ Top</a> </p>
<hr/>
<h2 id="availability" style="margin-bottom: .35em !important;">Vaccine Availability in Texas</h2>
<h3>Is Texas following CDC’s recommendations for vaccine priorities? If not, why not?</h3>
<p>While Texas does consider all CDC recommendations, it’s ultimately up to our state’s medical experts to decide vaccine priorities for Texas. DSHS’s Expert Vaccine Allocation Panel (EVAP) provides additional recommendations to the Health Commissioner.</p>
<h3>Who decides how many vaccines Texas gets?</h3>
<p>CDC determines how many doses of vaccine Texas will receive each week, based on population. Once the Texas Department of State Health Services (DSHS) is notified of the number of doses expected the following week, DSHS staff presents possibilities for vaccine distribution to the Expert Vaccine Allocation Panel (EVAP). The panel makes modifications and recommendations to the Commissioner of Health, who makes the final decision on that week’s distribution.</p>
<h3>Who decides how to distribute the vaccine in Texas?</h3>
<p>In Texas, DSHS distributes the vaccine with the guidance of the EVAP, appointed by the Health Commissioner, Dr. John Hellerstedt.</p>
<h3>How did DSHS decide who to immunize first?</h3>
<p>The Commissioner of Health appointed an EVAP to make recommendations on vaccine allocation decisions. This includes identifying groups that should be vaccinated first. The goal is to provide the most protection to vulnerable populations and critical state resources. EVAP developed <a href="/web/20210417152729/https://www.dshs.state.tx.us/coronavirus/immunize/vaccine/COVIDVaccine-AllocationPrinciples.pdf">Vaccine Allocation Guiding Principles</a> (PDF) that provide the foundation for the Texas vaccine allocation process.</p>
<h3>Who can get the vaccine now?</h3>
<p>As of Monday, March 29, 2021, <strong>everyone age 16 and older is now eligible to receive a COVID-19 vaccine in Texas.</strong></p>
<p>The state’s Expert Vaccine Allocation Panel recommended opening vaccination to everyone who falls under the current Food and Drug Administration emergency use authorizations. All vaccines are authorized for people age 18 and older. The Pfizer vaccine is authorized for people 16 and older.</p>
<h3>If I’m eligible for vaccine now, how do I get one?</h3>
<p>There are multiple tools to help you find vaccine in Texas. Check these tools frequently, as more providers and pharmacies will be added to each over the coming weeks.</p>
<h4 style="font-size: 1.15em;">Vaccine Locator Tools</h4>
 <a href="https://web.archive.org/web/20210417152729/https://tdem.maps.arcgis.com/apps/webappviewer/index.html?id=3700a84845c5470cb0dc3ddace5c376b" class="ctaButton floatLeft">TX Vaccine Availability Map <span class="arrow"></span></a>  <a href="https://web.archive.org/web/20210417152729/http://www.vaccinefinder.org/" class="ctaButton floatLeft">National Vaccine Finder <span class="arrow"></span></a><br/>
<p style="font-size: 90%;"><em>View an <a href="https://web.archive.org/web/20210417152729/https://genesis.soc.texas.gov/files/accessibility/vaccineprovideraccessibilitydata.csv">accessible alternative list of Texas vaccination providers</a> (CSV)</em></p>
<h4 style="font-size: 1.15em;">Large Vaccination Hub List</h4>
<p>Beginning in January, Texas established large vaccination sites or hubs around the state. Check the <a href="/web/20210417152729/https://www.dshs.state.tx.us/coronavirus/immunize/vaccine-hubs.aspx">COVID‑19 Vaccination Hub Providers page</a> to find a hub near you and learn how to register.</p>
<h4 style="font-size: 1.15em;">Retail Pharmacies List</h4>
<p>Check your local pharmacy’s website to see if vaccine appointments are available. On March 29, the White House announced that the Federal Retail Pharmacy Program is doubling the number of pharmacies receiving the vaccine by April 19. To find out which pharmacies are participating in the program, visit <a href="https://web.archive.org/web/20210417152729/https://www.cdc.gov/vaccines/covid-19/retail-pharmacy-program/participating-pharmacies.html">CDC’s Federal Retail Pharmacy Program website</a>.</p>
<h4 style="font-size: 1.15em;">Texas Public Health Vaccine Scheduler</h4>
<p>The new Texas Vaccine Scheduler helps Texans get scheduled for a COVID-19 vaccine at clinics hosted by participating Texas public health entities.</p>
<p>Register online at <a href="https://web.archive.org/web/20210417152729/https://getthevaccine.dshs.texas.gov/">GetTheVaccine.dshs.texas.gov</a>. You will be notified by email or text when and where to get the vaccine. If there’s not an available clinic near you, you will be directed to other places to get your vaccine.</p>
<p>Call (833) 832-7067 if you don’t have internet or need help signing up. Call center support is available 7am-7pm, 7 days a week.</p>
<p>Spanish language and other translators are available to help callers.</p>
<h4 style="font-size: 1.15em;">Tips for Your Vaccine Search</h4>
<p>When searching for a vaccination site, remember:</p>
<ul>
    <li>While vaccine supply and distribution are expanding, not all local providers have vaccine each week and hubs may have waiting lists.</li>
    <li>Do not show up at a hub or provider looking for a vaccine.</li>
    <li>Instead, check the provider’s website. Call only if the website doesn’t answer your questions.</li>
</ul>
<div class="alertBox" style="width: 768px; float: left; border-left: 15px solid #ab2328; border-top: 1px solid #efefef; border-right: 1px solid #efefef; border-bottom: 1px solid #efefef; padding-top: .5em; padding-bottom: 0;">
  <p><strong>Do you know someone who is eligible for a vaccine but doesn’t have internet access? Please let them know they can call (833) 832-7067 for referral to a local vaccine provider.</strong></p>  
</div>
<h3 style="clear: left;">I am at least 75 years old and can’t leave my house. How can I get a COVID-19 vaccine?</h3>
<p>Texas launched the COVID-19 “Save Our Seniors Initiative” to identify and vaccinate Texans who are 75+ or are homebound.</p>
<p>To register, contact your <a href="https://web.archive.org/web/20210417152729/https://www.sos.state.tx.us/elections/voter/judges.shtml">Local County Judge</a> or <a href="https://web.archive.org/web/20210417152729/https://tdem.texas.gov/field-response/">Local Office for Emergency Management</a> to let them know you want to sign up.</p>
<p>The list of counties changes weekly. Counties currently participating include Angelina, Aransas, Brown, Callahan, Cameron, Camp, Delta, Ellis, Grimes, Hidalgo, Hunt, Jim Hogg, Kinney, Leon, Lubbock, Madison, Marion, Maverick, Menard, Mitchell, Morris, Robertson, San Augustine, San Jacinto, Starr, Terrell, Titus, Trinity, Val Verde, Webb, Wharton, Wichita, Willacy, Yoakum, and Zapata.</p>
<p>Click on the links below to read more about the Save Our Seniors Initiative.</p>
<p><a href="https://web.archive.org/web/20210417152729/https://gov.texas.gov/news/post/governor-abbott-announces-save-our-seniors-initiative-to-vaccinate-homebound-seniors-in-texas">Governor Abbott Announces Save Our Seniors Initiative to Vaccinate Homebound Seniors in Texas</a></p>
<p><a href="https://web.archive.org/web/20210417152729/https://gov.texas.gov/news/post/governor-abbott-announces-35-participating-counties-for-seventh-week-of-save-our-seniors-initiative">Governor Abbott, TDEM Launch Seventh Week of Save Our Seniors Initiative In 35 Counties</a></p>
<h3>Who can provide vaccines, and how does that happen?</h3>
<p>Any facility, organization or healthcare provider licensed to possess or administer vaccine or provide vaccination services is eligible to enroll as a COVID-19 vaccine provider. Each facility or location, including those that are part of a hospital system or clinic network, must register at <a href="https://web.archive.org/web/20210417152729/https://enrolltexasiz.dshs.texas.gov/emrlogin.asp">EnrollTexasIZ.dshs.texas.gov/emrlogin.asp</a> and complete the CDC COVID-19 Vaccination Program Provider Agreement.</p>
<h3>How can a long-term care facility get residents, staff and providers vaccinated now that the federal partnership sign-up has closed?</h3>
<p>A long-term care facility that has not already signed up for the federal partnership program has other options to get their residents, staff and providers vaccinated. See <a href="/web/20210417152729/https://www.dshs.state.tx.us/immunize/covid19/LTCOptions-COVID19Vaccination.pdf">Long-Term Care Options for COVID-19 Vaccination</a> (PDF) for additional information.</p>
<h3>I am hearing that the DSHS Pharmacy Branch has vaccines available. Can I get my vaccine there?</h3>
<p>No. The DSHS Pharmacy Branch is not a public pharmacy and does not vaccinate people. It receives and distributes medications to providers across the state. Please do not call or visit the DSHS Pharmacy Branch, as they do not vaccinate anyone at this location.</p>
<h3>What should I do to protect myself and others before I'm fully vaccinated? </h3>
<p>Practice the same safety habits you’ve been doing to prevent the spread of COVID-19. Take the following precautions to limit exposure for yourself and others:</p>
<ul>
  <li>Wear a mask or cloth face covering in public and when around people who don’t live in your household, especially when social distancing is not possible.</li>
  <li>Practice social distancing and avoid close contact with others:
    <ul>
      <li><strong>Outside your home:</strong> Stay at least 6 feet away from others and avoid crowded places.</li>
      <li><strong>Inside your home:</strong> Avoid close contact with household members who are sick. Avoid sharing personal items and use a separate room and bathroom for sick household members, if possible.</li>
    </ul>
  </li>
  <li>Wash your hands often with soap and water for at least 20 seconds, especially after going to the bathroom; before eating; and after blowing your nose, coughing, or sneezing. If soap and water are not readily available, you can use an alcohol-based hand sanitizer that contains at least 60% alcohol.</li>
  <li>Clean and disinfect frequently-touched objects and surfaces using a household disinfectant on <a href="https://web.archive.org/web/20210417152729/https://www.epa.gov/pesticide-registration/list-n-disinfectants-coronavirus-covid-19">List N: Disinfectants for COVID-19.</a> </li>
  <li>Avoid touching your eyes, nose, and mouth with unwashed hands.</li>
  <li>Cover your cough or sneeze with a tissue, then throw the tissue in the trash and wash your hands.</li>
  <li>Stay home when you are sick.</li>
</ul>
<p style="text-align:right;"> <a href="#top">▲ Top</a> </p>
<hr/>
<h2 id="effective" style="margin-bottom: .35em !important;">Effectiveness</h2>
<h3>Will vaccines prevent people from getting and spreading COVID-19?</h3>
<p>Studies show that COVID-19 vaccines are effective at keeping you from getting COVID-19. Getting a COVID-19 vaccine will also keep you from getting severely ill if you do get COVID-19. Experts are still learning how well vaccines prevent spreading the virus that causes COVID-19. CDC and DSHS will keep the public informed as they learn more.</p>
<h3 id="fully">When am I considered fully vaccinated?</h3>
<p>You are considered fully vaccinated two weeks after your second dose on two-dose vaccines and 2 weeks after your single dose on one-dose vaccines.</p>
<p>For the most up-to-date information, see the <a href="https://web.archive.org/web/20210417152729/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/fully-vaccinated.html">When You’ve Been Fully Vaccinated</a> section of the CDC website.</p>
<h3>How effective will the vaccine be against COVID-19, and for how long?</h3>
<p>All vaccines currently authorized for use in the US are highly effective at protecting against severe COVID-19 that can lead to hospitalization and death. At this time, experts do not know how long protection will last or whether a booster shot will be necessary later, after the initial recommended vaccine dose(s). CDC and DSHS will keep the public informed as they learn more.</p>
<p>To learn about efficacy rates for specific vaccines, see the CDC page on <a href="https://web.archive.org/web/20210417152729/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/different-vaccines.html">Different COVID-19 Vaccines</a>.</p>
<h3>After we get fully vaccinated, will we still need to wear a mask or cloth face covering and socially distance?</h3>
<p>It depends. For now, CDC reports that fully vaccinated people can gather indoors without physical distancing or wearing masks with:</p>
<ul>
    <li>Other people who are fully vaccinated</li>
    <li>Unvaccinated people from one other household, unless any of those people or anyone they live with has an <a href="https://web.archive.org/web/20210417152729/https://www.cdc.gov/coronavirus/2019-ncov/need-extra-precautions/people-with-medical-conditions.html">increased risk for severe illness from COVID-19</a></li>
</ul>
<p>Experts are still learning about the protection that COVID-19 vaccines provide under real-life conditions. The vaccines are not expected to be 100% effective. At this time, CDC recommends that everyone continue to use all the tools to protect ourselves and others from getting and spreading the virus. Wear a mask or cloth face covering whenever you are out in public or when around unvaccinated people from multiple households. These masks or face coverings help when you can’t avoid being in the same space as others.</p>
<p>Wearing a mask or cloth face covering does not mean you don’t need to stay a safe distance from others. Social distancing, or staying at least 6 feet apart from others, is still necessary to keep you and others safe.</p>
<h3>How long will I have to wear a mask, stay six feet from others and wash my hands after I’m fully vaccinated?</h3>
<p>Experts at CDC are learning about the protection that COVID-19 vaccines provide under real-life conditions. So, once you are fully vaccinated, keep wearing your mask, washing your hands and staying six feet from other people in other settings. Those settings include when you are in public or visiting with unvaccinated people from multiple households.</p>
<p style="text-align:right;"> <a href="#top">▲ Top</a> </p>
<hr/>
<h2 id="immunity" style="margin-bottom: .35em !important;">Immunity</h2>
<h3>Will the immunity after getting COVID-19 last longer than the protection provided by the vaccine?</h3>
<p>We are still learning about how long a recovered person is protected by “natural immunity.” Early evidence suggests that immunity after having COVID-19 may not last very long.</p>
<p>We also don’t know yet how long the vaccines’ protection lasts, called “vaccine-induced immunity.” However, getting vaccinated is a much safer way to build protection than getting infected with COVID-19. CDC and DSHS will keep the public informed as more information becomes available.</p>
<h3>Will we ever achieve “herd immunity” in Texas?</h3>
<p>Experts are still learning about what percentage of Texans would need to be vaccinated to achieve herd immunity. This term describes when enough people have protection, either from a previous infection or from vaccination, that it is unlikely a virus or bacteria can spread between people in a community and cause disease. The percentage needed to reach herd immunity varies by disease. CDC and DSHS will keep the public informed as more information becomes available.</p>
<p style="text-align:right;"> <a href="#top">▲ Top</a> </p>
<hr/>
<h2 id="getvax" style="margin-bottom: .35em !important;">Getting Vaccinated</h2>
<h3>Do I need to get vaccinated if I’ve already recovered from COVID-19?</h3>
<p>Yes. You should be vaccinated regardless of whether you already had COVID-19. That’s because experts do not yet know how long you are protected from getting sick again after recovering from COVID-19. Immunity from the COVID-19 vaccine may last longer than the natural immunity you get if you’ve already had COVID-19.</p>
<p>People who currently have COVID-19 should not be vaccinated while being sick.</p>
<h3>Will the COVID-19 vaccine be one or two shots? How long after the first dose do I take the second one?</h3>
<p>The number of doses needed depends on which vaccine you receive. To get the most protection:</p>
<ul>
    <li>It’s best to get the <a href="https://web.archive.org/web/20210417152729/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/different-vaccines/Pfizer-BioNTech.html">Pfizer</a> second dose 3 to 6 weeks after the Pfizer first dose.</li>
    <li>It’s best to get the <a href="https://web.archive.org/web/20210417152729/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/different-vaccines/Moderna.html">Moderna</a> second dose 4 to 6 weeks after the Moderna first dose.</li>
    <li><a href="https://web.archive.org/web/20210417152729/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/different-vaccines/Janssen.html">J&amp;J/Janssen</a> COVID-19 vaccine requires only one dose. However, as of April 13, 2021, the J&amp;J/Janssen vaccine was temporarily paused because six people experienced a rare and severe type of blood clot. For more information, see the <a href="#jj">Johnson &amp; Johnson Safety Information</a> section.</li>
</ul>
<p>At this time, experts do not know how long protection will last or whether a booster shot will be necessary later, after the initial recommended vaccine dose(s). CDC and DSHS will keep the public informed as they learn more.</p>
<h3>Can I just take one of the two doses?</h3>
<p>If you choose to get only one dose of a two-dose vaccine, the amount of protection you may have is not known.</p>
<p>When you get the vaccine, you will receive information about what kind of vaccine you got and when you need to come back for your second dose (for two-dose vaccines). You can register and use the new V-safe After Vaccination Health Checker to receive health check-ins after you receive a COVID-19 vaccination, as well as reminders to get your second dose if you need one.</p>
<h3>Do I have to get the second dose from the same location I got the first dose? My provider doesn't know when they'll get another vaccine shipment.</h3>
<p>You do not have to get your second dose from the same location as you got the first dose. But for two-dose vaccines, please try to get both doses from the same vaccine provider, because providers should receive second doses for those who received their first dose. However, if you need to locate a second dose, be sure it’s from the same manufacturer and after the recommended dose interval. For more information, refer to the vaccination materials you received from your provider when you received your first dose. Those may include a vaccination fact sheet and/or record card.</p>
<h3>If I got the first of a two-dose vaccine but I'm unable to get the second dose within the recommended timeframe, do I have to start all over?</h3>
<p>No, you do not have to start all over. Missing the suggested interval delays full protection. But you can still get the second dose later if you have difficulty getting it within the recommended time. Just don’t get it earlier than recommended.</p>
<p>According to CDC, if you need help scheduling your vaccine appointment for your second shot, contact the location that set up your appointment for assistance. Two-dose vaccines will need two shots to get the most protection.</p>
<p>The timing between your first and second shot depends on which vaccine you received. You should get your second shot:</p>
<ul>
    <li>It’s best to get the <a href="https://web.archive.org/web/20210417152729/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/different-vaccines/Pfizer-BioNTech.html">Pfizer</a> second dose 3 to 6 weeks after the Pfizer first dose.</li>
    <li>It’s best to get the <a href="https://web.archive.org/web/20210417152729/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/different-vaccines/Moderna.html">Moderna</a> second dose 4 to 6 weeks after the Moderna first dose.</li>
    <li><a href="https://web.archive.org/web/20210417152729/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/different-vaccines/Janssen.html">J&amp;J/Janssen</a> COVID-19 vaccine requires only one dose. However, as of April 13, 2021, the J&amp;J/Janssen vaccine was temporarily paused because six people experienced a rare and severe type of blood clot. For more information, see the <a href="#jj">Johnson &amp; Johnson Safety Information</a> section.</li>
</ul>
<p>For two-dose vaccines, you should <strong>get your second shot as close to the recommended 3-week or 1-month interval as possible</strong>. However, there is no maximum interval between the first and second doses for either vaccine. You should not get the second dose earlier than the recommended interval.</p>
<h3>Which vaccine should I get for COVID-19? Do I have a choice?</h3>
<p>You always have a choice about your health care. Talk to a healthcare provider to get information specific to your situation.</p>
<h3>Does the vaccine I choose depend on my age or underlying conditions?</h3>
<p>Any currently authorized COVID-19 vaccine can be administered to people with underlying medical conditions who have no contraindications to vaccination. Your age and/or underlying conditions may also affect when you are eligible to get the vaccine. The Advisory Committee on Immunization Practices (ACIP) does not state a product preference.</p>
<p>The Pfizer vaccine is recommended for people 16+.
<br/>The Moderna vaccine is recommended for people 18+.</p>
<p>Talk to a healthcare provider to get information specific to you and the COVID-19 vaccines currently available.</p>
<h3>Can I get the COVID-19 vaccine if I have COVID-19?</h3>
<p>No. People with COVID-19 who have symptoms should wait to be vaccinated until they have recovered from their illness and have met the <a href="https://web.archive.org/web/20210417152729/https://www.cdc.gov/coronavirus/2019-ncov/if-you-are-sick/isolation.html">criteria for discontinuing isolation</a>; those without symptoms should also wait until they meet the criteria before getting vaccinated. This guidance also applies to people who get COVID-19 before getting their second dose of vaccine.</p>
<h3>What are some side effects from the vaccines for COVID-19?</h3>
<p>COVID-19 vaccines are associated with a number of side effects, but almost all of them are mild. They include pain and redness at the injection site, fatigue, headache, body aches and even fever. </p>
<p>Having symptoms like fever after you get a vaccine is normal and a sign your immune system is building protection against the virus. The side effects from COVID-19 vaccination may feel like flu, but they should go away in a few days. </p>
<p>If you get the vaccine and experience severe side effects or ones that do not go away in a couple of days, contact your healthcare provider for further instructions on how to take care of yourself.</p>
<p>You can register and use the new <a href="https://web.archive.org/web/20210417152729/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/safety/vsafe.html">V-safe After Vaccination Health Checker</a> to receive health check-ins after you receive a COVID-19 vaccination, as well as reminders to get your second dose if you need one.</p>
<p>To learn what side effects to expect and get helpful tips on how to reduce pain and discomfort after your vaccination, visit the <a href="https://web.archive.org/web/20210417152729/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/expect/after.html">Possible Side Effects After Getting a COVID-19 Vaccine</a> section of the CDC website.</p>
<h3>Does the vaccine react poorly with any medications, or do the prescriptions I'm taking preclude me from being able to get a vaccine?</h3>
<p>You will need to check with your healthcare provider about whether your medication will interfere with being vaccinated.</p>
<p style="text-align:right;"> <a href="#top">▲ Top</a> </p>
<hr/>
<h2 id="safety" style="margin-bottom: .35em !important;">Safety</h2>
<h3>How do I know whether the COVID-19 vaccine is safe?</h3>
<p>Safety is a top priority while federal partners work to make COVID-19 vaccines available. The new COVID-19 vaccines have been evaluated in tens of thousands of volunteers during clinical trials. The vaccines are only authorized for use if they are found to be safe.</p>
<p>Even though they found no major safety issues during the clinical trials, CDC and other federal partners will continue to monitor the new vaccines. They watch out for serious side effects (or “adverse events”) using vaccine safety monitoring systems, like the new V-safe After Vaccination Health Checker app.</p>
<p>As of April 13, 2021, close monitoring led CDC, FDA and DSHS to recommend providers temporarily pause administering the Johnson &amp; Johnson COVID-19 vaccine as further evaluation is done.</p>
<p>For the most up-to-date information, see the <a href="https://web.archive.org/web/20210417152729/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/safety.html">Vaccine Safety</a> section of the CDC website.</p>
<p>To learn about CDC’s new vaccine safety monitoring system, see the <a href="https://web.archive.org/web/20210417152729/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/safety/vsafe.html">V-safe After Vaccination Health Checker</a> section of the CDC website.</p>
<h3>How do I report it if I have a bad reaction to a vaccine?</h3>
<p>CDC has a new smartphone-based tool for this effort called v-safe. This tool helps CDC check in on people’s health after they receive a COVID-19 vaccine. When you get your vaccine, you should also receive a v-safe information sheet telling you how to enroll in v-safe. If you enroll, you will get regular text messages directing you to surveys. Use these surveys to report any problems or adverse reactions you have after receiving a COVID-19 vaccine.</p>
<p>Read about v-safe with the <a href="/web/20210417152729/https://www.dshs.state.tx.us/immunize/covid19/vsafe_info_sheet.pdf">V-safe Information Sheet</a> (PDF).</p>
<p>According to CDC’s website, CDC and FDA encourage the public to report possible side effects (called adverse events) to the Vaccine Adverse Event Reporting System (VAERS). This national system collects these data to look for adverse events. Those may include ones that are unexpected, ones that appear to happen more often than expected or ones that have unusual patterns of occurrence. Reports to VAERS help CDC monitor the safety of vaccines. Safety is a top priority. As of April 13, 2021, the J&amp;J/Janssen vaccine was temporarily paused because six people experienced a rare and severe type of blood clot. For more information, see the <a href="#jj">Johnson &amp; Johnson Safety Information</a> section.</p>
<p>For more information about the difference between a vaccine side effect and an adverse event, visit the <a href="https://web.archive.org/web/20210417152729/https://www.cdc.gov/vaccinesafety/ensuringsafety/sideeffects/index.html">Understanding Side Effects and Adverse Events</a> section of the CDC website.</p>
<p>For more information about the reporting system, visit the <a href="https://web.archive.org/web/20210417152729/https://vaers.hhs.gov/">VAERS</a> website or call 800-822-7967.</p>
<p>You should also let your doctor know about your reaction. According to CDC, healthcare providers will need to report some vaccine side effects to VAERS.</p>
<h3>Can the COVID-19 vaccine make me sick or give me COVID-19?</h3>
<p>No. COVID-19 vaccines cannot give you COVID-19. The vaccine does not alter your DNA. COVID-19 vaccination will help protect you by creating an immune response without having to experience sickness. Sometimes after vaccination, the process of building immunity can cause symptoms, such as fever. These symptoms are normal and are signs that the body is building immunity.</p>
<p>To learn about COVID-19 vaccines, visit the <a href="https://web.archive.org/web/20210417152729/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/different-vaccines.html">Different COVID-19 Vaccines</a> section of the CDC website.</p>
<h3>Can children get the vaccine, or will they rely on their natural immune system to protect them?</h3>
<p>At this time, experts do not know how safe the COVID-19 vaccine is for children. People 16 years old and older are currently eligible to get the vaccine in Texas. Vaccines are currently in clinical trials for children 15 and younger. </p>
<h3>Can pregnant people get the vaccine?</h3>
<p>Yes, if you are pregnant, you might choose to be vaccinated. Based on how COVID-19 vaccines work, experts think they are unlikely to pose a specific risk for people who are pregnant. However, there are currently limited data on the safety of COVID-19 vaccines in pregnant people because these vaccines have not been widely studied in pregnant people. Systems are in place to continue to monitor vaccine safety, and so far, they have not identified any specific safety concerns for pregnant people. Clinical trials to evaluate the safety and efficacy of COVID-19 vaccines in pregnant people are underway or planned.</p>
<p>Discuss your options and any concerns with your healthcare provider if you have any reservations.</p>
<p>For more information about COVID-19 vaccines and pregnancy, visit the <a href="https://web.archive.org/web/20210417152729/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/recommendations/pregnancy.html">Information for People who are Pregnant or Breastfeeding</a> section of the CDC website. </p>
<p style="text-align:right;"> <a href="#top">▲ Top</a> </p>
<hr/>
<h2 id="moreinfo" style="margin-bottom: .35em !important;">More Information</h2>
<h3>Where can I get reliable information about vaccines for COVID-19?</h3>
<p>Three excellent sources of reliable information are the Texas Department of State Health Services (DSHS), Centers for Disease Control and Prevention (CDC), and the Food and Drug Administration (FDA).</p>
<h4>DSHS</h4>
<p> <a href="/web/20210417152729/https://www.dshs.state.tx.us/coronavirus/immunize/vaccine.aspx">COVID-19 Vaccine Information</a> </p>
<h4>CDC</h4>
<p> <a href="https://web.archive.org/web/20210417152729/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/index.html">COVID-19 Vaccines</a> </p>
<p> <a href="https://web.archive.org/web/20210417152729/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/safety.html">COVID-19 Vaccine Safety</a> </p>
<p> <a href="https://web.archive.org/web/20210417152729/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/keythingstoknow.html">Key Things to Know About COVID-19 Vaccines</a> </p>
<p> <a href="https://web.archive.org/web/20210417152729/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/your-vaccination.html">Your COVID-19 Vaccination</a> </p>
<p> <a href="https://web.archive.org/web/20210417152729/https://www.cdc.gov/coronavirus/2019-ncov/vaccines/safety/vsafe.html">V-safe After Vaccination Health Checker</a> </p>
<h4>FDA</h4>
<p> <a href="https://web.archive.org/web/20210417152729/https://www.fda.gov/emergency-preparedness-and-response/coronavirus-disease-2019-covid-19/covid-19-vaccines">COVID-19 Vaccines</a> </p>
<p> <a href="https://web.archive.org/web/20210417152729/https://www.fda.gov/">FDA Homepage</a> </p>
<p style="text-align:right;"> <a href="#top">▲ Top</a> </p>
<hr/>
<h2 id="provider">Vaccine Provider FAQs</h2>
<p>See the <a href="/web/20210417152729/https://www.dshs.state.tx.us/coronavirus/immunize/vaccine/providerfaq.aspx">COVID-19 Vaccine Provider FAQs</a> for answers to common questions for vaccinators in Texas.</p>
<p style="text-align:right;"> <a href="#top">▲ Top</a> </p>
<hr/>
<h2 id="general">General COVID-19 FAQs</h2>
<p>See the <a href="/web/20210417152729/https://www.dshs.state.tx.us/coronavirus/faq.aspx">COVID-19 FAQs</a> for answers to general questions about COVID-19.</p>
<p style="text-align:right;"> <a href="#top">▲ Top</a> </p>
<hr/>
	</div>
        </div>
        
<script type="text/javascript">
    $ektron('table.zebra tr:nth-child(even)')
        .add('table.zebraBorder tr:nth-child(even)')
        .addClass('zebraEven');
 </script>
        <!--/PAGEWATCH-->
        
</div>
        
        <div id="ctl00_ContentPlaceHolder1_DropZone2_uxUpdatePanel" class="">
	
                <div id="ctl00_ContentPlaceHolder1_DropZone2_uxDropZone" data-ux-pagebuilder="DropZone">
                    
                            <div data-ux-pagebuilder="Column" style="width:100%">
                                
                                <ul>
                                    
                                            <li>
                                                
        <div id="ctl00_ContentPlaceHolder1_DropZone2_uxColumnDisplay_ctl00_uxControlColumn_ctl00_uxWidgetHost_uxUpdatePanel">
		
                <div data-ux-pagebuilder="Widget">
                    
                    <div class="widgetBody">
                        
        
        
        <span id="ctl00_ContentPlaceHolder1_DropZone2_uxColumnDisplay_ctl00_uxControlColumn_ctl00_uxWidgetHost_uxWidgetHost_widget_errorLb"></span>
    

                    </div>
                </div>
            
	</div>
    
                                            </li>
                                        
                                            <li>
                                                
        <div id="ctl00_ContentPlaceHolder1_DropZone2_uxColumnDisplay_ctl00_uxControlColumn_ctl01_uxWidgetHost_uxUpdatePanel">
		
                <div data-ux-pagebuilder="Widget">
                    
                    <div class="widgetBody">
                        
        <div id="ctl00_ContentPlaceHolder1_DropZone2_uxColumnDisplay_ctl00_uxControlColumn_ctl01_uxWidgetHost_uxWidgetHost_widget_CB">
			<script>(function () {

var mppAttachPixel = function () {

    //startexchange

	var dbmTagManager = document.createElement("script");

	dbmTagManager.async = true;

	dbmTagManager.src = "//web.archive.org/web/20210417152729/https://www.googletagmanager.com/gtm.js?id=GTM-T5387JV";

	document.head.appendChild(dbmTagManager);

	window.dataLayer = window.dataLayer || [];

	function gtag(){dataLayer.push(arguments);}

	gtag('js', new Date());

	gtag('config', 'GTM-T5387JV');

	//endexchange

}

if (document.readyState === "complete") {

	mppAttachPixel();

} else {

	document.addEventListener("readystatechange", function(e) {

		if (document.readyState === "complete") {

			mppAttachPixel();

		}

	});

}

}())
</script>
		</div>
        
        <span id="ctl00_ContentPlaceHolder1_DropZone2_uxColumnDisplay_ctl00_uxControlColumn_ctl01_uxWidgetHost_uxWidgetHost_widget_errorLb"></span>
    

                    </div>
                </div>
            
	</div>
    
                                            </li>
                                        
                                            <li>
                                                
        <div id="ctl00_ContentPlaceHolder1_DropZone2_uxColumnDisplay_ctl00_uxControlColumn_ctl02_uxWidgetHost_uxUpdatePanel">
		
                <div data-ux-pagebuilder="Widget">
                    
                    <div class="widgetBody">
                        
	<script type="text/javascript">
	$ektron().ready(function(){				
		 $ektron('#navigation').accordion({
			    active: false,
			    header: '.head',
			    navigation: true,
			    event: 'mouseover',
			    fillSpace: true,
			    animated: 'slide'
		    });
		    $ektron('.navigation1').accordion({
			    active: false,
			    header: '.head',
			    navigation: true,
			    event: 'mouseover',
			    fillSpace: true,
			    animated: 'slide'
		    });
	});	
	</script>
	
    <style type="text/css">
	#navigation ul
        {
	      height: auto !important;	      
        }
	</style>

        
    

                    </div>
                </div>
            
	</div>
    
                                            </li>
                                        
                                </ul>
                            </div>
                        
                </div>
            
</div>
    
        
        <span id="ctl00_ContentPlaceHolder1_UxLastUpdatedDate_UxLastUpdated" class="lastUpdated">Last updated </span><span class="lastUpdatedDate"> April 16, 2021</span>
        
    </div>
        <div class="span-5 last">
            <div class="rightMargin page">
                <div class="suppNav"><ul></ul></div>
                
        <div id="ctl00_ContentPlaceHolder1_DropZone3_uxUpdatePanel" class="">
	
                <div id="ctl00_ContentPlaceHolder1_DropZone3_uxDropZone" data-ux-pagebuilder="DropZone">
                    
                            <div data-ux-pagebuilder="Column" style="width:100%">
                                
                                <ul>
                                    
                                </ul>
                            </div>
                        
                </div>
            
</div>
    
            </div>
        </div>

            </div>
        </div>
        <div id="footer">
            <div id="footerShadow">
                <div class="container">
                    <div id="footerContent">
                        <a name="footermenu" id="footermenu"></a>
                        <a href="/web/20210417152729/https://www.dshs.state.tx.us/contact.shtm">Contact Us</a> | <a href="/web/20210417152729/https://www.dshs.state.tx.us/visitor/default.shtm">Visitor Information</a> | <a href="/web/20210417152729/https://www.dshs.state.tx.us/policy/compact.shtm">Compact with Texans</a> | <a href="/web/20210417152729/https://www.dshs.state.tx.us/viewing.shtm">File Viewing Information</a> | <a href="/web/20210417152729/https://www.dshs.state.tx.us/policy.shtm">Site Policies</a> | <a href="https://web.archive.org/web/20210417152729/https://hhs.texas.gov/">Texas HHS</a> | <a href="/web/20210417152729/https://www.dshs.state.tx.us/Search-Jobs.aspx">Jobs at DSHS</a><p><a href="https://web.archive.org/web/20210417152729/http://governor.state.tx.us/homeland">Texas Homeland Security</a> | <a href="https://web.archive.org/web/20210417152729/https://www.tsl.texas.gov/trail/index.html">Statewide Search</a> | <a href="https://web.archive.org/web/20210417152729/http://www.texas.gov/">Texas.gov</a> | <a href="https://web.archive.org/web/20210417152729/https://veterans.portal.texas.gov/">Texas Veterans Portal</a> | <a href="/web/20210417152729/https://www.dshs.state.tx.us/privacypractices.aspx">Privacy Practices</a> | <a href="https://web.archive.org/web/20210417152729/https://oig.hhsc.texas.gov/report-fraud">Report Fraud, Waste, and Abuse</a></p>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="/web/20210417152729js_/https://www.dshs.state.tx.us/js/TrackDownloads.js"></script>

        
        <a id="ctl00_jshack" alt="jshack" href="javascript:__doPostBack('ctl00$jshack','')" style="display: none;">link added for javascript postback</a>
    </form>
</body>
</html>
<!--
     FILE ARCHIVED ON 15:27:29 Apr 17, 2021 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 05:40:04 Apr 25, 2022.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
-->
<!--
playback timings (ms):
  captures_list: 163.452
  exclusion.robots: 0.09
  exclusion.robots.policy: 0.084
  RedisCDXSource: 3.093
  esindex: 0.007
  LoadShardBlock: 138.943 (3)
  PetaboxLoader3.datanode: 123.558 (4)
  CDXLines.iter: 18.732 (3)
  load_resource: 126.695
  PetaboxLoader3.resolve: 57.59
-->