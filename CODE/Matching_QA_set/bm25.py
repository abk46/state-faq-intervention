from rank_bm25 import BM25Okapi, BM25L, BM25Plus
import string
from nltk.stem import PorterStemmer, SnowballStemmer, LancasterStemmer, RegexpStemmer
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from pandas import *

nc_csv = read_csv("./../../ASSOCIATED_FILES/nc_questions.csv")

nc_questions = nc_csv['NC Question'].tolist()
nc_questions_taxonomy = nc_csv['Taxonomy Label'].tolist()
nc_questions_taxonomy_2 = nc_csv['Taxonomy Label 2'].tolist()
for i in range(len(nc_questions_taxonomy)):
    if nc_questions_taxonomy_2[i] > 0:
        nc_questions_taxonomy[i] = [nc_questions_taxonomy[i], nc_questions_taxonomy_2[i]]
    elif nc_questions_taxonomy[i] > 0:
        nc_questions_taxonomy[i] = [nc_questions_taxonomy[i]]
    else:
        nc_questions_taxonomy[i] = []

antivax_csv = read_csv("./../../ASSOCIATED_FILES/antivax_sentences.csv")

antivax_sentences = antivax_csv['Antivax Sentence'].tolist()
antivax_sentences_taxonomy = antivax_csv['Taxonomy Label'].tolist()
antivax_sentences_taxonomy_2 = antivax_csv['Taxonomy Label 2'].tolist()
for i in range(len(antivax_sentences_taxonomy)):
    if antivax_sentences_taxonomy_2[i] > 0:
        antivax_sentences_taxonomy[i] = [antivax_sentences_taxonomy[i], antivax_sentences_taxonomy_2[i]]
    elif antivax_sentences_taxonomy[i] > 0:
        antivax_sentences_taxonomy[i] = [antivax_sentences_taxonomy[i]]
    else:
        antivax_sentences_taxonomy[i] = []

# Calculates MAP (also PPV and TPR aka precision and recall, but those aren't printed) for given query/documents and BM25 system
# This MAP is calculated for the queries of each label, overall (label = 6) "Specific"
# Also MAP is calculated where just returning an item that is in the taxonomy is positive "Any"

def IR_metrics(document, document_taxonomy, queries, queries_taxonomy):
    total_in_thing = [0, 0, 0, 0, 0, 0, 0]
    for label in range(1, 5+1):
        for i in document_taxonomy:
            if label in i:
                total_in_thing[label-1] += 1
                total_in_thing[7-1] += 1
    for i in document_taxonomy:
        if (1 in i) or (2 in i) or (3 in i) or (4 in i) or (5 in i):
            total_in_thing[6-1] += 1
    #print(total_in_thing)
    tokenized_document = [doc.split(" ") for doc in document]
    bm25 = BM25L(tokenized_document)
    #print("Label,Rank,Precision_Spec,Recall_Spec,Precision_Any,Recall_Any")
    MAP_specific = [0, 0, 0, 0, 0, 0]
    MAP_any = [0, 0, 0, 0, 0, 0]
    TPR_specific_overall = []
    PPV_specific_overall = []
    TPR_any_overall = []
    PPV_any_overall = []
    for i in range(len(document)):
        TPR_specific_overall.append(0)
        PPV_specific_overall.append(0)
        TPR_any_overall.append(0)
        PPV_any_overall.append(0)
    for label in range(1, 5+1):
        total_label_queries = 0
        TPR_specific = []
        PPV_specific = []
        TPR_any = []
        PPV_any = []
        for i in range(len(document)):
            TPR_specific.append(0)
            PPV_specific.append(0)
            TPR_any.append(0)
            PPV_any.append(0)
        for i in range(len(queries)):
            if len(queries_taxonomy[i]) == 0:
                continue
            query = queries[i]
            tokenized_query = query.split(" ")
            doc_scores = list(bm25.get_scores(tokenized_query))
            document_order = sorted(range(len(doc_scores)), key=lambda i: doc_scores[i], reverse=True)
            taxonomy_document_order = []
            for j in document_order:
                taxonomy_document_order.append(document_taxonomy[j])

            for query_tax in queries_taxonomy[i]:
                success_specific = 0
                failure_specific = 0
                success_any = 0
                failure_any = 0
                success_counts = [0, 0]
                MAP_spec = 0
                MAP_an = 0
                if query_tax != label:
                    continue
                total_label_queries += 1
                for j in range(len(taxonomy_document_order)):
                    if query_tax in taxonomy_document_order[j]:
                        success_specific += 1
                        MAP_spec += success_specific / (success_specific + failure_specific)
                    else:
                        failure_specific += 1
                    if len(taxonomy_document_order[j]) > 0:
                        success_any += 1
                        MAP_an += success_any / (success_any + failure_any)
                    else:
                        failure_any += 1
                    TPR_specific[success_specific+failure_specific-1] += success_specific / total_in_thing[label-1]
                    TPR_any[success_any+failure_any-1] += success_any / total_in_thing[6-1]
                    PPV_specific[success_specific+failure_specific-1] += success_specific / (success_specific + failure_specific)
                    PPV_any[success_any+failure_any-1] += success_any / (success_any + failure_any)
                MAP_spec /= total_in_thing[label-1]
                MAP_an /= total_in_thing[6-1]
                MAP_specific[label-1] += MAP_spec
                MAP_any[label-1] += MAP_an
               
        MAP_specific[label-1] /= total_label_queries
        MAP_any[label-1] /= total_label_queries 
        for i in range(len(document)):
            TPR_specific[i] /= total_label_queries
            PPV_specific[i] /= total_label_queries
            TPR_any[i] /= total_label_queries
            PPV_any[i] /= total_label_queries
            TPR_specific_overall[i] += (TPR_specific[i])*(total_in_thing[label-1]/total_in_thing[7-1])
            PPV_specific_overall[i] += (PPV_specific[i])*(total_in_thing[label-1]/total_in_thing[7-1])
            TPR_any_overall[i] += (TPR_any[i])*(total_in_thing[label-1]/total_in_thing[7-1])
            PPV_any_overall[i] += (PPV_any[i])*(total_in_thing[label-1]/total_in_thing[7-1])
    for i in range(len(document)):
        MAP_specific[6-1] += PPV_specific_overall[i]
        MAP_any[6-1] += PPV_any_overall[i]
    MAP_specific[6-1] /= len(document)
    MAP_any[6-1] /= len(document)
    print("Mean Average Precision")
    print("Label,Specific,Any")
    MAP_specific[6-1] = 0
    MAP_any[6-1] = 0
    for label in range(1, 5+1):
        MAP_specific[6-1] += (total_in_thing[label-1]/total_in_thing[7-1])*MAP_specific[label-1]
        MAP_any[6-1] += (total_in_thing[label-1]/total_in_thing[7-1])*MAP_any[label-1]
    for label in range(1, 6+1):
        print(label,MAP_specific[label-1],MAP_any[label-1])

# Typical BM25 with no pre-processing. Antivax sentence labels sourced from majority vote of labelers in Spring 2022. NC question labels sourced from hand labeling.

print("ANTIVAX document")
IR_metrics(antivax_sentences, antivax_sentences_taxonomy, nc_questions, nc_questions_taxonomy)

print("NC Q document")
IR_metrics(nc_questions, nc_questions_taxonomy, antivax_sentences, antivax_sentences_taxonomy)


# Pre-processing, no effect on stop words. Getting rid of punctuation, uppercase letters, stemming words
ps = PorterStemmer()
punct = '!()-[]{};:"\, <>./?@#$%^&*_~'
preprocessing_nc_questions = []
for q in nc_questions:
    temp = []
    tokenized_q = q.split()
    for word in tokenized_q:
        for x in punct:
            word = word.replace(x,"")
        temp.append(ps.stem(word).lower())
    preprocessing_nc_questions.append(" ".join(temp))

preprocessing_antivax_sentences = []
for s in antivax_sentences:
    temp = []
    tokenized_s = s.split()
    for word in tokenized_s:
        for x in punct:
            word = word.replace(x,"")
        temp.append(ps.stem(word).lower())
    preprocessing_antivax_sentences.append(" ".join(temp))

print("ANTIVAX document")
IR_metrics(preprocessing_antivax_sentences, antivax_sentences_taxonomy, preprocessing_nc_questions, nc_questions_taxonomy)

print("NC Q document")
IR_metrics(preprocessing_nc_questions, nc_questions_taxonomy, preprocessing_antivax_sentences, antivax_sentences_taxonomy)


# Pre-processing w/ stop words excluded
stop_words = set(stopwords.words('english'))
stopword_preprocessing_nc_questions = []
for q in nc_questions:
    temp = []
    tokenized_q = q.split()
    for word in tokenized_q:
        for x in punct:
            word = word.replace(x,"")
        if word not in stop_words:
            temp.append(word.lower())
    stopword_preprocessing_nc_questions.append(" ".join(temp))

stopword_preprocessing_antivax_sentences = []
for s in antivax_sentences:
    temp = []
    tokenized_s = s.split()
    for word in tokenized_s:
        for x in punct:
            word = word.replace(x,"")
        if word not in stop_words:
            temp.append(word.lower())
    stopword_preprocessing_antivax_sentences.append(" ".join(temp))

print("ANTIVAX document")
IR_metrics(stopword_preprocessing_antivax_sentences, antivax_sentences_taxonomy, stopword_preprocessing_nc_questions, nc_questions_taxonomy)

print("NC Q document")
IR_metrics(stopword_preprocessing_nc_questions, nc_questions_taxonomy, stopword_preprocessing_antivax_sentences, antivax_sentences_taxonomy)

