import random
import torch
import numpy as np
import pandas as pd
from tqdm import tqdm
from torch.utils.data import TensorDataset, DataLoader, RandomSampler, SequentialSampler
from transformers import AutoTokenizer, AutoModelForSequenceClassification, AdamW

# Heavily adapted from Qianqian's Spring 2021 MS Project

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
# Set seeds for reproducibility
random.seed(26)
np.random.seed(26)
torch.manual_seed(26)

tokenizer = AutoTokenizer.from_pretrained("roberta-base") 

model = AutoModelForSequenceClassification.from_pretrained("roberta-base")
model.to(device) # Send the model to the GPU if we have one

learning_rate = 1e-5
optimizer = AdamW(model.parameters(), lr=learning_rate, eps=1e-8)

def encode_data(tokenizer, questions, passages, max_length):
    """Encode the question/passage pairs into features than can be fed to the model."""
    input_ids = []
    attention_masks = []

    for question, passage in zip(questions, passages):
        encoded_data = tokenizer.encode_plus(question, passage, max_length=max_length, pad_to_max_length=True, truncation_strategy="longest_first")
        encoded_pair = encoded_data["input_ids"]
        attention_mask = encoded_data["attention_mask"]

        input_ids.append(encoded_pair)
        attention_masks.append(attention_mask)

    return np.array(input_ids), np.array(attention_masks)
# Loading data
train_data_df = pd.read_json("./../../ASSOCIATED_FILES/train.jsonl", lines=True, orient='records')
dev_data_df = pd.read_json("./../../ASSOCIATED_FILES/dev.jsonl", lines=True, orient="records")

passages_train = train_data_df.passage.values
questions_train = train_data_df.question.values
answers_train = train_data_df.answer.values.astype(int)

passages_dev = dev_data_df.passage.values
questions_dev = dev_data_df.question.values
answers_dev = dev_data_df.answer.values.astype(int)

# Encoding data
max_seq_length = 256
input_ids_train, attention_masks_train = encode_data(tokenizer, questions_train, passages_train, max_seq_length)
input_ids_dev, attention_masks_dev = encode_data(tokenizer, questions_dev, passages_dev, max_seq_length)

train_features = (input_ids_train, attention_masks_train, answers_train)
dev_features = (input_ids_dev, attention_masks_dev, answers_dev)

batch_size = 32

train_features_tensors = [torch.tensor(feature, dtype=torch.long) for feature in train_features]
dev_features_tensors = [torch.tensor(feature, dtype=torch.long) for feature in dev_features]

train_dataset = TensorDataset(*train_features_tensors)
dev_dataset = TensorDataset(*dev_features_tensors)

train_sampler = RandomSampler(train_dataset)
dev_sampler = SequentialSampler(dev_dataset)

train_dataloader = DataLoader(train_dataset, sampler=train_sampler, batch_size=batch_size)
dev_dataloader = DataLoader(dev_dataset, sampler=dev_sampler, batch_size=batch_size)

epochs = 5
grad_acc_steps = 1
train_loss_values = []
dev_acc_values = []

for _ in tqdm(range(epochs), desc="Epoch"):

  # Training
  epoch_train_loss = 0 # Cumulative loss
  model.train()
  model.zero_grad()

  for step, batch in enumerate(train_dataloader):

      input_ids = batch[0].to(device)
      attention_masks = batch[1].to(device)
      labels = batch[2].to(device)     

      outputs = model(input_ids, token_type_ids=None, attention_mask=attention_masks, labels=labels)

      loss = outputs[0]
      loss = loss / grad_acc_steps
      epoch_train_loss += loss.item()

      loss.backward()
      
      if (step+1) % grad_acc_steps == 0: # Gradient accumulation is over
        torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0) # Clipping gradients
        optimizer.step()
        model.zero_grad()

  epoch_train_loss = epoch_train_loss / len(train_dataloader)          
  train_loss_values.append(epoch_train_loss)

# EVALUATION -- output is suppressed
epoch_dev_accuracy = 0 # Cumulative accuracy
model.eval()

for batch in dev_dataloader:
  
  input_ids = batch[0].to(device)
  attention_masks = batch[1].to(device)
  labels = batch[2]

  with torch.no_grad():        
      outputs = model(input_ids, token_type_ids=None, attention_mask=attention_masks)
                  
  logits = outputs[0]
  logits = logits.detach().cpu().numpy()
  
  predictions = np.argmax(logits, axis=1).flatten()
  labels = labels.numpy().flatten()
  
  epoch_dev_accuracy += np.sum(predictions == labels) / len(labels)

epoch_dev_accuracy = epoch_dev_accuracy / len(dev_dataloader)
dev_acc_values.append(epoch_dev_accuracy)
#print(dev_acc_values)


column_names = ["Question", "Labels"]
myths_data = pd.read_csv('./../../ASSOCIATED_FILES/nc_questions.csv', names = column_names)

paragraphs = myths_data.Question.tolist()
labels = myths_data.Labels.tolist()
paragraphs = paragraphs[1:]
labels = labels[1:]

# Actual labels for the common myths dataset
myths_standards = {}
for i in range(len(paragraphs)):
  if labels[i] != labels[i]:
    myths_standards[paragraphs[i]] = []
  else:
    myths_standards[paragraphs[i]] = labels[i].split()
print(myths_standards)

def predict(question, passage, result_dict):
  
  sequence = tokenizer.encode_plus(question[0], passage, return_tensors="pt")['input_ids'].to(device)

  logits = model(sequence)[0]
  probabilities = torch.softmax(logits, dim=1).detach().cpu().tolist()[0]
  proba_yes = round(probabilities[1], 2)
  proba_no = round(probabilities[0], 2)

  result = ""

  if (proba_yes > 0.5):
    result = "Yes"
    if (passage in result_dict.keys()):
      #Store the predicted labels into a dictionary result_dict
      result_dict[passage].append(question[1])
    else:
      tmp_labels = []
      tmp_labels.append(question[1])
      result_dict[passage] = tmp_labels
  elif (proba_no> 0.5):
    result = "No"
  else:
    result = "Neutral"

  #print(f"Question: {question[1]}, Yes: {proba_yes}, No: {proba_no}, Answer: {result}")


questions = [
("Is research on vaccines insufficient?", "1.1"),         
("Is vaccine-related research poorly conducted?", "1.2"),  
("Could vaccine research be proven wrong in the future?", "1.3"), 
("Are vaccines only partly effective or not effective against deseases?", "2.1"),
("Does herd immunity offers protection against the disease, so vaccines are not needed?", "2.2"),
("Does natural infection provide better immunity aggainst the disease than vaccines?", "2.3"),
("Is the disease not common or dangerous, so that vaccines are not neccesary?", "2.4"),
("Are other medical treatments or preventions more effective against diseases than vaccines?", "2.5"),
("Can people contract the disease from the vaccine?", "3.1"),
("Do Vaccines Contain Harmful Preservatives, Adjuvants, Additives, or Residuals?", "3.2"),
("Do vaccines induce serious side effects such as pysical harm, mental illness (autism) or deadly deseases?", "3.3"),
("Are vaccination schedules dangerous?", "3.4"),
("Should people be exempted or ineligible of taking vaccines when they are ill, too young or too old, have allergies, or if they are pregnant / breastfeeding women?", "3.5"),
("Do religious beliefs or ethical concerns contradict vaccines?", "4.1"),
("Should people have their individual right to choose whether or not to take vaccines?", "4.2"),
("Do people mistrust medical personnel when they say vaccines are good and effective?", "5.1"),
("Do people believe that the development and manufaturing of vaccines substantially relies on profit motives of pharmaceutical companies?", "5.2"),
("Do people believe that media and censorship is behind the propogated benefits of vaccines?", "5.3"),
("Do people believe that vaccines are a part of a conspiracy?", "5.4")
]

reversed_questions = [
("Is there sufficient research on vaccines?", "1.1"),         
("Is the research on vaccines well done?", "1.2"),  
("Is the research on vaccines always correct?", "1.3"), 
("Do vaccines work to prevent diseases?", "2.1"),
("Are vaccines not required because herd immunity is sufficient to protect people against diseases?", "2.2"),
("Does contracting the disease induce better immunity against the disease than vaccines?", "2.3"),
("Are diseases prevented by vaccines common and dangerous?", "2.4"),
("Are vaccines neccessary when other medical treatments or preventions are effective against diseases?", "2.5"),
("Are people safe from contracting the disease from the vaccine?", "3.1"),
("Are vaccines free from harmful ingredients?", "3.2"),
("Are serious side effects, such as physical harm, mental illness, autism, deadly diseases not caused by vaccines?", "3.3"),
("Are vaccination schedules safe?", "3.4"),
("Should all people get vaccines, regardless of any medical exemptions or ineligibility?", "3.5"),
("Are vaccines in accordance with regligious or ethical beliefs?", "4.1"),
("Should vaccines be mandatory to people?", "4.2"),
("Should all people get vaccines, regardless of any medical exemptions or ineligibility?", "4.3"),
("Do people trust medical personnel when they say vaccines are good and effective?", "5.1"),
("Are the development and manufaturing of vaccines not due to profit motives of companies?", "5.2"),
("Are the propaganda of vaccines censored by the media?", "5.3"),
("Do people not believe that vaccines are a part of a conspiracy?", "5.4")
]

level1_questions = [
("Can vaccine research be doubted?", "1"),
("Are the benefits of vaccines exaggerated?", "2"),
("Can taking the vaccine cause health risks?", "3"),
("Do vaccines disregard individual rights?", "4"),
("Is the vaccine being promoted by untrustworthy actors?", "5")
]

reversed_level1_questions = [
("Can vaccine research be trusted?", "1"),
("Are the benefits of vaccines legitimate?", "2"),
("Is taking the vaccine free from health risks?", "3"),
("Do vaccines respect individual rights?", "4"),
("Is the vaccine being promoted by trustworthy actors?", "5")
]

#Store the predicted results into a dictionary, paragraph as the key
myths_result = {}
for each_paragraph in paragraphs:
  for each_question in questions:
    predict(each_question, each_paragraph, myths_result)
print(myths_result)

# RUNNING BOOLQ ON VARIOUS SETS

# Move Level2 questions --> Level1 Node
myths_result_parent = {}
for i in myths_result.keys():
  temp = []
  for item in myths_result[i]:
    if str(int(float(item))) not in temp:
      temp.append(str(int(float(item))))
  myths_result_parent[i] = temp
print(myths_result_parent)

# Try Level1 questions
myths_result_level1 = {}
for each_paragraph in paragraphs:
  for each_question in level1_questions:
    predict(each_question, each_paragraph, myths_result_level1)
print(myths_result_level1)

#Store the predicted results into a dictionary, paragraph as the key
myths_reverse = {}
for each_paragraph in paragraphs:
  for each_question in reversed_questions:
    predict(each_question, each_paragraph, myths_reverse)
print(myths_reverse)

# Move Level2 questions --> Level1 Node
myths_reverse_parent = {}
for i in myths_reverse.keys():
  temp = []
  for item in myths_reverse[i]:
    if str(int(float(item))) not in temp:
      temp.append(str(int(float(item))))
  myths_reverse_parent[i] = temp
print(myths_reverse_parent)

# Try Level1 questions
myths_reverse_level1 = {}
for each_paragraph in paragraphs:
  for each_question in reversed_level1_questions:
    predict(each_question, each_paragraph, myths_reverse_level1)
print(myths_reverse_level1)

# ACCURACY
nodes = []
for each in questions:
  nodes.append(each[1])
node_accuracy = []
print(nodes)
nodes = ['1', '2', '3', '4', '5']

def getAccuracy(predicted, actual):
  #Accuracy on each node
  print("Accuracy: ")
  for each_node in nodes:
    TP = 0
    TN = 0
    FN = 0
    FP = 0
    for key in predicted.keys():
      if (each_node in actual[key]):
        if (each_node in predicted[key]):
          TP += 1
      if (each_node in actual[key]):
        if (each_node not in predicted[key]):
          FN += 1
      if (each_node not in actual[key]):
        if (each_node not in predicted[key]):
          TN += 1
      if (each_node not in actual[key]):
        if (each_node in predicted[key]):
          FP += 1
    if (TP == 0 and FN == 0):       #No such label in this dataset
      accuracy = 0
    else:
      accuracy = (TP + TN)/(TP + TN + FN + FP)
    node_accuracy.append(accuracy)
    print(f"Node:{each_node}: {accuracy}")

ac_ar_ap = []
def getAveAcc(l):
  sum_accuracy = 0
  for acc in l:
    sum_accuracy += acc
  average_accuracy = sum_accuracy/len(l)
  ac_ar_ap.append(average_accuracy)
  print(f"Average accuracy over all nodes: {average_accuracy}")

    
getAccuracy(myths_result_parent, myths_standards)
getAveAcc(node_accuracy)
getAccuracy(myths_result_level1, myths_standards)
getAveAcc(node_accuracy)
getAccuracy(myths_reverse_parent, myths_standards)
getAveAcc(node_accuracy)
getAccuracy(myths_reverse_level1, myths_standards)
getAveAcc(node_accuracy)

# RECALL

node_recalls = []
def getNodeRecall(predicted, actual):
  print("Recall for each taxonomy node")
  for each_node in nodes:
    TP = 0
    FN = 0
    for key in predicted.keys():
      if (each_node in predicted[key]) and (each_node in actual[key]):
        TP += 1
      elif (each_node in actual[key]) and (each_node not in predicted[key]):
        FN += 1
    if (TP == 0 and FN == 0):
      recall_each = 0      # No such labels in this data set
    else: 
      recall_each = TP/(TP + FN)
    node_recalls.append(recall_each)
    print(f"Node {each_node}: {recall_each}")

def getAveNodeRecall(l):
  recall_sum = 0
  for each in l:
    recall_sum += each
  average_recall = recall_sum/len(l)
  print(f"Average recall: {average_recall}")
  

getNodeRecall(myths_result_parent, myths_standards)
getAveNodeRecall(node_recalls)
getNodeRecall(myths_result_level1, myths_standards)
getAveNodeRecall(node_recalls)
getNodeRecall(myths_reverse_parent, myths_standards)
getAveNodeRecall(node_recalls)
getNodeRecall(myths_reverse_level1, myths_standards)
getAveNodeRecall(node_recalls)

# PRECISION

node_precisions = []
def getNodePrecision(predicted, actual):

  print("Precision for each taxonomy node")
  for each_node in nodes:
    TP = 0
    FP = 0
    for key in predicted.keys():
      if (each_node in predicted[key]) and (each_node in actual[key]):
        TP += 1
      elif (each_node not in actual[key]) and (each_node in predicted[key]):
        FP += 1
    if (TP == 0 and FP == 0):
      precision_each = 0      # No such labels in this data set
    else: 
      precision_each = TP/(TP + FP)
    node_precisions.append(precision_each)
    print(f"Node {each_node}: {precision_each}")

def averageNodePrecision(l):
  precision_total = 0
  for each in l:
    precision_total += each
  average_precision = precision_total/len(l)
  ac_ar_ap.append(average_precision)
  print(f"Average node precision: {average_precision}")

getNodePrecision(myths_result_parent, myths_standards)
averageNodePrecision(node_precisions)
getNodePrecision(myths_result_level1, myths_standards)
averageNodePrecision(node_precisions)
getNodePrecision(myths_reverse_parent, myths_standards)
averageNodePrecision(node_precisions)
getNodePrecision(myths_reverse_level1, myths_standards)
averageNodePrecision(node_precisions)

#F1 SCORE

average_f1_score = 2*(ac_ar_ap[2] * ac_ar_ap[1])/(ac_ar_ap[2] + ac_ar_ap[1])
print(f"Average f1 score: {average_f1_score}")

