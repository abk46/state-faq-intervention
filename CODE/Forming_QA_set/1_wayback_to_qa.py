import os
import pandas as pd
from bs4 import BeautifulSoup
import pickle

# Taking all of a state's wayback machine data and creating pandas dataframe.
# Requires inspection of state HTML data to determine the right "q_tags", the HTML tags involved in questions from FAQ.
# Repeated questions/answers are not yet handled here.

def state_wayback_to_qa(state, q_tags, a_tags, exceptions, output_file):
    dates = []
    for root, dirs, files in os.walk("./../../ASSOCIATED_FILES/WAYBACK_files/"+state):
        if len(files) > 0:
            dates.append(root+'/'+files[0])
    overall_set = []
    question_no = 0
    for date in sorted(dates):
        q_set = []
        a_set = []
        # Check for any text in q_tags that ends in a ? for questions, then the successive a_tag is the answer.
        # Works on some sets, not on others, since it's relying on tag consistency across a single FAQ / multiple versions over time
        soup = BeautifulSoup(open(date, encoding='utf8'), 'html.parser')
        for node in soup.findAll(q_tags):
            if len(node.getText().strip()) > 0 and node.getText().strip()[len(node.getText().strip())-1] == '?' and not any(exception in node.getText().strip() for exception in exceptions):
                overall_set.append([question_no, date.split("/")[6], node.getText().strip(), node.find_next(a_tags[0]).getText()])
                question_no += 1
    overall_df = pd.DataFrame(overall_set, columns=['QNum','Date','Question','Answer'])
    print(overall_df)
    pickle.dump(overall_df, open(output_file, "wb"))


state_wayback_to_qa('AK', ['h3','h2'], ['p'], ["Can't find what you're looking for?", "Need help with COVID-19 questions or requests?", "Need help?"], './../../ASSOCIATED_FILES/QA_sets/AK.pkl')
state_wayback_to_qa('CA', ['h4'], ['p'], '', './../../ASSOCIATED_FILES/QA_sets/CA.pkl')
state_wayback_to_qa('CO', ['div'], ['p'], '', './../../ASSOCIATED_FILES/QA_sets/CO.pkl')
state_wayback_to_qa('DC', ['h3','p'], ['p'], ['A.'], './../../ASSOCIATED_FILES/QA_sets/DC.pkl')
state_wayback_to_qa('DE', ['a'], ['p'], '', './../../ASSOCIATED_FILES/QA_sets/DE.pkl')
state_wayback_to_qa('HI', ['div'], ['p'], '', './../../ASSOCIATED_FILES/QA_sets/HI.pkl')
state_wayback_to_qa('ID', ['div'], ['p'], '', './../../ASSOCIATED_FILES/QA_sets/ID.pkl')
state_wayback_to_qa('NC', ['a'], ['p'], '', './../../ASSOCIATED_FILES/QA_sets/NC.pkl')
state_wayback_to_qa('NY', ['h5'], ['p'], '', './../../ASSOCIATED_FILES/QA_sets/NY.pkl')
state_wayback_to_qa('OK', ['span'], ['li'], '', './../../ASSOCIATED_FILES/QA_sets/OK.pkl')
state_wayback_to_qa('SC', ['p'], ['p'], '', './../../ASSOCIATED_FILES/QA_sets/SC.pkl')
state_wayback_to_qa('SD', ['h2'], ['p'], '', './../../ASSOCIATED_FILES/QA_sets/SD.pkl')
state_wayback_to_qa('TX', ['h3'], ['p'], '', './../../ASSOCIATED_FILES/QA_sets/TX.pkl')
state_wayback_to_qa('VA', ['div'], ['p'], '', './../../ASSOCIATED_FILES/QA_sets/VA.pkl')
state_wayback_to_qa('VT', ['div'], ['p'], '', './../../ASSOCIATED_FILES/QA_sets/VT.pkl')


