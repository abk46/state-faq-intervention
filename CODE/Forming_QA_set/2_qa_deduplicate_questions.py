import os
import pandas as pd
import pickle
import torch
from sentence_transformers import CrossEncoder
from transformers import AutoTokenizer, AutoModelForSequenceClassification

# De-duplicates questions, original question "number" is its own pandas column, all questions are numbered
# First, for any questions that match lexically, set it to the original pandas question number
# Second, for any questions that are removed and any questions that are added between versions, check if they're duplicates using Quora questions method, then set it to original pandas question number

tokenizer = AutoTokenizer.from_pretrained("navteca/quora-roberta-base")
model = AutoModelForSequenceClassification.from_pretrained("navteca/quora-roberta-base")

def state_qa_deduplicate_questions(input_file, output_file):
    overall_df = pickle.load(open(input_file, "rb"))
    overall_df["Original_QNum"] = overall_df["QNum"]
    for i in range(overall_df.shape[0]):
        overall_df.loc[overall_df['Question'] == overall_df.iloc[i]['Question'], 'Original_QNum'] = overall_df.iloc[i]['Original_QNum']
    questions = []
    # Collect questions by date
    for date in overall_df['Date'].unique().tolist():
        questions.append(overall_df.loc[overall_df['Date'] == date, 'Original_QNum'].to_list())
    # For each pair of successive dates, compare questions
    for i in range(len(overall_df['Date'].unique().tolist())-1):
        old_questions = []
        new_questions = []
        old_questions_QNum = []
        new_questions_QNum = []
        # Create set of questions only in old set, not in new set
        for question in questions[i]:
            if question not in questions[i+1]:
                old_questions.append(overall_df.iloc[question]['Question'])
                old_questions_QNum.append(question)
        # Create set of questions only in new set, not in old set
        for question in questions[i+1]:
            if question not in questions[i]:
                new_questions.append(overall_df.iloc[question]['Question'])
                new_questions_QNum.append(question)
        # Perform Quora-trained RoBERTa on each pair within this set
        if len(old_questions) > 0 and len(new_questions) > 0:
            total = []
            total_QNum = []
            for old in range(len(old_questions)):
                for new in range(len(new_questions)):
                    total.append((old_questions[old], new_questions[new]))
                    total_QNum.append((old_questions_QNum[old], new_questions_QNum[new]))
            inputs = tokenizer(total, padding=True, truncation=True, return_tensors="pt")
            outputs = model(**inputs)
            # Given a 'positive', the questions are duplicates. Set the question number of the 'new' question to whatever the question number of the 'old' question is
            for t in range(len(total)):
                if float(outputs.logits[t]) >= 0.5:
                    overall_df.loc[(overall_df['Date'] == overall_df['Date'].unique().tolist()[i+1]) & (overall_df['Original_QNum'] == total_QNum[t][1]), 'Original_QNum'] == total_QNum[t][0]
    pickle.dump(overall_df, open(output_file, "wb"))

state_qa_deduplicate_questions('./../../ASSOCIATED_FILES/QA_sets/AK.pkl', './../../ASSOCIATED_FILES/QA_sets/AK_dedupQs.pkl')
state_qa_deduplicate_questions('./../../ASSOCIATED_FILES/QA_sets/CA.pkl', './../../ASSOCIATED_FILES/QA_sets/CA_dedupQs.pkl')
state_qa_deduplicate_questions('./../../ASSOCIATED_FILES/QA_sets/CO.pkl', './../../ASSOCIATED_FILES/QA_sets/CO_dedupQs.pkl')
state_qa_deduplicate_questions('./../../ASSOCIATED_FILES/QA_sets/DC.pkl', './../../ASSOCIATED_FILES/QA_sets/DC_dedupQs.pkl')
state_qa_deduplicate_questions('./../../ASSOCIATED_FILES/QA_sets/DE.pkl', './../../ASSOCIATED_FILES/QA_sets/DE_dedupQs.pkl')
state_qa_deduplicate_questions('./../../ASSOCIATED_FILES/QA_sets/HI.pkl', './../../ASSOCIATED_FILES/QA_sets/HI_dedupQs.pkl')
state_qa_deduplicate_questions('./../../ASSOCIATED_FILES/QA_sets/ID.pkl', './../../ASSOCIATED_FILES/QA_sets/ID_dedupQs.pkl')
state_qa_deduplicate_questions('./../../ASSOCIATED_FILES/QA_sets/NC.pkl', './../../ASSOCIATED_FILES/QA_sets/NC_dedupQs.pkl')
state_qa_deduplicate_questions('./../../ASSOCIATED_FILES/QA_sets/NY.pkl', './../../ASSOCIATED_FILES/QA_sets/NY_dedupQs.pkl')
state_qa_deduplicate_questions('./../../ASSOCIATED_FILES/QA_sets/OK.pkl', './../../ASSOCIATED_FILES/QA_sets/OK_dedupQs.pkl')
state_qa_deduplicate_questions('./../../ASSOCIATED_FILES/QA_sets/SC.pkl', './../../ASSOCIATED_FILES/QA_sets/SC_dedupQs.pkl')
state_qa_deduplicate_questions('./../../ASSOCIATED_FILES/QA_sets/SD.pkl', './../../ASSOCIATED_FILES/QA_sets/SD_dedupQs.pkl')
state_qa_deduplicate_questions('./../../ASSOCIATED_FILES/QA_sets/TX.pkl', './../../ASSOCIATED_FILES/QA_sets/TX_dedupQs.pkl')
state_qa_deduplicate_questions('./../../ASSOCIATED_FILES/QA_sets/VA.pkl', './../../ASSOCIATED_FILES/QA_sets/VA_dedupQs.pkl')
state_qa_deduplicate_questions('./../../ASSOCIATED_FILES/QA_sets/VT.pkl', './../../ASSOCIATED_FILES/QA_sets/VT_dedupQs.pkl')
