#!/bin/bash

# Waybackpack is a command line tool to download Wayback Machine versions. Here, we limit it to all data up to Jan 31, 2022. Also, we limit to one archive per day for each state.

waybackpack https://dhss.alaska.gov/dph/Epi/id/Pages/COVID-19/VaccineInfo.aspx --follow-redirects -d ./../../ASSOCIATED_FILES/WAYBACK_files/AK --to-date 20220131 --uniques-only --collapse=timestamp:8
waybackpack https://www.cdph.ca.gov/Programs/CID/DCDC/Pages/COVID-19/ThirdVaccineDoseQandA.aspx --follow-redirects -d ./../../ASSOCIATED_FILES/WAYBACK_files/CA --to-date 20220131 --uniques-only --collapse=timestamp:8
waybackpack https://covid19.colorado.gov/vaccine-FAQ --follow-redirects -d ./../../ASSOCIATED_FILES/WAYBACK_files/CO --to-date 20220131 --uniques-only --collapse=timestamp:8
waybackpack https://coronavirus.dc.gov/vaccine-information --follow-redirects -d ./../../ASSOCIATED_FILES/WAYBACK_files/DC --to-date 20220131 --uniques-only --collapse=timestamp:8
waybackpack https://coronavirus.delaware.gov/frequently-asked-questions/ --follow-redirects -d ./../../ASSOCIATED_FILES/WAYBACK_files/DE --to-date 20220131 --uniques-only --collapse=timestamp:8
waybackpack https://hawaiicovid19.com/vaccine-faqs/ --follow-redirects -d ./../../ASSOCIATED_FILES/WAYBACK_files/HI --to-date 20220131 --uniques-only --collapse=timestamp:8
waybackpack https://healthandwelfare.idaho.gov/idaho-covid-19-vaccination-information/covid-19-vaccine-faq --follow-redirects -d ./../../ASSOCIATED_FILES/WAYBACK_files/ID --to-date 20220131 --uniques-only --collapse=timestamp:8
waybackpack https://covid19.ncdhhs.gov/vaccines/frequently-asked-questions-about-covid-19-vaccinations --follow-redirects -d ./../../ASSOCIATED_FILES/WAYBACK_files/NC --to-date 20220131 --uniques-only --collapse=timestamp:8
waybackpack https://covid19vaccine.health.ny.gov/frequently-asked-questions-0 --follow-redirects -d ./../../ASSOCIATED_FILES/WAYBACK_files/NY --to-date 20220131 --uniques-only --collapse=timestamp:8
waybackpack https://oklahoma.gov/covid19/vaccine-information/vaccine-faqs.html --follow-redirects -d ./../../ASSOCIATED_FILES/WAYBACK_files/OK --to-date 20220131 --uniques-only --collapse=timestamp:8
waybackpack https://scdhec.gov/covid19/covid-19-vaccine/covid-19-vaccine-faqs --follow-redirects -d ./../../ASSOCIATED_FILES/WAYBACK_files/SC --to-date 20220131 --uniques-only --collapse=timestamp:8
waybackpack https://doh.sd.gov/COVID/Vaccine/faqs.aspx --follow-redirects -d ./../../ASSOCIATED_FILES/WAYBACK_files/SD --to-date 20220131 --uniques-only --collapse=timestamp:8
waybackpack https://www.dshs.state.tx.us/coronavirus/immunize/vaccine-faqs.aspx --follow-redirects -d ./../../ASSOCIATED_FILES/WAYBACK_files/TX --to-date 20220131 --uniques-only --collapse=timestamp:8
waybackpack https://www.vdh.virginia.gov/covid-19-faq/vaccination/ --follow-redirects -d ./../../ASSOCIATED_FILES/WAYBACK_files/VA --to-date 20220131 --uniques-only --collapse=timestamp:8
waybackpack https://www.healthvermont.gov/covid-19/faqs --follow-redirects -d ./../../ASSOCIATED_FILES/WAYBACK_files/VT --to-date 20220131 --uniques-only --collapse=timestamp:8








